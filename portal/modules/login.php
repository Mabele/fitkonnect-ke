<?php
$configs = parse_ini_file(WPATH . "core/configs.ini");

require_once WPATH . "modules/classes/Users_Management.php";
$users_management = new Users_Management();

$_SESSION['web_url'] = $configs["web_url"];
$_SESSION['admin_url'] = $configs["admin_url"];
$_SESSION['till_number'] = $configs["till_number"];
$_SESSION['application_email'] = $configs["application_email"];
$_SESSION['application_phone'] = $configs["application_phone"];
$_SESSION['mail_host'] = $configs["mail_host"];
$_SESSION['SMTPAuth'] = $configs["SMTPAuth"];
$_SESSION['MUsername'] = $configs["MUsername"];
$_SESSION['MPassword'] = $configs["MPassword"];
$_SESSION['SMTPSecure'] = $configs["SMTPSecure"];
$_SESSION['Port'] = $configs["Port"];
$_SESSION['MUsernameFrom'] = $configs["MUsernameFrom"];
$_SESSION['Feedback'] = $configs["Feedback"];
$_SESSION['Null_Feedback'] = $configs["Null_Feedback"];
$_SESSION['sms_username'] = $configs["sms_username"];
$_SESSION['sms_username'] = $configs["sms_username"];

if (!empty($_POST)) {
    $success = $users_management->execute();
    if (is_bool($success) && $success == true) {
        if ($_SESSION['logged_in_user_details']['password_new'] == 'YES') {
            App::redirectTo("?update_password");
        }
        if ($_SESSION['logged_in_user_details']['status'] == 1100) {
            $_SESSION['account_blocked'] = true;
        } else if ($_SESSION['logged_in_user_type_details']['name'] == "TRAINEE" OR $_SESSION['logged_in_user_type_details']['name'] == "TRAINER") { 
            App::redirectTo($_SESSION['web_url']);
        } else {
            App::redirectTo("?dashboard");
        } 
    }
}

if (isset($_SESSION['update_pass_forgot']) AND ( $_SESSION['update_pass_forgot'] == true)) {
    ?>
    <strong>Password successfully updated. Please check your email for the updated login credentials.</strong>
    <?php
    unset($_SESSION['update_pass_forgot']);
}

if (isset($_SESSION['login_error'])) {
    ?>
    <strong>Login Error : Wrong username/password combination</strong>
    <?php
    unset($_SESSION['login_error']);
}
if (isset($_SESSION['account_blocked'])) {
    ?>
    <strong>Login Error : Your Account is deactivated please contact <a href="mailto:hello@fitnessiq.com">hello@fitnessiq.com</a></strong>
    <?php
    unset($_SESSION['account_blocked']);
}
?> 

<div class="login-container">

    <div class="login-box animated fadeInDown">
        <div class="login-logo"></div>
        <div class="login-body">
            <div class="login-title"><strong>Welcome</strong>, Please login</div>
            <form class="form-horizontal" method="POST">
                <input type="hidden" name="action" value="secure_login"/>
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" name="username" class="form-control" placeholder="Username"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="password" name="password" class="form-control" placeholder="Password"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <a href="?forgot_password" class="btn btn-link btn-block">Forgot your password?</a>
                    </div>
                    <div class="col-md-6">
                        <!--<input type="submit" value="Sign In" />-->
                        <button type="submit" class="btn btn-info btn-block">Sign In</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="login-footer">
            <div class="pull-left">
                &copy; <?php echo date("Y"); ?> fitnessIQ | Coaches Portal
            </div>
            <div class="pull-right">
                <a href="#">About</a> |
                <a href="#">Privacy</a> |
                <a href="#">Contact Us</a>
            </div>
        </div>
    </div>

</div>