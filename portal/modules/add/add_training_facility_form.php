<?php
if (!App::isLoggedIn()) App::redirectTo("?");
require_once WPATH . "modules/classes/Training_Facilities.php";
require_once WPATH . "modules/classes/Trainees.php";
require_once WPATH . "modules/classes/Users_Management.php";
$users_management = new Users_Management();
$trainees = new Trainees();
$training_facilities = new Training_Facilities();

if (!empty($_POST)) {
    $_SESSION['name'] = $_POST['name'];
    $_SESSION['description'] = $_POST['description'];
    $_SESSION['coordinates'] = $_POST['coordinates'];
    $_SESSION['ownership_type'] = $_POST['ownership_type'];
    $_SESSION['services_offered'] = $_POST['services_offered'];
    $_SESSION['ref_type'] = $users_management->getUserRefTypeId($_POST['user_type']);
    App::redirectTo("?add_contact&ref_type=" . $_SESSION['ref_type']);
}
?>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="POST">
            <input type="hidden" name="user_type" value="TRAINING FACILITY">
            <input type="hidden" name="action" value="add_training_facility"/>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Add </strong>Training Facility Registration Form</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <p>Training facility registration.</p>
                </div>
                <div class="panel-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Name</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-building"></span></span>
                                    <input type="text" name="name" class="form-control"/>
                                </div>                                            
                                <span class="help-block">Name assigned to the training facility.</span>
                            </div>
                        </div>
                        <div class="form-group">                                        
                            <label class="col-md-3 control-label">Coordinates</label>
                            <div class="col-md-9 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-location-arrow"></span></span>
                                    <input type="text" name="coordinates" class="form-control"/>
                                </div>            
                                <span class="help-block">Coordinates describing the exact physical location of the training facility.</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Description</label>
                            <div class="col-md-9 col-xs-12">                                            
                                <textarea class="form-control" name="description" rows="5"></textarea>
                                <span class="help-block">A brief description of the training facility</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                            <label class="col-md-3 control-label">Ownership Type</label>
                            <div class="col-md-9 col-xs-12">                                                                                            
                                <select  name="ownership_type"class="form-control">
                                    <option value="PUBLIC">PUBLIC</option>
                                    <option value="PRIVATE">PRIVATE</option>
                                </select>
                                <span class="help-block">List of Ownership Types(Select)</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Services Offered</label>
                            <div class="col-md-9 col-xs-12">                                                                                            
                                <select  name="services_offered" class="form-control">
                                    <?php echo $training_facilities->getTrainingFacilityServices(); ?>
                                </select>
                                <span class="help-block">Nature of services offered at the training facility (SWIMMING/GYM/BOTH)(Select)</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <!--<button class="btn btn-default">Clear Form</button>-->                                    
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>              