<?php
if (!App::isLoggedIn()) App::redirectTo("?");
require_once WPATH . "modules/classes/Staff.php";
require_once WPATH . "modules/classes/Users_Management.php";
$users_management = new Users_Management();
$staff = new Staff();

if (!empty($_POST)) {    
    $_SESSION['pf_number'] = $_POST['pf_number'];
    $_SESSION['firstname'] = $_POST['firstname'];
    $_SESSION['middlename'] = $_POST['middlename'];
    $_SESSION['lastname'] = $_POST['lastname'];
    $_SESSION['gender'] = $_POST['gender'];
    $_SESSION['idnumber'] = $_POST['idnumber'];    
    $_SESSION['position'] = $_POST['position'];
    $_SESSION['ref_type'] = $users_management->getUserRefTypeId($_POST['user_type']);
    
    App::redirectTo("?add_contact&ref_type=" . $_SESSION['ref_type']);
    
}
?>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="POST">
            <input type="hidden" name="user_type" value="STAFF">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Staff Registration Form</strong></h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">         
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">PF Number</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="pf_number" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Staff's PF number.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">First Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="firstname" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">First name of staff.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Middle Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="middlename" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Middle name of staff.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Last Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="lastname" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Last name of staff.</span>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">                                        
                                <label class="col-md-3 control-label">Gender</label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-bolt"></span></span>
                                        <select  name="gender"class="form-control">
                                            <?php include 'snippets/gender.php'; ?>
                                        </select>
                                    </div>            
                                    <span class="help-block">Gender, please select.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">ID Number</label>
                                <div class="col-md-9 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-copy"></span></span>
                                        <input type="text" name="idnumber" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Registered ID number.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Position</label>
                                <div class="col-md-9 col-xs-12">   
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cube"></span></span>
                                        <select  name="position"class="form-control">
                                            <?php echo $staff->getStaffPositions(); ?>
                                        </select>
                                    </div>
                                    <span class="help-block">List of potential Positions</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <!--<button class="btn btn-default">Clear Form</button>-->                                    
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form>

    </div>
</div>              