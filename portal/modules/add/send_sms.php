<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/Transactions.php";
$transactions = new Transactions();
$users_management = new Users_Management();

//$transactions->sendShortCodeSMS('254710534013', 'testsms');

if (!empty($_POST)) {
    $success = $transactions->execute();
//    if (is_bool($success) && $success == true) {
//        $_SESSION['add_success'] = true;
//        $_SESSION['feedback_message'] = "<strong>Successful:</strong> The SMS(s) has/have been sent successfully.";
//        App::redirectTo("?view_outgoing_sms");
//    } else {
//        $_SESSION['add_fail'] = true;
//        $_SESSION['feedback_message'] = "<strong>Error!</strong> There was an error sending the SMS(s). Please try again.";
//        App::redirectTo("?send_sms");
//    }
}
?>
<script>
    function fun_showtextbox()
    {
        var select_status = $('#recipient_group').val();
        /* if select individual from select box then show my text box */
        if (select_status == 'INDIVIDUAL')
        {
            $('#recipient').show();// By using this id you can show your content 
        } else
        {
            $('#recipient').hide();
        }// otherwise hide 
    }

</script>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="POST">
            <input type="hidden" name="action" value="send_sms"/>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Send Message </strong></h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body"> 
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Recipients</label>
                        <div class="col-md-6 col-xs-12">                
                            <select name="recipient_group" id="recipient_group" class="form-control" onchange="fun_showtextbox();">
                                <?php echo $users_management->getContactGroups(); ?>
                            </select>
                            <span class="help-block">List of Probable Recipients(Select)</span>
                        </div>
                    </div>
                    <div style="display: none;" id="recipient" class="form-group">
                        <label class="col-md-3 control-label">Recipient Contacts</label>
                        <div class="col-md-9">                                            
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-book"></span></span>
                                <input type="text" name="recipient_contacts" class="form-control" placeholder="+254721XXXXXX,+254733XXXXXX,+254710XXXXXX"/>
                            </div>                                            
                            <span class="help-block">Contacts of individual recipients separated by commas (,).</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Message</label>
                        <div class="col-md-9 col-xs-12">                                            
                            <textarea class="form-control" name="message" rows="5"></textarea>
                            <span class="help-block">The message content</span>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <!--<button class="btn btn-default">Clear Form</button>-->                                    
                    <button class="btn btn-primary pull-right">Send Message</button>
                </div>
            </div>
        </form>
    </div>
</div>                    