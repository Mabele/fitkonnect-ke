<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/Trainers.php";
require_once WPATH . "modules/classes/Trainees.php";
require_once WPATH . "modules/classes/System_Administration.php";
require_once WPATH . "modules/classes/Staff.php";
$staff = new Staff();
$system_administration = new System_Administration();
$users_management = new Users_Management();
$trainers = new Trainers();
$trainees = new Trainees();
$ref_type = $_GET['ref_type'];  //If STAFF, TRAINER etc

if (!empty($_POST)) {
    $_SESSION['phone_number1'] = $_POST['phone_number1'];
    $_SESSION['phone_number2'] = $_POST['phone_number2'];
    $_SESSION['email'] = $_POST['email'];
    $_SESSION['postal_number'] = $_POST['postal_number'];
    $_SESSION['postal_code'] = $_POST['postal_code'];
    $_SESSION['postal_town'] = $_POST['postal_town'];
    $_SESSION['county'] = $_POST['county'];
    $_SESSION['ward'] = $_POST['ward'];
    $_SESSION['website'] = $_POST['website'];

    $filename = md5($trainees->randomString(10) . time());
    $prof_picture_name = $_FILES['prof_picture']['name'];
    $tmp_name = $_FILES['prof_picture']['tmp_name'];
    $extension = substr($prof_picture_name, strpos($prof_picture_name, '.') + 1);
    $prof_picture = strtoupper($filename . '.' . $extension);
    $_SESSION['prof_picture_filename'] = $prof_picture;

    if ($ref_type == $users_management->getUserRefTypeId("STAFF")) {
        $location = 'img/staff/';
    } else if ($ref_type == $users_management->getUserRefTypeId("TRAINER")) {
        $location = 'img/trainers/profile_pictures/';
    } else if ($ref_type == $users_management->getUserRefTypeId("TRAINEE")) {
        $location = 'img/trainees/';
    } else if ($ref_type == $users_management->getUserRefTypeId("TRAINING FACILITY")) {
        $location = 'img/training_facilities/';
    }

    if (move_uploaded_file($tmp_name, $location . $prof_picture)) {
        if ($ref_type == $users_management->getUserRefTypeId("STAFF")) {
            $success = $staff->execute();
            if (is_bool($success) && $success == true) {
                $_SESSION['add_success'] = true;
                $_SESSION['feedback_message'] = "<strong>Successful:</strong> The staff record has been created successfully.";
                App::redirectTo("?view_staff");
            } else {
                $_SESSION['add_fail'] = true;
                $_SESSION['feedback_message'] = "<strong>Error!</strong> There was an error creating the staff record. Please try again.";
                App::redirectTo("?add_staff");
            }
        } else if ($ref_type == $users_management->getUserRefTypeId("TRAINER")) {
            $success = $trainers->execute();
            if (is_bool($success) && $success == true) {
                $_SESSION['add_success'] = true;
                $_SESSION['feedback_message'] = "<strong>Successful:</strong> The trainer record has been created successfully.";
                App::redirectTo("?view_trainers");
            } else {
                $_SESSION['add_fail'] = true;
                $_SESSION['feedback_message'] = "<strong>Error!</strong> There was an error creating the trainer record. Please try again.";
                App::redirectTo("?add_trainer");
            }
        } else if ($ref_type == $users_management->getUserRefTypeId("TRAINEE")) {
            $success = $trainees->execute();
            if (is_bool($success) && $success == true) {
                $_SESSION['add_success'] = true;
                $_SESSION['feedback_message'] = "<strong>Successful:</strong> The trainee record has been created successfully.";
                App::redirectTo("?view_trainers");
            } else {
                $_SESSION['add_fail'] = true;
                $_SESSION['feedback_message'] = "<strong>Error!</strong> There was an error creating the trainee record. Please try again.";
                App::redirectTo("?add_trainer");
            }
        } else if ($ref_type == $users_management->getUserRefTypeId("TRAINING FACILITY")) {
            App::redirectTo("?add_training_facility_administrator");
        }
    } else {
        $_SESSION['create_error'] = "Error uploading photo. Please try again.";
    }
}
?>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="action" value="add_contact"/>
            <?php if ($ref_type == $users_management->getUserRefTypeId("STAFF")) { ?>
                <input type="hidden" name="action" value="add_staff"/>
            <?php } else if ($ref_type == $users_management->getUserRefTypeId("TRAINER")) { ?>
                <input type="hidden" name="action" value="add_trainer"/>
            <?php } else if ($ref_type == $users_management->getUserRefTypeId("TRAINEE")) { ?>
                <input type="hidden" name="action" value="add_trainee"/>
<?php } ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Add </strong>Contacts Registration Form</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <p>Contacts setup registration.</p>
                </div>
                <div class="panel-body">         
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Profile Picture</label>
                                <div class="col-md-9">                                                                                                                                        
                                    <input type="file" class="fileinput btn-primary" name="prof_picture" id="profile_picture" title="Select an image"/>
                                    <span class="help-block">Upload Institution's/User's Photo.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Website</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-globe"></span></span>
                                        <input type="text" name="website" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Institution's/User's Website.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Phone Number</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                        <input type="tel" name="phone_number1" class="form-control" required=""/>
                                    </div>                                            
                                    <span class="help-block">Institution's/User's Phone number.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Alternative Phone Number</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                        <input type="tel" name="phone_number2" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Institution's/User's Alternative Phone number.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Email</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
                                        <input type="email" name="email" class="form-control" required=""/>
                                    </div>                                            
                                    <span class="help-block">Institution's/User's email.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Postal Number</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
                                        <input type="text" name="postal_number" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Institution's/User's Postal Number.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Postal Code</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-code"></span></span>
                                        <input type="text" name="postal_code" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Institution's/User's Postal Code.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Postal Town</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-building"></span></span>
                                        <input type="text" name="postal_town" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Institution's/User's Postal Town.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Physical Address - County</label>
                                <div class="col-md-9 col-xs-12">   
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-magnet"></span></span>
                                        <select  name="county" class="form-control">
<?php echo $system_administration->getCounties(); ?>
                                        </select>
                                    </div>
                                    <span class="help-block">Institution's/User's County of residence.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Physical Address - Ward</label>
                                <div class="col-md-9 col-xs-12">   
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-magnet"></span></span>
                                        <select  name="ward" class="form-control">
<?php echo $system_administration->getWards(); ?>
                                        </select>
                                    </div>
                                    <span class="help-block">Institution's/User's Ward of residence.</span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <!--<button class="btn btn-default">Clear Form</button>-->                                    
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form>

    </div>
</div>              