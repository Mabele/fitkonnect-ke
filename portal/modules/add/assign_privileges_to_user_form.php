<?php
if (!App::isLoggedIn()) App::redirectTo("?");
require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/System_Administration.php";
$system_administration = new System_Administration();
$users_management = new Users_Management();
$code = 1; //$_SESSION['user_ref'];
$details = $users_management->fetchUserDetails($code);

if (!empty($_POST)) {
    $success = $system_administration->execute();
    if (is_bool($success) && $success == true) {
        $_SESSION['add_success'] = true;
        $_SESSION['feedback_message'] = "<strong>Successful:</strong> The user privileges have been updated successfully.";
        App::redirectTo("?view_role_privileges");
    } else {
        $_SESSION['add_fail'] = true;
        $_SESSION['feedback_message'] = "<strong>Error!</strong> There was an error assigning the privilege to the user. Please try again.";
        App::redirectTo("?assign_privileges_to_user");
    }
}
?>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="POST">
            <input type="hidden" name="action" value="assign_privilege_to_user"/>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Add </strong>User Privileges Registration Form</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <p>Reference ID/Name of the user in question.</p>
                </div>
                <div class="panel-body">       
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">User Name</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                <input type="text" name="user_ref" class="form-control" placeholder="User Name" value="<?php echo $details['id']; ?>" readonly="yes"/>
                            </div>                                            
                            <span class="help-block">Reference ID/Name of the user in question.</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Privilege</label>
                        <div class="col-md-6 col-xs-12">                                                                                            
                            <select  name="privilege"class="form-control">
                                <?php echo $system_administration->getSystemPrivileges(); ?>
                            </select>
                            <span class="help-block">List of Privileges(Select)</span>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <!--<button class="btn btn-default">Clear Form</button>-->                                    
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>                    