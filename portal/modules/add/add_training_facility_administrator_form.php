<?php
if (!App::isLoggedIn()) App::redirectTo("?");
require_once WPATH . "modules/classes/Training_Facilities.php";
$training_facilities = new Training_Facilities();

if (!empty($_POST)) {

    $_SESSION['admin_firstname'] = $_POST['firstname'];
    $_SESSION['admin_lastname'] = $_POST['lastname'];
    $_SESSION['admin_phone_number'] = $_POST['phone_number'];
    $_SESSION['admin_email'] = $_POST['email'];
    $_SESSION['admin_idnumber'] = $_POST['idnumber'];

    $success = $training_facilities->execute();
    if (is_bool($success) && $success == true) {
        $_SESSION['add_success'] = true;
        $_SESSION['feedback_message'] = "<strong>Successful:</strong> The training facility record has been created successfully.";
        App::redirectTo("?view_training_facilities");
    } else {
        $_SESSION['add_fail'] = true;
        $_SESSION['feedback_message'] = "<strong>Error!</strong> There was an error creating the training facility record. Please try again.";
        App::redirectTo("?add_training_facility");
    }
}
?>

<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="POST">
            <input type="hidden" name="action" value="add_training_facility"/>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Add </strong>Training Facility Administrator Registration Form</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <p>Training facility administrator registration.</p>
                </div>
                <div class="panel-body">         
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">First Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="firstname" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">The administrator’s first name.</span>
                                </div>
                            </div>
                            <div class="form-group">                                        
                                <label class="col-md-3 control-label">Last Name</label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="lastname" class="form-control"/>
                                    </div>            
                                    <span class="help-block">The administrator’s last name.</span>
                                </div>
                            </div>
                            <div class="form-group">                                        
                                <label class="col-md-3 control-label">Phone Number</label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                        <input type="tel" name="phone_number" class="form-control"/>
                                    </div>            
                                    <span class="help-block">The administrator’s telephone number.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">                                        
                                <label class="col-md-3 control-label">Email</label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-mail-reply"></span></span>
                                        <input type="email" name="email" class="form-control"/>
                                    </div>            
                                    <span class="help-block">The administrator’s email address.</span>
                                </div>
                            </div>
                            <div class="form-group">                                        
                                <label class="col-md-3 control-label">ID Number</label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-navicon"></span></span>
                                        <input type="idnumber" name="idnumber" class="form-control"/>
                                    </div>            
                                    <span class="help-block">The administrator’s national ID / Passport number.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <!--<button class="btn btn-default">Clear Form</button>-->                                    
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form>

    </div>
</div>              