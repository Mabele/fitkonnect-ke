
<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Lessons.php";
require_once WPATH . "modules/classes/Training_Facilities.php";
$training_facilities = new Training_Facilities();
$lessons = new Lessons();

if (isset($_SERVER['HTTP_REFERER'])) {
    $previous_url = $_SERVER['HTTP_REFERER'];
}
if (!empty($_POST) AND $_POST['action'] == "add_to_lessons_cart") {
    $productByCode = $lessons->fetchLessonDetails($_POST["code"]);
    $itemArray = array($productByCode["id"] => array('id' => $productByCode["id"], 'name' => $productByCode["name"], 'price' => $productByCode["price"], 'number_of_classes' => $_POST["number_of_classes"]));

    if (!empty($_SESSION["cart_item"])) {
        if (in_array($productByCode["id"], array_keys($_SESSION["cart_item"]))) {
            foreach ($_SESSION["cart_item"] as $k => $v) {
                if ($productByCode["id"] == $v['id']) {
                    if (empty($_SESSION["cart_item"][$k]["number_of_classes"])) {
                        $_SESSION["cart_item"][$k]["number_of_classes"] = 0;
                    }
                    $_SESSION["cart_item"][$k]["number_of_classes"] += $_POST["number_of_classes"];
                }
            }
        } else {
            $_SESSION["cart_item"] = array_merge($_SESSION["cart_item"], $itemArray);
        }
    } else {
        $_SESSION["cart_item"] = $itemArray;
    }
    App::redirectTo("{$previous_url}");
}

$lessons_data[] = $lessons->getAllLessons();
if (isset($_SESSION['no_records']) AND $_SESSION['no_records'] == true) {
    ?>
    <div style="text-align:left"><strong>No lesson found....</strong></div>
    <?php
    unset($_SESSION['no_records']);
} else if (isset($_SESSION['yes_records']) AND $_SESSION['yes_records'] == true) {
    ?>

    <div class="row">
        <?php
        foreach ($lessons_data as $key => $value) {
            $inner_array[$key] = json_decode($value, true); // this will give key val pair array
            foreach ((array) $inner_array[$key] as $key2 => $value2) {
                $lesson_category_details = $lessons->fetchLessonCategoryDetails($value2['category']);
                $training_facility_details = $training_facilities->fetchTrainingFacilityDetails($value2['training_facility']);
                ?>
                <div class="col-md-3">
                    <!-- START WIDGET SLIDER -->
                    <div class="widget widget-default widget-carousel">
                        <div class="owl-carousel" id="owl-example">
                            <div>                                    
                                <div class="widget-title"><?php echo 'CATEGORY: ' . $lesson_category_details['name']; ?></div>                                                                        
                                <div class="widget-title"><?php echo 'NAME: ' . $value2['name']; ?></div>
                                <div class="widget-title"><?php echo 'PRICE: ' . $value2['price']; ?></div>
                                <div class="widget-subtitle">
                                    <form role="form" method="post">
                                        <input type="hidden" name="action" value="add_to_lessons_cart"/>
                                        <input type="hidden" name="code" value="<?php echo $value2['id']; ?>"/>
                                        <input type="hidden" name="number_of_classes" value="1"/>
                                        <button type="submit">Book Lesson</button>
                                    </form>
                                </div>
                            </div>
                            <div>                                    
                                <div class="widget-subtitle"><?php echo 'FACILITY: ' . $training_facility_details['name']; ?></div>
                                <div class="widget-subtitle"><?php echo 'START TIME: ' . $value2['start_time']; ?></div>
                                <div class="widget-subtitle"><?php echo 'END TIME: ' . $value2['end_time']; ?></div>
                                <div class="widget-subtitle">
                                    <form role="form" method="post">
                                        <input type="hidden" name="action" value="add_to_lessons_cart"/>
                                        <input type="hidden" name="code" value="<?php echo $value2['id']; ?>"/>
                                        <input type="hidden" name="number_of_classes" value="1"/>
                                        <button type="submit">Book Lesson</button>
                                    </form>
                                </div>
                            </div>
                            <div>                                    
                                <div class="widget-title"><?php echo 'CAPACITY: ' . $value2['capacity']; ?></div>
                                <div class="widget-title"><?php echo 'CURRENT POPULATION: ' . $value2['current_population']; ?></div>
                                <div class="widget-subtitle">
                                    <form role="form" method="post">
                                        <input type="hidden" name="action" value="add_to_lessons_cart"/>
                                        <input type="hidden" name="code" value="<?php echo $value2['id']; ?>"/>
                                        <input type="hidden" name="number_of_classes" value="1"/>
                                        <button type="submit">Book Lesson</button>
                                    </form>
                                </div>
                            </div>
                        </div>                            
                        <div class="widget-controls">                                
                            <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                        </div>                             
                    </div>         
                    <!-- END WIDGET SLIDER -->
                </div>
                <?php
            }
        }
        ?>
    </div>
    <?php
    unset($_SESSION['yes_records']);
}
?>
