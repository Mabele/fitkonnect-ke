<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Trainees.php";
require_once WPATH . "modules/classes/Trainers.php";
require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/Training_Facilities.php";
$training_facilities = new Training_Facilities();
$users_management = new Users_Management();
$trainers = new Trainers();
$trainees = new Trainees();

if (!empty($_POST)) {
    $_SESSION['firstname'] = $_POST['firstname'];
    $_SESSION['lastname'] = $_POST['lastname'];
    $_SESSION['gender'] = $_POST['gender'];
    $_SESSION['idnumber'] = $_POST['idnumber'];
    $_SESSION['birth_date'] = $_POST['birth_date'];
    $_SESSION['fk_profile'] = $_POST['fk_profile'];
    $_SESSION['training_level'] = $_POST['training_level'];
    $_SESSION['guarantor1'] = $_POST['guarantor1'];
    $_SESSION['guarantor2'] = $_POST['guarantor2'];
    $_SESSION['facility1'] = $_POST['facility1'];
    $_SESSION['facility2'] = $_POST['facility2'];
    $_SESSION['ref_type'] = $users_management->getUserRefTypeId($_POST['user_type']);

    $filename1 = md5('' . $trainees->randomString(10) . time());
    $curriculum_vitae_name = $_FILES['curriculum_vitae']['name'];
    $tmp_name_curriculum_vitae = $_FILES['curriculum_vitae']['tmp_name'];
    $extension_curriculum_vitae = substr($curriculum_vitae_name, strpos($curriculum_vitae_name, '.') + 1);
    $curriculum_vitae = strtoupper($filename1 . '.' . $extension_curriculum_vitae);
    $_SESSION['curriculum_vitae_filename'] = $curriculum_vitae;
    $location1 = 'img/trainers/CVs/';

    $filename2 = md5('' . $trainees->randomString(10) . time());
    $certificate_name = $_FILES['certificate']['name'];
    $tmp_name_certificate = $_FILES['certificate']['tmp_name'];
    $extension_certificate = substr($certificate_name, strpos($certificate_name, '.') + 1);
    $certificate = strtoupper($filename2 . '.' . $extension_certificate);
    $_SESSION['certificate_filename'] = $certificate;
    $location2 = 'img/trainers/certificates/';

    if (move_uploaded_file($tmp_name_curriculum_vitae, $location1 . $curriculum_vitae) AND move_uploaded_file($tmp_name_certificate, $location2 . $certificate)) {
        App::redirectTo("?add_contact&ref_type=" . $_SESSION['ref_type']);
    } else {
        $_SESSION['create_error'] = "Error uploading attachments. Kindly create account holder again.";
    }
    App::redirectTo("?add_contact&ref_type=" . $_SESSION['ref_type']);
}
?>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="user_type" value="TRAINER">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Add </strong>Trainer Registration Form</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <p>Trainer registration.</p>
                </div>
                <div class="panel-body">         
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">First Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="firstname" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">First name of trainer.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Last Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="lastname" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Last name of trainer.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Trainer Profile</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <textarea rows="4" name="fk_profile" class="form-control">
                                        A brief profileof the trainer.
                                        </textarea>
                                    </div>                                            
                                    <span class="help-block">A brief profile of the trainer.</span>
                                </div>
                            </div>
                            <div class="form-group">                                        
                                <label class="col-md-3 control-label">Gender</label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-bolt"></span></span>
                                        <select  name="gender"class="form-control">
                                            <?php include 'snippets/gender.php'; ?>
                                        </select>
                                    </div>            
                                    <span class="help-block">Gender, please select.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">ID Number</label>
                                <div class="col-md-9 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-copy"></span></span>
                                        <input type="text" name="idnumber" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Registered ID number.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Birth Date</label>
                                <div class="col-md-9 col-xs-12">   
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        <input type="text" name="birth_date" class="form-control datepicker" value="2018-02-28"> 
                                    </div>
                                    <span class="help-block">Select Date</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Training Level</label>
                                <div class="col-md-9 col-xs-12">                                                                                            
                                    <select name="training_level" class="form-control">
                                        <option value="CERTIFICATE">CERTIFICATE</option>
                                        <option value="DIPLOMA">DIPLOMA</option>
                                        <option value="GRADUATE">GRADUATE</option>
                                        <option value="POST-GRADUATE">POST-GRADUATE</option>
                                    </select>
                                    <span class="help-block">The rating of service delivery accorded to the training facility(Select)</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Attach Certificate</label>
                                <div class="col-md-9">                                                                                                                                        
                                    <input type="file" class="btn-primary" name="certificate" id="premises" title="Select the certificate"/>
                                    <span class="help-block">Upload your certificate</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Attach Curriculum Vitae</label>
                                <div class="col-md-9">                                                                                                                                        
                                    <input type="file" class="btn-primary" name="curriculum_vitae" id="premises" title="Select a CV"/>
                                    <span class="help-block">Upload your CV in PDF format.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">First Guarantor</label>
                                <div class="col-md-9 col-xs-12">    
                                    <input type="text" name="guarantor1" class="form-control"/>
<!--                                    <select name="guarantor1" class="form-control">
                                    <?php // $trainers->getTrainers(); ?>
                                    </select>-->
                                    <span class="help-block">(Select/input first guarantor)</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Second Guarantor</label>
                                <div class="col-md-9 col-xs-12">    
                                    <input type="text" name="guarantor2" class="form-control"/>
<!--                                    <select name="guarantor2" class="form-control">
                                    <?php // $trainers->getTrainers(); ?>
                                    </select>-->
                                    <span class="help-block">(Select/input second guarantor)</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">First Facility</label>
                                <div class="col-md-9 col-xs-12">     
                                    <input type="text" name="facility1" class="form-control"/>
<!--                                    <select name="facility1" class="form-control">
                                    <?php // $training_facilities->getTrainingFacilities(); ?>
                                    </select>-->
                                    <span class="help-block">(Select/input first facility)</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Second Facility</label>
                                <div class="col-md-9 col-xs-12">     
                                    <input type="text" name="facility2" class="form-control"/>
<!--                                    <select name="facility2" class="form-control">
                                    <?php // $training_facilities->getTrainingFacilities(); ?>
                                    </select>-->
                                    <span class="help-block">(Select/input second facility)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <!--<button class="btn btn-default">Clear Form</button>-->                                    
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>              