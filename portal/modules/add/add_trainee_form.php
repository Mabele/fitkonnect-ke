<?php
if (!App::isLoggedIn()) App::redirectTo("?");
require_once WPATH . "modules/classes/Users_Management.php";
$users_management = new Users_Management();

if (!empty($_POST)) {    
    $_SESSION['firstname'] = $_POST['firstname'];
    $_SESSION['lastname'] = $_POST['lastname'];
    $_SESSION['gender'] = $_POST['gender'];
    $_SESSION['birth_date'] = $_POST['birth_date'];
    $_SESSION['ref_type'] = $users_management->getUserRefTypeId($_POST['user_type']);
    
    App::redirectTo("?add_contact&ref_type=" . $_SESSION['ref_type']);
    
}
?>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="POST">
            <input type="hidden" name="user_type" value="TRAINEE">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Add </strong>Trainee Registration Form</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <p>Trainee registration.</p>
                </div>
                <div class="panel-body">         
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">First Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="firstname" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">First name of trainee.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Last Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="lastname" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Last name of trainee.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">                                        
                                <label class="col-md-3 control-label">Gender</label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-bolt"></span></span>
                                        <select  name="gender"class="form-control">
                                            <?php include '../snippets/gender.php'; ?>
                                        </select>
                                    </div>            
                                    <span class="help-block">Gender, please select.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Birth Date</label>
                                <div class="col-md-9 col-xs-12">   
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        <input type="text" name="birth_date" class="form-control datepicker" value="2018-02-28"> 
                                    </div>
                                    <span class="help-block">Select Date</span>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <!--<button class="btn btn-default">Clear Form</button>-->                                    
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form>

    </div>
</div>              