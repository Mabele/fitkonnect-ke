<?php
if (!App::isLoggedIn()) App::redirectTo("?");
require_once WPATH . "modules/classes/Lessons.php";
$lessons = new Lessons();

if (!empty($_POST)) {
    $success = $lessons->execute();
    
    if (is_bool($success) && $success == true) {
        $_SESSION['add_success'] = true;
        $_SESSION['feedback_message'] = "<strong>Successful:</strong> The lesson category record has been created successfully.";
        App::redirectTo("?view_lesson_categories");
    } else {
        $_SESSION['add_fail'] = true;
        $_SESSION['feedback_message'] = "<strong>Error!</strong> There was an error creating the lesson category record. Please try again.";
        App::redirectTo("?add_lesson_category");
    }
}
?>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="POST">
            <input type="hidden" name="action" value="add_lesson_category"/>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Add</strong> Lesson Category Registration Form</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <p>Name assigned to the lesson category.</p>
                </div>
                <div class="panel-body">       
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Category Name</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-lemon-o"></span></span>
                                <input type="text" name="name" class="form-control"/>
                            </div>                                            
                            <span class="help-block">Name assigned to the lesson category.</span>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
<!--                    <button class="btn btn-default">Clear Form</button>                                    -->
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>                    