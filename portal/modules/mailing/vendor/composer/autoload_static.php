<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit2ecbc72984ad68f29cbc3ad58058bce1
{
    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Safaricom\\Mpesa\\' => 16,
        ),
        'P' => 
        array (
            'PHPMailer\\PHPMailer\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Safaricom\\Mpesa\\' => 
        array (
            0 => __DIR__ . '/..' . '/safaricom/mpesa/src',
        ),
        'PHPMailer\\PHPMailer\\' => 
        array (
            0 => __DIR__ . '/..' . '/phpmailer/phpmailer/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit2ecbc72984ad68f29cbc3ad58058bce1::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit2ecbc72984ad68f29cbc3ad58058bce1::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
