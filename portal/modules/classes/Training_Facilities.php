<?php

require_once WPATH . "modules/classes/Trainees.php";
require_once WPATH . "modules/classes/Users_Management.php";

class Training_Facilities extends Database {

    public function execute() {
        if ($_POST['action'] == "add_training_facility_service") {
            return $this->addTrainingFacilityService();
        } else if ($_POST['action'] == "edit_training_facility_service") {
            return $this->editTrainingFacilityService();
        } else if ($_POST['action'] == "add_training_facility") {
            return $this->addTrainingFacility();
        }
    }

    private function addTrainingFacilityService() {
        $sql = "INSERT INTO training_facility_services (name, createdby, lastmodifiedby)"
                . " VALUES (:name, :createdby, :lastmodifiedby)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("name", strtoupper($_POST['name']));
        $stmt->bindValue("createdby", $_SESSION['user_id']); 
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']);       
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    private function editTrainingFacilityService() {
        $sql = "UPDATE training_facility_services SET name=:name, lastmodifiedby=:lastmodifiedby, lastmodifiedat=:lastmodifiedat WHERE id=:id";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("id", $_SESSION['training_facility_service']);
        $stmt->bindValue("name", strtoupper($_POST['name']));
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']);
        $stmt->bindValue("lastmodifiedat", date("Y-m-d H:i:s"));
        if ($stmt->execute()) {
            return true;
        } else
            return false;
    }
    
    private function addTrainingFacility() {

        $training_facility_id = $this->getNextTrainingFacilityId();

        $sql = "INSERT INTO training_facilities (name, description, coordinates, ownership_type, services_offered, createdby, lastmodifiedby)"
                . " VALUES (:name, :description, :coordinates, :ownership_type, :services_offered, :createdby, :lastmodifiedby)";

        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("name", strtoupper($_SESSION['name']));
        $stmt->bindValue("description", strtoupper($_SESSION['description']));
        $stmt->bindValue("coordinates", strtoupper($_SESSION['coordinates']));
        $stmt->bindValue("ownership_type", strtoupper($_SESSION['ownership_type']));
        $stmt->bindValue("services_offered", strtoupper($_SESSION['services_offered']));
        $stmt->bindValue("createdby", $_SESSION['user_id']); 
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']); 
        $stmt->execute();

        $sql_contacts = "INSERT INTO contacts (user_id, phone_number1, phone_number2, email, postal_number, postal_code, postal_town, county, ward, prof_picture, website, createdby, lastmodifiedby)"
                . " VALUES (:user_id, :phone_number1, :phone_number2, :email, :postal_number, :postal_code, :postal_town, :county, :ward, :prof_picture, :website, :createdby, :lastmodifiedby)";

        $stmt_contacts = $this->prepareQuery($sql_contacts);
        $stmt_contacts->bindValue("user_id", 1); //$_SESSION['user_id']
        $stmt_contacts->bindValue("phone_number1", $_SESSION['phone_number1']);
        $stmt_contacts->bindValue("phone_number2", $_SESSION['phone_number2']);
        $stmt_contacts->bindValue("email", strtoupper($_SESSION['email']));
        $stmt_contacts->bindValue("postal_number", $_SESSION['postal_number']);
        $stmt_contacts->bindValue("postal_code", $_SESSION['postal_code']);
        $stmt_contacts->bindValue("postal_town", strtoupper($_SESSION['postal_town']));
        $stmt_contacts->bindValue("county", $_SESSION['county']);
        $stmt_contacts->bindValue("ward", $_SESSION['ward']);
        $stmt_contacts->bindValue("prof_picture", $_SESSION['prof_picture_filename']);
        $stmt_contacts->bindValue("website", strtoupper($_SESSION['website']));
        $stmt_contacts->bindValue("createdby", $_SESSION['user_id']); 
        $stmt_contacts->bindValue("lastmodifiedby", $_SESSION['user_id']); 
        $stmt_contacts->execute();

        $sql_administrator = "INSERT INTO training_facility_administrators (firstname, lastname, idnumber, phone_number, email, facility, createdby, lastmodifiedby)"
                . " VALUES (:firstname, :lastname, :idnumber, :phone_number, :email, :facility, :createdby, :lastmodifiedby)";

        $stmt_administrator = $this->prepareQuery($sql_administrator);
        $stmt_administrator->bindValue("firstname", strtoupper($_SESSION['admin_firstname']));
        $stmt_administrator->bindValue("lastname", strtoupper($_SESSION['admin_lastname']));
        $stmt_administrator->bindValue("idnumber", strtoupper($_SESSION['admin_idnumber']));
        $stmt_administrator->bindValue("phone_number", strtoupper($_SESSION['admin_phone_number']));
        $stmt_administrator->bindValue("email", strtoupper($_SESSION['admin_email']));
        $stmt_administrator->bindValue("facility", $training_facility_id);
        $stmt_administrator->bindValue("createdby", $_SESSION['user_id']); 
        $stmt_administrator->bindValue("lastmodifiedby", $_SESSION['user_id']); 
        $stmt_administrator->execute();

        $password = $this->randomString(10);

        $users_management = new Users_Management();
        $user_ref_type = $users_management->getUserRefTypeId('TRAINING FACILITY ADMINISTRATOR');

        $sql_userlogs = "INSERT INTO system_users (reference_type, reference_id, username, password)"
                . " VALUES (:reference_type, :reference_id, :username, :password)";

        $stmt_userlogs = $this->prepareQuery($sql_userlogs);
        $stmt_userlogs->bindValue("reference_type", $user_ref_type);
        $stmt_userlogs->bindValue("reference_id", $training_facility_id);
        $stmt_userlogs->bindValue("username", strtoupper($_SESSION['admin_email']));
        $stmt_userlogs->bindValue("password", sha1($password));
        if ($stmt_userlogs->execute()) {
            return true;
        } else {
            return false;
        }


//        $this->addUserToRole($role, $id, $createdby);
//        $this->addPrivilegesToUser($ref_type, $id, $role, $createdby);
//
//        $code = $this->randomString(20);
//
//        $mail = new PHPMailer;
//        // Set mailer to use SMTP
//        $mail->Host = $_SESSION["mail_host"];                                   // Specify main and backup SMTP servers
//        $mail->SMTPAuth = $_SESSION["SMTPAuth"];                                // Enable SMTP authentication
//        $mail->Username = $_SESSION["MUsername"];                               // SMTP username
//        $mail->Password = $_SESSION["MPassword"];                               // SMTP password
//        $mail->SMTPSecure = $_SESSION["SMTPSecure"];                            // Enable TLS encryption, `ssl` also accepted
//        $mail->Port = $_SESSION["Port"];                                        // TCP port to connect to
//        $mail->setFrom($_SESSION["MUsername"], $_SESSION["MUsernameFrom"]);
//
//        $mail->addAddress($email, $firstname);                  // Add a recipient
//        $mail->isHTML(true);                                    // Set email format to HTML
//        $mail->Subject = "New User Registration";
//        $mail->Body = "<html><body>"
//                . "<p><b>Hello " . $firstname . ",</b><br/>"
//                . "Your account has been successfully created. Your login credentials are as below. <br/>"
//                . "<ul>"
//                . "<li><b>Username: </b>" . $email . "</li>"
//                . "<li><b>Password: </b>" . $password . "</li>"
//                . "</ul>"
//                . "Click on this link: <a href=' " . $_SESSION['admin_url'] . "'>User Login</a> to proceed with the login. <br/>"
//                . "For any enquiries, kindly contact us on:   <br/>"
//                . "<ul>"
//                . "<li><b>Telephone Number(s): </b>" . $_SESSION['institution_phone'] . "</li>"
//                . "<li><b>Email Address: </b>" . $_SESSION["MUsername"] . "</li>"
//                . "</ul>"
//                . "Visit <a href='{$_SESSION['website_url']}'>" . $_SESSION['displayed_website_link'] . "</a> for more information.<br/>"
//                . "</body></html>";
//        $mail->AltBody = $_SESSION["AltBody"];
//        if ($mail->send()) {
//            return true;
//        } else {
//            return false;
//        }
    }

    public function getTrainingFacilityServices() {
        $stmt = $this->prepareQuery("SELECT id, name, status FROM training_facility_services ORDER BY id ASC");
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $currentGroup = null;
        $html = "";
        foreach ($info as $row) {
            if (is_null($currentGroup)) {
                $currentGroup = $row['name'];
                if (!empty($_POST['training_facility_service']) && $_POST['training_facility_service'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['name']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['name']}</option>";
                }
            } else {
                if (!empty($_POST['training_facility_service']) && $_POST['training_facility_service'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['name']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['name']}</option>";
                }
            }
        }
        if ($html == "") {
            $html = "<option value=\"\">No training facility service entered into the database!</option>";
        }
        echo $html;
        return $currentGroup;
    }

    public function getTrainingFacilities() {
        $stmt = $this->prepareQuery("SELECT id, name, status FROM training_facilities ORDER BY id ASC");
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $currentGroup = null;
        $html = "";
        foreach ($info as $row) {
            if (is_null($currentGroup)) {
                $currentGroup = $row['name'];
                if (!empty($_POST['training_facility']) && $_POST['training_facility'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['name']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['name']}</option>";
                }
            } else {
                if (!empty($_POST['training_facility']) && $_POST['training_facility'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['name']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['name']}</option>";
                }
            }
        }
        if ($html == "") {
            $html = "<option value=\"\">No training facility entered into the database!</option>";
        }
        echo $html;
        return $currentGroup;
    }

    public function getAllTrainingFacilityServices() {
        $sql = "SELECT * FROM training_facility_services ORDER BY id ASC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("id" => $data['id'], "name" => $data['name'], "status" => $data['status'], "createdat" => $data['createdat'], "createdby" => $data['createdby'], "lastmodifiedat" => $data['lastmodifiedat'], "lastmodifiedby" => $data['lastmodifiedby']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function getAllTrainingFacilityAdministrators() {
        $sql = "SELECT * FROM training_facility_administrators ORDER BY id ASC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("id" => $data['id'], "firstname" => $data['firstname'], "lastname" => $data['lastname'], "idnumber" => $data['idnumber'], "phone_number" => $data['phone_number'], "email" => $data['email'], "facility" => $data['facility'], "status" => $data['status'], "createdat" => $data['createdat'], "createdby" => $data['createdby'], "lastmodifiedat" => $data['lastmodifiedat'], "lastmodifiedby" => $data['lastmodifiedby']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function getAllTrainingFacilities() {
        $sql = "SELECT * FROM training_facilities ORDER BY id ASC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("id" => $data['id'], "name" => $data['name'], "description" => $data['description'], "coordinates" => $data['coordinates'], "ownership_type" => $data['ownership_type'], "facility_rating" => $data['facility_rating'], "services_offered" => $data['services_offered'], "status" => $data['status'], "createdat" => $data['createdat'], "createdby" => $data['createdby'], "lastmodifiedat" => $data['lastmodifiedat'], "lastmodifiedby" => $data['lastmodifiedby']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function fetchTrainingFacilityDetails($code) {
        $sql = "SELECT * FROM training_facilities WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function fetchTrainingFacilityAdministratorDetails($code) {
        $sql = "SELECT * FROM training_facility_administrators WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function fetchTrainingFacilityServiceDetails($code) {
        $sql = "SELECT * FROM training_facility_services WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function getNextTrainingFacilityId() {
        $training_facility_id = $this->executeQuery("SELECT max(id) as training_facility_id_max FROM training_facilities");
        $training_facility_id = $training_facility_id[0]['training_facility_id_max'] + 1;
        return $training_facility_id;
    }

    private function randomString($number) {
        $trainees = new Trainees();
        return $trainees->randomString($number);
    }

}
