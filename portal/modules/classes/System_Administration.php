<?php

class System_Administration extends Database {

    public function execute() {
        if ($_POST['action'] == "add_system_component") {
            return $this->addSystemComponent();
        } else if ($_POST['action'] == "edit_system_component") {
            return $this->editSystemComponent();
        } else if ($_POST['action'] == "add_system_privilege") {
            return $this->addSystemPrivilege();
        } else if ($_POST['action'] == "edit_system_privilege") {
            return $this->editSystemPrivilege();
        } else if ($_POST['action'] == "add_system_status_code") {
            return $this->addSystemStatusCode();
        } else if ($_POST['action'] == "edit_system_status_code") {
            return $this->editSystemStatusCode();
        } else if ($_POST['action'] == "add_county") {
            return $this->addCounty();
        } else if ($_POST['action'] == "edit_county") {
            return $this->editCounty();
        } else if ($_POST['action'] == "add_ward") {
            return $this->addWard();
        } else if ($_POST['action'] == "edit_ward") {
            return $this->editWard();
        } else if ($_POST['action'] == "add_system_role") {
            return $this->addSystemRole();
        } else if ($_POST['action'] == "edit_system_role") {
            return $this->editSystemRole();
        } else if ($_POST['action'] == "assign_privilege_to_role") {
            return $this->assignPrivilegeToRole();
        } else if ($_POST['action'] == "add_system_administrator") {
            return $this->addSystemAdministrator();
        } else if ($_POST['action'] == "edit_system_administrator") {
            return $this->editSystemAdministrator();
        }
    }

    private function addSystemComponent() {
        $sql = "INSERT INTO system_components (name, acronym, createdby, lastmodifiedby)"
                . " VALUES (:name, :acronym, :createdby, :lastmodifiedby)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("name", strtoupper($_POST['name']));
        $stmt->bindValue("acronym", strtoupper($_POST['acronym']));
        $stmt->bindValue("createdby", $_SESSION['user_id']);
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    private function editSystemComponent() {
        $sql = "UPDATE system_components SET name=:name, acronym=:acronym, lastmodifiedby=:lastmodifiedby, lastmodifiedat=:lastmodifiedat WHERE id=:id";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("id", $_SESSION['system_component']);
        $stmt->bindValue("name", strtoupper($_POST['name']));
        $stmt->bindValue("acronym", strtoupper($_POST['acronym']));
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']);
        $stmt->bindValue("lastmodifiedat", date("Y-m-d H:i:s"));
        if ($stmt->execute()) {
            return true;
        } else
            return false;
    }

    private function addSystemPrivilege() {
        $sql = "INSERT INTO system_privileges (name, component, createdby, lastmodifiedby)"
                . " VALUES (:name, :component, :createdby, :lastmodifiedby)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("name", strtoupper($_POST['name']));
        $stmt->bindValue("component", strtoupper($_POST['component']));
        $stmt->bindValue("createdby", $_SESSION['user_id']);
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    private function editSystemPrivilege() {
        $sql = "UPDATE system_privileges SET name=:name, component=:component, lastmodifiedby=:lastmodifiedby, lastmodifiedat=:lastmodifiedat WHERE id=:id";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("id", $_SESSION['system_privilege']);
        $stmt->bindValue("name", strtoupper($_POST['name']));
        $stmt->bindValue("component", strtoupper($_POST['component']));
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']);
        $stmt->bindValue("lastmodifiedat", date("Y-m-d H:i:s"));
        if ($stmt->execute()) {
            return true;
        } else
            return false;
    }

    private function addSystemStatusCode() {
        $sql = "INSERT INTO status_codes (status_code, description, display_value, createdby, lastmodifiedby)"
                . " VALUES (:status_code, :description, :display_value, :createdby, :lastmodifiedby)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("status_code", strtoupper($_POST['status_code']));
        $stmt->bindValue("description", strtoupper($_POST['description']));
        $stmt->bindValue("display_value", strtoupper($_POST['display_value']));
        $stmt->bindValue("createdby", $_SESSION['user_id']);
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    private function editSystemStatusCode() {
        $sql = "UPDATE status_codes SET status_code=:status_code, description=:description, display_value=:display_value, lastmodifiedby=:lastmodifiedby, lastmodifiedat=:lastmodifiedat WHERE id=:id";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("id", $_SESSION['system_status_code']);
        $stmt->bindValue("status_code", strtoupper($_POST['status_code']));
        $stmt->bindValue("description", strtoupper($_POST['description']));
        $stmt->bindValue("display_value", strtoupper($_POST['display_value']));
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']);
        $stmt->bindValue("lastmodifiedat", date("Y-m-d H:i:s"));
        if ($stmt->execute()) {
            return true;
        } else
            return false;
    }

    private function addCounty() {
        $sql = "INSERT INTO counties (name, createdby, lastmodifiedby)"
                . " VALUES (:name, :createdby, :lastmodifiedby)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("name", strtoupper($_POST['name']));
        $stmt->bindValue("createdby", $_SESSION['user_id']);
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    private function editCounty() {
        $sql = "UPDATE counties SET name=:name, lastmodifiedby=:lastmodifiedby, lastmodifiedat=:lastmodifiedat WHERE id=:id";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("id", $_SESSION['county']);
        $stmt->bindValue("name", strtoupper($_POST['name']));
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']);
        $stmt->bindValue("lastmodifiedat", date("Y-m-d H:i:s"));
        if ($stmt->execute()) {
            return true;
        } else
            return false;
    }

    private function addWard() {
        $sql = "INSERT INTO wards (name, county, createdby, lastmodifiedby)"
                . " VALUES (:name, :county, :createdby, :lastmodifiedby)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("name", strtoupper($_POST['name']));
        $stmt->bindValue("county", strtoupper($_POST['county']));
        $stmt->bindValue("createdby", $_SESSION['user_id']);
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    private function editWard() {
        $sql = "UPDATE wards SET name=:name, county=:county, lastmodifiedby=:lastmodifiedby, lastmodifiedat=:lastmodifiedat WHERE id=:id";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("id", $_SESSION['ward']);
        $stmt->bindValue("name", strtoupper($_POST['name']));
        $stmt->bindValue("county", strtoupper($_POST['county']));
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']);
        $stmt->bindValue("lastmodifiedat", date("Y-m-d H:i:s"));
        if ($stmt->execute()) {
            return true;
        } else
            return false;
    }

    private function addSystemRole() {
        $sql = "INSERT INTO system_roles (name, description, createdby, lastmodifiedby)"
                . " VALUES (:name, :description, :createdby, :lastmodifiedby)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("name", strtoupper($_POST['name']));
        $stmt->bindValue("description", strtoupper($_POST['description']));
        $stmt->bindValue("createdby", $_SESSION['user_id']);
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    private function editSystemRole() {
        $sql = "UPDATE system_roles SET name=:name, description=:description, lastmodifiedby=:lastmodifiedby, lastmodifiedat=:lastmodifiedat WHERE id=:id";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("id", $_SESSION['system_role']);
        $stmt->bindValue("name", strtoupper($_POST['name']));
        $stmt->bindValue("description", strtoupper($_POST['description']));
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']);
        $stmt->bindValue("lastmodifiedat", date("Y-m-d H:i:s"));
        if ($stmt->execute()) {
            return true;
        } else
            return false;
    }

    private function addSystemAdministrator() {
        $sql = "INSERT INTO system_administrators (firstname, lastname, phone_number, email, idnumber, createdby, lastmodifiedby)"
                . " VALUES (:firstname, :lastname, :phone_number, :email, :idnumber, :createdby, :lastmodifiedby)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("firstname", strtoupper($_POST['firstname']));
        $stmt->bindValue("lastname", strtoupper($_POST['lastname']));
        $stmt->bindValue("phone_number", strtoupper($_POST['phone_number']));
        $stmt->bindValue("email", strtoupper($_POST['email']));
        $stmt->bindValue("idnumber", strtoupper($_POST['idnumber']));
        $stmt->bindValue("createdby", $_SESSION['user_id']); //or 'SYSTEM SETUP';
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']); //or 'SYSTEM SETUP';        
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    private function assignPrivilegeToRole() {
        $role_id = $this->getRoleId(strtoupper($role));
        $sql = "INSERT INTO role_privileges (role, privilege, createdby, lastmodifiedby)"
                . " VALUES (:role, :privilege, :createdby, :lastmodifiedby)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("role", $role_id);
        $stmt->bindValue("privilege", strtoupper($privilege));
        $stmt->bindValue("createdby", $createdby);
        $stmt->bindValue("lastmodifiedby", $createdby);
        $stmt->execute();
        return true;
    }

    public function getCounties() {
        $stmt = $this->prepareQuery("SELECT id, name, status FROM counties ORDER BY id ASC");
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $currentGroup = null;
        $html = "";
        foreach ($info as $row) {
            if (is_null($currentGroup)) {
                $currentGroup = $row['name'];
                if (!empty($_POST['county']) && $_POST['county'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['name']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['name']}</option>";
                }
            } else {
                if (!empty($_POST['county']) && $_POST['county'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['name']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['name']}</option>";
                }
            }
        }
        if ($html == "") {
            $html = "<option value=\"\">No county entered into the database!</option>";
        }
        echo $html;
        return $currentGroup;
    }

    public function getWards() {
        $stmt = $this->prepareQuery("SELECT id, name, status FROM wards ORDER BY id ASC");
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $currentGroup = null;
        $html = "";
        foreach ($info as $row) {
            if (is_null($currentGroup)) {
                $currentGroup = $row['name'];
                if (!empty($_POST['ward']) && $_POST['ward'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['name']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['name']}</option>";
                }
            } else {
                if (!empty($_POST['ward']) && $_POST['ward'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['name']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['name']}</option>";
                }
            }
        }
        if ($html == "") {
            $html = "<option value=\"\">No ward entered into the database!</option>";
        }
        echo $html;
        return $currentGroup;
    }

    public function getSystemComponents() {
        $stmt = $this->prepareQuery("SELECT id, name, status FROM system_components ORDER BY id ASC");
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $currentGroup = null;
        $html = "";
        foreach ($info as $row) {
            if (is_null($currentGroup)) {
                $currentGroup = $row['name'];
                if (!empty($_POST['system_component']) && $_POST['system_component'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['name']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['name']}</option>";
                }
            } else {
                if (!empty($_POST['system_component']) && $_POST['system_component'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['name']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['name']}</option>";
                }
            }
        }
        if ($html == "") {
            $html = "<option value=\"\">No component entered into the database!</option>";
        }
        echo $html;
        return $currentGroup;
    }

    public function getSystemPrivileges() {
        $stmt = $this->prepareQuery("SELECT id, name, status FROM system_privileges WHERE status=1011 "
                . "OR status=1021 OR status=1031 ORDER BY id ASC");
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $currentGroup = null;
        $html = "";
        foreach ($info as $row) {
            if (is_null($currentGroup)) {
                $currentGroup = $row['name'];
                if (!empty($_POST['system_privilege']) && $_POST['system_privilege'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['name']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['name']}</option>";
                }
            } else {
                if (!empty($_POST['system_privilege']) && $_POST['system_privilege'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['name']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['name']}</option>";
                }
            }
        }
        if ($html == "") {
            $html = "<option value=\"\">No privilege entered into the database!</option>";
        }
        echo $html;
        return $currentGroup;
    }

    public function getSystemRoles() {
        $stmt = $this->prepareQuery("SELECT id, name, status FROM system_roles ORDER BY id ASC");
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $currentGroup = null;
        $html = "";
        foreach ($info as $row) {
            if (is_null($currentGroup)) {
                $currentGroup = $row['name'];
                if (!empty($_POST['system_role']) && $_POST['system_role'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['name']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['name']}</option>";
                }
            } else {
                if (!empty($_POST['system_role']) && $_POST['system_role'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['name']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['name']}</option>";
                }
            }
        }
        if ($html == "") {
            $html = "<option value=\"\">No system role entered into the database!</option>";
        }
        echo $html;
        return $currentGroup;
    }

    public function getAllCounties() {
        $sql = "SELECT * FROM counties ORDER BY id ASC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("id" => $data['id'], "name" => $data['name'], "status" => $data['status'], "createdat" => $data['createdat'], "createdby" => $data['createdby'], "lastmodifiedat" => $data['lastmodifiedat'], "lastmodifiedby" => $data['lastmodifiedby']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function getAllWards() {
        $sql = "SELECT * FROM wards ORDER BY id ASC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("id" => $data['id'], "name" => $data['name'], "county" => $data['county'], "status" => $data['status'], "createdat" => $data['createdat'], "createdby" => $data['createdby'], "lastmodifiedat" => $data['lastmodifiedat'], "lastmodifiedby" => $data['lastmodifiedby']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function getAllSystemComponents() {
        $sql = "SELECT * FROM system_components ORDER BY id ASC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("id" => $data['id'], "name" => $data['name'], "acronym" => $data['acronym'], "status" => $data['status'], "createdat" => $data['createdat'], "createdby" => $data['createdby'], "lastmodifiedat" => $data['lastmodifiedat'], "lastmodifiedby" => $data['lastmodifiedby']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function getAllSystemPrivileges() {
        $sql = "SELECT * FROM system_privileges ORDER BY id ASC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("id" => $data['id'], "name" => $data['name'], "component" => $data['component'], "status" => $data['status'], "createdat" => $data['createdat'], "createdby" => $data['createdby'], "lastmodifiedat" => $data['lastmodifiedat'], "lastmodifiedby" => $data['lastmodifiedby']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function getAllSystemStatusCodes() {
        $sql = "SELECT * FROM status_codes ORDER BY id ASC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("id" => $data['id'], "status_code" => $data['status_code'], "description" => $data['description'], "display_value" => $data['display_value']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function getAllSystemRoles() {
        $sql = "SELECT * FROM system_roles ORDER BY id ASC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("id" => $data['id'], "name" => $data['name'], "description" => $data['description'], "status" => $data['status'], "createdat" => $data['createdat'], "createdby" => $data['createdby'], "lastmodifiedat" => $data['lastmodifiedat'], "lastmodifiedby" => $data['lastmodifiedby']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function getAllSystemAdministrators() {
        $sql = "SELECT * FROM system_administrators ORDER BY id ASC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("id" => $data['id'], "firstname" => $data['firstname'], "lastname" => $data['lastname'], "phone_number" => $data['phone_number'], "email" => $data['email'], "idnumber" => $data['idnumber'], "status" => $data['status'], "createdat" => $data['createdat'], "createdby" => $data['createdby'], "lastmodifiedat" => $data['lastmodifiedat'], "lastmodifiedby" => $data['lastmodifiedby']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function fetchSystemComponentDetails($code) {
        $sql = "SELECT * FROM system_components WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function fetchSystemStatusDetails($code) {
        $sql = "SELECT * FROM status_codes WHERE status_code=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function fetchSystemStatusCodeDetails($code) {
        $sql = "SELECT * FROM status_codes WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function fetchSystemPrivilegeDetails($code) {
        $sql = "SELECT * FROM system_privileges WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function fetchSystemRoleDetails($code) {
        $sql = "SELECT * FROM system_roles WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function fetchSystemAdministratorDetails($code) {
        $sql = "SELECT * FROM system_administrators WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function fetchCountyDetails($code) {
        $sql = "SELECT * FROM counties WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function fetchWardDetails($code) {
        $sql = "SELECT * FROM wards WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function getSystemRoleId($role) {
        $sql = "SELECT id, status FROM system_roles WHERE name=:role";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("role", $role);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $data = $data[0];
        return strtoupper($data['id']);
    }

}
