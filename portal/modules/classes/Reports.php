<?php

class Reports extends Database {

    public function countAllStaffMembers() {
        $sql = "SELECT * FROM staff";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }

    public function countAllTrainees() {
        $sql = "SELECT * FROM trainees";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }
    
    public function countAllTrainers() {
        $sql = "SELECT * FROM trainers";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }
    
    public function countAllSystemAdministrators() {
        $sql = "SELECT * FROM system_administrators";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }

    public function countAllFacilityAdministrators() {
        $sql = "SELECT * FROM training_facility_administrators";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }

    public function countAllTrainingFacilities() {
        $sql = "SELECT * FROM training_facilities";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }

    public function countAllLessons() {
        $sql = "SELECT * FROM lessons";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }

    public function countAllSMSMessages() {
        $sql = "SELECT * FROM sms_log";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }

    public function countSentSMSMessages() {
        $sql = "SELECT * FROM sms_log WHERE type=:sms_type";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("sms_type", 'OUTBOX');
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }

    public function countReceivedSMSMessages() {
        $sql = "SELECT * FROM sms_log WHERE type=:sms_type";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("sms_type", 'INBOX');
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }

    public function countReceivedEnquiryMessages() {
        $sql = "SELECT * FROM inbox_messages";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }

    public function countAllTransactions() {
        $sql = "SELECT * FROM booking_transactions";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }

    public function countAllPaymentTransactions() {
        $sql = "SELECT * FROM payment_transactions";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }
    
    public function sumAllPaymentAmountsReceived() {
        $sql = "SELECT SUM(transaction_amount) as sum FROM  payment_transactions";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0]['sum'];
    }
    
    public function countAllLessonTransactions() {
        $sql = "SELECT * FROM booking_transactions_details";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }

    public function countAllOpenLessonTransactions() {
        $sql = "SELECT * FROM booking_transactions_details WHERE status=1003 OR status=1005";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }

    public function countAllClosedLessonTransactions() {
        $sql = "SELECT * FROM booking_transactions_details WHERE status=:status";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("status", 1004);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }

//    public function countAllRedeemableItems() {
//        $sql = "SELECT * FROM redeemable_items";
//        $stmt = $this->prepareQuery($sql);
//        $stmt->execute();
//        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
//        return count($info);
//    }

    
    
//    public function countAllUnassignedLessonTransactions() {
//        $sql = "SELECT * FROM booking_transactions_details";
//        $stmt = $this->prepareQuery($sql);
//        $stmt->bindValue("status", 2005);
//        $stmt->execute();
//        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
//        return count($info);
//    }
//
//    public function countAllBidsByProject($project_id) {
//        $sql = "SELECT * FROM project_bids WHERE project_id=:project_id AND bid_amount != 0";
//        $stmt = $this->prepareQuery($sql);
//        $stmt->bindValue("project_id", $project_id);
//        $stmt->execute();
//        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
//        return count($info);
//    }
//
//    public function sumAllBidsByProject($project_id) {
//        $sql = "SELECT SUM(bid_amount) as sum FROM project_bids WHERE project_id=:project_id";
//        $stmt = $this->prepareQuery($sql);
//        $stmt->bindValue("project_id", $project_id);
//        $stmt->execute();
//        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
//        return $info[0]['sum'];
//    }
//
//    public function countAllTransactionRecordsByTransactionType($transaction_type) {
//        if ($transaction_type == "ALL") {
//            $sql = "SELECT * FROM transactions";
//            $stmt = $this->prepareQuery($sql);
//        } else {
//            $settings = new Settings();
//            $transaction_type_ref_id = $settings->getTransactionRefTypeId($transaction_type);
//            $sql = "SELECT * FROM transactions WHERE transaction_type=:transaction_type";
//            $stmt = $this->prepareQuery($sql);
//            $stmt->bindValue("transaction_type", $transaction_type_ref_id);
//        }
//        $stmt->execute();
//        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
//        return count($info);
//    }
//
//    public function sumAllTransactionAmountsByTransactionType($transaction_type) {
//        if ($transaction_type == "ALL") {
//            $sql = "SELECT SUM(amount) as sum FROM transactions";
//            $stmt = $this->prepareQuery($sql);
//        } else {
//            $settings = new Settings();
//            $transaction_type_ref_id = $settings->getTransactionRefTypeId($transaction_type);
//            $sql = "SELECT SUM(amount) as sum FROM transactions WHERE transaction_type=:transaction_type";
//            $stmt = $this->prepareQuery($sql);
//            $stmt->bindValue("transaction_type", $transaction_type_ref_id);
//        }
//        $stmt->execute();
//        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
//        return $info[0]['sum'];
//    }
//
//    public function countAllAccountTransactionRecordsByTransactionType($transaction_type, $account) {
//        if ($transaction_type == "ALL") {
//            $sql = "SELECT * FROM transactions WHERE account_number=:account_number";
//            $stmt = $this->prepareQuery($sql);
//            $stmt->bindValue("account_number", $account);
//        } else {
//            $settings = new Settings();
//            $transaction_type_ref_id = $settings->getTransactionRefTypeId($transaction_type);
//            $sql = "SELECT * FROM transactions WHERE  account_number=:account_number AND transaction_type=:transaction_type";
//            $stmt = $this->prepareQuery($sql);
//            $stmt->bindValue("account_number", $account);
//            $stmt->bindValue("transaction_type", $transaction_type_ref_id);
//        }
//        $stmt->execute();
//        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
//        return count($info);
//    }

}
