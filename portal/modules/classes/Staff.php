<?php
require_once WPATH . "modules/classes/Trainees.php";

class Staff extends Database {
    
    public function execute() {
        if ($_POST['action'] == "add_staff_position") {
            return $this->addStaffPosition();
        } else if ($_POST['action'] == "edit_staff_position") {
            return $this->editStaffPosition();
        } else if ($_POST['action'] == "add_staff") {
            return $this->addStaff();
        } else if ($_POST['action'] == "edit_staff") {
            return $this->editStaff();
        }
    }

    private function addStaff() {      
    $staff_id = $this->getNextStaffId();
    
        $sql = "INSERT INTO staff (pf_number, firstname, middlename, lastname, gender, idnumber, position, createdby, lastmodifiedby)"
                . " VALUES (:pf_number, :firstname, :middlename, :lastname, :gender, :idnumber, :position, :createdby, :lastmodifiedby)";

        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("pf_number", strtoupper($_SESSION['pf_number']));
        $stmt->bindValue("firstname", strtoupper($_SESSION['firstname']));
        $stmt->bindValue("middlename", strtoupper($_SESSION['middlename']));
        $stmt->bindValue("lastname", strtoupper($_SESSION['lastname']));
        $stmt->bindValue("gender", strtoupper($_SESSION['gender']));
        $stmt->bindValue("idnumber", strtoupper($_SESSION['idnumber']));
        $stmt->bindValue("position", $_SESSION['position']);
        $stmt->bindValue("createdby", $_SESSION['user_id']); 
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']); 
        $stmt->execute();
        
        $sql_contacts = "INSERT INTO contacts (user_id, phone_number1, phone_number2, email, postal_number, postal_code, postal_town, county, ward, prof_picture, website, createdby, lastmodifiedby)"
                . " VALUES (:user_id, :phone_number1, :phone_number2, :email, :postal_number, :postal_code, :postal_town, :county, :ward, :prof_picture, :website, :createdby, :lastmodifiedby)";

        $stmt_contacts = $this->prepareQuery($sql_contacts);
        $stmt_contacts->bindValue("user_id", 1); //$_SESSION['user_id']
        $stmt_contacts->bindValue("phone_number1", $_SESSION['phone_number1']);
        $stmt_contacts->bindValue("phone_number2", $_SESSION['phone_number2']);
        $stmt_contacts->bindValue("email", strtoupper($_SESSION['email']));
        $stmt_contacts->bindValue("postal_number", $_SESSION['postal_number']);
        $stmt_contacts->bindValue("postal_code", $_SESSION['postal_code']);
        $stmt_contacts->bindValue("postal_town", strtoupper($_SESSION['postal_town']));
        $stmt_contacts->bindValue("county", $_SESSION['county']);
        $stmt_contacts->bindValue("ward", $_SESSION['ward']);
        $stmt_contacts->bindValue("prof_picture", $_SESSION['prof_picture_filename']);
        $stmt_contacts->bindValue("website", strtoupper($_SESSION['website']));        
        $stmt_contacts->bindValue("createdby", $_SESSION['user_id']); 
        $stmt_contacts->bindValue("lastmodifiedby", $_SESSION['user_id']); 
        $stmt_contacts->execute();

        $password = $this->randomString(10);
        
        $sql_userlogs = "INSERT INTO system_users (reference_type, reference_id, username, password)"
                . " VALUES (:reference_type, :reference_id, :username, :password)";

        $stmt_userlogs = $this->prepareQuery($sql_userlogs);
        $stmt_userlogs->bindValue("reference_type", $_SESSION['ref_type']);
        $stmt_userlogs->bindValue("reference_id", $staff_id);
        $stmt_userlogs->bindValue("username", strtoupper($_SESSION['email']));
        $stmt_userlogs->bindValue("password", sha1($password));        
        if ($stmt_userlogs->execute()) {
            return true;
        } else {
            return false;
        }

//        $this->addUserToRole($role, $id, $createdby);
//        $this->addPrivilegesToUser($ref_type, $id, $role, $createdby);
//
//        $code = $this->randomString(20);
//
//        $mail = new PHPMailer;
//        // Set mailer to use SMTP
//        $mail->Host = $_SESSION["mail_host"];                                   // Specify main and backup SMTP servers
//        $mail->SMTPAuth = $_SESSION["SMTPAuth"];                                // Enable SMTP authentication
//        $mail->Username = $_SESSION["MUsername"];                               // SMTP username
//        $mail->Password = $_SESSION["MPassword"];                               // SMTP password
//        $mail->SMTPSecure = $_SESSION["SMTPSecure"];                            // Enable TLS encryption, `ssl` also accepted
//        $mail->Port = $_SESSION["Port"];                                        // TCP port to connect to
//        $mail->setFrom($_SESSION["MUsername"], $_SESSION["MUsernameFrom"]);
//
//        $mail->addAddress($email, $firstname);                  // Add a recipient
//        $mail->isHTML(true);                                    // Set email format to HTML
//        $mail->Subject = "New User Registration";
//        $mail->Body = "<html><body>"
//                . "<p><b>Hello " . $firstname . ",</b><br/>"
//                . "Your account has been successfully created. Your login credentials are as below. <br/>"
//                . "<ul>"
//                . "<li><b>Username: </b>" . $email . "</li>"
//                . "<li><b>Password: </b>" . $password . "</li>"
//                . "</ul>"
//                . "Click on this link: <a href=' " . $_SESSION['admin_url'] . "'>User Login</a> to proceed with the login. <br/>"
//                . "For any enquiries, kindly contact us on:   <br/>"
//                . "<ul>"
//                . "<li><b>Telephone Number(s): </b>" . $_SESSION['institution_phone'] . "</li>"
//                . "<li><b>Email Address: </b>" . $_SESSION["MUsername"] . "</li>"
//                . "</ul>"
//                . "Visit <a href='{$_SESSION['website_url']}'>" . $_SESSION['displayed_website_link'] . "</a> for more information.<br/>"
//                . "</body></html>";
//        $mail->AltBody = $_SESSION["AltBody"];
//        if ($mail->send()) {
//            return true;
//        } else {
//            return false;
//        }
    }    
    
    private function editStaff() {
        $sql = "UPDATE staff SET pf_number=:pf_number, firstname=:firstname, middlename=:middlename, lastname=:lastname, gender=:gender, idnumber=:idnumber, position=:position, "
                . "lastmodifiedby=:lastmodifiedby, lastmodifiedat=:lastmodifiedat WHERE id=:id";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("id", $_SESSION['staff']);
        $stmt->bindValue("pf_number", strtoupper($_POST['pf_number']));
        $stmt->bindValue("firstname", strtoupper($_POST['firstname']));
        $stmt->bindValue("middlename", strtoupper($_POST['middlename']));
        $stmt->bindValue("lastname", strtoupper($_POST['lastname']));
        $stmt->bindValue("gender", strtoupper($_POST['gender']));
        $stmt->bindValue("idnumber", strtoupper($_POST['idnumber']));
        $stmt->bindValue("position", $_POST['position']);
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']); 
        $stmt->bindValue("lastmodifiedat", date("Y-m-d H:i:s"));
        if ($stmt->execute()) {
            return true;
        } else
            return false;
    }
    
    private function addStaffPosition() {
        $sql = "INSERT INTO staff_positions (name, createdby, lastmodifiedby)"
                . " VALUES (:name, :createdby, :lastmodifiedby)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("name", strtoupper($_POST['name']));
        $stmt->bindValue("createdby", $_SESSION['user_id']); 
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']);      
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }    
    
    private function editStaffPosition() {
        $sql = "UPDATE staff_positions SET name=:name, lastmodifiedby=:lastmodifiedby, lastmodifiedat=:lastmodifiedat WHERE id=:id";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("id", $_SESSION['staff_position']);
        $stmt->bindValue("name", strtoupper($_POST['name']));
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']);
        $stmt->bindValue("lastmodifiedat", date("Y-m-d H:i:s"));
        if ($stmt->execute()) {
            return true;
        } else
            return false;
    }
    
    public function getStaffPositions() {
        $stmt = $this->prepareQuery("SELECT id, name, status FROM staff_positions ORDER BY id ASC");
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $currentGroup = null;
        $html = "";
        foreach ($info as $row) {
            if (is_null($currentGroup)) {
                $currentGroup = $row['name'];
                if (!empty($_POST['staff_position']) && $_POST['staff_position'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['name']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['name']}</option>";
                }
            } else {
                if (!empty($_POST['staff_position']) && $_POST['staff_position'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['name']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['name']}</option>";
                }
            }
        }
        if ($html == "") {
            $html = "<option value=\"\">No staff position entered into the database!</option>";
        }
        echo $html;
        return $currentGroup;
    }

    public function getAllStaffPositions() {
        $sql = "SELECT * FROM staff_positions ORDER BY id ASC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("id" => $data['id'], "name" => $data['name'], "status" => $data['status'], "createdat" => $data['createdat'], "createdby" => $data['createdby'], "lastmodifiedat" => $data['lastmodifiedat'], "lastmodifiedby" => $data['lastmodifiedby']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function getAllStaffMembers() {
        $sql = "SELECT * FROM staff ORDER BY id ASC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("id" => $data['id'], "pf_number" => $data['pf_number'], "firstname" => $data['firstname'], "middlename" => $data['middlename'], "lastname" => $data['lastname'], "gender" => $data['gender'], "idnumber" => $data['idnumber'], "position" => $data['position'], "status" => $data['status'], "createdat" => $data['createdat'], "createdby" => $data['createdby'], "lastmodifiedat" => $data['lastmodifiedat'], "lastmodifiedby" => $data['lastmodifiedby']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function fetchStaffPositionDetails($code) {
        $sql = "SELECT * FROM staff_positions WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }
        
    public function fetchStaffDetails($code) {
        $sql = "SELECT * FROM staff WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }
        
    public function getNextStaffId() {
        $staff_id = $this->executeQuery("SELECT max(id) as staff_id_max FROM staff");
        $staff_id = $staff_id[0]['staff_id_max'] + 1;
        return $staff_id;
    }
        
    private function randomString($number) {
        $trainees = new Trainees();
        return $trainees->randomString($number);
    }
    
}
