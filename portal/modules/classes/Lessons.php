<?php

class Lessons extends Database {

    public function execute() {
        if ($_POST['action'] == "add_lesson_category") {
            return $this->addLessonCategory();
        } else if ($_POST['action'] == "edit_lesson_category") {
            return $this->editLessonCategory();
        } else if ($_POST['action'] == "add_lesson") {
            return $this->addLesson();
        } else if ($_POST['action'] == "edit_lesson") {
            return $this->editLesson();
        }
    }

    private function addLessonCategory() {
        $sql = "INSERT INTO lesson_categories (name, createdby, lastmodifiedby)"
                . " VALUES (:name, :createdby, :lastmodifiedby)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("name", strtoupper($_POST['name']));
        $stmt->bindValue("createdby", $_SESSION['user_id']);
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    private function editLessonCategory() {
        $sql = "UPDATE lesson_categories SET name=:name, lastmodifiedby=:lastmodifiedby, lastmodifiedat=:lastmodifiedat WHERE id=:id";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("id", $_SESSION['lesson_category']);
        $stmt->bindValue("name", strtoupper($_POST['name']));
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']);
        $stmt->bindValue("lastmodifiedat", date("Y-m-d H:i:s"));
        if ($stmt->execute()) {
            return true;
        } else
            return false;
    }

    private function addLesson() {
        $sql = "INSERT INTO lessons (name, description, price, createdby, lastmodifiedby)"
                . " VALUES (:name, :description, :price, :createdby, :lastmodifiedby)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("name", strtoupper($_POST['name']));
        $stmt->bindValue("description", strtoupper($_POST['description']));
//        $stmt->bindValue("category", $_POST['category']);
//        $stmt->bindValue("start_time", strtoupper($_POST['start_time']));
//        $stmt->bindValue("end_time", strtoupper($_POST['end_time']));
//        $stmt->bindValue("training_facility", $_POST['training_facility']);
        if(isset($_POST['price']) AND !empty($_POST['price'])) {
            $stmt->bindValue("price", $_POST['price']);
        } else {
            $stmt->bindValue("price", NULL);
        }        
//        $stmt->bindValue("capacity", $_POST['capacity']);
//        $stmt->bindValue("current_population", $_POST['current_population']);
        $stmt->bindValue("createdby", $_SESSION['user_id']);
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    private function editLesson() {
        $sql = "UPDATE lessons SET name=:name, description=:description, category=:category, start_time=:start_time, end_time=:end_time, training_facility=:training_facility, price=:price, capacity=:capacity, current_population=:current_population, lastmodifiedby=:lastmodifiedby, lastmodifiedat=:lastmodifiedat WHERE id=:id";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("id", $_SESSION['lesson']);
        $stmt->bindValue("name", strtoupper($_POST['name']));
        $stmt->bindValue("description", strtoupper($_POST['description']));
        $stmt->bindValue("category", $_POST['category']);
        $stmt->bindValue("start_time", strtoupper($_POST['start_time']));
        $stmt->bindValue("end_time", strtoupper($_POST['end_time']));
        $stmt->bindValue("training_facility", $_POST['training_facility']);
        $stmt->bindValue("price", $_POST['price']);
        $stmt->bindValue("capacity", $_POST['capacity']);
        $stmt->bindValue("current_population", $_POST['current_population']);
        $stmt->bindValue("current_population", $_POST['current_population']);        
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']);
        $stmt->bindValue("lastmodifiedat", date("Y-m-d H:i:s"));
        if ($stmt->execute()) {
            return true;
        } else
            return false;
    }

    public function getLessons() {
        $stmt = $this->prepareQuery("SELECT id, name, status FROM lesson_categories ORDER BY id ASC");
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $currentGroup = null;
        $html = "";
        foreach ($info as $row) {
            if (is_null($currentGroup)) {
                $currentGroup = $row['name'];
                if (!empty($_POST['lesson_category']) && $_POST['lesson_category'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['name']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['name']}</option>";
                }
            } else {
                if (!empty($_POST['lesson_category']) && $_POST['lesson_category'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['name']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['name']}</option>";
                }
            }
        }
        if ($html == "") {
            $html = "<option value=\"\">No lesson category entered into the database!</option>";
        }
        echo $html;
        return $currentGroup;
    }

    public function getAllLessonCategories() {
        $sql = "SELECT * FROM lesson_categories ORDER BY id ASC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("id" => $data['id'], "name" => $data['name'], "status" => $data['status'], "createdat" => $data['createdat'], "createdby" => $data['createdby'], "lastmodifiedat" => $data['lastmodifiedat'], "lastmodifiedby" => $data['lastmodifiedby']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function getAllLessons() {
        $sql = "SELECT * FROM lessons ORDER BY id ASC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("id" => $data['id'], "name" => $data['name'], "description" => $data['description'], "category" => $data['category'], "start_time" => $data['start_time'], "end_time" => $data['end_time'], "training_facility" => $data['training_facility'], "price" => $data['price'], "capacity" => $data['capacity'], "current_population" => $data['current_population'], "status" => $data['status'], "createdat" => $data['createdat'], "createdby" => $data['createdby'], "authorizedat" => $data['authorizedat'], "authorizedby" => $data['authorizedby'], "lastmodifiedat" => $data['lastmodifiedat'], "lastmodifiedby" => $data['lastmodifiedby']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function fetchLessonCategoryDetails($code) {
        $sql = "SELECT * FROM lesson_categories WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function fetchLessonDetails($code) {
        $sql = "SELECT * FROM lessons WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

}
