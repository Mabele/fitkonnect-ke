<?php

$configs = parse_ini_file(WPATH . "core/configs.ini");
require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/Trainers.php";
require_once WPATH . "modules/classes/Trainees.php";
require_once WPATH . "modules/classes/Lessons.php";
require_once WPATH . "modules/classes/IXR_Library.inc.php";

class Transactions extends Database {

    public function execute() {
        if ($_POST['action'] == "contact_us") {
            return $this->sendMessage();
        } else if ($_POST['action'] == "send_sms") {
            return $this->sendBulkSMS();
        }
    }

    public function sendReminderSMSCronJob() {
        $sql = "SELECT * FROM booking_transactions_details WHERE approvedat IS NOT NULL AND reminder_sms_sent=:is_reminder_sms_sent AND attendance_status=:attendance_status ORDER BY count ASC";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("is_reminder_sms_sent", "NO");
        $stmt->bindValue("attendance_status", 5005);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($info) == 0) {
            $_SESSION['no_reminder_sms_records'] = true;
            return false;
        } else {
            foreach ($info as $data) {
                $lessons = new Lessons();
                $lesson_details = $lessons->fetchLessonDetails($data['lesson']);
                $lesson_start_hour = substr($lesson_details['start_time'], 0, 2);
                $lesson_start_minute = substr($lesson_details['start_time'], 2, 2);

                if ($data['is_rescheduled'] == "YES") {
                    $lesson_start_time = date("Y-m-d H:i:s", strtotime("+ {$lesson_start_hour} Hours + {$lesson_start_minute} Minutes", strtotime($data['new_start_date'])));
                } else {
                    $lesson_start_time = date("Y-m-d H:i:s", strtotime("+ {$lesson_start_hour} Hours + {$lesson_start_minute} Minutes", strtotime($data['start_date'])));
                }

                $check_time = date("Y-m-d H:i:s", strtotime("- 5Hours", strtotime($lesson_start_time)));

                if ($check_time < date("Y-m-d H:i:s")) {
                    $users_management = new Users_Management();
                    $trainers = new Trainers();
                    $trainees = new Trainees();
                    $transaction_details = $this->fetchBookingTransactionDetails($data["transaction_id"]);
                    if ($data['is_reassigned'] == "YES") {
                        $assigned_trainer = $data['new_trainer'];
                    } else {
                        $assigned_trainer = $data['assigned_trainer'];
                    }

                    $trainer_contact_details = $users_management->fetchIndividualContactDetails($assigned_trainer);
                    $trainer_system_user_details = $users_management->fetchUserDetails($assigned_trainer);
                    $trainer_details = $trainers->fetchTrainerDetails($trainer_system_user_details['reference_id']);
                    $trainee_contact_details = $users_management->fetchIndividualContactDetails($transaction_details["booker_id"]);
                    $trainee_system_user_details = $users_management->fetchUserDetails($transaction_details["booker_id"]);
                    $trainee_details = $trainees->fetchTraineeDetails($trainee_system_user_details['reference_id']);

                    if ($data['is_rescheduled'] == "YES") {
                        $trainer_message = "Hello " . $trainer_details['firstname'] . ", this is a reminder SMS to confirm attendance to your lesson - " . $data['count'] . " scheduled to begin on " . $data['new_start_date'] . " at " . $lesson_details['start_time'] . ". If you have any reservations on the lesson scheduling, please reply: TESTSMS:LESSON ASSIGNMENT,DECLINE" . $data['count'] . " to 21211 to decline the lesson assignment or reply to the same via the sent email notification, else do not reply to this text. Once the lesson is completed reply: TESTSMS:CLOSE" . $data['count'] . " to 21211 to end the lesson assignment ";
                        $trainee_message = "Hello " . $trainee_details['firstname'] . ", this is a reminder SMS to confirm attendance to your lesson - " . $data['count'] . " scheduled to begin on " . $data['new_start_date'] . " at " . $lesson_details['start_time'] . ". If you have any reservations on the lesson scheduling, please reply: TESTSMS:ATTENDANCE CONFIRMATION,CANCEL" . $data['count'] . " to 21211 to cancel the lesson or reply to the same via the sent email notification, else do not reply to this text.";

                        $trainer_email = $trainer_contact_details['email'];
                        $trainer_name = $trainer_details['firstname'] . " " . $trainer_details['lastname'];
                        $trainer_email_subject = "Lesson Assignment Reminder";
                        $trainer_email_message = "<html><body>"
                                . "<p><b>Hello " . $trainer_details['firstname'] . ",</b><br/>"
                                . "This is a reminder message to confirm attendance to your lesson - " . $data['count'] . " scheduled to begin on " . $data['new_start_date'] . " at " . $lesson_details['start_time'] . ". "
                                . "If you have any reservations on the lesson scheduling, please <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=lesson_assignment&response=decline{$data['count']}'> Click here </a> to decline the lesson assignment. "
                                . "Once the lesson is completed please <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=close{$data['count']}'> Click here </a> to end the lesson assignment. <br/>"
                                . "For any enquiries, kindly contact us on:   <br/>"
                                . "<ul>"
                                . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                                . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                                . "</ul>"
                                . "</body></html>";

                        $trainee_email = $trainee_contact_details['email'];
                        $trainee_name = $trainee_details['firstname'] . " " . $trainee_details['lastname'];
                        $trainee_email_subject = "Lesson Assignment Reminder";
                        $trainee_email_message = "<html><body>"
                                . "<p><b>Hello " . $trainee_details['firstname'] . ",</b><br/>"
                                . "This is a reminder message to confirm attendance to your lesson - " . $data['count'] . " scheduled to begin on " . $data['new_start_date'] . " at " . $lesson_details['start_time'] . ". "
                                . "If you have any reservations on the lesson scheduling, please <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=attendance_confirmation&response=cancel{$data['count']}'> Click here </a> to cancel the lesson. <br/>"
                                . "For any enquiries, kindly contact us on:   <br/>"
                                . "<ul>"
                                . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                                . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                                . "</ul>"
                                . "</body></html>";
                    } else {
                        $trainer_message = "Hello " . $trainer_details['firstname'] . ", this is a reminder SMS to confirm attendance to your lesson - " . $data['count'] . " scheduled to begin on " . $data['start_date'] . " at " . $lesson_details['start_time'] . ". If you have any reservations on the lesson scheduling, please reply: TESTSMS:LESSON ASSIGNMENT,DECLINE" . $data['count'] . " to 21211 to decline the lesson assignment or reply to the same via the sent email notification, else do not reply to this text. Once the lesson is completed reply: TESTSMS:CLOSE" . $data['count'] . " to 21211 to end the lesson assignment ";
                        $trainee_message = "Hello " . $trainee_details['firstname'] . ", this is a reminder SMS to confirm attendance to your lesson - " . $data['count'] . " scheduled to begin on " . $data['start_date'] . " at " . $lesson_details['start_time'] . ". If you have any reservations on the lesson scheduling, please reply: TESTSMS:ATTENDANCE CONFIRMATION,CANCEL" . $data['count'] . " to 21211 to cancel the lesson or TESTSMS:ATTENDANCE CONFIRMATION,RESCHEDULE" . $data['count'] . ",YYYY-MM-DD,BRIEF REASON FOR RESCHEDULING to reschedule the lesson or reply to the same via the sent email notification, else do not reply to this text.";

                        $trainer_email = $trainer_contact_details['email'];
                        $trainer_name = $trainer_details['firstname'] . " " . $trainer_details['lastname'];
                        $trainer_email_subject = "Lesson Assignment Reminder";
                        $trainer_email_message = "<html><body>"
                                . "<p><b>Hello " . $trainer_details['firstname'] . ",</b><br/>"
                                . "This is a reminder message to confirm attendance to your lesson - " . $data['count'] . " scheduled to begin on " . $data['new_start_date'] . " at " . $lesson_details['start_time'] . ". "
                                . "If you have any reservations on the lesson scheduling, please <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=lesson_assignment&response=decline{$data['count']}'> Click here </a> to decline the lesson assignment. "
                                . "Once the lesson is completed please <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=close{$data['count']}'> Click here </a> to end the lesson assignment. <br/>"
                                . "For any enquiries, kindly contact us on:   <br/>"
                                . "<ul>"
                                . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                                . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                                . "</ul>"
                                . "</body></html>";

                        $trainee_email = $trainee_contact_details['email'];
                        $trainee_name = $trainee_details['firstname'] . " " . $trainee_details['lastname'];
                        $trainee_email_subject = "Lesson Assignment Reminder";
                        $trainee_email_message = "<html><body>"
                                . "<p><b>Hello " . $trainee_details['firstname'] . ",</b><br/>"
                                . "This is a reminder message to confirm attendance to your lesson - " . $data['count'] . " scheduled to begin on " . $data['new_start_date'] . " at " . $lesson_details['start_time'] . ". "
                                . "If you have any reservations on the lesson scheduling, please <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=attendance_confirmation&response=cancel{$data['count']}'> Click here </a> to cancel the lesson or <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=attendance_confirmation&response=reschedule{$data['count']}'> Click here </a> to reschedule the lesson. <br/>"
                                . "For any enquiries, kindly contact us on:   <br/>"
                                . "<ul>"
                                . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                                . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                                . "</ul>"
                                . "</body></html>";
                    }

                    $users_management = new Users_Management();
                    $send_trainer_email = $users_management->sendEmail($trainer_email, $trainer_name, $trainer_email_subject, $trainer_email_message);
                    $send_trainee_email = $users_management->sendEmail($trainee_email, $trainee_name, $trainee_email_subject, $trainee_email_message);

                    $send_trainer_sms = $this->sendSingleSMS($trainer_contact_details['phone_number1'], $trainer_message);
                    $send_trainee_sms = $this->sendSingleSMS($trainee_contact_details['phone_number1'], $trainee_message);

                    $sms_details[] = $send_trainer_sms;
                    foreach ($sms_details as $key => $value) {
                        $inner_array[$key] = json_decode($value, true); // this will give key val pair array
                        foreach ((array) $inner_array[$key] as $key2 => $value2) {
                            if (isset($value2['messageid'])) {
                                $sql_sms_sent_update = "UPDATE booking_transactions_details SET reminder_sms_sent=:reminder_sms_sent WHERE count=:code";
                                $stmt_sms_sent_update = $this->prepareQuery($sql_sms_sent_update);
                                $stmt_sms_sent_update->bindValue("code", $data['count']);
                                $stmt_sms_sent_update->bindValue("reminder_sms_sent", "YES");
                                $stmt_sms_sent_update->execute();
                            } else {
                                $_SESSION['sms_sent'] = false;
                            }
                        }
                    }
                    return true;

//                    argDump($data);
//                    exit();
                }
            }
        }
    }

    public function sendCheckSMSCreditBalanceRequest($data_string) {
        $curl_session = curl_init();
        curl_setopt($curl_session, CURLOPT_URL, 'http://messaging.advantasms.com/bulksms/smscredit.jsp?');
        curl_setopt($curl_session, CURLOPT_POST, true);
        curl_setopt($curl_session, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl_session);
        curl_close($curl_session);
        return $response;
    }

    public function sendBulkSMSRequest($data_string) {

        argDump($data_string);
        exit();

        $curl_session = curl_init();
        curl_setopt($curl_session, CURLOPT_URL, 'http://bulksms.reflexconcepts.co.ke/sendsms.jsp?');
        curl_setopt($curl_session, CURLOPT_POST, true);
        curl_setopt($curl_session, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($curl_session, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl_session);
        curl_close($curl_session);
        return $response;
    }

    public function sendSingleSMSRequest($data_string) {
        $curl_session = curl_init();
        curl_setopt($curl_session, CURLOPT_URL, 'http://messaging.advantasms.com/bulksms/sendsms.jsp?');
        curl_setopt($curl_session, CURLOPT_POST, true);
        curl_setopt($curl_session, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl_session);
        curl_close($curl_session);
        return $response;
    }

    public function getSMSDeliveryReportRequest($data_string) {
        $curl_session = curl_init();
        curl_setopt($curl_session, CURLOPT_URL, 'http://messaging.advantasms.com/bulksms/getDLR.jsp?');
        curl_setopt($curl_session, CURLOPT_POST, true);
        curl_setopt($curl_session, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl_session);
        curl_close($curl_session);
        return $response;
    }

    public function getSMSDeliveryReport() {
        $data['userid'] = "Reflexconcepts"; // $configs["sms_username"];
        $data['password'] = "r12345"; // $configs["sms_password"];

        $data['messageid'] = ""; // optional: whenever you send a message you will get this message id as a response. eg. 671295686, 671295687
        $data['externalid'] = ""; // optional: set your message unique id, if you have
        $data['drquantity'] = ""; // optional: how many dr records you want
        $data['fromdate'] = ""; // optional
        $data['todate'] = ""; // optional
        $data['redownload'] = ""; // optional: If you want to a DR in a second time you must set redownload paramater “yes” other wise you will not get DR, if you first time download you do not set this paramater.
        $data['responcetype'] = ""; //In which format you want to get response (ex. Xml, csv, tsv)

        $data_string = http_build_query($data);
        $process_request = $this->getSMSDeliveryReportRequest($data_string);
        $decoded_response = simplexml_load_string($process_request) or die("Error: Cannot create object");
        return $decoded_response;
    }

    public function checkSMSCreditBalance() {
        $data['user'] = "Reflexconcepts"; // $configs["sms_username"];
        $data['password'] = "r12345"; // $configs["sms_password"];
        $data_string = http_build_query($data);
        $process_request = $this->sendCheckSMSCreditBalanceRequest($data_string);
        $decoded_response = simplexml_load_string($process_request) or die("Error: Cannot create object");
        return $decoded_response;
    }

    public function sendSingleSMS($phone_number, $message) {
        $data['user'] = "Reflexconcepts";
        $data['password'] = "r12345";
        $data['mobiles'] = $phone_number;
        $data['sms'] = $message;
        $data_string = http_build_query($data);
        $process_request = $this->sendSingleSMSRequest($data_string);
        $decoded_response = simplexml_load_string($process_request) or die("Error: Cannot create object");
        $this->logSentSMSDetails("OUTBOX", json_encode($decoded_response));
        return json_encode($decoded_response);
    }

    public function sendShortCodeSMS($phone_number, $message) {
        $client = new IXR_Client('http://prs.advantasms.com/sdp_apps/services/thirdparty/sdp_server.php');

//        $data_string = "<methodCall>";
//        $data_string .= "<methodName>ADVANTA.sendSms</methodName>";
//        $data_string .= "<params>";
//        $data_string .= "<param>";
//        $data_string .= "<value>";
//        $data_string .= "<array>";
//        $data_string .= "<data>";
//        $data_string .= "<value>";
//        $data_string .= "<struct>";
//        $data_string .= "<member>";
//        $data_string .= "<name>Reflexconcepts</name>";
//        $data_string .= "<value>";
//        $data_string .= "<string>sdp</string>";
//        $data_string .= "</value>";
//        $data_string .= "</member>";
//        $data_string .= "<member>";
//        $data_string .= "<name>r12345</name>";
//        $data_string .= "<value>";
//        $data_string .= "<string>sdpsms</string>";
//        $data_string .= "</value>";
//        $data_string .= "</member>";
//        $data_string .= "</struct>";
//        $data_string .= "</value>";
//        $data_string .= "<value>";
//        $data_string .= "<struct>";
//        $data_string .= "<member>";
//        $data_string .= "<name>CHANNELID</name>";
//        $data_string .= "<value>";
//        $data_string .= "<string>0</string>";
//        $data_string .= "</value>";
//        $data_string .= "</member>";
//        $data_string .= "<member>";
//        $data_string .= "<name>MSISDN</name>";
//        $data_string .= "<value>";
//        $data_string .= "<string>" . $phone_number . "</string>";
//        $data_string .= "</value>";
//        $data_string .= "</member>";
//        $data_string .= "<member>";
//        $data_string .= "<name>SOURCEADDR</name>";
//        $data_string .= "<value>";
//        $data_string .= "<string>" . $source_address . "</string>";
//        $data_string .= "</value>";
//        $data_string .= "</member>";
//        $data_string .= "<member>";
//        $data_string .= "<name>MESSAGE</name>";
//        $data_string .= "<value>";
//        $data_string .= "<string>" . $message . "</string>";
//        $data_string .= "</value>";
//        $data_string .= "</member>";
//        $data_string .= "</struct>";
//        $data_string .= "</value>";
//        $data_string .= "</data>";
//        $data_string .= "</array>";
//        $data_string .= "</value>";
//        $data_string .= "</param>";
//        $data_string .= "</params>";
//        $data_string .= "</methodCall>";
//send sms
        $response = $client->query('ADVANTA.sendSms', array(
            array(
                'USERNAME' => 'testadmin',
                'PASSWORD' => 'testadmin'
            ),
            array(
                'CHANNELID' => '60',
                'MSISDN' => $phone_number,
                'SOURCEADDR' => '21211',
                'MESSAGE' => $message
            )
        ));
        if (!$response) {
            die('An error occurred - ' . $client->getErrorCode() . ":" . $client->getErrorMessage());
        }
        print_r($client->getResponse());
    }

    public function sendBulkSMS() {
        $user = "Reflexconcepts";
        $password = "r12345";

        if ($_POST['recipient_group'] == "ALL") {
            $users_management = new Users_Management();
            $contact_details[] = $users_management->getAllContacts();

            $data_string = "<?xml version='1.0'?>";
            $data_string .= "<smslist>";
            foreach ($contact_details as $key => $value) {
                $inner_array[$key] = json_decode($value, true);
                foreach ((array) $inner_array[$key] as $key2 => $value2) {
                    $data_string .= "<sms>";
                    $data_string .= "<user>" . $user . "</user>";
                    $data_string .= "<password>" . $password . "</password>";
                    $data_string .= "<message>" . $_POST['message'] . "</message>";
                    $data_string .= "<mobiles>" . $value2['phone_number1'] . "</mobiles>";
                    $data_string .= "<clientsmsid>1</clientsmsid>";
                    $data_string .= "<unicode>0</unicode>";
                    $data_string .= "</sms>";
                }
            }
            $data_string .= "</smslist>";
        } else if ($_POST['recipient_group'] == "INDIVIDUAL") {
            $contact_details[] = explode(",", $_POST['recipient_contacts']);

            $data_string = "<?xml version='1.0'?>";
            $data_string .= "<smslist>";
            foreach ($contact_details as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    $data_string .= "<sms>";
                    $data_string .= "<user>" . $user . "</user>";
                    $data_string .= "<password>" . $password . "</password>";
                    $data_string .= "<message>" . $_POST['message'] . "</message>";
                    $data_string .= "<mobiles>" . $value2 . "</mobiles>";
                    $data_string .= "<clientsmsid>1</clientsmsid>";
                    $data_string .= "<unicode>0</unicode>";
                    $data_string .= "</sms>";
                }
            }
            $data_string .= "</smslist>";
        } else {
            // get all contacts where user_id (in contacts table) is an id (in system_users table) that has a refence type equivalent to the passed $_POST['recipient_group'] value
            $users_management = new Users_Management();
            $contact_details[] = $users_management->getGroupContacts($_POST['recipient_group']);

            $data_string = "<?xml version='1.0'?>";
            $data_string .= "<smslist>";
            foreach ($contact_details as $key => $value) {
                $inner_array[$key] = json_decode($value, true);
                foreach ((array) $inner_array[$key] as $key2 => $value2) {
                    $data_string .= "<sms>";
                    $data_string .= "<user>" . $user . "</user>";
                    $data_string .= "<password>" . $password . "</password>";
                    $data_string .= "<message>" . $_POST['message'] . "</message>";
                    $data_string .= "<mobiles>" . $value2['phone_number1'] . "</mobiles>";
                    $data_string .= "<clientsmsid>1</clientsmsid>";
                    $data_string .= "<unicode>0</unicode>";
                    $data_string .= "</sms>";
                }
            }
            $data_string .= "</smslist>";
        }
        $process_request = $this->sendBulkSMSRequest($data_string);
        $decoded_response = simplexml_load_string($process_request) or die("Error: Cannot create object");
        return $process_request;
    }

    public function getTransactionId($payment_option, $transactedby, $total_amount) {
        $transaction_id = md5($payment_option . $transactedby . $total_amount . time());
        return strtoupper($transaction_id);
    }

    public function logSentSMSDetails($message_type, $sms_delivery_response) {
        $sms_details[] = $sms_delivery_response;
        foreach ($sms_details as $key => $value) {
            $inner_array[$key] = json_decode($value, true); // this will give key val pair array
            foreach ((array) $inner_array[$key] as $key2 => $value2) {
                if ($message_type == "OUTBOX") {
                    if (isset($value2['messageid'])) {
                        $sql = "INSERT INTO sms_log (phone_number, smsclientid, messageid, type)"
                                . " VALUES (:phone_number, :smsclientid, :messageid, :message_type)";
                        $stmt = $this->prepareQuery($sql);
                        $stmt->bindValue("phone_number", $value2['mobile-no']);
                        $stmt->bindValue("smsclientid", $value2['smsclientid']);
                        $stmt->bindValue("messageid", $value2['messageid']);
                        $stmt->bindValue("message_type", $message_type);
                        $stmt->execute();
                    } else {
                        $sql = "INSERT INTO sms_log (phone_number, smsclientid, error_code, error_description, error_action, type)"
                                . " VALUES (:phone_number, :smsclientid, :error_code, :error_description, :error_action, :message_type)";
                        $stmt = $this->prepareQuery($sql);
                        $stmt->bindValue("phone_number", $value2['mobile-no']);
                        $stmt->bindValue("smsclientid", $value2['smsclientid']);
                        $stmt->bindValue("error_code", $value2['error-code']);
                        $stmt->bindValue("error_description", $value2['error-description']);
                        $stmt->bindValue("error_action", $value2['error-action']);
                        $stmt->bindValue("message_type", $message_type);
                        $stmt->execute();
                    }
                }
            }
        }
        return true;
    }

    public function addTransaction() {
        $sql = "INSERT INTO booking_transactions (transaction_id, amount, booker_id, payment_option)"
                . " VALUES (:transaction_id, :amount, :booker_id, :payment_option)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("transaction_id", $_SESSION["transaction_id"]);
        $stmt->bindValue("amount", $_SESSION["cart_total_cost"]);
        $stmt->bindValue("booker_id", $_SESSION['user_id']);
        $stmt->bindValue("payment_option", strtoupper($_SESSION['payment_option']));
        if ($stmt->execute()) {
            if (App::isLoggedIn()) {
                if ($_SESSION['logged_in_user_type_details']['name'] == "STAFF" OR $_SESSION['logged_in_user_type_details']['name'] == "TRAINEE" OR $_SESSION['logged_in_user_type_details']['name'] == "TRAINER") {
                    $phone_number = $_SESSION['contacts']['phone_number1'];
                    $email_address = $_SESSION['contacts']['email'];
                } else if ($_SESSION['logged_in_user_type_details']['name'] == "TRAINING FACILITY ADMINISTRATOR") {
                    $phone_number = $_SESSION['logged_in_user_details']['phone_number'];
                    $email_address = $_SESSION['logged_in_user_details']['email'];
                } else if ($_SESSION['logged_in_user_type_details']['name'] == "SYSTEM ADMINISTRATOR") {
                    $phone_number = $_SESSION['logged_in_user_details']['phone_number'];
                    $email_address = $_SESSION['logged_in_user_details']['email'];
                }
            } else if (!empty($_POST)) {
                $phone_number = $_POST['phone_number'];
                $email_address = $_POST['email'];
            }
            $acknowledgement_message = "Hello " . $_SESSION['logged_in_user_details']['firstname'] . ", thank you for engaging us for your fitness sessions. Your request is being processed. We shall engage you shortly.";

            $acknowledgement_email = $email_address;
            $acknowledgement_name = $_SESSION['logged_in_user_details']['firstname'] . " " . $_SESSION['logged_in_user_details']['lastname'];
            $acknowledgement_email_subject = "Transaction Acknowledgement";
            $acknowledgement_email_message = "<html><body>"
                    . "<p><b>Hello " . $_SESSION['logged_in_user_details']['firstname'] . ",</b><br/>"
                    . "Thank you for engaging us for your fitness sessions. Your request is being processed. We shall engage you shortly."
                    . "For any enquiries, kindly contact us on:   <br/>"
                    . "<ul>"
                    . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                    . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                    . "</ul>"
                    . "</body></html>";

            $users_management = new Users_Management();
            $send_acknowledgement_email = $users_management->sendEmail($acknowledgement_email, $acknowledgement_name, $acknowledgement_email_subject, $acknowledgement_email_message);

            $send_single_sms_details = $this->sendSingleSMS($phone_number, $acknowledgement_message);
            return true;
        } else
            return false;
    }

    public function addKopokopoPaymentTransaction($service_name, $business_number, $transaction_reference, $internal_transaction_id, $transaction_timestamp, $transaction_type, $account_number, $sender_phone, $firstname, $middlename, $lastname, $amount, $currency, $signature) {
        $sql = "INSERT INTO payment_transactions (source, transaction_type, transaction_reference, transaction_time, transaction_amount, business_short_code, third_party_trans_id, phone_number, firstname, middlename, lastname)"
                . " VALUES (:source, :transaction_type, :transaction_reference, :transaction_time, :transaction_amount, :business_short_code, :third_party_trans_id, :phone_number, :firstname, :middlename, :lastname)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("source", $service_name);
        $stmt->bindValue("transaction_type", $transaction_type);
        $stmt->bindValue("transaction_reference", $transaction_reference);
        $stmt->bindValue("transaction_time", $transaction_timestamp);
        $stmt->bindValue("transaction_amount", $amount);
        $stmt->bindValue("business_short_code", $business_number);
        $stmt->bindValue("third_party_trans_id", $internal_transaction_id);
        $stmt->bindValue("phone_number", $sender_phone);
        $stmt->bindValue("firstname", $firstname);
        $stmt->bindValue("middlename", $middlename);
        $stmt->bindValue("lastname", $lastname);
        if ($stmt->execute()) {
            return true;
        } else
            return false;
    }

    public function addMpesaPaymentTransaction2($payment_details) {
        $sql = "INSERT INTO mpesa_response (response)"
                . " VALUES (:payment_details)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("payment_details", $payment_details);
        if ($stmt->execute()) {
            return true;
        } else
            return false;
    }

    public function addMpesaPaymentTransaction($payment_details) {
        $sql = "INSERT INTO payment_transactions (transaction_id, source, transaction_type, transaction_reference, transaction_time, transaction_amount, business_short_code, bill_ref_number, invoice_number, third_party_trans_id, phone_number, firstname, middlename, lastname, account_balance)"
                . " VALUES (:transaction_id, :source, :transaction_type, :transaction_reference, :transaction_time, :transaction_amount, :business_short_code, :bill_ref_number, :invoice_number, :third_party_trans_id, :phone_number, :firstname, :middlename, :lastname, :account_balance)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("transaction_id", $_SESSION["transaction_id"]);
        $stmt->bindValue("source", 'MPESA');
        $stmt->bindValue("transaction_type", $payment_details['TransactionType']);
        $stmt->bindValue("transaction_reference", $payment_details['TransID']);
        $stmt->bindValue("transaction_time", $payment_details['TransTime']);
        $stmt->bindValue("transaction_amount", $payment_details['TransAmount']);
        $stmt->bindValue("business_short_code", $payment_details['BusinessShortCode']);
        $stmt->bindValue("bill_ref_number", $payment_details['BillRefNumber']);
        $stmt->bindValue("invoice_number", $payment_details['InvoiceNumber']);
        $stmt->bindValue("third_party_trans_id", "TEST");  // $payment_details['TransactionType']
        $stmt->bindValue("phone_number", $payment_details['MSISDN']);
        $stmt->bindValue("firstname", $payment_details['FirstName']);
        $stmt->bindValue("middlename", $payment_details['MiddleName']);
        $stmt->bindValue("lastname", $payment_details['LastName']);
        $stmt->bindValue("account_balance", $payment_details['OrgAccountBalance']);
        if ($stmt->execute()) {
            $this->updateTransactionPaymentStatus($transaction_id);
            return true;
        } else
            return false;
    }

    public function updateTransactionPaymentStatus($transaction_id) {
        $sql = "UPDATE booking_transactions_details SET payment_status=:payment_status WHERE transaction_id=:transaction_id";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("transaction_id", $transaction_id);
        $stmt->bindValue("payment_status", 2003);
        if ($stmt->execute()) {
            return true;
        } else
            return false;
    }

    public function addTransactionDetails() {
        $sql = "INSERT INTO booking_transactions_details (transaction_id, lesson, number_of_classes, unit_price, start_date, training_facility)"
                . " VALUES (:transaction_id, :lesson, :number_of_classes, :unit_price, :start_date, :training_facility)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("transaction_id", $_SESSION["transaction_id"]);
        $stmt->bindValue("lesson", $_SESSION['lesson_details']["id"]);
        $stmt->bindValue("number_of_classes", $_SESSION["number_of_classes"]);
        $stmt->bindValue("unit_price", $_SESSION['lesson_details']["price"]);
        $stmt->bindValue("start_date", $_SESSION["start_date"]);
        $stmt->bindValue("training_facility", $_SESSION['lesson_details']["training_facility"]);
        $stmt->execute();
        return true;
    }

    public function getAllLessonBookingTransactions() {
        $sql = "SELECT * FROM booking_transactions ORDER BY count DESC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("count" => $data['count'], "transaction_id" => $data['transaction_id'], "transaction_type" => $data['transaction_type'], "amount" => $data['amount'], "booker_id" => $data['booker_id'], "payment_option" => $data['payment_option'], "createdat" => $data['createdat'], "payment_status" => $data['payment_status'], "status" => $data['status']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function getAllPaymentTransactions() {
        $sql = "SELECT * FROM payment_transactions ORDER BY count DESC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("count" => $data['count'], "transaction_id" => $data['transaction_id'], "source" => $data['source'], "transaction_type" => $data['transaction_type'], "transaction_reference" => $data['transaction_reference'], "transaction_time" => $data['transaction_time'], "transaction_amount" => $data['transaction_amount'], "business_short_code" => $data['business_short_code'], "bill_ref_number" => $data['bill_ref_number'], "invoice_number" => $data['invoice_number'], "third_party_trans_id" => $data['third_party_trans_id'], "phone_number" => $data['phone_number'], "firstname" => $data['firstname'], "middlename" => $data['middlename'], "lastname" => $data['lastname'], "status" => $data['status'], "createdat" => $data['createdat'], "account_balance" => $data['account_balance']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function getAllLessonBookingTransactionsDetails() {
        $sql = "SELECT * FROM booking_transactions_details ORDER BY count DESC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("count" => $data['count'], "transaction_id" => $data['transaction_id'], "lesson" => $data['lesson'], "number_of_classes" => $data['number_of_classes'], "unit_price" => $data['unit_price'], "start_date" => $data['start_date'], "is_rescheduled" => $data['is_rescheduled'], "reschedule_reason" => $data['reschedule_reason'], "new_start_date" => $data['new_start_date'], "training_facility" => $data['training_facility'], "payment_status" => $data['payment_status'], "approvedat" => $data['approvedat'], "approvedby" => $data['approvedby'], "approval_comment" => $data['approval_comment'], "assigned_trainer" => $data['assigned_trainer'], "reminder_sms_sent" => $data['reminder_sms_sent'], "trainer_approvedat" => $data['trainer_approvedat'], "trainer_approval_comment" => $data['trainer_approval_comment'], "is_reassigned" => $data['is_reassigned'], "re_assignedat" => $data['re_assignedat'], "reassign_reason" => $data['reassign_reason'], "new_trainer" => $data['new_trainer'], "new_reminder_sms_sent" => $data['new_reminder_sms_sent'], "new_trainer_approvedat" => $data['new_trainer_approvedat'], "new_trainer_approval_comment" => $data['new_trainer_approval_comment'], "attendance_status" => $data['attendance_status'], "trainer_rating" => $data['trainer_rating'], "facility_rating" => $data['facility_rating'], "status" => $data['status']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function getLessonBookingTransactionsDetails($transaction_id) {
        $sql = "SELECT * FROM booking_transactions_details WHERE transaction_id=:transaction_id ORDER BY count DESC";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("transaction_id", $transaction_id);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("count" => $data['count'], "transaction_id" => $data['transaction_id'], "lesson" => $data['lesson'], "number_of_classes" => $data['number_of_classes'], "unit_price" => $data['unit_price'], "start_date" => $data['start_date'], "training_facility" => $data['training_facility'], "payment_status" => $data['payment_status'], "approvedat" => $data['approvedat'], "approvedby" => $data['approvedby'], "approval_comment" => $data['approval_comment'], "assigned_trainer" => $data['assigned_trainer'], "reminder_sms_sent" => $data['reminder_sms_sent'], "trainer_approvedat" => $data['trainer_approvedat'], "trainer_approval_comment" => $data['trainer_approval_comment'], "attendance_status" => $data['attendance_status'], "trainer_rating" => $data['trainer_rating'], "facility_rating" => $data['facility_rating'], "status" => $data['status']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function getTheLessonsIHaveBeenAssigned() {
        $sql = "SELECT * FROM booking_transactions_details WHERE assigned_trainer=:assigned_trainer ORDER BY count DESC";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("assigned_trainer", $_SESSION['user_id']);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("count" => $data['count'], "transaction_id" => $data['transaction_id'], "lesson" => $data['lesson'], "number_of_classes" => $data['number_of_classes'], "unit_price" => $data['unit_price'], "start_date" => $data['start_date'], "training_facility" => $data['training_facility'], "payment_status" => $data['payment_status'], "approvedat" => $data['approvedat'], "approvedby" => $data['approvedby'], "approval_comment" => $data['approval_comment'], "assigned_trainer" => $data['assigned_trainer'], "reminder_sms_sent" => $data['reminder_sms_sent'], "trainer_approvedat" => $data['trainer_approvedat'], "trainer_approval_comment" => $data['trainer_approval_comment'], "attendance_status" => $data['attendance_status'], "trainer_rating" => $data['trainer_rating'], "facility_rating" => $data['facility_rating'], "status" => $data['status']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function getTheLessonsIHaveBooked() {
        $sql = "SELECT * FROM booking_transactions_details a INNER JOIN booking_transactions b ON a.transaction_id = b.transaction_id WHERE b.booker_id=:booker_id ORDER BY a.count DESC";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("booker_id", $_SESSION['user_id']);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("count" => $data['count'], "transaction_id" => $data['transaction_id'], "lesson" => $data['lesson'], "number_of_classes" => $data['number_of_classes'], "unit_price" => $data['unit_price'], "start_date" => $data['start_date'], "training_facility" => $data['training_facility'], "payment_status" => $data['payment_status'], "approvedat" => $data['approvedat'], "approvedby" => $data['approvedby'], "approval_comment" => $data['approval_comment'], "assigned_trainer" => $data['assigned_trainer'], "reminder_sms_sent" => $data['reminder_sms_sent'], "trainer_approvedat" => $data['trainer_approvedat'], "trainer_approval_comment" => $data['trainer_approval_comment'], "attendance_status" => $data['attendance_status'], "trainer_rating" => $data['trainer_rating'], "facility_rating" => $data['facility_rating'], "status" => $data['status']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function approveLessonBookingTransaction($code, $transaction_id, $approval_comment, $assigned_trainer) {
        $sql_booking_transactions_details = "UPDATE booking_transactions_details SET approvedat=:approvedat, approvedby=:approvedby, approval_comment=:approval_comment, assigned_trainer=:assigned_trainer WHERE count=:code";
        $stmt_booking_transactions_details = $this->prepareQuery($sql_booking_transactions_details);
        $stmt_booking_transactions_details->bindValue("code", $code);
        $stmt_booking_transactions_details->bindValue("approvedat", date("Y-m-d H:i:s"));
        $stmt_booking_transactions_details->bindValue("approvedby", $_SESSION['user_id']);
        $stmt_booking_transactions_details->bindValue("approval_comment", strtoupper($approval_comment));
        $stmt_booking_transactions_details->bindValue("assigned_trainer", $assigned_trainer);
        if ($stmt_booking_transactions_details->execute()) {

// Check the status of all items in the booking_transactions_details with the associated transaction_id then update the status in the booking_transactions table accordingly

            $users_management = new Users_Management();
            $trainers = new Trainers();
            $trainees = new Trainees();
            $trainer_contact_details = $users_management->fetchIndividualContactDetails($assigned_trainer);
            $trainer_system_user_details = $users_management->fetchUserDetails($assigned_trainer);
            $trainer_details = $trainers->fetchTrainerDetails($trainer_system_user_details['reference_id']);
            $transaction_details = $this->fetchBookingTransactionDetails($transaction_id);
            $trainee_contact_details = $users_management->fetchIndividualContactDetails($transaction_details['booker_id']);
            $trainee_system_user_details = $users_management->fetchUserDetails($transaction_details['booker_id']);
            $trainee_details = $trainees->fetchTraineeDetails($trainee_system_user_details['reference_id']);

            $trainer_message = "Hello " . $trainer_details['firstname'] . ", you have been assigned a trainee - " . $trainee_details['firstname'] . " " . $trainee_details['lastname'] . ". You can reach your trainee on " . $trainee_contact_details['phone_number1'] . ". If you have any reservations on the assignment, please reply: TESTSMS:LESSON ASSIGNMENT,DECLINE" . $code . " to 21211 to decline the assignment, else do not reply to this text. You can approve the same from your email or by logging into the FitnessIQ portal.";
            $trainee_message = "Hello " . $trainee_details['firstname'] . ", you have been assigned a trainer - " . $trainer_details['firstname'] . " " . $trainer_details['lastname'] . ". You can reach your trainer on - Telephone (" . $trainer_contact_details['phone_number1'] . "), Email(" . $trainee_contact_details['email'] . "). If you have any reservations on the allocated trainer please reply: TESTSMS:TRAINER ACCEPTANCE,DECLINE" . $code . " to 21211, else do not reply to this text. Thank you for engaging us for your fitness sessions.";

            $trainer_email = $trainer_contact_details['email'];
            $trainer_name = $trainer_details['firstname'] . " " . $trainer_details['lastname'];
            $trainer_email_subject = "Lesson Assignment";
            $trainer_email_message = "<html><body>"
                    . "<p><b>Hello " . $trainer_details['firstname'] . ",</b><br/>"
                    . "You have been assigned a trainee - " . $trainee_details['firstname'] . " " . $trainee_details['lastname'] . ". "
                    . "You can reach your trainee on " . $trainee_contact_details['phone_number1'] . ". "
                    . "If you have any reservations on the assignment, please <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=lesson_assignment&response=decline{$code}'> Click here </a> to decline the lesson assignment. <br />"
                    . "For any enquiries, kindly contact us on:   <br/>"
                    . "<ul>"
                    . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                    . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                    . "</ul>"
                    . "</body></html>";

            $trainee_email = $trainee_contact_details['email'];
            $trainee_name = $trainee_details['firstname'] . " " . $trainee_details['lastname'];
            $trainee_email_subject = "Trainer Assignment";
            $trainee_email_message = "<html><body>"
                    . "<p><b>Hello " . $trainee_details['firstname'] . ",</b><br/>"
                    . "You have been assigned a trainer - " . $trainer_details['firstname'] . " " . $trainer_details['lastname'] . ". "
                    . "You can reach your trainer on - Telephone (" . $trainer_contact_details['phone_number1'] . "), Email(" . $trainee_contact_details['email'] . "). "
                    . "If you have any reservations on the allocated trainer please <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=trainer_acceptance&response=decline{$code}'> Click here </a> to decline the assignment. <br />"
                    . "For any enquiries, kindly contact us on:   <br/>"
                    . "<ul>"
                    . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                    . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                    . "</ul>"
                    . "</body></html>";

            $users_management = new Users_Management();
            $send_trainer_email = $users_management->sendEmail($trainer_email, $trainer_name, $trainer_email_subject, $trainer_email_message);
            $send_trainee_email = $users_management->sendEmail($trainee_email, $trainee_name, $trainee_email_subject, $trainee_email_message);

            $send_trainer_sms = $this->sendSingleSMS($trainer_contact_details['phone_number1'], $trainer_message);
            $send_trainee_sms = $this->sendSingleSMS($trainee_contact_details['phone_number1'], $trainee_message);

            return true;
        } else
            return false;
    }

    public function rejectLessonBookingTransaction($code, $approval_comment) {
        $sql_booking_transactions_details = "UPDATE booking_transactions_details SET status=:status, approvedat=:approvedat, approvedby=:approvedby, approval_comment=:approval_comment WHERE count=:code";
        $stmt_booking_transactions_details = $this->prepareQuery($sql_booking_transactions_details);
        $stmt_booking_transactions_details->bindValue("code", $code);
        $stmt_booking_transactions_details->bindValue("status", 1007);
        $stmt_booking_transactions_details->bindValue("approvedat", date("Y-m-d H:i:s"));
        $stmt_booking_transactions_details->bindValue("approvedby", $_SESSION['user_id']);
        $stmt_booking_transactions_details->bindValue("approval_comment", strtoupper($approval_comment));
        if ($stmt_booking_transactions_details->execute()) {
            return true;
        } else
            return false;
    }


//    public function deleteLessonBookingTransaction($code) {
//        $sql_booking_transactions_details = "UPDATE booking_transactions_details SET status=:status, approvedat=:approvedat, approvedby=:approvedby WHERE count=:code";
//        $stmt_booking_transactions_details = $this->prepareQuery($sql_booking_transactions_details);
//        $stmt_booking_transactions_details->bindValue("code", $code);
//        $stmt_booking_transactions_details->bindValue("status", 1009);
//        $stmt_booking_transactions_details->bindValue("approvedat", date("Y-m-d H:i:s"));
//        $stmt_booking_transactions_details->bindValue("approvedby", $_SESSION['user_id']);
//        if ($stmt_booking_transactions_details->execute()) {
//            return true;
//        } else
//            return false;
//    }

    public function activateRecord($item, $code) {
        if ($item == 'contact') {
            $activate_sql = "UPDATE contacts SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'county') {
            $activate_sql = "UPDATE counties SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'lesson') {
            $activate_sql = "UPDATE lessons SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'lesson_category') {
            $activate_sql = "UPDATE lesson_categories SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
//        } else if ($item == 'role_privilege') {
//            $activate_sql = "UPDATE role_privileges SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'staff') {
            $activate_sql = "UPDATE staff SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'staff_position') {
            $activate_sql = "UPDATE staff_positions SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'system_administrator') {
            $activate_sql = "UPDATE system_administrators SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'system_component') {
            $activate_sql = "UPDATE system_components SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'system_privilege') {
            $activate_sql = "UPDATE system_privileges SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'system_role') {
            $activate_sql = "UPDATE system_roles SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'trainee') {
            $activate_sql = "UPDATE trainees SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'trainer') {
            $activate_sql = "UPDATE trainers SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
//        } else if ($item == 'training_facility') {
//            $activate_sql = "UPDATE training_facilities SET status=:status, approvedat=:approvedat, approvedby=:approvedby WHERE count=:code";
//        } else if ($item == 'training_facility_administrator') {
//            $activate_sql = "UPDATE training_facility_administrators SET status=:status, approvedat=:approvedat, approvedby=:approvedby WHERE count=:code";
        } else if ($item == 'training_facility_service') {
            $activate_sql = "UPDATE training_facility_services SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'user_type') {
            $activate_sql = "UPDATE user_types SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'ward') {
            $activate_sql = "UPDATE wards SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        }
        $stmt_activate = $this->prepareQuery($activate_sql);
        $stmt_activate->bindValue("code", $code);
        $stmt_activate->bindValue("status", 1001);
        $stmt_activate->bindValue("lastmodifiedat", date("Y-m-d H:i:s"));
        $stmt_activate->bindValue("lastmodifiedby", $_SESSION['user_id']);
        if ($stmt_activate->execute()) {
            return true;
        } else
            return false;
    }

    public function deactivateRecord($item, $code) {
        if ($item == 'contact') {
            $deactivate_sql = "UPDATE contacts SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'county') {
            $deactivate_sql = "UPDATE counties SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'lesson') {
            $deactivate_sql = "UPDATE lessons SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'lesson_category') {
            $deactivate_sql = "UPDATE lesson_categories SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
//        } else if ($item == 'role_privilege') {
//            $deactivate_sql = "UPDATE role_privileges SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'staff') {
            $deactivate_sql = "UPDATE staff SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'staff_position') {
            $deactivate_sql = "UPDATE staff_positions SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'system_administrator') {
            $deactivate_sql = "UPDATE system_administrators SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'system_component') {
            $deactivate_sql = "UPDATE system_components SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'system_privilege') {
            $deactivate_sql = "UPDATE system_privileges SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'system_role') {
            $deactivate_sql = "UPDATE system_roles SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'trainee') {
            $deactivate_sql = "UPDATE trainees SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'trainer') {
            $deactivate_sql = "UPDATE trainers SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
//        } else if ($item == 'training_facility') {
//            $deactivate_sql = "UPDATE training_facilities SET status=:status, approvedat=:approvedat, approvedby=:approvedby WHERE count=:code";
//        } else if ($item == 'training_facility_administrator') {
//            $deactivate_sql = "UPDATE training_facility_administrators SET status=:status, approvedat=:approvedat, approvedby=:approvedby WHERE count=:code";
        } else if ($item == 'training_facility_service') {
            $deactivate_sql = "UPDATE training_facility_services SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'user_type') {
            $deactivate_sql = "UPDATE user_types SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'ward') {
            $deactivate_sql = "UPDATE wards SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        }
        $stmt_deactivate = $this->prepareQuery($deactivate_sql);
        $stmt_deactivate->bindValue("code", $code);
        $stmt_deactivate->bindValue("status", 1002);
        $stmt_deactivate->bindValue("lastmodifiedat", date("Y-m-d H:i:s"));
        $stmt_deactivate->bindValue("lastmodifiedby", $_SESSION['user_id']);
        if ($stmt_deactivate->execute()) {
            return true;
        } else
            return false;
    }

    public function deleteRecord($item, $code) {
        if ($item == 'contact') {
            $delete_sql = "UPDATE contacts SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'county') {
            $delete_sql = "UPDATE counties SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'lesson') {
            $delete_sql = "UPDATE lessons SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'lesson_booking') {
            $delete_sql = "UPDATE booking_transactions_details SET status=:status, approvedat=:lastmodifiedat, approvedby=:lastmodifiedby WHERE count=:code";
        } else if ($item == 'lesson_category') {
            $delete_sql = "UPDATE lesson_categories SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
//        } else if ($item == 'role_privilege') {
//            $delete_sql = "UPDATE role_privileges SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'staff') {
            $delete_sql = "UPDATE staff SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'staff_position') {
            $delete_sql = "UPDATE staff_positions SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'system_administrator') {
            $delete_sql = "UPDATE system_administrators SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'system_component') {
            $delete_sql = "UPDATE system_components SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'system_privilege') {
            $delete_sql = "UPDATE system_privileges SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'system_role') {
            $delete_sql = "UPDATE system_roles SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'trainee') {
            $delete_sql = "UPDATE trainees SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'trainer') {
            $delete_sql = "UPDATE trainers SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
            //        } else if ($item == 'training_facility') {
//            $delete_sql = "UPDATE training_facilities SET status=:status, approvedat=:approvedat, approvedby=:approvedby WHERE count=:code";
            //        } else if ($item == 'training_facility_administrator') {
//            $delete_sql = "UPDATE training_facility_administrators SET status=:status, approvedat=:approvedat, approvedby=:approvedby WHERE count=:code";
                    } else if ($item == 'training_facility_service') {
            $delete_sql = "UPDATE training_facility_services SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        } else if ($item == 'user_type') {
            $delete_sql = "UPDATE user_types SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
                    } else if ($item == 'ward') {
            $delete_sql = "UPDATE wards SET status=:status, lastmodifiedat=:lastmodifiedat, lastmodifiedby=:lastmodifiedby WHERE id=:code";
        }
        $stmt_delete = $this->prepareQuery($delete_sql);
        $stmt_delete->bindValue("code", $code);
        $stmt_delete->bindValue("status", 1009);
        $stmt_delete->bindValue("lastmodifiedat", date("Y-m-d H:i:s"));
        $stmt_delete->bindValue("lastmodifiedby", $_SESSION['user_id']);
        if ($stmt_delete->execute()) {
            return true;
        } else
            return false;
    }

    public function closeRecord($item, $code) {
        if ($item == 'inbox_message') {
            $deactivate_sql = "UPDATE inbox_messages SET status=:status, closedat=:lastmodifiedat WHERE id=:code";
//        } else if ($item == 'training_facility_administrator') {
//            $deactivate_sql = "UPDATE training_facility_administrators SET status=:status, approvedat=:approvedat, approvedby=:approvedby WHERE count=:code";
//        } else if ($item == 'training_facility_service') {
//            $deactivate_sql = "UPDATE training_facility_services SET status=:status, approvedat=:approvedat, approvedby=:approvedby WHERE count=:code";
        }
        $stmt_deactivate = $this->prepareQuery($deactivate_sql);
        $stmt_deactivate->bindValue("code", $code);
        $stmt_deactivate->bindValue("status", 1004);
        $stmt_deactivate->bindValue("lastmodifiedat", date("Y-m-d H:i:s"));
        if ($item != 'inbox_message') {
            $stmt_deactivate->bindValue("lastmodifiedby", $_SESSION['user_id']);
        }        
        if ($stmt_deactivate->execute()) {
            return true;
        } else
            return false;
    }

    public function approveTrainingRequest($code, $transaction_id, $approval_comment) {
        $sql_trainer_approval = "UPDATE booking_transactions_details SET status=:status, trainer_approvedat=:trainer_approvedat, trainer_approval_comment=:approval_comment WHERE count=:code";
        $stmt_trainer_approval = $this->prepareQuery($sql_trainer_approval);
        $stmt_trainer_approval->bindValue("code", $code);
        $stmt_trainer_approval->bindValue("status", 5002);
        $stmt_trainer_approval->bindValue("trainer_approvedat", date("Y-m-d H:i:s"));
        $stmt_trainer_approval->bindValue("approval_comment", strtoupper($approval_comment));
        if ($stmt_trainer_approval->execute()) {
            $users_management = new Users_Management();
            $trainees = new Trainees();
            $transaction_details = $this->fetchBookingTransactionDetails($transaction_id);
            $trainee_contact_details = $users_management->fetchIndividualContactDetails($transaction_details['booker_id']);
            $trainee_system_user_details = $users_management->fetchUserDetails($transaction_details['booker_id']);
            $trainee_details = $trainees->fetchTraineeDetails($trainee_system_user_details['reference_id']);
            $trainee_message = "Hello " . $trainee_details['firstname'] . ", your assigned trainer has approved the training request. Thank you for engaging us for your fitness sessions.";
            $send_trainee_sms = $this->sendSingleSMS($trainee_contact_details['phone_number1'], $trainee_message);
            return true;
        } else
            return false;
    }

    public function declineLessonAssignment($code, $new_trainer, $decline_reason) {
        $users_management = new Users_Management();
        $trainers = new Trainers();
        $trainees = new Trainees();
        $sql_decline_lesson_assignment = "UPDATE booking_transactions_details SET reassign_reason=:reassign_reason WHERE count=:code";
        $stmt_decline_lesson_assignment = $this->prepareQuery($sql_reassign_trainer);
        $stmt_reassign_trainer->bindValue("code", $code);
        $stmt_reassign_trainer->bindValue("reassign_reason", $reassign_reason);
        if ($stmt_reassign_trainer->execute()) {
            $record_details = $this->fetchBookingTransactionDetailDetails($code);
            $transaction_details = $this->fetchBookingTransactionDetails($record_details["transaction_id"]);
            $trainer_contact_details = $users_management->fetchIndividualContactDetails($new_trainer);
            $trainer_system_user_details = $users_management->fetchUserDetails($new_trainer);
            $trainer_details = $trainers->fetchTrainerDetails($trainer_system_user_details['reference_id']);
            $trainee_contact_details = $users_management->fetchIndividualContactDetails($transaction_details["booker_id"]);
            $trainee_system_user_details = $users_management->fetchUserDetails($transaction_details["booker_id"]);
            $trainee_details = $trainees->fetchTraineeDetails($trainee_system_user_details['reference_id']);

            $trainer_message = "Hello " . $trainer_details['firstname'] . ", you have been assigned a trainee - " . $trainee_details['firstname'] . " " . $trainee_details['lastname'] . ". You can reach your trainee on " . $trainee_contact_details['phone_number1'] . ". If you have any reservations on the assignment, please reply: TESTSMS:LESSON ASSIGNMENT,DECLINE" . $code . " to 21211 to decline the assignment, else do not reply to this text.. You can approve the same from your email or by logging into the FitnessIQ portal.";
            $trainee_message = "Hello " . $trainee_details['firstname'] . ", your lesson order - " . $code . " has been reassigned to a new trainer " . $trainer_details['firstname'] . " " . $trainer_details['lastname'] . ". You can reach your new trainer on - Telephone (" . $trainer_contact_details['phone_number1'] . "), Email(" . $trainee_contact_details['email'] . "). Sorry for any inconvenience caused. If you have any reservations on the allocated trainer please reply: TESTSMS:TRAINER ACCEPTANCE,DECLINE" . $code . " to 21211, else do not reply to this text.";

            $trainer_email = $trainer_contact_details['email'];
            $trainer_name = $trainer_details['firstname'] . " " . $trainer_details['lastname'];
            $trainer_email_subject = "Lesson Assignment";
            $trainer_email_message = "<html><body>"
                    . "<p><b>Hello " . $trainer_details['firstname'] . ",</b><br/>"
                    . "You have been assigned a trainee - " . $trainee_details['firstname'] . " " . $trainee_details['lastname'] . ". "
                    . "You can reach your trainee on " . $trainee_contact_details['phone_number1'] . ". "
                    . "If you have any reservations on the assignment, please <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=lesson_assignment&response=decline{$code}'> Click here </a> to decline the lesson assignment. <br />"
                    . "For any enquiries, kindly contact us on:   <br/>"
                    . "<ul>"
                    . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                    . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                    . "</ul>"
                    . "</body></html>";

            $trainee_email = $trainee_contact_details['email'];
            $trainee_name = $trainee_details['firstname'] . " " . $trainee_details['lastname'];
            $trainee_email_subject = "Lesson Assignment";
            $trainee_email_message = "<html><body>"
                    . "<p><b>Hello " . $trainee_details['firstname'] . ",</b><br/>"
                    . "Your lesson order - " . $code . " has been reassigned to a new trainer " . $trainer_details['firstname'] . " " . $trainer_details['lastname'] . ". "
                    . "You can reach your new trainer on - Telephone (" . $trainer_contact_details['phone_number1'] . "), Email(" . $trainee_contact_details['email'] . "). "
                    . "Sorry for any inconvenience caused. "
                    . "If you have any reservations on the allocated trainer please <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=trainer_acceptance&response=decline{$code}'> Click here </a> to decline the assignment. <br />"
                    . "For any enquiries, kindly contact us on:   <br/>"
                    . "<ul>"
                    . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                    . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                    . "</ul>"
                    . "</body></html>";

            $users_management = new Users_Management();
            $send_trainer_email = $users_management->sendEmail($trainer_email, $trainer_name, $trainer_email_subject, $trainer_email_message);
            $send_trainee_email = $users_management->sendEmail($trainee_email, $trainee_name, $trainee_email_subject, $trainee_email_message);

            $send_trainer_sms = $this->sendSingleSMS($trainer_contact_details['phone_number1'], $trainer_message);
            $send_trainee_sms = $this->sendSingleSMS($trainee_contact_details['phone_number1'], $trainee_message);

            unset($_SESSION['reassign_trainer']);

            return true;
        } else
            return false;
    }

    public function autoReassignLessonTrainer($code, $new_trainer, $reassign_reason) {
        $users_management = new Users_Management();
        $trainers = new Trainers();
        $trainees = new Trainees();
        $sql_reassign_trainer = "UPDATE booking_transactions_details SET is_reassigned=:is_reassigned, re_assignedat=:re_assignedat, reassign_reason=:reassign_reason, new_trainer=:new_trainer, new_trainer_approvedat=:new_trainer_approvedat, new_trainer_approval_comment=:new_trainer_approval_comment WHERE count=:code";
        $stmt_reassign_trainer = $this->prepareQuery($sql_reassign_trainer);
        $stmt_reassign_trainer->bindValue("code", $code);
        $stmt_reassign_trainer->bindValue("is_reassigned", "YES");
        $stmt_reassign_trainer->bindValue("re_assignedat", date("Y-m-d H:i:s"));
        $stmt_reassign_trainer->bindValue("reassign_reason", $reassign_reason);
        $stmt_reassign_trainer->bindValue("new_trainer", $new_trainer);
        $stmt_reassign_trainer->bindValue("new_trainer_approvedat", date("Y-m-d H:i:s"));
        $stmt_reassign_trainer->bindValue("new_trainer_approval_comment", "Auto-reassigned");
        if ($stmt_reassign_trainer->execute()) {
            $record_details = $this->fetchBookingTransactionDetailDetails($code);
            $transaction_details = $this->fetchBookingTransactionDetails($record_details["transaction_id"]);
            $trainer_contact_details = $users_management->fetchIndividualContactDetails($new_trainer);
            $trainer_system_user_details = $users_management->fetchUserDetails($new_trainer);
            $trainer_details = $trainers->fetchTrainerDetails($trainer_system_user_details['reference_id']);
            $trainee_contact_details = $users_management->fetchIndividualContactDetails($transaction_details["booker_id"]);
            $trainee_system_user_details = $users_management->fetchUserDetails($transaction_details["booker_id"]);
            $trainee_details = $trainees->fetchTraineeDetails($trainee_system_user_details['reference_id']);

            $trainer_message = "Hello " . $trainer_details['firstname'] . ", you have been assigned a trainee - " . $trainee_details['firstname'] . " " . $trainee_details['lastname'] . ". You can reach your trainee on " . $trainee_contact_details['phone_number1'] . ". If you have any reservations on the assignment, please reply: TESTSMS:LESSON ASSIGNMENT,DECLINE" . $code . " to 21211 to decline the assignment, else do not reply to this text.. You can approve the same from your email or by logging into the FitnessIQ portal.";
            $trainee_message = "Hello " . $trainee_details['firstname'] . ", your lesson order - " . $code . " has been reassigned to a new trainer " . $trainer_details['firstname'] . " " . $trainer_details['lastname'] . ". You can reach your new trainer on - Telephone (" . $trainer_contact_details['phone_number1'] . "), Email(" . $trainee_contact_details['email'] . "). Sorry for any inconvenience caused. If you have any reservations on the allocated trainer please reply: TESTSMS:TRAINER ACCEPTANCE,DECLINE" . $code . " to 21211, else do not reply to this text.";

            $trainer_email = $trainer_contact_details['email'];
            $trainer_name = $trainer_details['firstname'] . " " . $trainer_details['lastname'];
            $trainer_email_subject = "Lesson Assignment";
            $trainer_email_message = "<html><body>"
                    . "<p><b>Hello " . $trainer_details['firstname'] . ",</b><br/>"
                    . "You have been assigned a trainee - " . $trainee_details['firstname'] . " " . $trainee_details['lastname'] . ". "
                    . "You can reach your trainee on " . $trainee_contact_details['phone_number1'] . ". "
                    . "If you have any reservations on the assignment, please <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=lesson_assignment&response=decline{$code}'> Click here </a> to decline the lesson assignment. <br />"
                    . "For any enquiries, kindly contact us on:   <br/>"
                    . "<ul>"
                    . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                    . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                    . "</ul>"
                    . "</body></html>";

            $trainee_email = $trainee_contact_details['email'];
            $trainee_name = $trainee_details['firstname'] . " " . $trainee_details['lastname'];
            $trainee_email_subject = "Lesson Assignment";
            $trainee_email_message = "<html><body>"
                    . "<p><b>Hello " . $trainee_details['firstname'] . ",</b><br/>"
                    . "Your lesson order - " . $code . " has been reassigned to a new trainer " . $trainer_details['firstname'] . " " . $trainer_details['lastname'] . ". "
                    . "You can reach your new trainer on - Telephone (" . $trainer_contact_details['phone_number1'] . "), Email(" . $trainee_contact_details['email'] . "). "
                    . "Sorry for any inconvenience caused. "
                    . "If you have any reservations on the allocated trainer please <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=trainer_acceptance&response=decline{$code}'> Click here </a> to decline the assignment. <br />"
                    . "For any enquiries, kindly contact us on:   <br/>"
                    . "<ul>"
                    . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                    . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                    . "</ul>"
                    . "</body></html>";

            $users_management = new Users_Management();
            $send_trainer_email = $users_management->sendEmail($trainer_email, $trainer_name, $trainer_email_subject, $trainer_email_message);
            $send_trainee_email = $users_management->sendEmail($trainee_email, $trainee_name, $trainee_email_subject, $trainee_email_message);

            $send_trainer_sms = $this->sendSingleSMS($trainer_contact_details['phone_number1'], $trainer_message);
            $send_trainee_sms = $this->sendSingleSMS($trainee_contact_details['phone_number1'], $trainee_message);

            unset($_SESSION['reassign_trainer']);

            return true;
        } else
            return false;
    }

    public function rescheduleLesson($code, $new_start_date, $reschedule_reason) {
        $users_management = new Users_Management();
        $trainers = new Trainers();
        $trainees = new Trainees();
        $lessons = new Lessons();

        $record_details = $this->fetchBookingTransactionDetailDetails($code);
        if ($record_details['is_reassigned'] == "YES") {
            $assigned_trainer = $record_details['new_trainer'];
        } else {
            $assigned_trainer = $record_details['assigned_trainer'];
        }
        $transaction_details = $this->fetchBookingTransactionDetails($record_details["transaction_id"]);
        $trainer_contact_details = $users_management->fetchIndividualContactDetails($assigned_trainer);
        $trainer_system_user_details = $users_management->fetchUserDetails($assigned_trainer);
        $trainer_details = $trainers->fetchTrainerDetails($trainer_system_user_details['reference_id']);
        $trainee_contact_details = $users_management->fetchIndividualContactDetails($transaction_details["booker_id"]);
        $trainee_system_user_details = $users_management->fetchUserDetails($transaction_details["booker_id"]);
        $trainee_details = $trainees->fetchTraineeDetails($trainee_system_user_details['reference_id']);
        $lesson_details = $lessons->fetchLessonDetails($record_details['lesson']);

        if ($record_details['is_rescheduled'] != "YES") {
            $sql_reschedule_lesson = "UPDATE booking_transactions_details SET is_rescheduled=:is_rescheduled, new_start_date=:new_start_date, reschedule_reason=:reschedule_reason WHERE count=:code";
            $stmt_reschedule_lesson = $this->prepareQuery($sql_reschedule_lesson);
            $stmt_reschedule_lesson->bindValue("code", $code);
            $stmt_reschedule_lesson->bindValue("is_rescheduled", "YES");
            $stmt_reschedule_lesson->bindValue("new_start_date", $new_start_date);
            $stmt_reschedule_lesson->bindValue("reschedule_reason", $reschedule_reason);
            if ($stmt_reschedule_lesson->execute()) {
                $trainer_message = "Hello " . $trainer_details['firstname'] . ", kindly note that the lesson - " . $code . " assigned to you earlier has been rescheduled by the client to " . $new_start_date . " at " . $lesson_details['start_time'] . ". Sorry for any inconvenience caused.";
                $trainee_message = "Hello " . $trainee_details['firstname'] . ", your lesson order - " . $code . " has been been rescheduled successfully to " . $new_start_date . " at " . $lesson_details['start_time'] . ". Thank you for engaging us";

                $trainer_email = $trainer_contact_details['email'];
                $trainer_name = $trainer_details['firstname'] . " " . $trainer_details['lastname'];
                $trainer_email_subject = "Lesson Assignment";
                $trainer_email_message = "<html><body>"
                        . "<p><b>Hello " . $trainer_details['firstname'] . ",</b><br/>"
                        . "Kindly note that the lesson - " . $code . " assigned to you earlier has been rescheduled by the client to " . $new_start_date . " at " . $lesson_details['start_time'] . ". "
                        . "Sorry for any inconvenience caused. <br />"
                        . "For any enquiries, kindly contact us on:   <br/>"
                        . "<ul>"
                        . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                        . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                        . "</ul>"
                        . "</body></html>";

                $trainee_email = $trainee_contact_details['email'];
                $trainee_name = $trainee_details['firstname'] . " " . $trainee_details['lastname'];
                $trainee_email_subject = "Lesson Assignment";
                $trainee_email_message = "<html><body>"
                        . "<p><b>Hello " . $trainee_details['firstname'] . ",</b><br/>"
                        . "Your lesson order - " . $code . " has been been rescheduled successfully to " . $new_start_date . " at " . $lesson_details['start_time'] . ".  <br/>"
                        . "Thank you for engaging us. <br/>"
                        . "For any enquiries, kindly contact us on:   <br/>"
                        . "<ul>"
                        . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                        . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                        . "</ul>"
                        . "</body></html>";

                $users_management = new Users_Management();
                $send_trainer_email = $users_management->sendEmail($trainer_email, $trainer_name, $trainer_email_subject, $trainer_email_message);
                $send_trainee_email = $users_management->sendEmail($trainee_email, $trainee_name, $trainee_email_subject, $trainee_email_message);

                $send_trainer_sms = $this->sendSingleSMS($trainer_contact_details['phone_number1'], $trainer_message);
                $send_trainee_sms = $this->sendSingleSMS($trainee_contact_details['phone_number1'], $trainee_message);
                return true;
            } else {
                $trainee_message = "Hello " . $trainee_details['firstname'] . ", there was an error rescheduling your lesson order - " . $code . ". Please wait for some few minutes then try rescheduling it again. Thank you for engaging us";
                $send_trainee_sms = $this->sendSingleSMS($trainee_contact_details['phone_number1'], $trainee_message);
                return false;
            }
        } else {
            $trainee_message = "Hello " . $trainee_details['firstname'] . ", kindly note that a lesson can only be rescheduled once. Your lesson order - " . $code . " had been rescheduled before from " . $record_details['start_date'] . " to " . $record_details['new_start_date'] . " and can therefore not be rescheduled again.";
            $send_trainee_sms = $this->sendSingleSMS($trainee_contact_details['phone_number1'], $trainee_message);
            return false;
        }
    }

    public function cancelLesson($code) {
        $users_management = new Users_Management();
        $trainers = new Trainers();
        $trainees = new Trainees();
        $sql_cancel_lesson = "UPDATE booking_transactions_details SET attendance_status=:attendance_status, status=:status WHERE count=:code";
        $stmt_cancel_lesson = $this->prepareQuery($sql_cancel_lesson);
        $stmt_cancel_lesson->bindValue("code", $code);
        $stmt_cancel_lesson->bindValue("attendance_status", 1008);
        $stmt_cancel_lesson->bindValue("status", 1008);
        if ($stmt_cancel_lesson->execute()) {
            $record_details = $this->fetchBookingTransactionDetailDetails($code);
            if ($record_details['is_reassigned'] == "YES") {
                $assigned_trainer = $record_details['new_trainer'];
            } else {
                $assigned_trainer = $record_details['assigned_trainer'];
            }
            $transaction_details = $this->fetchBookingTransactionDetails($record_details["transaction_id"]);
            $trainer_contact_details = $users_management->fetchIndividualContactDetails($assigned_trainer);
            $trainer_system_user_details = $users_management->fetchUserDetails($assigned_trainer);
            $trainer_details = $trainers->fetchTrainerDetails($trainer_system_user_details['reference_id']);
            $trainee_contact_details = $users_management->fetchIndividualContactDetails($transaction_details["booker_id"]);
            $trainee_system_user_details = $users_management->fetchUserDetails($transaction_details["booker_id"]);
            $trainee_details = $trainees->fetchTraineeDetails($trainee_system_user_details['reference_id']);

            $trainer_message = "Hello " . $trainer_details['firstname'] . ", kindly note that the lesson - " . $code . " assigned to you earlier has been cancelled by the client. Sorry for any inconvenience caused.";
            $trainee_message = "Hello " . $trainee_details['firstname'] . ", your lesson order - " . $code . " has been been cancelled successfully. Thank you for engaging us";

            $trainer_email = $trainer_contact_details['email'];
            $trainer_name = $trainer_details['firstname'] . " " . $trainer_details['lastname'];
            $trainer_email_subject = "Lesson Assignment";
            $trainer_email_message = "<html><body>"
                    . "<p><b>Hello " . $trainer_details['firstname'] . ",</b><br/>"
                    . "Kindly note that the lesson - " . $code . " assigned to you earlier has been cancelled by the client. "
                    . "Sorry for any inconvenience caused. <br />"
                    . "For any enquiries, kindly contact us on:   <br/>"
                    . "<ul>"
                    . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                    . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                    . "</ul>"
                    . "</body></html>";

            $trainee_email = $trainee_contact_details['email'];
            $trainee_name = $trainee_details['firstname'] . " " . $trainee_details['lastname'];
            $trainee_email_subject = "Lesson Assignment";
            $trainee_email_message = "<html><body>"
                    . "<p><b>Hello " . $trainee_details['firstname'] . ",</b><br/>"
                    . "Your lesson order - " . $code . " has been been cancelled successfully. <br/>"
                    . "Thank you for engaging us. <br/>"
                    . "For any enquiries, kindly contact us on:   <br/>"
                    . "<ul>"
                    . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                    . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                    . "</ul>"
                    . "</body></html>";

            $users_management = new Users_Management();
            $send_trainer_email = $users_management->sendEmail($trainer_email, $trainer_name, $trainer_email_subject, $trainer_email_message);
            $send_trainee_email = $users_management->sendEmail($trainee_email, $trainee_name, $trainee_email_subject, $trainee_email_message);

            $send_trainer_sms = $this->sendSingleSMS($trainer_contact_details['phone_number1'], $trainer_message);
            $send_trainee_sms = $this->sendSingleSMS($trainee_contact_details['phone_number1'], $trainee_message);

            return true;
        } else
            return false;
    }

    public function closeLessonTraining($code, $transaction_id) {
        $sql = "UPDATE booking_transactions_details SET status=:status WHERE count=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("code", $code);
        $stmt->bindValue("status", 1004);
        if ($stmt->execute()) {
            $users_management = new Users_Management();
            $trainers = new Trainers();
            $trainees = new Trainees();
            $transaction_details = $this->fetchBookingTransactionDetails($transaction_id);
            $transaction_detail_details = $this->fetchBookingTransactionDetailDetails($code);
            $trainer_system_user_details = $users_management->fetchUserDetails($transaction_detail_details['assigned_trainer']);
            $trainer_details = $trainers->fetchTrainerDetails($trainer_system_user_details['reference_id']);
            $trainee_contact_details = $users_management->fetchIndividualContactDetails($transaction_details['booker_id']);
            $trainee_system_user_details = $users_management->fetchUserDetails($transaction_details['booker_id']);
            $trainee_details = $trainees->fetchTraineeDetails($trainee_system_user_details['reference_id']);

            $sql_iq_points = "INSERT INTO iq_points (user_id, transaction_id, transaction_type, points)"
                    . " VALUES (:user_id, :transaction_id, :transaction_type, :points)";
            $stmt_iq_points = $this->prepareQuery($sql_iq_points);
            $stmt_iq_points->bindValue("user_id", $transaction_detail_details['assigned_trainer']);
            $stmt_iq_points->bindValue("transaction_id", $transaction_id);
            $stmt_iq_points->bindValue("transaction_type", 6);
            $stmt_iq_points->bindValue("points", 0.05 * $transaction_detail_details['unit_price'] * $transaction_detail_details['number_of_classes']);
            if ($stmt_iq_points->execute()) {
                $user_iq_points_gained = $this->getUserIqPointsGained($transaction_detail_details['assigned_trainer']);
                $user_iq_points_redeemed = $this->getUserIqPointsRedeemed($transaction_detail_details['assigned_trainer']);
                $iq_points = $user_iq_points_gained - $user_iq_points_redeemed;

                $sql_update_iq_points_balance = "UPDATE system_users SET iq_points=:iq_points WHERE id=:user_id";
                $stmt_update_iq_points_balance = $this->prepareQuery($sql_update_iq_points_balance);
                $stmt_update_iq_points_balance->bindValue("user_id", $transaction_detail_details['assigned_trainer']);
                $stmt_update_iq_points_balance->bindValue("iq_points", $iq_points);
                $stmt_update_iq_points_balance->execute();
                return true;
            } else {
                return false;
            }

            $trainee_email = $trainee_contact_details['email'];
            $trainee_name = $trainee_details['firstname'] . " " . $trainee_details['lastname'];
            $trainee_email_subject = "Lesson Closure Notification";
            $trainee_email_message = "<html><body>"
                    . "<p><b>Hello " . $trainee_details['firstname'] . ",</b><br/>"
                    . "Your training session has been marked as closed. Thank you for choosing to train with us. "
                    . "Please <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=rate{$code}'> Click here </a> to rate your trainer and the training facility."
                    . "Your feedback is of great importance to us. <br/>"
                    . "For any enquiries, kindly contact us on:   <br/>"
                    . "<ul>"
                    . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                    . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                    . "</ul>"
                    . "</body></html>";

            $users_management = new Users_Management();
            $send_trainee_email = $users_management->sendEmail($trainee_email, $trainee_name, $trainee_email_subject, $trainee_email_message);

            $trainee_message = "Hello " . $trainee_details['firstname'] . ", your training session has been marked as closed. Thank you for choosing to train with us. Please rate trainer " . $trainer_details['firstname'] . " " . $trainer_details['lastname'] . " as well as the training facility attended by replying: TESTSMS:RATE" . $code . ",TRAINER4,FACILITY3 to 21211 to rate the training session.";
            $send_trainee_sms = $this->sendSingleSMS($trainee_contact_details['phone_number1'], $trainee_message);
        } else
            return false;
    }

    public function rateTrainingSession($code, $transaction_id, $trainer_rating, $facility_rating) {
        $sql = "UPDATE booking_transactions_details SET trainer_rating =:trainer_rating, facility_rating=:facility_rating  WHERE count=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("code", $code);
        $stmt->bindValue("trainer_rating", $trainer_rating);
        $stmt->bindValue("facility_rating", $facility_rating);
        if ($stmt->execute()) {
            $users_management = new Users_Management();
            $trainers = new Trainers();
            $trainees = new Trainees();
            $transaction_details = $this->fetchBookingTransactionDetails($transaction_id);
            $transaction_detail_details = $this->fetchBookingTransactionDetailDetails($code);
            $trainer_system_user_details = $users_management->fetchUserDetails($transaction_detail_details['assigned_trainer']);
            $trainer_contact_details = $users_management->fetchIndividualContactDetails($transaction_detail_details['assigned_trainer']);
            $trainer_details = $trainers->fetchTrainerDetails($trainer_system_user_details['reference_id']);
            $trainee_contact_details = $users_management->fetchIndividualContactDetails($transaction_details['booker_id']);
            $trainee_system_user_details = $users_management->fetchUserDetails($transaction_details['booker_id']);
            $trainee_details = $trainees->fetchTraineeDetails($trainee_system_user_details['reference_id']);

            $total_trainer_ratings = $this->getTrainerRatingsGained($transaction_detail_details['assigned_trainer']);
            $total_trainer_occurrences = $this->getTrainerRatingOccurrences($transaction_detail_details['assigned_trainer']);
            $average_trainer_rating = $total_trainer_ratings / $total_trainer_occurrences;

            $total_facility_ratings = $this->getFacilityRatingsGained($transaction_detail_details['training_facility']);
            $total_facility_occurrences = $this->getFacilityRatingOccurrences($transaction_detail_details['training_facility']);
            $average_facility_rating = $total_facility_ratings / $total_facility_occurrences;

            $sql_update_trainer_rating = "UPDATE trainers SET trainer_rating=:trainer_rating WHERE id=:trainer_id";
            $stmt_update_trainer_rating = $this->prepareQuery($sql_update_trainer_rating);
            $stmt_update_trainer_rating->bindValue("trainer_id", $trainer_system_user_details['reference_id']);
            $stmt_update_trainer_rating->bindValue("trainer_rating", $average_trainer_rating);
            $stmt_update_trainer_rating->execute();

            $sql_update_facility_rating = "UPDATE training_facilities SET facility_rating=:facility_rating WHERE id=:facility_id";
            $stmt_update_facility_rating = $this->prepareQuery($sql_update_facility_rating);
            $stmt_update_facility_rating->bindValue("facility_id", $transaction_detail_details['training_facility']);
            $stmt_update_facility_rating->bindValue("facility_rating", $average_facility_rating);
            $stmt_update_facility_rating->execute();

            $trainee_message = "Hello " . $trainee_details['firstname'] . ", thank you for using our services.";
            $trainer_message = "Hello " . $trainer_details['firstname'] . ", thank you for partnering with us. You were awarded a score of " . $trainer_rating . "* in the ended lesson - " . $code . ".";
            $send_trainee_sms = $this->sendSingleSMS($trainee_contact_details['phone_number1'], $trainee_message);
            $send_trainer_sms = $this->sendSingleSMS($trainer_contact_details['phone_number1'], $trainer_message);
            return true;
        } else
            return false;
    }

    public function getTrainerRatingsGained($trainer_id) {
        $sql = "SELECT SUM(trainer_rating) FROM booking_transactions_details WHERE assigned_trainer=:assigned_trainer";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("assigned_trainer", $trainer_id);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data[0]['SUM(trainer_rating)'];
    }

    public function getFacilityRatingsGained($facility_id) {
        $sql = "SELECT SUM(facility_rating) FROM booking_transactions_details WHERE training_facility=:training_facility";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("training_facility", $facility_id);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data[0]['SUM(facility_rating)'];
    }

    public function getTrainerRatingOccurrences($trainer_id) {
        $sql = "SELECT SUM(trainer_rating) FROM booking_transactions_details WHERE assigned_trainer=:assigned_trainer";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("assigned_trainer", $trainer_id);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($data);
    }

    public function getFacilityRatingOccurrences($facility_id) {
        $sql = "SELECT SUM(facility_rating) FROM booking_transactions_details WHERE training_facility=:training_facility";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("training_facility", $facility_id);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($data);
    }

    private function sendMessage() {
        $sql = "INSERT INTO inbox_messages (name, email, subject, message) VALUES(:name, :email, :subject, :message)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("name", strtoupper($_POST['name']));
        $stmt->bindValue("email", strtoupper($_POST['email']));
        $stmt->bindValue("subject", strtoupper($_POST['subject']));
        $stmt->bindValue("message", strtoupper($_POST['message']));
        $stmt->execute();
        return true;
    }

    public function fetchBookingTransactionDetails($transaction_id) {
        $sql = "SELECT * FROM booking_transactions WHERE transaction_id=:transaction_id";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("transaction_id", $transaction_id);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function fetchPaymentTransactionDetails($count_id) {
        $sql = "SELECT * FROM payment_transactions WHERE count=:count_id";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("count_id", $count_id);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function fetchBookingTransactionDetailDetails($code) {
        $sql = "SELECT * FROM booking_transactions_details WHERE count=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

}
