<?php

class Trainees extends Database {

    public function execute() {
        if ($_POST['action'] == "add_trainee") {
            return $this->addTrainee();
        }
    }

    private function addTrainee() {      
    $trainee_id = $this->getNextTraineeId();
    
        $sql = "INSERT INTO trainees (firstname, lastname, gender, birth_date, createdby, lastmodifiedby)"
                . " VALUES (:firstname, :lastname, :gender, :birth_date, :createdby, :lastmodifiedby)";

        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("firstname", strtoupper($_SESSION['firstname']));
        $stmt->bindValue("lastname", strtoupper($_SESSION['lastname']));
        $stmt->bindValue("gender", strtoupper($_SESSION['gender']));
        $stmt->bindValue("birth_date", $_SESSION['birth_date']);
        $stmt->bindValue("createdby", $_SESSION['user_id']); 
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']); 
        $stmt->execute();

        $sql_contacts = "INSERT INTO contacts (user_id, phone_number1, phone_number2, email, postal_number, postal_code, postal_town, county, ward, prof_picture, website, createdby, lastmodifiedby)"
                . " VALUES (:user_id, :phone_number1, :phone_number2, :email, :postal_number, :postal_code, :postal_town, :county, :ward, :prof_picture, :website, :createdby, :lastmodifiedby)";

        $stmt_contacts = $this->prepareQuery($sql_contacts);
        $stmt_contacts->bindValue("user_id", 1); //$_SESSION['user_id']
        $stmt_contacts->bindValue("phone_number1", $_SESSION['phone_number1']);
        $stmt_contacts->bindValue("phone_number2", $_SESSION['phone_number2']);
        $stmt_contacts->bindValue("email", strtoupper($_SESSION['email']));
        $stmt_contacts->bindValue("postal_number", $_SESSION['postal_number']);
        $stmt_contacts->bindValue("postal_code", $_SESSION['postal_code']);
        $stmt_contacts->bindValue("postal_town", strtoupper($_SESSION['postal_town']));
        $stmt_contacts->bindValue("county", $_SESSION['county']);
        $stmt_contacts->bindValue("ward", $_SESSION['ward']);
        $stmt_contacts->bindValue("prof_picture", $_SESSION['prof_picture_filename']);
        $stmt_contacts->bindValue("website", strtoupper($_SESSION['website']));        
        $stmt_contacts->bindValue("createdby", $_SESSION['user_id']); 
        $stmt_contacts->bindValue("lastmodifiedby", $_SESSION['user_id']); 
        $stmt_contacts->execute();

        $password = $this->randomString(10);
        
        $sql_userlogs = "INSERT INTO system_users (reference_type, reference_id, username, password)"
                . " VALUES (:reference_type, :reference_id, :username, :password)";

        $stmt_userlogs = $this->prepareQuery($sql_userlogs);
        $stmt_userlogs->bindValue("reference_type", $_SESSION['ref_type']);
        $stmt_userlogs->bindValue("reference_id", $trainee_id);
        $stmt_userlogs->bindValue("username", strtoupper($_SESSION['email']));
        $stmt_userlogs->bindValue("password", sha1($password));        
        if ($stmt_userlogs->execute()) {
            return true;
        } else {
            return false;
        }

//        $this->addUserToRole($role, $id, $createdby);
//        $this->addPrivilegesToUser($ref_type, $id, $role, $createdby);
//
//        $code = $this->randomString(20);
//
//        $mail = new PHPMailer;
//        // Set mailer to use SMTP
//        $mail->Host = $_SESSION["mail_host"];                                   // Specify main and backup SMTP servers
//        $mail->SMTPAuth = $_SESSION["SMTPAuth"];                                // Enable SMTP authentication
//        $mail->Username = $_SESSION["MUsername"];                               // SMTP username
//        $mail->Password = $_SESSION["MPassword"];                               // SMTP password
//        $mail->SMTPSecure = $_SESSION["SMTPSecure"];                            // Enable TLS encryption, `ssl` also accepted
//        $mail->Port = $_SESSION["Port"];                                        // TCP port to connect to
//        $mail->setFrom($_SESSION["MUsername"], $_SESSION["MUsernameFrom"]);
//
//        $mail->addAddress($email, $firstname);                  // Add a recipient
//        $mail->isHTML(true);                                    // Set email format to HTML
//        $mail->Subject = "New User Registration";
//        $mail->Body = "<html><body>"
//                . "<p><b>Hello " . $firstname . ",</b><br/>"
//                . "Your account has been successfully created. Your login credentials are as below. <br/>"
//                . "<ul>"
//                . "<li><b>Username: </b>" . $email . "</li>"
//                . "<li><b>Password: </b>" . $password . "</li>"
//                . "</ul>"
//                . "Click on this link: <a href=' " . $_SESSION['admin_url'] . "'>User Login</a> to proceed with the login. <br/>"
//                . "For any enquiries, kindly contact us on:   <br/>"
//                . "<ul>"
//                . "<li><b>Telephone Number(s): </b>" . $_SESSION['institution_phone'] . "</li>"
//                . "<li><b>Email Address: </b>" . $_SESSION["MUsername"] . "</li>"
//                . "</ul>"
//                . "Visit <a href='{$_SESSION['website_url']}'>" . $_SESSION['displayed_website_link'] . "</a> for more information.<br/>"
//                . "</body></html>";
//        $mail->AltBody = $_SESSION["AltBody"];
//        if ($mail->send()) {
//            return true;
//        } else {
//            return false;
//        }
    }    
    
    public function getAllTrainees() {
        $sql = "SELECT * FROM trainees ORDER BY id ASC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("id" => $data['id'], "firstname" => $data['firstname'], "lastname" => $data['lastname'], "gender" => $data['gender'], "birth_date" => $data['birth_date'], "status" => $data['status'], "createdat" => $data['createdat'], "createdby" => $data['createdby'], "lastmodifiedat" => $data['lastmodifiedat'], "lastmodifiedby" => $data['lastmodifiedby']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function fetchTraineeDetails($code) {
        $sql = "SELECT * FROM trainees WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }
        
    public function getNextTraineeId() {
        $trainee_id = $this->executeQuery("SELECT max(id) as trainee_id_max FROM trainees");
        $trainee_id = $trainee_id[0]['trainee_id_max'] + 1;
        return $trainee_id;
    }
    
    public function randomString($length) {
        $original_string = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
        $original_string = implode("", $original_string);
        return substr(str_shuffle($original_string), 0, $length);
    }


}
