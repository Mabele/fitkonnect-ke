<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/System_Administration.php";
$system_administration = new System_Administration();
$_SESSION['system_status_code'] = $_GET['code'];
$details = $system_administration->fetchSystemStatusCodeDetails($_SESSION['system_status_code']);

if (!empty($_POST)) {
    $success = $system_administration->execute();
    if (is_bool($success) && $success == true) {
        $_SESSION['update_success'] = true;
        $_SESSION['feedback_message'] = "<strong>Successful:</strong> The system status code record has been updated successfully.";
        App::redirectTo("?view_system_status_codes");
    } else {
        $_SESSION['update_fail'] = true;
        $_SESSION['feedback_message'] = "<strong>Error!</strong> There was an error updating the system status code record. Please try again.";
        App::redirectTo("?update_system_status_code_details&code={$_SESSION['system_status_code']}");
    }
}
?>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="POST">
            <input type="hidden" name="action" value="edit_system_status_code"/>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Updating System Status Code Details</strong></h3> <h3 class="panel-title"> || </h3>
                    <a href="?view_system_status_codes"><h3 class="panel-title">System Status Codes </h3></a>

                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">         
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Status Code</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="status_code" value="<?php echo $details['status_code']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Unique short code to identify the status entry.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Description</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <textarea type="text" rows="5" name="description" class="form-control"><?php echo $details['description']; ?></textarea>
                                    </div>                                            
                                    <span class="help-block">A brief description of the status entry.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Display Text</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="display_value" value="<?php echo $details['display_value']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Text to be displayed when referring to this status entry.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <!--<button class="btn btn-default">Clear Form</button>-->                                    
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form>

    </div>
</div>              