<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/System_Administration.php";
$system_administration = new System_Administration();
$_SESSION['ward'] = $_GET['code'];
$details = $system_administration->fetchWardDetails($_SESSION['ward']);

if (!empty($_POST)) {
    $success = $system_administration->execute();
    if (is_bool($success) && $success == true) {
        $_SESSION['update_success'] = true;
        $_SESSION['feedback_message'] = "<strong>Successful:</strong> The ward record has been updated successfully.";
        App::redirectTo("?view_wards");
    } else {
        $_SESSION['update_fail'] = true;
        $_SESSION['feedback_message'] = "<strong>Error!</strong> There was an error updating the ward record. Please try again.";
        App::redirectTo("?update_ward_details&code={$_SESSION['ward']}");
    }
}
?>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="POST">
            <input type="hidden" name="action" value="edit_ward"/>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Updating Ward Details</strong></h3> <h3 class="panel-title"> || </h3>
                    <a href="?view_wards"><h3 class="panel-title">Wards </h3></a>

                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">         
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Ward Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="name" value="<?php echo $details['name']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Name assigned to the ward.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">County</label>
                                <div class="col-md-6 col-xs-12">                                                                                            
                                    <select  name="county"class="form-control select">
                                        <?php echo $system_administration->getCounties(); ?>
                                    </select>
                                    <span class="help-block">List of Counties(Select)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <!--<button class="btn btn-default">Clear Form</button>-->                                    
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form>

    </div>
</div>              