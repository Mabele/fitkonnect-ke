<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Training_Facilities.php";
$training_facilities = new Training_Facilities();
$_SESSION['training_facility_service'] = $_GET['code'];
$details = $training_facilities->fetchTrainingFacilityServiceDetails($_SESSION['training_facility_service']);

if (!empty($_POST)) {
    $success = $training_facilities->execute();
    if (is_bool($success) && $success == true) {
        $_SESSION['update_success'] = true;
        $_SESSION['feedback_message'] = "<strong>Successful:</strong> The training facility service record has been updated successfully.";
        App::redirectTo("?view_training_facility_services");
    } else {
        $_SESSION['update_fail'] = true;
        $_SESSION['feedback_message'] = "<strong>Error!</strong> There was an error updating the training facility service record. Please try again.";
        App::redirectTo("?update_training_facility_service_details&code={$_SESSION['training_facility_service']}");
    }
}
?>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="POST">
            <input type="hidden" name="action" value="edit_training_facility_service"/>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Updating County Details</strong></h3> <h3 class="panel-title"> || </h3>
                    <a href="?view_training_facility_services"><h3 class="panel-title">Training Facility Services </h3></a>

                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">         
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Service Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="name" value="<?php echo $details['name']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Name assigned to the facility service.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <!--<button class="btn btn-default">Clear Form</button>-->                                    
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form>

    </div>
</div>              