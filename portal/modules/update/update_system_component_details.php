<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/System_Administration.php";
$system_administration = new System_Administration();
$_SESSION['system_component'] = $_GET['code'];
$details = $system_administration->fetchSystemComponentDetails($_SESSION['system_component']);

if (!empty($_POST)) {
    $success = $system_administration->execute();
    if (is_bool($success) && $success == true) {
        $_SESSION['update_success'] = true;
        $_SESSION['feedback_message'] = "<strong>Successful:</strong> The system component record has been updated successfully.";
        App::redirectTo("?view_system_components");
    } else {
        $_SESSION['update_fail'] = true;
        $_SESSION['feedback_message'] = "<strong>Error!</strong> There was an error updating the system component record. Please try again.";
        App::redirectTo("?update_system_component_details&code={$_SESSION['system_component']}");
    }
}
?>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="POST">
            <input type="hidden" name="action" value="edit_system_component"/>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Updating County Details</strong></h3> <h3 class="panel-title"> || </h3>
                    <a href="?view_system_components"><h3 class="panel-title">System Components </h3></a>

                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">         
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Component Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="name" value="<?php echo $details['name']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Name assigned to the system component.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Acronym</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="acronym" value="<?php echo $details['acronym']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Unique short code to identify the system component.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <!--<button class="btn btn-default">Clear Form</button>-->                                    
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form>

    </div>
</div>              