

<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/Staff.php";
require_once WPATH . "modules/classes/Trainers.php";
require_once WPATH . "modules/classes/Trainees.php";
require_once WPATH . "modules/classes/Training_Facilities.php";
require_once WPATH . "modules/classes/System_Administration.php";
$system_administration = new System_Administration();
$training_facilities = new Training_Facilities();
$trainees = new Trainees();
$trainers = new Trainers();
$staff = new Staff();
$users_management = new Users_Management();
$details = $users_management->fetchContactDetails($_SESSION['contact']);

if (!empty($_POST)) {
    $success = $users_management->execute();
    if (is_bool($success) && $success == true) {
        $_SESSION['update_success'] = true;
        $_SESSION['feedback_message'] = "<strong>Successful:</strong> The staff record has been updated successfully.";
        App::redirectTo("?view_contacts");
    } else {
        $_SESSION['update_fail'] = true;
        $_SESSION['feedback_message'] = "<strong>Error!</strong> There was an error updating the contact record. Please try again.";
        App::redirectTo("?update_contact_details&code={$_SESSION['contact']}");
    }
}
?>

<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="POST">
            <input type="hidden" name="action" value="edit_contact"/>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Updating Staff Details</strong></h3> <h3 class="panel-title"> || </h3>
                    <a href="?view_staff"><h3 class="panel-title">Staff Members </h3></a>

                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">         
                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group">
                                <label class="col-md-3 control-label">Website</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-globe"></span></span>
                                        <input type="text" name="website" value="<?php echo $details['website']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Institution's/User's Website.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Phone Number</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                        <input type="tel" name="phone_number1" value="<?php echo $details['phone_number1']; ?>" class="form-control" required=""/>
                                    </div>                                            
                                    <span class="help-block">Institution's/User's Phone number.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Alternative Phone Number</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                        <input type="tel" name="phone_number2" value="<?php echo $details['phone_number2']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Institution's/User's Alternative Phone number.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Email</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
                                        <input type="email" name="email" value="<?php echo $details['email']; ?>" class="form-control" required=""/>
                                    </div>                                            
                                    <span class="help-block">Institution's/User's email.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Postal Number</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
                                        <input type="text" name="postal_number" value="<?php echo $details['postal_number']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Institution's/User's Postal Number.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Postal Code</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-code"></span></span>
                                        <input type="text" name="postal_code" value="<?php echo $details['postal_code']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Institution's/User's Postal Code.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Postal Town</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-building"></span></span>
                                        <input type="text" name="postal_town" value="<?php echo $details['postal_town']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Institution's/User's Postal Town.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Physical Address - County</label>
                                <div class="col-md-9 col-xs-12">   
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-magnet"></span></span>
                                        <select  name="county" value="<?php // echo $details['county']; ?>" class="form-control">
                                            <?php echo $system_administration->getCounties(); ?>
                                        </select>
                                    </div>
                                    <span class="help-block">Institution's/User's County of residence.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Physical Address - Ward</label>
                                <div class="col-md-9 col-xs-12">   
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-magnet"></span></span>
                                        <select  name="ward" value="<?php // echo $details['ward']; ?>" class="form-control">
                                            <?php echo $system_administration->getWards(); ?>
                                        </select>
                                    </div>
                                    <span class="help-block">Institution's/User's Ward of residence.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <!--<button class="btn btn-default">Clear Form</button>-->                                    
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form>

    </div>
</div>              