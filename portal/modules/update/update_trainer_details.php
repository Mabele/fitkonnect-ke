<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Trainers.php";
require_once WPATH . "modules/classes/Trainees.php";
$trainees = new Trainees();
$trainers = new Trainers();

if (isset($_GET['code'])) {
    $_SESSION['trainer'] = $_GET['code'];
}

$details = $trainers->fetchTrainerDetails($_SESSION['trainer']);

if (!empty($_POST)) {
    $filename1 = md5('' . $trainees->randomString(10) . time());
    $curriculum_vitae_name = $_FILES['curriculum_vitae']['name'];
    $tmp_name_curriculum_vitae = $_FILES['curriculum_vitae']['tmp_name'];
    $extension_curriculum_vitae = substr($curriculum_vitae_name, strpos($curriculum_vitae_name, '.') + 1);
    $curriculum_vitae = strtoupper($filename1 . '.' . $extension_curriculum_vitae);
    $_SESSION['curriculum_vitae_filename'] = $curriculum_vitae;
    $location1 = 'img/trainers/CVs/';

    $filename2 = md5('' . $trainees->randomString(10) . time());
    $certificate_name = $_FILES['certificate']['name'];
    $tmp_name_certificate = $_FILES['certificate']['tmp_name'];
    $extension_certificate = substr($certificate_name, strpos($certificate_name, '.') + 1);
    $certificate = strtoupper($filename2 . '.' . $extension_certificate);
    $_SESSION['certificate_filename'] = $certificate;
    $location2 = 'img/trainers/certificates/';

    if (move_uploaded_file($tmp_name_curriculum_vitae, $location1 . $curriculum_vitae) AND move_uploaded_file($tmp_name_certificate, $location2 . $certificate)) {
        $success = $trainers->execute();
        if (is_bool($success) && $success == true) {
            $_SESSION['update_success'] = true;
            $_SESSION['feedback_message'] = "<strong>Successful:</strong> The trainer record has been updated successfully.";
            App::redirectTo("?view_trainers");
        } else {
            $_SESSION['update_fail'] = true;
            $_SESSION['feedback_message'] = "<strong>Error!</strong> There was an error updating the trainer record. Please try again.";
            App::redirectTo("?update_trainer_details&code={$_SESSION['trainer']}");
        }
    } else {
        $_SESSION['update_fail'] = true;
        $_SESSION['feedback_message'] = "<strong>Error!</strong> There was an error uploading attachments. Please try again.";
        App::redirectTo("?update_trainer_details&code={$_SESSION['trainer']}");
    }
}
?>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="action" value="edit_trainer"/>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Updating Trainer Details</strong></h3> <h3 class="panel-title"> || </h3>
                    <a href="?view_trainers"><h3 class="panel-title">Trainers </h3></a>

                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body"> 
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">First Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="firstname" value="<?php echo $details['firstname']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">First name of trainer.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Last Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="lastname" value="<?php echo $details['lastname']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Last name of trainer.</span>
                                </div>
                            </div>
                            <div class="form-group">                                        
                                <label class="col-md-3 control-label">Gender</label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-bolt"></span></span>
                                        <select  name="gender"class="form-control">
                                            <?php include '../snippets/gender.php'; ?>
                                        </select>
                                    </div>            
                                    <span class="help-block">Gender, please select.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">ID Number</label>
                                <div class="col-md-9 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-copy"></span></span>
                                        <input type="text" name="idnumber" value="<?php echo $details['idnumber']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Registered ID number.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Birth Date</label>
                                <div class="col-md-9 col-xs-12">   
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        <input type="text" name="birth_date" class="form-control datepicker" value="<?php echo $details['birth_date']; ?>"> 
                                    </div>
                                    <span class="help-block">Select Date</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Training Level</label>
                                <div class="col-md-9 col-xs-12">                                                                                            
                                    <select name="training_level" class="form-control">
                                        <option value="CERTIFICATE">CERTIFICATE</option>
                                        <option value="DIPLOMA">DIPLOMA</option>
                                        <option value="GRADUATE">GRADUATE</option>
                                        <option value="POST-GRADUATE">POST-GRADUATE</option>
                                    </select>
                                    <span class="help-block">The rating of service delivery accorded to the training facility(Select)</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Attach Certificate</label>
                                <div class="col-md-9">                                                                                                                                        
                                    <input type="file" class="btn-primary" name="certificate" value="<?php echo $details['certificate']; ?>" title="Select the certificate"/>
                                    <span class="help-block">Upload your certificate</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Attach Curriculum Vitae</label>
                                <div class="col-md-9">                                                                                                                                        
                                    <input type="file" class="btn-primary" name="curriculum_vitae" value="<?php echo $details['curriculum_vitae']; ?>" title="Select a CV"/>
                                    <span class="help-block">Upload your CV in PDF format.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">First Guarantor</label>
                                <div class="col-md-9 col-xs-12">   
                                    <input type="text" name="guarantor1" value="<?php echo $details['guarantor1']; ?>" class="form-control"/>
<!--                                    <select name="guarantor1" class="form-control">
                                    <?php // $trainers->getTrainers();  ?>
                                    </select>-->
                                    <span class="help-block">(Select/input first guarantor)</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Second Guarantor</label>
                                <div class="col-md-9 col-xs-12">  
                                    <input type="text" name="guarantor2" value="<?php echo $details['guarantor2']; ?>" class="form-control"/>
<!--                                    <select name="guarantor2" class="form-control">
                                    <?php // $trainers->getTrainers();  ?>
                                    </select>-->
                                    <span class="help-block">(Select/input second guarantor)</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">First Facility</label>
                                <div class="col-md-9 col-xs-12">    
                                    <input type="text" name="facility1" value="<?php echo $details['facility1']; ?>" class="form-control"/>
<!--                                    <select name="facility1" class="form-control">
                                    <?php // $training_facilities->getTrainingFacilities();  ?>
                                    </select>-->
                                    <span class="help-block">(Select/input first facility)</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Second Facility</label>
                                <div class="col-md-9 col-xs-12">   
                                    <input type="text" name="facility2" value="<?php echo $details['facility2']; ?>" class="form-control"/>
<!--                                    <select name="facility2" class="form-control">
                                    <?php // $training_facilities->getTrainingFacilities();  ?>
                                    </select>-->
                                    <span class="help-block">(Select/input second facility)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <!--<button class="btn btn-default">Clear Form</button>-->                                    
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form>

    </div>
</div>              