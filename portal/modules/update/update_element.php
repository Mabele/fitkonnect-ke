<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Trainers.php";
require_once WPATH . "modules/classes/Transactions.php";
require_once WPATH . "modules/classes/Lessons.php";
require_once WPATH . "modules/classes/Users_Management.php";
$users_management = new Users_Management();
$lessons = new Lessons();
$transactions = new Transactions();
$trainers = new Trainers();
$code = $_GET['code'];
$update_type = $_GET['update_type'];
$item = $_GET['item'];

if ($update_type == "delete") {
    $success = $transactions->deleteRecord($item, $code);
    if ($item == 'contact') { App::redirectTo("?view_contacts"); }
    else if ($item == 'county') { App::redirectTo("?view_counties"); }
    else if ($item == 'lesson_booking') { App::redirectTo("?view_lesson_bookings_details"); }
    else if ($item == 'lesson_category') { App::redirectTo("?view_lesson_categories"); }
    else if ($item == 'lesson') { App::redirectTo("?view_lessons"); }
    else if ($item == 'staff') { App::redirectTo("?view_staff"); }
    else if ($item == 'staff_position') { App::redirectTo("?view_staff_positions"); }
    else if ($item == 'system_administrator') { App::redirectTo("?view_system_administrators"); }
    else if ($item == 'system_component') { App::redirectTo("?view_system_components"); }
    else if ($item == 'system_privilege') { App::redirectTo("?view_system_privileges"); }
    else if ($item == 'system_role') { App::redirectTo("?view_system_roles"); }
    else if ($item == 'trainee') { App::redirectTo("?view_trainees"); }
    else if ($item == 'trainer') { App::redirectTo("?view_trainers"); }
    else if ($item == 'training_facility') { App::redirectTo("?view_training_facilities"); }
    else if ($item == 'training_facility_administrator') { App::redirectTo("?view_training_facility_administrators"); }
    else if ($item == 'training_facility_service') { App::redirectTo("?view_training_facility_services"); }
    else if ($item == 'user_type') { App::redirectTo("?view_user_types"); }
    else if ($item == 'ward') { App::redirectTo("?view_wards"); }
} else if ($update_type == "deactivate") {
    $success = $transactions->deactivateRecord($item, $code);
    if ($item == 'contact') { App::redirectTo("?view_contacts"); }
    else if ($item == 'county') { App::redirectTo("?view_counties"); }
    else if ($item == 'lesson_booking') { App::redirectTo("?view_lesson_bookings_details"); }
    else if ($item == 'lesson_category') { App::redirectTo("?view_lesson_categories"); }
    else if ($item == 'lesson') { App::redirectTo("?view_lessons"); }
    else if ($item == 'staff') { App::redirectTo("?view_staff"); }
    else if ($item == 'staff_position') { App::redirectTo("?view_staff_positions"); }
    else if ($item == 'system_administrator') { App::redirectTo("?view_system_administrators"); }
    else if ($item == 'system_component') { App::redirectTo("?view_system_components"); }
    else if ($item == 'system_privilege') { App::redirectTo("?view_system_privileges"); }
    else if ($item == 'system_role') { App::redirectTo("?view_system_roles"); }
    else if ($item == 'trainee') { App::redirectTo("?view_trainees"); }
    else if ($item == 'trainer') { App::redirectTo("?view_trainers"); }
    else if ($item == 'training_facility') { App::redirectTo("?view_training_facilities"); }
    else if ($item == 'training_facility_administrator') { App::redirectTo("?view_training_facility_administrators"); }
    else if ($item == 'training_facility_service') { App::redirectTo("?view_training_facility_services"); }
    else if ($item == 'user_type') { App::redirectTo("?view_user_types"); }
    else if ($item == 'ward') { App::redirectTo("?view_wards"); }    
} else if ($update_type == "activate") {
    $success = $transactions->activateRecord($item, $code);
    if ($item == 'contact') { App::redirectTo("?view_contacts"); }
    else if ($item == 'county') { App::redirectTo("?view_counties"); }
    else if ($item == 'lesson_booking') { App::redirectTo("?view_lesson_bookings_details"); }
    else if ($item == 'lesson_category') { App::redirectTo("?view_lesson_categories"); }
    else if ($item == 'lesson') { App::redirectTo("?view_lessons"); }
    else if ($item == 'staff') { App::redirectTo("?view_staff"); }
    else if ($item == 'staff_position') { App::redirectTo("?view_staff_positions"); }
    else if ($item == 'system_administrator') { App::redirectTo("?view_system_administrators"); }
    else if ($item == 'system_component') { App::redirectTo("?view_system_components"); }
    else if ($item == 'system_privilege') { App::redirectTo("?view_system_privileges"); }
    else if ($item == 'system_role') { App::redirectTo("?view_system_roles"); }
    else if ($item == 'trainee') { App::redirectTo("?view_trainees"); }
    else if ($item == 'trainer') { App::redirectTo("?view_trainers"); }
    else if ($item == 'training_facility') { App::redirectTo("?view_training_facilities"); }
    else if ($item == 'training_facility_administrator') { App::redirectTo("?view_training_facility_administrators"); }
    else if ($item == 'training_facility_service') { App::redirectTo("?view_training_facility_services"); }
    else if ($item == 'user_type') { App::redirectTo("?view_user_types"); }
    else if ($item == 'ward') { App::redirectTo("?view_wards"); }
} else if ($update_type == "approve") {
    if ($item == 'lesson_booking') {
        if (!empty($_POST)) {
            $trainer_user_id = $users_management->getUserId($users_management->getUserRefTypeId("TRAINER"), $_POST['assigned_trainer']);
            $success = $transactions->approveLessonBookingTransaction($code, $_POST['transaction_id'], $_POST['approval_comment'], $trainer_user_id);
            App::redirectTo("?view_lesson_bookings_details");
        }
    } else if ($item == 'trainer_acceptance') {
        if (!empty($_POST)) {
            $success = $transactions->approveTrainingRequest($code, $_POST['transaction_id'], $_POST['approval_comment']);
            App::redirectTo("?view_lesson_bookings_details");
        }
    }
} else if ($update_type == "reject") {
    if ($item == 'lesson_booking') {
        if (!empty($_POST)) {
            $success = $transactions->rejectLessonBookingTransaction($code, $_POST['approval_comment']);
            App::redirectTo("?view_lesson_bookings_details");
        }
    } else if ($item == 'trainer_acceptance') {
        if (!empty($_POST)) {
            $success = $transactions->rejectTrainingRequest($code, $_POST['approval_comment']);
            App::redirectTo("?view_lesson_bookings_details");
        }
    }
} else if ($update_type == "close") {
    $success = $transactions->closeRecord($item, $code);
    if ($item == 'inbox_message') { App::redirectTo("?view_inbox_messages"); }
//    else if ($item == 'county') { App::redirectTo("?view_counties"); }
    
//    if ($item == 'lesson_training') {
//        $transaction_details = $transactions->fetchBookingTransactionDetailDetails($code);
//        $success = $transactions->closeLessonTraining($code, $transaction_details['transaction_id']);
//        App::redirectTo("?view_my_assigned_lessons");
//    } else if ($item == 'inbox_message') {
//        $success = $transactions->closeInboxMessage($code);
//        App::redirectTo("?view_inbox_messages");
//    }
}
?>

<?php
if ($update_type == "approve" AND $item == 'lesson_booking') {
    $transaction_details = $transactions->fetchBookingTransactionDetailDetails($code);
    $lesson_details = $lessons->fetchLessonDetails($transaction_details['lesson']);
    $lesson_category_details = $lessons->fetchLessonCategoryDetails($lesson_details['category']);
    ?>
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal" method="POST">
                <input type="hidden" name="transaction_id" value="<?php echo $transaction_details['transaction_id']; ?>"/>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Approve Lesson Booking Transaction</strong></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">       
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Transaction Details</label>
                            <div class="col-md-6 col-xs-12">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                    <input type="text" name="transaction_details" value="<?php echo $transaction_details['transaction_id'] . ' : ' . $lesson_category_details['name'] . ' - ' . $lesson_details['name']; ?>" class="form-control" readonly/>
                                </div> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Assigned Trainer</label>
                            <div class="col-md-6 col-xs-12">                
                                <select name="assigned_trainer"class="form-control">
                                    <?php echo $trainers->getTrainers(); ?>
                                </select>
                                <span class="help-block">List of Trainers(Select)</span>
                            </div>
                        </div>     
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Assignment Comment</label>
                            <div class="col-md-6 col-xs-12">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <textarea class="form-control" type="text" rows="5" name="approval_comment" placeholder="Please note that......" required></textarea>
                                </div>                                            
                                <span class="help-block">A brief description of the assignment</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <!--<button class="btn btn-default">Clear Form</button>-->                                    
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
        </div>
    </form>
    </div>
    </div>    
    <?php
} else if ($update_type == "approve" AND $item == 'trainer_acceptance') {
    $transaction_details = $transactions->fetchBookingTransactionDetailDetails($code);
    $lesson_details = $lessons->fetchLessonDetails($transaction_details['lesson']);
    $lesson_category_details = $lessons->fetchLessonCategoryDetails($lesson_details['category']);
    ?>
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal" method="POST">
                <input type="hidden" name="transaction_id" value="<?php echo $transaction_details['transaction_id']; ?>"/>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Approve Lesson Booking Transaction</strong></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">       
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Transaction Details</label>
                            <div class="col-md-6 col-xs-12">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                    <input type="text" name="transaction_details" value="<?php echo $transaction_details['transaction_id'] . ' : ' . $lesson_category_details['name'] . ' - ' . $lesson_details['name']; ?>" class="form-control" readonly/>
                                </div> 
                            </div>
                        </div>  
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Approval Comment</label>
                            <div class="col-md-6 col-xs-12">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <textarea class="form-control" type="text" rows="5" name="approval_comment" placeholder="Please note that......" required></textarea>
                                </div>                                            
                                <span class="help-block">A brief comment</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <!--<button class="btn btn-default">Clear Form</button>-->                                    
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
        </div>
    </form>
    </div>
    </div>    
<?php } else if ($update_type == "reject" AND $item == 'lesson_booking') { ?>
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal" method="POST">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Reject Transaction </strong></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">     
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Rejection Comment</label>
                            <div class="col-md-6 col-xs-12">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <textarea class="form-control" type="text" rows="5" name="approval_comment" placeholder="This transaction has been rejected because ......" required></textarea>
                                </div>                                            
                                <span class="help-block">A brief description of the role.</span>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <!--<button class="btn btn-default">Clear Form</button>-->                                    
                        <button class="btn btn-primary pull-right">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>    
<?php } else if ($update_type == "reject" AND $item == 'trainer_acceptance') { ?>
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal" method="POST">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Reject Training Request</strong></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">     
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Rejection Comment</label>
                            <div class="col-md-6 col-xs-12">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <textarea class="form-control" type="text" rows="5" name="approval_comment" placeholder="I reject this lesson because ......" required></textarea>
                                </div>                                            
                                <span class="help-block">A brief description of the rejection reason.</span>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <!--<button class="btn btn-default">Clear Form</button>-->                                    
                        <button class="btn btn-primary pull-right">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>    
<?php } ?>

