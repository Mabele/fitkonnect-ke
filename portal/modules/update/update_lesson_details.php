<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Lessons.php";
require_once WPATH . "modules/classes/Training_Facilities.php";
$training_facilities = new Training_Facilities();
$lessons = new Lessons();

if(isset($_GET['code'])) {
    $_SESSION['lesson'] = $_GET['code'];
}

$details = $lessons->fetchLessonDetails($_SESSION['lesson']);

if (!empty($_POST)) {
    $success = $lessons->execute();
    if (is_bool($success) && $success == true) {
        $_SESSION['update_success'] = true;
        $_SESSION['feedback_message'] = "<strong>Successful:</strong> The lesson record has been updated successfully.";
        App::redirectTo("?view_lessons");
    } else {
        $_SESSION['update_fail'] = true;
        $_SESSION['feedback_message'] = "<strong>Error!</strong> There was an error updating the lesson record. Please try again.";
        App::redirectTo("?update_lesson_details&code={$_SESSION['lesson']}");
    }
}
?>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="POST">
            <input type="hidden" name="action" value="edit_lesson"/>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Updating Lesson Details</strong></h3> <h3 class="panel-title"> || </h3>
                    <a href="?view_lessons"><h3 class="panel-title">Lessons </h3></a>

                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body"> 
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-book"></span></span>
                                        <input type="text" name="name" value="<?php echo $details['name']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Name assigned to lesson.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Description</label>
                                <div class="col-md-9 col-xs-12">                                            
                                    <textarea class="form-control" name="description" rows="5"><?php echo $details['description']; ?></textarea>
                                    <span class="help-block">A brief description of the lesson</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Category</label>
                                <div class="col-md-9 col-xs-12"> 
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-mortar-board"></span></span>
                                        <select  name="category"class="form-control">
                                            <?php echo $lessons->getLessons(); ?>
                                        </select>
                                    </div>
                                    <span class="help-block">List of Categories associated with lesson(Select)</span>
                                </div>
                            </div>                            
                            <div class="form-group">                                        
                                <label class="col-md-3 control-label">Training Facility</label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-building"></span></span>
                                        <select  name="training_facility"class="form-control">
                                            <?php echo $training_facilities->getTrainingFacilities(); ?>
                                        </select>
                                    </div>            
                                    <span class="help-block">Trainer associated with the lesson.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">                                        
                                <label class="col-md-3 control-label">Start Time</label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
                                        <select  name="start_time"class="form-control">
                                            <option value="0100HOURS">0100 HOURS</option>
                                            <option value="0200HOURS">0200 HOURS</option>
                                            <option value="0300HOURS">0300 HOURS</option>
                                            <option value="0400HOURS">0400 HOURS</option>
                                            <option value="0500HOURS">0500 HOURS</option>
                                            <option value="0600HOURS">0600 HOURS</option>
                                            <option value="0700HOURS">0700 HOURS</option>
                                            <option value="0800HOURS">0800 HOURS</option>
                                            <option value="0900HOURS">0900 HOURS</option>
                                            <option value="1000HOURS">1000 HOURS</option>
                                            <option value="1100HOURS">1100 HOURS</option>
                                            <option value="1200HOURS">1200 HOURS</option>
                                            <option value="1300HOURS">1300 HOURS</option>
                                            <option value="1400HOURS">1400 HOURS</option>
                                            <option value="1500HOURS">1500 HOURS</option>
                                            <option value="1600HOURS">1600 HOURS</option>
                                            <option value="1700HOURS">1700 HOURS</option>
                                            <option value="1800HOURS">1800 HOURS</option>
                                            <option value="1900HOURS">1900 HOURS</option>
                                            <option value="2000HOURS">2000 HOURS</option>
                                            <option value="2100HOURS">2100 HOURS</option>
                                            <option value="2200HOURS">2200 HOURS</option>
                                            <option value="2300HOURS">2300 HOURS</option>
                                            <option value="2400HOURS">2400 HOURS</option>
                                        </select>
                                    </div>            
                                    <span class="help-block">Time of day when the lesson is scheduled to start.</span>
                                </div>
                            </div>
                            <div class="form-group">                                        
                                <label class="col-md-3 control-label">End Time</label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
                                        <select  name="end_time"class="form-control">
                                            <option value="0100HOURS">0100 HOURS</option>
                                            <option value="0200HOURS">0200 HOURS</option>
                                            <option value="0300HOURS">0300 HOURS</option>
                                            <option value="0400HOURS">0400 HOURS</option>
                                            <option value="0500HOURS">0500 HOURS</option>
                                            <option value="0600HOURS">0600 HOURS</option>
                                            <option value="0700HOURS">0700 HOURS</option>
                                            <option value="0800HOURS">0800 HOURS</option>
                                            <option value="0900HOURS">0900 HOURS</option>
                                            <option value="1000HOURS">1000 HOURS</option>
                                            <option value="1100HOURS">1100 HOURS</option>
                                            <option value="1200HOURS">1200 HOURS</option>
                                            <option value="1300HOURS">1300 HOURS</option>
                                            <option value="1400HOURS">1400 HOURS</option>
                                            <option value="1500HOURS">1500 HOURS</option>
                                            <option value="1600HOURS">1600 HOURS</option>
                                            <option value="1700HOURS">1700 HOURS</option>
                                            <option value="1800HOURS">1800 HOURS</option>
                                            <option value="1900HOURS">1900 HOURS</option>
                                            <option value="2000HOURS">2000 HOURS</option>
                                            <option value="2100HOURS">2100 HOURS</option>
                                            <option value="2200HOURS">2200 HOURS</option>
                                            <option value="2300HOURS">2300 HOURS</option>
                                            <option value="2400HOURS">2400 HOURS</option>
                                        </select>
                                    </div>            
                                    <span class="help-block">Time of day when the lesson is scheduled to start.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Price</label>
                                <div class="col-md-9 col-xs-12">   
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-usd"></span></span>
                                        <input type="number" name="price" value="<?php echo $details['price']; ?>" class="form-control"> 
                                    </div>
                                    <span class="help-block">Per trainee fee charged for one session of this lesson.</span>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="col-md-3 control-label">Capacity</label>
                                <div class="col-md-9 col-xs-12">   
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-group"></span></span>
                                        <input type="number" name="capacity" value="<?php echo $details['capacity']; ?>" class="form-control"> 
                                    </div>
                                    <span class="help-block">Maximum number of trainees that can be handled within one session of this lesson.</span>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="col-md-3 control-label">Current Population</label>
                                <div class="col-md-9 col-xs-12">   
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-group"></span></span>
                                        <input type="number" name="current_population" value="<?php echo $details['current_population']; ?>" class="form-control"> 
                                    </div>
                                    <span class="help-block">Maximum number of trainees that can be handled within one session of this lesson</span>
                                </div>
                            </div> 
                        </div>
                    </div>                   
                </div>
                <div class="panel-footer">
                    <!--<button class="btn btn-default">Clear Form</button>-->                                    
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form>

    </div>
</div>              