<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Staff.php";
require_once WPATH . "modules/classes/Users_Management.php";
$users_management = new Users_Management();
$staff = new Staff();
$details = $staff->fetchStaffDetails($_SESSION['staff']);

if (!empty($_POST)) {
    $success = $staff->execute();
    if (is_bool($success) && $success == true) {
        $_SESSION['update_success'] = true;
        $_SESSION['feedback_message'] = "<strong>Successful:</strong> The staff record has been updated successfully.";
        App::redirectTo("?view_staff");
    } else {
        $_SESSION['update_fail'] = true;
        $_SESSION['feedback_message'] = "<strong>Error!</strong> There was an error updating the staff record. Please try again.";
        App::redirectTo("?update_staff_details&code={$_SESSION['staff']}");
    }
}
?>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="POST">
            <input type="hidden" name="action" value="edit_staff"/>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Updating Staff Details</strong></h3> <h3 class="panel-title"> || </h3>
                    <a href="?view_staff"><h3 class="panel-title">Staff Members </h3></a>

                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">         
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">PF Number</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="pf_number" value="<?php echo $details['pf_number']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Staff's PF number.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">First Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="firstname" value="<?php echo $details['firstname']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">First name of staff.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Middle Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="middlename" value="<?php echo $details['middlename']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Middle name of staff.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Last Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="lastname" value="<?php echo $details['lastname']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Last name of staff.</span>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">                                        
                                <label class="col-md-3 control-label">Gender</label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-bolt"></span></span>
                                        <select  name="gender"class="form-control">
                                            <?php include '../snippets/gender.php'; ?>
                                        </select>
                                    </div>            
                                    <span class="help-block">Gender, please select.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">ID Number</label>
                                <div class="col-md-9 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-copy"></span></span>
                                        <input type="text" name="idnumber" value="<?php echo $details['idnumber']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Registered ID number.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Position</label>
                                <div class="col-md-9 col-xs-12">   
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cube"></span></span>
                                        <select  name="position"class="form-control">
                                            <?php echo $staff->getStaffPositions(); ?>
                                        </select>
                                    </div>
                                    <span class="help-block">List of potential Positions</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <!--<button class="btn btn-default">Clear Form</button>-->                                    
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form>

    </div>
</div>              