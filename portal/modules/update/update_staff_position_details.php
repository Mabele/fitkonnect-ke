<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Staff.php";
$staff = new Staff();
$_SESSION['staff_position'] = $_GET['code'];
$details = $staff->fetchStaffPositionDetails($_SESSION['staff_position']);

if (!empty($_POST)) {
    $success = $staff->execute();
    if (is_bool($success) && $success == true) {
        $_SESSION['update_success'] = true;
        $_SESSION['feedback_message'] = "<strong>Successful:</strong> The staff position record has been updated successfully.";
        App::redirectTo("?view_staff_positions");
    } else {
        $_SESSION['update_fail'] = true;
        $_SESSION['feedback_message'] = "<strong>Error!</strong> There was an error updating the staff position record. Please try again.";
        App::redirectTo("?update_staff_position_details&code={$_SESSION['staff_position']}");
    }
}
?>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="POST">
            <input type="hidden" name="action" value="edit_staff_position"/>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Updating Staff Position Details</strong></h3> <h3 class="panel-title"> || </h3>
                    <a href="?view_staff_positions"><h3 class="panel-title">Staff Positions </h3></a>

                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">         
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Position Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" name="name" value="<?php echo $details['name']; ?>" class="form-control"/>
                                    </div>                                            
                                    <span class="help-block">Name assigned to the staff position.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <!--<button class="btn btn-default">Clear Form</button>-->                                    
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form>

    </div>
</div>              