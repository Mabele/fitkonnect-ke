<?php

//header("Content-Type:application/json");
require_once WPATH . "modules/classes/Transactions.php";
$transactions = new Transactions();

if (!isset($_GET["token"])) {
    echo "Technical error";
    exit();
}

if ($_GET["token"] != 'RefCon@2018_05') {
    echo "Invalid authorization";
    exit();
}

if (!$request = file_get_contents('php://input')) {
    echo "Invalid input";
    exit();
}

//Put the json string that we received from Safaricom to an array
$array = json_decode($request, true);

$transaction = $transactions->addMpesaPaymentTransaction($array);
if (is_bool($transaction) && $transaction == true) {
    echo '{"ResultCode":0,"ResultDesc":"Confirmation received successfully"}';
}
?>