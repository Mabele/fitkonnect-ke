<?php

$inputJSON = file_get_contents('php://input');

require_once WPATH . "modules/classes/Transactions.php";
$transactions = new Transactions();

//if ($_POST['password'] == '' && $_POST['username'] == '') {
if (isset($_POST['service_name']) && isset($_POST['signature'])) {
    $service_name = $_POST['service_name'];
    $business_number = $_POST['business_number'];
    $transaction_reference = $_POST['transaction_reference'];
    $internal_transaction_id = $_POST['internal_transaction_id'];
    $transaction_timestamp = $_POST['transaction_timestamp'];
    $transaction_type = $_POST['transaction_type'];
    $account_number = $_POST['account_number'];
    $sender_phone = $_POST['sender_phone'];
    $firstname = $_POST['first_name'];
    $middlename = $_POST['middle_name'];
    $lastname = $_POST['last_name'];
    $amount = $_POST['amount'];
    $currency = $_POST['currency'];
    $signature = $_POST['signature'];
}

$transaction = $transactions->addKopokopoPaymentTransaction($service_name, $business_number, $transaction_reference, $internal_transaction_id, $transaction_timestamp, $transaction_type, $account_number, $sender_phone, $firstname, $middlename, $lastname, $amount, $currency, $signature);

if (is_bool($transaction) && $transaction == true) {

    $data1 = array(
        'status' => '01',
        'description' => 'Accepted',
        'subscriber_message' => 'We have received your payment with transaction  ID .' . $transaction_reference
    );

    echo json_encode($data1);
}
//} else {
//    $valid = "invalid";
//}
?>
