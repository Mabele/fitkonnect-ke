<?php

require_once WPATH . "modules/classes/Transactions.php";

//require_once WPATH . "modules/classes/Transactions.php";

$transactions = new Transactions();

$success = $transactions->sendReminderSMSCronJob();
if (is_bool($success) && $success == true) {
    if (isset($_SESSION['sms_sent']) && $success == false) {
        $_SESSION['feedback_message'] = "There was an error sending the reminder SMS(s)";
    } else {
        $_SESSION['feedback_message'] = "The reminder SMSs were sent successfully";
    }
} else if (is_bool($success) && $success == false) {
    $_SESSION['feedback_message'] = "There are no due reminder SMSs";
}

argDump($_SESSION['feedback_message']);
?>