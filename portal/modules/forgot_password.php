
<?php
require_once WPATH . "modules/classes/Users_Management.php";
$users_management = new Users_Management();

if (!empty($_POST)) {
    $success = $users_management->execute();
    if (is_bool($success) && $success == true) {
        $_SESSION['update_pass_forgot'] = true;
    } else {
        $_SESSION['update_pass_forgot'] = false;
    }
}

if (isset($_SESSION['update_pass_forgot']) AND ( $_SESSION['update_pass_forgot'] == false)) {
    ?>
    <strong>Sorry, there was an error updating your password. Please confirm your email address and try the update process again.</strong>
    <?php
    unset($_SESSION['update_pass_forgot']);
} else if (isset($_SESSION['update_pass_forgot']) AND ( $_SESSION['update_pass_forgot'] == true)) {
    App::redirectTo("?login");
}
?> 

<div class="login-container">

    <div class="login-box animated fadeInDown">
        <div class="login-logo"></div>
        <div class="login-body">
            <div class="login-title"><strong>FitnessIQ</strong> Password Recovery</div>
            <form class="form-horizontal" method="POST">
                <input type="hidden" name="action" value="forgot_password"/>
                <div class="form-group">
                    <div class="col-md-12">
                        <input  type="email" name="email" class="form-control" placeholder="Email Address">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <a href="?login" class="btn btn-link btn-block">Just Login</a>
                    </div>
                    <div class="col-md-6">
                        <!--<input type="submit" value="Sign In" />-->
                        <button type="submit" class="btn btn-info btn-block">Reset Password</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="login-footer">
            <div class="pull-left">
                &copy; 2018 fitnessIQ | Coaches Portal
            </div>
            <div class="pull-right">
                <a href="#">About</a> |
                <a href="#">Privacy</a> |
                <a href="#">Contact Us</a>
            </div>
        </div>
    </div>

</div>