<?php

if (isset($_FILES['curriculum_vitae']['tmp_name'])) {
    $cvs_location = 'img/trainers/CVs/';
    $certificates_location = 'img/trainers/certificates/';
    $prof_pics_location = 'img/trainers/profile_pictures/';

    move_uploaded_file($_FILES['curriculum_vitae']['tmp_name'], $cvs_location . $_POST['curriculum_vitae_name']);
    move_uploaded_file($_FILES['certificate']['tmp_name'], $certificates_location . $_POST['certificate_name']);
    move_uploaded_file($_FILES['prof_picture']['tmp_name'], $prof_pics_location . $_POST['prof_picture_name']);
}
