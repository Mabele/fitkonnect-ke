<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/Lessons.php";
require_once WPATH . "modules/classes/Staff.php";
require_once WPATH . "modules/classes/System_Administration.php";
require_once WPATH . "modules/classes/Training_Facilities.php";
require_once WPATH . "modules/classes/Trainees.php";
require_once WPATH . "modules/classes/Trainers.php";
$trainers = new Trainers();
$trainees = new Trainees();
$training_facilities = new Training_Facilities();
$system_administration = new System_Administration();
$lessons = new Lessons();
$staff = new Staff();
$users_management = new Users_Management();
$code = $_GET['code'];
$_SESSION['lesson'] = $code;

$details = $lessons->fetchLessonDetails($code);
$createdby_details = $users_management->fetchUserDetails($details['createdby']);
$lastmodifiedby_details = $users_management->fetchUserDetails($details['lastmodifiedby']);
//$category_details = $lessons->fetchLessonCategoryDetails($details['category']);
//$training_facility_details = $training_facilities->fetchTrainingFacilityDetails($details['training_facility']);
$createdby_user_type_details = $users_management->fetchUserTypeDetails($createdby_details['reference_type']);
$lastmodifiedby_user_type_details = $users_management->fetchUserTypeDetails($lastmodifiedby_details['reference_type']);
$status_details = $system_administration->fetchSystemStatusDetails($details['status']);
$status = $status_details['display_value'];

if (!is_null($details['authorizedat'])) {
    $authorizedby_details = $users_management->fetchUserDetails($details['authorizedby']);
    $authorizedby_user_type_details = $users_management->fetchUserTypeDetails($authorizedby_details['reference_type']);
}

if ($createdby_user_type_details['name'] == "STAFF") {
    $createdby_owner_details = $staff->fetchStaffDetails($createdby_details['reference_id']);
    $createdby_owner_name = $createdby_owner_details['firstname'] . " " . $createdby_owner_details['lastname'];
} else if ($createdby_user_type_details['name'] == "TRAINEE") {
    $createdby_owner_details = $trainees->fetchTraineeDetails($createdby_details['reference_id']);
    $createdby_owner_name = $createdby_owner_details['firstname'] . " " . $createdby_owner_details['lastname'];
} else if ($createdby_user_type_details['name'] == "TRAINER") {
    $createdby_owner_details = $trainers->fetchTrainerDetails($createdby_details['reference_id']);
    $createdby_owner_name = $createdby_owner_details['firstname'] . " " . $createdby_owner_details['lastname'];
} else if ($createdby_user_type_details['name'] == "TRAINING FACILITY ADMINISTRATOR") {
    $createdby_owner_details = $training_facilities->fetchTrainingFacilityAdministratorDetails($createdby_details['reference_id']);
    $createdby_owner_name = $createdby_owner_details['firstname'] . " " . $createdby_owner_details['lastname'];
} else if ($createdby_user_type_details['name'] == "SYSTEM ADMINISTRATOR") {
    $createdby_owner_details = $system_administration->fetchSystemAdministratorDetails($createdby_details['reference_id']);
    $createdby_owner_name = $createdby_owner_details['firstname'] . " " . $createdby_owner_details['lastname'];
}

if ($lastmodifiedby_user_type_details['name'] == "STAFF") {
    $lastmodifiedby_owner_details = $staff->fetchStaffDetails($lastmodifiedby_details['reference_id']);
    $lastmodifiedby_owner_name = $lastmodifiedby_owner_details['firstname'] . " " . $lastmodifiedby_owner_details['lastname'];
} else if ($lastmodifiedby_user_type_details['name'] == "TRAINEE") {
    $lastmodifiedby_owner_details = $trainees->fetchTraineeDetails($lastmodifiedby_details['reference_id']);
    $lastmodifiedby_owner_name = $lastmodifiedby_owner_details['firstname'] . " " . $lastmodifiedby_owner_details['lastname'];
} else if ($lastmodifiedby_user_type_details['name'] == "TRAINER") {
    $lastmodifiedby_owner_details = $trainers->fetchTrainerDetails($lastmodifiedby_details['reference_id']);
    $lastmodifiedby_owner_name = $lastmodifiedby_owner_details['firstname'] . " " . $lastmodifiedby_owner_details['lastname'];
} else if ($lastmodifiedby_user_type_details['name'] == "TRAINING FACILITY ADMINISTRATOR") {
    $lastmodifiedby_owner_details = $training_facilities->fetchTrainingFacilityAdministratorDetails($lastmodifiedby_details['reference_id']);
    $lastmodifiedby_owner_name = $lastmodifiedby_owner_details['firstname'] . " " . $lastmodifiedby_owner_details['lastname'];
} else if ($lastmodifiedby_user_type_details['name'] == "SYSTEM ADMINISTRATOR") {
    $lastmodifiedby_owner_details = $system_administration->fetchSystemAdministratorDetails($lastmodifiedby_details['reference_id']);
    $lastmodifiedby_owner_name = $lastmodifiedby_owner_details['firstname'] . " " . $lastmodifiedby_owner_details['lastname'];
}

if (!is_null($details['authorizedat'])) {
    if ($authorizedby_user_type_details['name'] == "STAFF") {
        $authorizor_details = $staff->fetchStaffDetails($assignedby_details['reference_id']);
        $authorizedby_owner_name = $authorizor_details['firstname'] . " " . $authorizor_details['lastname'];
    } else if ($authorizedby_user_type_details['name'] == "TRAINEE") {
        $authorizor_details = $trainees->fetchTraineeDetails($assignedby_details['reference_id']);
        $authorizedby_owner_name = $authorizor_details['firstname'] . " " . $authorizor_details['lastname'];
    } else if ($authorizedby_user_type_details['name'] == "TRAINER") {
        $authorizor_details = $trainers->fetchTrainerDetails($assignedby_details['reference_id']);
        $authorizedby_owner_name = $authorizor_details['firstname'] . " " . $authorizor_details['lastname'];
    } else if ($authorizedby_user_type_details['name'] == "TRAINING FACILITY ADMINISTRATOR") {
        $authorizor_details = $training_facilities->fetchTrainingFacilityAdministratorDetails($assignedby_details['reference_id']);
        $authorizedby_owner_name = $authorizor_details['firstname'] . " " . $authorizor_details['lastname'];
    } else if ($authorizedby_user_type_details['name'] == "SYSTEM ADMINISTRATOR") {
        $authorizor_details = $system_administration->fetchSystemAdministratorDetails($assignedby_details['reference_id']);
        $authorizedby_owner_name = $authorizor_details['firstname'] . " " . $authorizor_details['lastname'];
    }
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">                                   
                <h3 class="panel-title"><?php echo $details['name'] . "(" . ucwords(strtolower($status)) . ") || "; ?> </h3>
                <?php
                if ($details['status'] != 1009) {
                    if ($details['status'] == 1002) {
                        ?>
                        <a href="?update_element&item=lesson&update_type=activate&code=<?php echo $code; ?>"><h3 class="panel-title">Activate</h3></a> <h3 class="panel-title"> || </h3>
                    <?php } else if ($details['status'] == 1001) { ?>
                        <a href="?update_element&item=lesson&update_type=deactivate&code=<?php echo $code; ?>"><h3 class="panel-title">Deactivate</h3></a> <h3 class="panel-title"> || </h3>
                    <?php } ?>
                    <a href="?update_element&item=lesson&update_type=delete&code=<?php echo $code; ?>"><h3 class="panel-title">Delete </h3></a> <h3 class="panel-title"> || </h3>                    
                    <a href="?update_lesson_details"><h3 class="panel-title">Edit </h3></a> <h3 class="panel-title"> || </h3>
                <?php } ?>
                <a href="?view_lessons"><h3 class="panel-title">Lessons </h3></a>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">  
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-5 control-label">Name</label>
                        <label class="col-md-5 control-label"><?php echo $details['name']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Description</label>
                        <label class="col-md-5 control-label"><?php echo $details['description']; ?></label>
                    </div>
<!--                    <div class="form-group">
                        <label class="col-md-5 control-label">Category</label>
                        <label class="col-md-5 control-label"><?php // echo $category_details['name']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Starting Time</label>
                        <label class="col-md-5 control-label"><?php // echo $details['start_time']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Ending Time</label>
                        <label class="col-md-5 control-label"><?php // echo $details['end_time']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Training Facility</label>
                        <label class="col-md-5 control-label"><?php // echo $training_facility_details['name']; ?></label>
                    </div>-->
                    <div class="form-group">
                        <label class="col-md-5 control-label">Price</label>
                        <label class="col-md-5 control-label"><?php echo $details['price']; ?></label>
                    </div>
<!--                    <div class="form-group">
                        <label class="col-md-5 control-label">Capacity</label>
                        <label class="col-md-5 control-label"><?php // echo $details['capacity']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Current Population</label>
                        <label class="col-md-5 control-label"><?php // echo $details['current_population']; ?></label>
                    </div>-->
                    <div class="form-group">
                        <label class="col-md-5 control-label">Created At</label>
                        <label class="col-md-5 control-label"><?php echo $details['createdat']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Created By</label>
                        <label class="col-md-5 control-label"><?php echo $createdby_owner_name; ?></label>
                    </div>

                    <?php if (!is_null($details['authorizedat'])) { ?>
                        <div class="form-group">
                            <label class="col-md-5 control-label">Authorized At</label>
                            <label class="col-md-5 control-label"><?php echo $details['authorizedat']; ?></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-5 control-label">Authorized By</label>
                            <label class="col-md-5 control-label"><?php echo $authorizedby_owner_name; ?></label>
                        </div>
                    <?php } ?>

                    <div class="form-group">
                        <label class="col-md-5 control-label">Last Modified At</label>
                        <label class="col-md-5 control-label"><?php echo $details['lastmodifiedat']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Last Modified By</label>
                        <label class="col-md-5 control-label"><?php echo $lastmodifiedby_owner_name; ?></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>              