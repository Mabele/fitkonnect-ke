<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/System_Administration.php";
$system_administration = new System_Administration();
$users_management = new Users_Management();

unset($_SESSION['contact']);
unset($_SESSION['search']);
?>

<div class="row">
    <div class="col-md-12">
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-heading">      

                <?php
                if (is_menu_set('view_sms') != "") {
                    $sms_type = "ALL";
                    ?>
                    <h3 class="panel-title">All Messages || </h3> 
                    <a href="?view_incoming_sms"><h3 class="panel-title">Inbox </h3></a> <h3 class="panel-title"> || </h3>
                    <a href="?view_outgoing_sms"><h3 class="panel-title">Outbox </h3></a> <h3 class="panel-title"> || </h3>
                    <?php
                } else if (is_menu_set('view_incoming_sms') != "") {
                    $sms_type = "INBOX";
                    ?>
                    <h3 class="panel-title">Incoming Messages || </h3>
                    <a href="?view_outgoing_sms"><h3 class="panel-title">Outbox </h3></a> <h3 class="panel-title"> || </h3>
                    <a href="?view_sms"><h3 class="panel-title">All Messages </h3></a> <h3 class="panel-title"> || </h3>
                    <?php
                } else if (is_menu_set('view_outgoing_sms') != "") {
                    $sms_type = "OUTBOX";
                    ?>
                    <h3 class="panel-title">Outgoing Messages || </h3>
                    <a href="?view_incoming_sms"><h3 class="panel-title">Inbox </h3></a> <h3 class="panel-title"> || </h3>
                    <a href="?view_sms"><h3 class="panel-title">All Messages </h3></a> <h3 class="panel-title"> || </h3>
                <?php } ?>
                <a href="?send_sms"><h3 class="panel-title">Send SMS </h3></a> <h3 class="panel-title"> || </h3>
                <a href="?cron_file"><h3 class="panel-title">Reminder SMS </h3></a> <h3 class="panel-title"> || </h3>
                <a href="?send_email"><h3 class="panel-title">Send Email</h3></a>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Phone Number</th>
                            <?php if (is_menu_set('view_sms') != "") { ?>
                                <th>SMS Type</th>
                            <?php } ?>
                            <th>Message ID</th>
                            <th>Message</th>
                            <th>Status</th>
                            <th>Created At</th>
                        </tr>
                    </thead>
                    <tbody>                        
                        <?php
                        $count = 1;
                        if (!empty($_POST)) {
                            $sms_messages[] = $users_management->execute();
                        } else {
                            $sms_messages[] = $users_management->getAllSMSMessages($sms_type);
                        }
                        if (isset($_SESSION['no_records']) AND $_SESSION['no_records'] == true) {
                            echo "<tr>";
                            echo "<td>  No record found...</td>";
                            echo "<td> </td>";
                            if (is_menu_set('view_sms') != "") {
                                echo "<td> </td>";
                            }
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "</tr>";
                            unset($_SESSION['no_records']);
                        } else if (isset($_SESSION['yes_records']) AND $_SESSION['yes_records'] == true) {
                            foreach ($sms_messages as $key => $value) {
                                $inner_array[$key] = json_decode($value, true); // this will give key val pair array
                                foreach ((array) $inner_array[$key] as $key2 => $value2) {
                                    if (is_null($value2['messageid'])) {
                                        $status = "NOT SENT";
                                    } else {
                                        $status = "SENT";
                                    }
                                    echo "<tr>";
                                    echo "<td>" . $count++ . "</td>";
                                    echo "<td>" . $value2['phone_number'] . "</td>";
                                    if (is_menu_set('view_sms') != "") {
                                        echo "<td>" . $value2['type'] . "</td>";
                                    }
                                    echo "<td>" . $value2['messageid'] . "</td>";
                                    echo "<td>" . $value2['message'] . "</td>";
                                    echo "<td>" . $status . "</td>";
                                    echo "<td>" . $value2['createdat'] . "</td>";
                                    echo "</tr>";
                                }
                            }
                            unset($_SESSION['yes_records']);
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>                                
