<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/Lessons.php";
require_once WPATH . "modules/classes/Staff.php";
require_once WPATH . "modules/classes/System_Administration.php";
require_once WPATH . "modules/classes/Training_Facilities.php";
require_once WPATH . "modules/classes/Trainees.php";
require_once WPATH . "modules/classes/Trainers.php";
require_once WPATH . "modules/classes/Transactions.php";
$transactions = new Transactions();
$trainers = new Trainers();
$trainees = new Trainees();
$training_facilities = new Training_Facilities();
$system_administration = new System_Administration();
$lessons = new Lessons();
$staff = new Staff();
$users_management = new Users_Management();
$code = $_GET['code'];
$_SESSION['lesson_booking'] = $code;

$details = $transactions->fetchBookingTransactionDetailDetails($code);
$lesson_details = $lessons->fetchLessonDetails($details['lesson']);
$training_facility_details = $training_facilities->fetchTrainingFacilityDetails($details['training_facility']);
$status_details = $system_administration->fetchSystemStatusDetails($details['status']);
$attendance_status_details = $system_administration->fetchSystemStatusDetails($details['attendance_status']);
$status = $status_details['display_value'];
$attendance_status = $attendance_status_details['display_value'];

if (!is_null($details['approvedat'])) {
    $approvedby_details = $users_management->fetchUserDetails($details['approvedby']);
    $approvedby_user_type_details = $users_management->fetchUserTypeDetails($approvedby_details['reference_type']);
    $assigned_trainer_details = $users_management->fetchUserDetails($details['assigned_trainer']);
    $assigned_trainer_owner_details = $trainers->fetchTrainerDetails($assigned_trainer_details['reference_id']);
    $assigned_trainer_owner_name = $assigned_trainer_owner_details['firstname'] . " " . $assigned_trainer_owner_details['lastname'];

    if ($details['is_reassigned'] == 'YES') {
        $new_assigned_trainer_details = $users_management->fetchUserDetails($details['new_trainer']);
        $new_assigned_trainer_owner_details = $trainers->fetchTrainerDetails($new_assigned_trainer_details['reference_id']);
        $new_assigned_trainer_owner_name = $new_assigned_trainer_owner_details['firstname'] . " " . $new_assigned_trainer_owner_details['lastname'];
    }
}

if (!is_null($details['approvedat'])) {
    if ($approvedby_user_type_details['name'] == "STAFF") {
        $approver_details = $staff->fetchStaffDetails($approvedby_details['reference_id']);
        $approvedby_owner_name = $approver_details['firstname'] . " " . $approver_details['lastname'];
    } else if ($approvedby_user_type_details['name'] == "TRAINEE") {
        $approver_details = $trainees->fetchTraineeDetails($approvedby_details['reference_id']);
        $approvedby_owner_name = $approver_details['firstname'] . " " . $approver_details['lastname'];
    } else if ($approvedby_user_type_details['name'] == "TRAINER") {
        $approver_details = $trainers->fetchTrainerDetails($approvedby_details['reference_id']);
        $approvedby_owner_name = $approver_details['firstname'] . " " . $approver_details['lastname'];
    } else if ($approvedby_user_type_details['name'] == "TRAINING FACILITY ADMINISTRATOR") {
        $approver_details = $training_facilities->fetchTrainingFacilityAdministratorDetails($approvedby_details['reference_id']);
        $approvedby_owner_name = $approver_details['firstname'] . " " . $approver_details['lastname'];
    } else if ($approvedby_user_type_details['name'] == "SYSTEM ADMINISTRATOR") {
        $approver_details = $system_administration->fetchSystemAdministratorDetails($approvedby_details['reference_id']);
        $approvedby_owner_name = $approver_details['firstname'] . " " . $approver_details['lastname'];
    }
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">                                   
                <h3 class="panel-title"><?php echo $details['transaction_id'] . ' - ' . $details['count'] . "(" . ucwords(strtolower($status)) . ") || "; ?> </h3>
                <?php
                if ($details['status'] != 1009) {
                    if ($details['status'] == 1002) {
                        ?>
                        <a href="?update_element&item=lesson_booking&update_type=activate&code=<?php echo $code; ?>"><h3 class="panel-title">Activate</h3></a> <h3 class="panel-title"> || </h3>
                    <?php } else if ($details['status'] == 1001) { ?>
                        <a href="?update_element&item=lesson_booking&update_type=deactivate&code=<?php echo $code; ?>"><h3 class="panel-title">Deactivate</h3></a> <h3 class="panel-title"> || </h3>
                    <?php } ?>
                    <a href="?update_element&item=lesson_booking&update_type=delete&code=<?php echo $code; ?>"><h3 class="panel-title">Delete </h3></a> <h3 class="panel-title"> || </h3>                    
                    <a href="?update_staff_details"><h3 class="panel-title">Edit </h3></a> <h3 class="panel-title"> || </h3>
                <?php } ?>
                <a href="?view_lesson_booking_details"><h3 class="panel-title">Booked Lessons </h3></a>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">  
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-5 control-label">Transaction ID</label>
                        <label class="col-md-5 control-label"><?php echo $details['transaction_id']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Lesson</label>
                        <label class="col-md-5 control-label"><?php echo $lesson_details['name']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Number of Classes</label>
                        <label class="col-md-5 control-label"><?php echo $details['number_of_classes']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Unit Price</label>
                        <label class="col-md-5 control-label"><?php echo $details['unit_price']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Starting Date</label>
                        <label class="col-md-5 control-label"><?php echo $details['start_date']; ?></label>
                    </div>

                    <?php if ($details['is_rescheduled'] == 'YES') { ?>
                        <div class="form-group">
                            <label class="col-md-5 control-label">Is Rescheduled</label>
                            <label class="col-md-5 control-label"><?php echo $details['is_rescheduled']; ?></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-5 control-label">Reschedule Reason</label>
                            <label class="col-md-5 control-label"><?php echo $details['reschedule_reason']; ?></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-5 control-label">New Start Date</label>
                            <label class="col-md-5 control-label"><?php echo $details['new_start_date']; ?></label>
                        </div>
                    <?php } ?>

                    <div class="form-group">
                        <label class="col-md-5 control-label">Training Facility</label>
                        <label class="col-md-5 control-label"><?php echo $training_facility_details['name']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Payment Status</label>
                        <label class="col-md-5 control-label"><?php echo $details['payment_status']; ?></label>
                    </div>
                    <?php if (!is_null($details['approvedat'])) { ?>
                        <div class="form-group">
                            <label class="col-md-5 control-label">Approved At</label>
                            <label class="col-md-5 control-label"><?php echo $details['approvedat']; ?></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-5 control-label">Approved By</label>
                            <label class="col-md-5 control-label"><?php echo $approvedby_owner_name; ?></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-5 control-label">Approval Comment</label>
                            <label class="col-md-5 control-label"><?php echo $details['approval_comment']; ?></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-5 control-label">Assigned Trainer</label>
                            <label class="col-md-5 control-label"><?php echo $assigned_trainer_owner_name; ?></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-5 control-label">Reminder SMS Sent</label>
                            <label class="col-md-5 control-label"><?php echo $details['reminder_sms_sent']; ?></label>
                        </div>
                    <?php } ?>

                    <?php if (!is_null($details['trainer_approvedat'])) { ?>
                        <div class="form-group">
                            <label class="col-md-5 control-label">Trainer Approved At</label>
                            <label class="col-md-5 control-label"><?php echo $details['trainer_approvedat']; ?></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-5 control-label">Trainer Approval Comment</label>
                            <label class="col-md-5 control-label"><?php echo $details['trainer_approval_comment']; ?></label>
                        </div>
                    <?php } ?>

                    <?php if ($details['is_reassigned'] == 'YES') { ?>
                        <div class="form-group">
                            <label class="col-md-5 control-label">Is Reassigned</label>
                            <label class="col-md-5 control-label"><?php echo $details['is_reassigned']; ?></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-5 control-label">Reassigned At</label>
                            <label class="col-md-5 control-label"><?php echo $details['re_assignedat']; ?></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-5 control-label">Reassign Reason</label>
                            <label class="col-md-5 control-label"><?php echo $details['reassign_reason']; ?></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-5 control-label">New Trainer</label>
                            <label class="col-md-5 control-label"><?php echo $new_assigned_trainer_owner_name; ?></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-5 control-label">New Reminder SMS Sent</label>
                            <label class="col-md-5 control-label"><?php echo $details['new_reminder_sms_sent']; ?></label>
                        </div>

                        <?php if (!is_null($details['new_trainer_approvedat'])) { ?>
                            <div class="form-group">
                                <label class="col-md-5 control-label">New Trainer Approved At</label>
                                <label class="col-md-5 control-label"><?php echo $details['new_trainer_approvedat']; ?></label>
                            </div>
                            <div class="form-group">
                                <label class="col-md-5 control-label">New Trainer Approval Comment</label>
                                <label class="col-md-5 control-label"><?php echo $details['new_trainer_approval_comment']; ?></label>
                            </div>
                            <?php
                        }
                    }
                    ?>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Attendance Status</label>
                        <label class="col-md-5 control-label"><?php echo $attendance_status; ?></label>
                    </div>
                    <?php if (!is_null($details['trainer_rating'])) { ?>
                        <div class="form-group">
                            <label class="col-md-5 control-label">Trainer Rating</label>
                            <label class="col-md-5 control-label"><?php echo $details['trainer_rating']; ?></label>
                        </div>
                        <?php
                    }
                    if (!is_null($details['facility_rating'])) {
                        ?>
                        <div class="form-group">
                            <label class="col-md-5 control-label">Facility Rating</label>
                            <label class="col-md-5 control-label"><?php echo $details['facility_rating']; ?></label>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>              