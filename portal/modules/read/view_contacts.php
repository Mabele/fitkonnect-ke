<?php
if (!App::isLoggedIn()) App::redirectTo("?");
require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/Staff.php";
require_once WPATH . "modules/classes/Trainers.php";
require_once WPATH . "modules/classes/Trainees.php";
require_once WPATH . "modules/classes/Training_Facilities.php";
require_once WPATH . "modules/classes/System_Administration.php";
$system_administration = new System_Administration();
$training_facilities = new Training_Facilities();
$trainees = new Trainees();
$trainers = new Trainers();
$staff = new Staff();
$users_management = new Users_Management();

unset($_SESSION['contact']);
unset($_SESSION['search']);
?>

<div class="row">
    <div class="col-md-12">
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-heading">                                
                <h3 class="panel-title">Contacts || </h3>
                <a href="?send_sms"><h3 class="panel-title">Send SMS </h3></a> <h3 class="panel-title"> || </h3>
                <a href="?send_email"><h3 class="panel-title">Send Email</h3></a>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>User Type</th>
                            <th>Phone Number</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>                        
                        <?php
                        $count = 1;
                        if (!empty($_POST)) {
                            $contacts[] = $users_management->execute();
                        } else {
                            $contacts[] = $users_management->getAllContacts();
                        }
                        if (isset($_SESSION['no_records']) AND $_SESSION['no_records'] == true) {
                            echo "<tr>";
                            echo "<td>  No record found...</td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "</tr>";
                            unset($_SESSION['no_records']);
                        } else if (isset($_SESSION['yes_records']) AND $_SESSION['yes_records'] == true) {
                            foreach ($contacts as $key => $value) {
                                $inner_array[$key] = json_decode($value, true); // this will give key val pair array
                                foreach ((array) $inner_array[$key] as $key2 => $value2) {
                                    
                                    $user_details = $users_management->fetchUserDetails($value2['user_id']);
                                    $user_type_details = $users_management->fetchUserTypeDetails($user_details['reference_type']);
                                    $status_details = $system_administration->fetchSystemStatusDetails($value2['status']);
                                    $status = $status_details['display_value'];
                                    

                                    if ($user_type_details['name'] == "STAFF") {
                                        $contact_owner_details = $staff->fetchStaffDetails($user_details['reference_id']);
                                        $contact_owner_name = $contact_owner_details['firstname'] . " " . $contact_owner_details['lastname'];
                                    } else if ($user_type_details['name'] == "TRAINEE") {
                                        $contact_owner_details = $trainees->fetchTraineeDetails($user_details['reference_id']);
                                        $contact_owner_name = $contact_owner_details['firstname'] . " " . $contact_owner_details['lastname'];
                                    } else if ($user_type_details['name'] == "TRAINER") {
                                        $contact_owner_details = $trainers->fetchTrainerDetails($user_details['reference_id']);
                                        $contact_owner_name = $contact_owner_details['firstname'] . " " . $contact_owner_details['lastname'];
                                    } else if ($user_type_details['name'] == "TRAINING FACILITY ADMINISTRATOR") {
                                        $contact_owner_details = $training_facilities->fetchTrainingFacilityAdministratorDetails($user_details['reference_id']);
                                        $contact_owner_name = $contact_owner_details['firstname'] . " " . $contact_owner_details['lastname'];
                                    } else if ($user_type_details['name'] == "SYSTEM ADMINISTRATOR") {
                                        $contact_owner_details = $system_administration->fetchSystemAdministratorDetails($user_details['reference_id']);
                                        $contact_owner_name = $contact_owner_details['firstname'] . " " . $contact_owner_details['lastname'];
                                    }
                                    
                                    echo "<tr>";
                                    echo "<td>".$count++."</td>";
                                    echo "<td> <a href='?view_contact_details&code={$value2['id']}'>" . $contact_owner_name . "</td>";
                                    echo "<td>" . $user_type_details['name'] . "</td>";
                                    echo "<td>" . $value2['phone_number1'] . "</td>";
                                    echo "<td>" . $value2['email'] . "</td>";
                                    echo "<td>" . $status . "</td>";
                                    if ($value2['status'] == 1002) {
                                        echo "<td> <a href='?update_element&item=contact&update_type=activate&code=" . $value2['id'] . "'>ACTIVATE</td>";
                                    } else if ($value2['status'] == 1001) {
                                        echo "<td> <a href='?update_element&item=contact&update_type=deactivate&code=" . $value2['id'] . "'> DEACTIVATE </td>";
                                    } else {
                                        echo "<td>" . $status . "</td>";
                                    }
                                    if ($value2['status'] == 1009) {
                                        echo "<td> DELETED </td>";
                                    } else {
                                        echo "<td> <a href='?update_element&item=contact&update_type=delete&code=" . $value2['id'] . "'> DELETE </td>";
                                    }
                                    echo "</tr>";
                                }
                            }
                            unset($_SESSION['yes_records']);
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>                                
