<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/System_Administration.php";
$system_administration = new System_Administration();
$users_management = new Users_Management();

unset($_SESSION['inbox_message']);
unset($_SESSION['search']);
?>

<div class="row">
    <div class="col-md-12">
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-heading"> 
                <h3 class="panel-title">All Messages || </h3> 
                <a href="?send_sms"><h3 class="panel-title">Send SMS </h3></a> <h3 class="panel-title"> || </h3>
                <a href="?cron_file"><h3 class="panel-title">Reminder SMS </h3></a> <h3 class="panel-title"> || </h3>
                <a href="?send_email"><h3 class="panel-title">Send Email</h3></a>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                             <th>ID</th>
                            <th>Sender's Name</th>
                            <th>Sender's Email</th>
                            <th>Subject</th>
                            <th>Message</th>
                            <th>Created At</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>                        
                        <?php
                        $count = 1;


                        if (!empty($_POST)) {
                            $inbox_messages[] = $users_management->execute();
                        } else {
                            $inbox_messages[] = $users_management->getAllInboxMessages();
                        }

                        if (isset($_SESSION['no_records']) AND $_SESSION['no_records'] == true) {
                            echo "<tr>";
                            echo "<td>  No record found...</td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "</tr>";
                            unset($_SESSION['no_records']);
                        } else if (isset($_SESSION['yes_records']) AND $_SESSION['yes_records'] == true) {
                            foreach ($inbox_messages as $key => $value) {
                                $inner_array[$key] = json_decode($value, true); // this will give key val pair array
                                foreach ((array) $inner_array[$key] as $key2 => $value2) {
                                    echo "<tr>";
//                                            echo "<td> <a href='?individual_inbox_message&code=" . $value2['id'] . "'>" . $value2['id'] . "</td>";
                                    echo "<td> <a href='#'>" . $value2['id'] . "</td>";
                                    echo "<td> <a href='?view_inbox_message_details&code={$value2['id']}'>" . $value2['id'] . "</td>";
                                    echo "<td>" . $value2['name'] . "</td>";
                                    echo "<td>" . $value2['email'] . "</td>";
                                    echo "<td>" . $value2['subject'] . "</td>";
                                    echo "<td>" . $value2['message'] . "</td>";
                                    echo "<td>" . $value2['createdat'] . "</td>";
                                    if ($value2['status'] == 1003) {
                                        echo "<td> OPEN </td>";
                                    } else {
                                        echo "<td> CLOSED </td>";
                                    }
                                    if ($value2['status'] == 1003) {
                                        echo "<td> <a href='?update_element&item=inbox_message&update_type=close&code=" . $value2['id'] . "'> CLOSE </td>";
                                    } else {
                                        echo "<td> CLOSED </td>";
                                    }
                                    echo "</tr>";
                                }
                            }
                            unset($_SESSION['yes_records']);
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>                                
