<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/System_Administration.php";
$system_administration = new System_Administration();

unset($_SESSION['ward']);
unset($_SESSION['search']);
?>

<div class="row">
    <div class="col-md-12">
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-heading"> 
                <h3 class="panel-title">Wards || </h3>
                <a href="?add_ward"><h3 class="panel-title">Add Ward</h3></a>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>County</th>
                            <th>Status</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>                        
                        <?php
                        $count = 1;
                        if (!empty($_POST)) {
                            $wards[] = $system_administration->execute();
                        } else {
                            $wards[] = $system_administration->getAllWards();
                        }
                        if (isset($_SESSION['no_records']) AND $_SESSION['no_records'] == true) {
                            echo "<tr>";
                            echo "<td>  No record found...</td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "</tr>";
                            unset($_SESSION['no_records']);
                        } else if (isset($_SESSION['yes_records']) AND $_SESSION['yes_records'] == true) {
                            foreach ($wards as $key => $value) {
                                $inner_array[$key] = json_decode($value, true); // this will give key val pair array
                                foreach ((array) $inner_array[$key] as $key2 => $value2) {

                                    $county_details = $system_administration->fetchCountyDetails($value2['county']);
                                    $status_details = $system_administration->fetchSystemStatusDetails($value2['status']);
                                    $status = $status_details['display_value'];

                                    echo "<tr>";
                                    echo "<td>" . $count++ . "</td>";
                                    echo "<td>" . $value2['name'] . "</td>";
                                    echo "<td>" . $county_details['name'] . "</td>";
                                    echo "<td>" . $status . "</td>";
                                    if ($value2['status'] == 1002) {
                                        echo "<td> <a href='?update_element&item=ward&update_type=activate&code=" . $value2['id'] . "'>ACTIVATE</td>";
                                    } else if ($value2['status'] == 1001) {
                                        echo "<td> <a href='?update_element&item=ward&update_type=deactivate&code=" . $value2['id'] . "'> DEACTIVATE </td>";
                                    } else {
                                        echo "<td>" . $status . "</td>";
                                    }
                                    if ($value2['status'] == 1009) {
                                        echo "<td> DELETED </td>";
                                        echo "<td> DELETED </td>";
                                    } else {
                                        echo "<td> <a href='?update_ward_details&code=" . $value2['id'] . "'> EDIT </td>";
                                        echo "<td> <a href='?update_element&item=ward&update_type=delete&code=" . $value2['id'] . "'> DELETE </td>";
                                    }
                                    echo "</tr>";
                                }
                            }
                            unset($_SESSION['yes_records']);
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>      