<?php
if (!App::isLoggedIn()) App::redirectTo("?");
require_once WPATH . "modules/classes/Lessons.php";
$lessons = new Lessons();

if (isset($_SERVER['HTTP_REFERER'])) {
    $previous_url = $_SERVER['HTTP_REFERER'];
}

if (isset($_SESSION["cart_item"])) {
    $_SESSION["cart_number_of_items"] = count($_SESSION["cart_item"]);
    foreach ($_SESSION["cart_item"] as $item) {
        $item_total += ($item["price"] * $item["number_of_classes"]);
        $_SESSION["cart_total_cost"] = $item_total;
    }
} else {
    $_SESSION["cart_number_of_items"] = 0;
    $_SESSION["cart_total_cost"] = 0;
}

if (!empty($_GET["cart_action"])) {
    switch ($_GET["cart_action"]) {
        case "remove":
            if (!empty($_SESSION["cart_item"])) {
                foreach ($_SESSION["cart_item"] as $k => $v) {
                    if ($_GET["code"] == $v['id'])
                        unset($_SESSION["cart_item"][$k]);
                    if (empty($_SESSION["cart_item"]))
                        unset($_SESSION["cart_item"]);
                }
            }
            break;
        case "empty":
            unset($_SESSION["cart_item"]);
            unset($_SESSION["item_total"]);
            break;
    }
    App::redirectTo("{$previous_url}");
}

if (!empty($_POST) AND $_POST['action'] == "update_cart") {
    if (!empty($_SESSION["cart_item"])) {
        foreach ($_SESSION["cart_item"] as $k => $v) {
            if ($_POST["code"] == $v['id'])
                unset($_SESSION["cart_item"][$k]);
            if (empty($_SESSION["cart_item"]))
                unset($_SESSION["cart_item"]);
        }
    }
    $productByCode = $lessons->fetchLessonDetails($_POST["code"]);
    $itemArray = array($productByCode["id"] => array('id' => $productByCode["id"], 'name' => $productByCode["name"], 'price' => $productByCode["price"], 'number_of_classes' => ($_POST["number_of_classes"] + 1)));

    if (!empty($_SESSION["cart_item"])) {
        if (in_array($productByCode["id"], array_keys($_SESSION["cart_item"]))) {
            foreach ($_SESSION["cart_item"] as $k => $v) {
                if ($productByCode["id"] == $k) {
                    if (empty($_SESSION["cart_item"][$v]["number_of_classes"])) {
                        $_SESSION["cart_item"][$k][$v]["number_of_classes"] = 0;
                    }
                    $_SESSION["cart_item"][$k][$v]["number_of_classes"] += ($_POST["number_of_classes"] + 1);
                }
            }
        } else {
            $_SESSION["cart_item"] = array_merge($_SESSION["cart_item"], $itemArray);
        }
    } else {
        $_SESSION["cart_item"] = $itemArray;
    }
//    App::redirectTo($_POST["previous_url"]);
}

require_once "core/template/header.php";

if (isset($_SESSION["cart_number_of_items"]) AND $_SESSION["cart_number_of_items"] == 0) {
    ?>
    <div class="alert alert-block alert-error fade in">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>No lessons added to lessons-cart. <a href="?book_lesson"> Click here </a> to add lessons to your lessons-cart before proceeding.</strong>
    </div>
    <?php
}
?>

<div class="row">
    <div class="col-md-12">
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-heading">                                
                <h3 class="panel-title">My Bookings</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">


                <?php
                if (isset($_SESSION["cart_item"])) {
                    $item_total = 0;
                    ?>

                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Product Name</th>
                                <th>Unit Price</th>
                                <th>Number of Classes</th>
                                <th>Sub-Total (KES)</th>
                            </tr>
                        </thead>
                        <tbody>    

                            <?php
                            foreach ($_SESSION["cart_item"] as $item) {
                                $sub_item_total = ($item["price"] * $item["number_of_classes"]);
                                $lesson_details = $lessons->fetchLessonDetails($item["id"]);
                                ?>
                                <tr>
                                    <td>
                                        <a href="?lessons_cart&cart_action=remove&code=<?php echo $item["id"]; ?>" >Remove Item</a>
                                    </td>
                                    <td>
                                        <a href="#"><?php echo $lesson_details['name']; ?></a>					
                                    </td>
                                    <td>
                                        <?php echo "KES " . $lesson_details['price']; ?>				
                                    </td>
                            <form method="post">
                                <input type="hidden" name="action" value="update_cart"/>
                                <input type="hidden" name="code" value="<?php echo $item['id']; ?>"/>
                                <input type="hidden" name="previous_url" value="<?php echo $previous_url; ?>"/>
                                <td>
                                    <input type="number" name="number_of_classes" placeholder="Number of Classes" max-width="16" value="<?php echo $item["number_of_classes"]; ?>">
<!--                                    <input type="submit" value="Update Number" name="update_cart" class="button">-->
                                </td>
                            </form>

                            <td>
                                <?php echo "KES " . $sub_item_total; ?>				
                            </td>
                            </tr>
                            <?php
                            $item_total += ($item["price"] * $item["number_of_classes"]);
                        }
                        ?>
                        </tbody>
                    </table>
                <?php } ?>

                <h2>Booking Summary</h2>
                <div class="table-responsive">
                    <table cellspacing="0" class="table">
                        <tbody>
                            <tr>
                                <th>Total Amount</th>
                                <td><strong class="amount"><?php echo "KES " . $item_total; ?></strong></td>
                            </tr>
                            <tr>
                                <th>IQ Points</th>
                                <td><strong class="amount"><?php echo "KES " . 0.20 * $item_total; ?></strong></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <a href="?lessons_cart&cart_action=empty">Empty Lessons Cart</a> | <a href="?home">Book more lessons</a> | <a href="?checkout">Proceed to Checkout</a>
            </div>
        </div>
        <!-- END DEFAULT DATATABLE -->
    </div>
</div>                                

</div>
<!-- PAGE CONTENT WRAPPER -->  