<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/Staff.php";
require_once WPATH . "modules/classes/Trainers.php";
require_once WPATH . "modules/classes/Trainees.php";
require_once WPATH . "modules/classes/Training_Facilities.php";
require_once WPATH . "modules/classes/System_Administration.php";
$system_administration = new System_Administration();
$training_facilities = new Training_Facilities();
$trainees = new Trainees();
$trainers = new Trainers();
$staff = new Staff();
$users_management = new Users_Management();
$code = $_GET['code'];
$_SESSION['contact'] = $code;

$details = $users_management->fetchContactDetails($code);
$user_details = $users_management->fetchUserDetails($details['user_id']);
$createdby_details = $users_management->fetchUserDetails($details['createdby']);
$lastmodifiedby_details = $users_management->fetchUserDetails($details['lastmodifiedby']);
$user_type_details = $users_management->fetchUserTypeDetails($user_details['reference_type']);
$createdby_user_type_details = $users_management->fetchUserTypeDetails($createdby_details['reference_type']);
$lastmodifiedby_user_type_details = $users_management->fetchUserTypeDetails($lastmodifiedby_details['reference_type']);
$status_details = $system_administration->fetchSystemStatusDetails($details['status']);
$status = $status_details['display_value'];
$county_details = $system_administration->fetchCountyDetails($details['county']);
$ward_details = $system_administration->fetchWardDetails($details['ward']);

if ($user_type_details['name'] == "STAFF") {
    $contact_owner_details = $staff->fetchStaffDetails($user_details['reference_id']);
    $contact_owner_name = $contact_owner_details['firstname'] . " " . $contact_owner_details['lastname'];
} else if ($user_type_details['name'] == "TRAINEE") {
    $contact_owner_details = $trainees->fetchTraineeDetails($user_details['reference_id']);
    $contact_owner_name = $contact_owner_details['firstname'] . " " . $contact_owner_details['lastname'];
} else if ($user_type_details['name'] == "TRAINER") {
    $contact_owner_details = $trainers->fetchTrainerDetails($user_details['reference_id']);
    $contact_owner_name = $contact_owner_details['firstname'] . " " . $contact_owner_details['lastname'];
} else if ($user_type_details['name'] == "TRAINING FACILITY ADMINISTRATOR") {
    $contact_owner_details = $training_facilities->fetchTrainingFacilityAdministratorDetails($user_details['reference_id']);
    $contact_owner_name = $contact_owner_details['firstname'] . " " . $contact_owner_details['lastname'];
} else if ($user_type_details['name'] == "SYSTEM ADMINISTRATOR") {
    $contact_owner_details = $system_administration->fetchSystemAdministratorDetails($user_details['reference_id']);
    $contact_owner_name = $contact_owner_details['firstname'] . " " . $contact_owner_details['lastname'];
}


if ($createdby_user_type_details['name'] == "STAFF") {
    $createdby_owner_details = $staff->fetchStaffDetails($createdby_details['reference_id']);
    $createdby_owner_name = $createdby_owner_details['firstname'] . " " . $createdby_owner_details['lastname'];
} else if ($createdby_user_type_details['name'] == "TRAINEE") {
    $createdby_owner_details = $trainees->fetchTraineeDetails($createdby_details['reference_id']);
    $createdby_owner_name = $createdby_owner_details['firstname'] . " " . $createdby_owner_details['lastname'];
} else if ($createdby_user_type_details['name'] == "TRAINER") {
    $createdby_owner_details = $trainers->fetchTrainerDetails($createdby_details['reference_id']);
    $createdby_owner_name = $createdby_owner_details['firstname'] . " " . $createdby_owner_details['lastname'];
} else if ($createdby_user_type_details['name'] == "TRAINING FACILITY ADMINISTRATOR") {
    $createdby_owner_details = $training_facilities->fetchTrainingFacilityAdministratorDetails($createdby_details['reference_id']);
    $createdby_owner_name = $createdby_owner_details['firstname'] . " " . $createdby_owner_details['lastname'];
} else if ($createdby_user_type_details['name'] == "SYSTEM ADMINISTRATOR") {
    $createdby_owner_details = $system_administration->fetchSystemAdministratorDetails($createdby_details['reference_id']);
    $createdby_owner_name = $createdby_owner_details['firstname'] . " " . $createdby_owner_details['lastname'];
}


if ($lastmodifiedby_user_type_details['name'] == "STAFF") {
    $lastmodifiedby_owner_details = $staff->fetchStaffDetails($lastmodifiedby_details['reference_id']);
    $lastmodifiedby_owner_name = $lastmodifiedby_owner_details['firstname'] . " " . $lastmodifiedby_owner_details['lastname'];
} else if ($lastmodifiedby_user_type_details['name'] == "TRAINEE") {
    $lastmodifiedby_owner_details = $trainees->fetchTraineeDetails($lastmodifiedby_details['reference_id']);
    $lastmodifiedby_owner_name = $lastmodifiedby_owner_details['firstname'] . " " . $lastmodifiedby_owner_details['lastname'];
} else if ($lastmodifiedby_user_type_details['name'] == "TRAINER") {
    $lastmodifiedby_owner_details = $trainers->fetchTrainerDetails($lastmodifiedby_details['reference_id']);
    $lastmodifiedby_owner_name = $lastmodifiedby_owner_details['firstname'] . " " . $lastmodifiedby_owner_details['lastname'];
} else if ($lastmodifiedby_user_type_details['name'] == "TRAINING FACILITY ADMINISTRATOR") {
    $lastmodifiedby_owner_details = $training_facilities->fetchTrainingFacilityAdministratorDetails($lastmodifiedby_details['reference_id']);
    $lastmodifiedby_owner_name = $lastmodifiedby_owner_details['firstname'] . " " . $lastmodifiedby_owner_details['lastname'];
} else if ($lastmodifiedby_user_type_details['name'] == "SYSTEM ADMINISTRATOR") {
    $lastmodifiedby_owner_details = $system_administration->fetchSystemAdministratorDetails($lastmodifiedby_details['reference_id']);
    $lastmodifiedby_owner_name = $lastmodifiedby_owner_details['firstname'] . " " . $lastmodifiedby_owner_details['lastname'];
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">                                   
                <h3 class="panel-title"><?php echo $contact_owner_name . "(" . ucwords(strtolower($status)) . ") || "; ?> </h3>
                <?php
                if ($details['status'] != 1009) {
                    if ($details['status'] == 1002) {
                        ?>
                        <a href="?update_element&item=contact&update_type=activate&code=<?php echo $code; ?>"><h3 class="panel-title">Activate</h3></a> <h3 class="panel-title"> || </h3>
                    <?php } else if ($details['status'] == 1001) { ?>
                        <a href="?update_element&item=contact&update_type=deactivate&code=<?php echo $code; ?>"><h3 class="panel-title">Deactivate</h3></a> <h3 class="panel-title"> || </h3>
                    <?php } ?>
                    <a href="?update_element&item=contact&update_type=delete&code=<?php echo $code; ?>"><h3 class="panel-title">Delete </h3></a> <h3 class="panel-title"> || </h3>                    
                    <a href="?update_contact_details"><h3 class="panel-title">Edit </h3></a> <h3 class="panel-title"> || </h3>
                <?php } ?>
                <a href="?view_contacts"><h3 class="panel-title">Contacts </h3></a>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">  
                <div class="col-md-10">
<!--                    <div class="form-group">
                        <label class="col-md-5 control-label">Profile Picture</label>
                        <label class="col-md-5 control-label"><?php // echo $details['prof_picture']; ?></label>
                    </div>-->
                    <div class="form-group">
                        <label class="col-md-5 control-label">Website</label>
                        <label class="col-md-5 control-label"><?php echo $details['website']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Phone Number</label>
                        <label class="col-md-5 control-label"><?php echo $details['phone_number1']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Alternative Phone Number</label>
                        <label class="col-md-5 control-label"><?php echo $details['phone_number2']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Email</label>
                        <label class="col-md-5 control-label"><?php echo $details['email']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Postal Number</label>
                        <label class="col-md-5 control-label"><?php echo $details['postal_number']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Postal Code</label>
                        <label class="col-md-5 control-label"><?php echo $details['postal_code']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Postal Town</label>
                        <label class="col-md-5 control-label"><?php echo $details['postal_town']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Physical Address - County</label>
                        <label class="col-md-5 control-label"><?php echo $county_details['name']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Physical Address - Ward</label>
                        <label class="col-md-5 control-label"><?php echo $ward_details['name']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Created At</label>
                        <label class="col-md-5 control-label"><?php echo $details['createdat']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Created By</label>
                        <label class="col-md-5 control-label"><?php echo $createdby_owner_name; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Last Modified At</label>
                        <label class="col-md-5 control-label"><?php echo $county_details['lastmodifiedat']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Last Modified By</label>
                        <label class="col-md-5 control-label"><?php echo $lastmodifiedby_owner_name; ?></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>              