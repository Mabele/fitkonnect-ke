<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Lessons.php";
require_once WPATH . "modules/classes/Transactions.php";
require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/Trainees.php";
require_once WPATH . "modules/classes/Staff.php";
require_once WPATH . "modules/classes/System_Administration.php";
require_once WPATH . "modules/classes/Trainers.php";
require_once WPATH . "modules/classes/Training_Facilities.php";
$training_facilities = new Training_Facilities();
$trainers = new Trainers();
$system_administration = new System_Administration();
$staff = new Staff();
$trainees = new Trainees();
$users_management = new Users_Management();
$transactions = new Transactions();
$lessons = new Lessons();

unset($_SESSION['lesson_booking']);
?>

<div class="row">
    <div class="col-md-12">
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-heading">                                
                <h3 class="panel-title">Lesson Bookings</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
<!--                            <th>ID</th>-->
                            <th>Transaction ID</th>
                            <th>Transaction Type</th>                                                                         
                            <th>Client</th>
                            <th>Amount</th>                                 
                            <th>Payment Option</th>                                 
                            <th>Payment Status</th> 
                        </tr>
                    </thead>
                    <tbody>                        
                        <?php
                        $count = 1;
                        if (!empty($_POST)) {
                            $lesson_booking_transactions[] = $transactions->execute();
                        } else {
                            $lesson_booking_transactions[] = $transactions->getAllLessonBookingTransactions();
                        }
                        if (isset($_SESSION['no_records']) AND $_SESSION['no_records'] == true) {
                            echo "<tr>";
                            echo "<td>  No record found...</td>";
//                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "</tr>";
                            unset($_SESSION['no_records']);
                        } else if (isset($_SESSION['yes_records']) AND $_SESSION['yes_records'] == true) {
                            foreach ($lesson_booking_transactions as $key => $value) {
                                $inner_array[$key] = json_decode($value, true); // this will give key val pair array
                                foreach ((array) $inner_array[$key] as $key2 => $value2) {
                                    $payment_status_details = $system_administration->fetchSystemStatusDetails($value2['payment_status']);
                                    $payment_status = $payment_status_details['display_value'];
                                    $user_details = $users_management->fetchUserDetails($value2['booker_id']);
                                    $booker_user_type_details = $users_management->fetchUserTypeDetails($user_details['reference_type']);

                                    if ($booker_user_type_details['name'] == "STAFF") {
                                        $booker_details = $staff->fetchStaffDetails($user_details['reference_id']);
                                        $booker = $booker_details['firstname'] . " " . $booker_details['lastname'];
                                    } else if ($booker_user_type_details['name'] == "TRAINEE") {
                                        $booker_details = $trainees->fetchTraineeDetails($user_details['reference_id']);
                                        $booker = $booker_details['firstname'] . " " . $booker_details['lastname'];
                                    } else if ($booker_user_type_details['name'] == "TRAINER") {
                                        $booker_details = $trainers->fetchTrainerDetails($user_details['reference_id']);
                                        $booker = $booker_details['firstname'] . " " . $booker_details['lastname'];
                                    } else if ($booker_user_type_details['name'] == "TRAINING FACILITY ADMINISTRATOR") {
                                        $booker_details = $training_facilities->fetchTrainingFacilityAdministratorDetails($user_details['reference_id']);
                                        $booker = $booker_details['firstname'] . " " . $booker_details['lastname'];
                                    } else if ($booker_user_type_details['name'] == "SYSTEM ADMINISTRATOR") {
                                        $booker_details = $system_administration->fetchSystemAdministratorDetails($user_details['reference_id']);
                                        $booker = $booker_details['firstname'] . " " . $booker_details['lastname'];
                                    }

                                    echo "<tr>";
                                    echo "<td>" . $count++ . "</td>";
//                                    echo "<td>" . $value2['count'] . "</td>";
                                    echo "<td> <a href='?view_individual_booking_transaction&code=" . $value2['transaction_id'] . "'>" . $value2['transaction_id'] . "</td>";
                                    echo "<td>" . $value2['transaction_type'] . "</td>";
                                    echo "<td>" . $booker . "</td>";
                                    echo "<td>" . $value2['amount'] . "</td>";
                                    echo "<td>" . $value2['payment_option'] . "</td>";
                                    echo "<td>" . $payment_status . "</td>";
                                    echo "</tr>";
                                }
                            }
                            unset($_SESSION['yes_records']);
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END DEFAULT DATATABLE -->
    </div>
</div>                                

</div>
<!-- PAGE CONTENT WRAPPER -->  