<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/System_Administration.php";
require_once WPATH . "modules/classes/Staff.php";
require_once WPATH . "modules/classes/Trainees.php";
require_once WPATH . "modules/classes/Trainers.php";
require_once WPATH . "modules/classes/Training_Facilities.php";
$staff = new Staff();
$trainees = new Trainees();
$trainers = new Trainers();
$training_facilities = new Training_Facilities();
$system_administration = new System_Administration();
$users_management = new Users_Management();
$code = $_GET['code'];
$_SESSION['inbox_message'] = $code;

$details = $users_management->fetchInboxMessageDetails($code);
$status_details = $system_administration->fetchSystemStatusDetails($details['status']);
$status = $status_details['display_value'];

if ($details['is_assigned'] == "YES") {
    $assignedby_details = $users_management->fetchUserDetails($details['assignedby']);
    $assignedto_details = $users_management->fetchUserDetails($details['assignedto']);
    $assignedby_user_type_details = $users_management->fetchUserTypeDetails($assignedby_details['reference_type']);
    $assignedto_user_type_details = $users_management->fetchUserTypeDetails($assignedto_details['reference_type']);

    if ($assignedby_user_type_details['name'] == "STAFF") {
        $assigner_details = $staff->fetchStaffDetails($assignedby_details['reference_id']);
        $assigner_name = $assigner_details['firstname'] . " " . $assigner_details['lastname'];
    } else if ($assignedby_user_type_details['name'] == "TRAINEE") {
        $assigner_details = $trainees->fetchTraineeDetails($assignedby_details['reference_id']);
        $assigner_name = $assigner_details['firstname'] . " " . $assigner_details['lastname'];
    } else if ($assignedby_user_type_details['name'] == "TRAINER") {
        $assigner_details = $trainers->fetchTrainerDetails($assignedby_details['reference_id']);
        $assigner_name = $assigner_details['firstname'] . " " . $assigner_details['lastname'];
    } else if ($assignedby_user_type_details['name'] == "TRAINING FACILITY ADMINISTRATOR") {
        $assigner_details = $training_facilities->fetchTrainingFacilityAdministratorDetails($assignedby_details['reference_id']);
        $assigner_name = $assigner_details['firstname'] . " " . $assigner_details['lastname'];
    } else if ($assignedby_user_type_details['name'] == "SYSTEM ADMINISTRATOR") {
        $assigner_details = $system_administration->fetchSystemAdministratorDetails($assignedby_details['reference_id']);
        $assigner_name = $assigner_details['firstname'] . " " . $assigner_details['lastname'];
    }

    if ($assignedto_user_type_details['name'] == "STAFF") {
        $assignee_details = $staff->fetchStaffDetails($assignedby_details['reference_id']);
        $assigner_name = $assignee_details['firstname'] . " " . $assignee_details['lastname'];
    } else if ($assignedto_user_type_details['name'] == "TRAINEE") {
        $assignee_details = $trainees->fetchTraineeDetails($assignedby_details['reference_id']);
        $assigner_name = $assignee_details['firstname'] . " " . $assignee_details['lastname'];
    } else if ($assignedto_user_type_details['name'] == "TRAINER") {
        $assignee_details = $trainers->fetchTrainerDetails($assignedby_details['reference_id']);
        $assigner_name = $assignee_details['firstname'] . " " . $assignee_details['lastname'];
    } else if ($assignedto_user_type_details['name'] == "TRAINING FACILITY ADMINISTRATOR") {
        $assignee_details = $training_facilities->fetchTrainingFacilityAdministratorDetails($assignedby_details['reference_id']);
        $assigner_name = $assignee_details['firstname'] . " " . $assignee_details['lastname'];
    } else if ($assignedto_user_type_details['name'] == "SYSTEM ADMINISTRATOR") {
        $assignee_details = $system_administration->fetchSystemAdministratorDetails($assignedby_details['reference_id']);
        $assigner_name = $assignee_details['firstname'] . " " . $assignee_details['lastname'];
    }
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">                                   
                <h3 class="panel-title"><?php echo $details['subject'] . "(" . ucwords(strtolower($status)) . ") || "; ?> </h3>
                <?php if ($details['status'] == 1003) { ?>
                    <a href="?update_element&item=inbox_message&update_type=close&code=<?php echo $code; ?>"><h3 class="panel-title">Close</h3></a> <h3 class="panel-title"> || </h3>
                <?php } ?>
                <a href="?view_inbox_messages"><h3 class="panel-title">Inbox Messages </h3></a>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">  
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="col-md-5 control-label">Sender's Name</label>
                        <label class="col-md-5 control-label"><?php echo $details['name']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Sender's Phone Number</label>
                        <label class="col-md-5 control-label"><?php echo $details['phone_number']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Email</label>
                        <label class="col-md-5 control-label"><?php echo $details['email']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Message</label>
                        <label class="col-md-5 control-label"><?php echo $details['message']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Created At</label>
                        <label class="col-md-5 control-label"><?php echo $details['createdat']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Assignment Status</label>
                        <label class="col-md-5 control-label"><?php echo $details['is_assigned']; ?></label>
                    </div>                    
                    <?php if ($details['is_assigned'] == "YES") { ?>                    
                        <div class="form-group">
                            <label class="col-md-5 control-label">Assigned At</label>
                            <label class="col-md-5 control-label"><?php echo $details['assignedat']; ?></label>
                        </div>                    
                        <div class="form-group">
                            <label class="col-md-5 control-label">Assigned By</label>
                            <label class="col-md-5 control-label"><?php echo $details['assignedby']; ?></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-5 control-label">Assigned To</label>
                            <label class="col-md-5 control-label"><?php echo $details['assignedto']; ?></label>
                        </div>
                    <?php } ?>

                    <?php if ($details['status'] == 1004) { ?>                    
                        <div class="form-group">
                            <label class="col-md-5 control-label">Closed At</label>
                            <label class="col-md-5 control-label"><?php echo $details['closedat']; ?></label>
                        </div>                    
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</div>              