<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Transactions.php";
require_once WPATH . "modules/classes/Lessons.php";
$lessons = new Lessons();
$transactions = new Transactions();

if (isset($_SESSION["cart_number_of_items"]) AND $_SESSION["cart_number_of_items"] == 0) {
    ?>
    <div class="alert alert-block alert-error fade in">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>No items added to cart. <a href="?book_lesson"> Click here </a> to add items to your shopping cart before proceeding.</strong>
    </div>
    <?php
}

if (!empty($_POST) AND $_POST['action'] == "checkout_transaction") {

    if ($_POST["terms_and_conditions"] != "Yes") {
        $_SESSION['terms_and_conditions'] == false;
        App::redirectTo("?process_feedback");
    } else {
        if (isset($_SESSION["cart_item"])) {
            $_SESSION["start_date"] = $_POST["start_date"];
            $_SESSION["payment_option"] = $_POST["payment_method"];
            $_SESSION["transaction_id"] = $transactions->getTransactionId($_SESSION["payment_option"], 'TESTING', $_SESSION["cart_total_cost"]); // Replace 'TESTING' with $_SESSION["user_id"];
            $transaction = $transactions->addTransaction();
            if (is_bool($transaction) && $transaction == true) {
                foreach ($_SESSION["cart_item"] as $item) {
                    $_SESSION["number_of_classes"] = $item["number_of_classes"];
                    $_SESSION['lesson_details'] = $lessons->fetchLessonDetails($item["id"]);
                    $transaction_details = $transactions->addTransactionDetails();
                }


                $sender = "hello@fitnessiq.com";
                $headers = "From: Fitness IQ <$sender>\r\n";
                $headers .= "MIME-Version: 1.0\r\n";
                $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                $subject = "Transaction Acknowledgement";
                $message = "<html><body>"
                        . "<p><b>Hello " . 'Test1' . " " . 'Test2' . ",</b><br/>" //Retrieve user details
                        . "Thank you for transacting with us on Fitness IQ.<br/>"
                ?>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">                                
                                <h3 class="panel-title">Your transaction details are as follows:</h3>
                            </div>
                            <div class="panel-body">

                                <?php
                                if (isset($_SESSION["cart_item"])) {
                                    $item_total = 0;
                                    ?>     

                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>Product Name</th>
                                                <th>Total (KES)</th>
                                            </tr>
                                        </thead>
                                        <tbody>    

                                            <?php
                                            foreach ($_SESSION["cart_item"] as $item) {
                                                $sub_item_total = ($item["price"] * $item["number_of_classes"]);
                                                $lesson_details = $lessons->fetchLessonDetails($item["id"]);
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $lesson_details['name']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $sub_item_total; ?>						
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Total</th>
                                                <td><strong><?php echo $_SESSION["payment_option"]; ?></strong> </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                mail('bmugeni@reflexconcepts.co.ke', $subject, $message, $headers); //Retrieve mailing address

                unset($_SESSION['cart_item']);
                unset($_SESSION["item_total"]);
                $_SESSION["transaction_status"] = "success";
            } else {
                $_SESSION["transaction_status"] = "process_error";
            }
            App::redirectTo("?process_feedback"); // Have a centralized feedback page
        }
    }
}
?>

<form method="post">                    
    <input type="hidden" name="action" value="checkout_transaction"/>
    <div class="row">
        <div class="col-md-12">
            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Your transaction details are as follows:</h3>
                </div>
                <div class="panel-body">

                    <?php
                    if (isset($_SESSION["cart_item"])) {
                        $item_total = 0;
                        ?>     

                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Total (KES)</th>
                                </tr>
                            </thead>
                            <tbody>    

                                <?php
                                foreach ($_SESSION["cart_item"] as $item) {
                                    $sub_item_total = ($item["price"] * $item["number_of_classes"]);
                                    $lesson_details = $lessons->fetchLessonDetails($item["id"]);
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $lesson_details['name']; ?>
                                        </td>
                                        <td>
                                            <?php echo $sub_item_total; ?>						
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Total</th>
                                    <td><strong><?php echo $_SESSION["cart_total_cost"]; ?></strong> </td>
                                </tr>
                            </tfoot>
                        </table>
                    <?php } ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Start Date</label>
                <div class="col-md-9 col-xs-12">   
                    <div class="input-group">
                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                        <input type="text" name="start_date" class="form-control datepicker" value="2018-02-28"> 
                    </div>
                    <span class="help-block">Select Date</span>
                </div>
            </div>
            <ul>
                <li>
                    <input type="radio" value="mpesa" name="payment_method" checked="checked">
                    <label for="payment_method_mpesa">M-Pesa</label>
                </li>
                <li>
                    <input type="radio" value="bank_transfer" name="payment_method">
                    <label for="payment_method_bank">Direct Bank Transfer</label>
                </li>
                <li>
                    <input type="radio" value="paypal" name="payment_method">
                    <label for="payment_method_paypal">Paypal</label>
                </li>
                <li>
                    <input type="radio" value="credit_card" name="payment_method">
                    <label for="payment_method_card">Credit Card</label>
                </li>
            </ul>
            <div class="form-row">
                <input type="checkbox" name="terms_and_conditions" value="Yes" required=""/> <label for="remember"> &nbsp I accept FitnessIQ's <a href="?tac">terms and conditions</a></label>
            </div>
            <div class="form-row place-order">
                <input type="submit" value="Submit">
            </div>
        </div>
    </div>
</form>
