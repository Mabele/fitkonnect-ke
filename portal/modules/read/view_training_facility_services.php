<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Training_Facilities.php";
require_once WPATH . "modules/classes/System_Administration.php";
$system_administration = new System_Administration();
$training_facilities = new Training_Facilities();

unset($_SESSION['training_facility_service']);
unset($_SESSION['search']);
?>

<div class="row">
    <div class="col-md-12">
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-heading">    
                <h3 class="panel-title">Training Facility Services || </h3>
                <a href="?add_training_facility_service"><h3 class="panel-title">Add Training Facility Service</h3></a>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>                        
                        <?php
                        $count = 1;
                        if (!empty($_POST)) {
                            $training_facility_services[] = $training_facilities->execute();
                        } else {
                            $training_facility_services[] = $training_facilities->getAllTrainingFacilityServices();
                        }
                        if (isset($_SESSION['no_records']) AND $_SESSION['no_records'] == true) {
                            echo "<tr>";
                            echo "<td>  No record found...</td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "</tr>";
                            unset($_SESSION['no_records']);
                        } else if (isset($_SESSION['yes_records']) AND $_SESSION['yes_records'] == true) {
                            foreach ($training_facility_services as $key => $value) {
                                $inner_array[$key] = json_decode($value, true); // this will give key val pair array
                                foreach ((array) $inner_array[$key] as $key2 => $value2) {
                                    $status_details = $system_administration->fetchSystemStatusDetails($value2['status']);
                                    $status = $status_details['display_value'];

                                    echo "<tr>";
                                    echo "<td>" . $count++ . "</td>";
                                    echo "<td>" . $value2['name'] . "</td>";
                                    echo "<td>" . $status . "</td>";
                                    if ($value2['status'] == 1002) {
                                        echo "<td> <a href='?update_element&item=training_facility_service&update_type=activate&code=" . $value2['id'] . "'>ACTIVATE</td>";
                                    } else if ($value2['status'] == 1001) {
                                        echo "<td> <a href='?update_element&item=training_facility_service&update_type=deactivate&code=" . $value2['id'] . "'> DEACTIVATE </td>";
                                    } else {
                                        echo "<td>" . $status . "</td>";
                                    }
                                    if ($value2['status'] == 1009) {
                                        echo "<td> DELETED </td>";
                                        echo "<td> DELETED </td>";
                                    } else {
                                        echo "<td> <a href='?update_training_facility_service_details&code=" . $value2['id'] . "'> EDIT </td>";
                                        echo "<td> <a href='?update_element&item=training_facility_service&update_type=delete&code=" . $value2['id'] . "'> DELETE </td>";
                                    }
                                    echo "</tr>";
                                }
                            }
                            unset($_SESSION['yes_records']);
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>                           