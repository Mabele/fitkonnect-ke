<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Lessons.php";
require_once WPATH . "modules/classes/Transactions.php";
require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/Trainers.php";
require_once WPATH . "modules/classes/System_Administration.php";
$system_administration = new System_Administration();
$trainers = new Trainers();
$users_management = new Users_Management();
$transactions = new Transactions();
$lessons = new Lessons();

unset($_SESSION['lesson_booking']);
?>

<div class="row">
    <div class="col-md-12">
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-heading">                                
                <h3 class="panel-title">Lesson Booking Details</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>

                            <?php if ($_SESSION['logged_in_user_type_details']['name'] == "STAFF" OR $_SESSION['logged_in_user_type_details']['name'] == "SYSTEM ADMINISTRATOR") { ?> 
                                <th>#</th>
                                <!--<th>ID</th>-->
                                <th>Transaction ID</th>
                                <th>Lesson</th> 
                                <th>Number of Classes</th>                                 
                                <th>Start Date</th>   
                                <th>Payment Status</th>
                                <th>Trainer</th>
                                <th>Approve</th>
                                <th>Reject</th>
                                <th>Delete</th>
                                <th>Status</th>
                            <?php } else if ($_SESSION['logged_in_user_type_details']['name'] == "TRAINER") { ?> 
                                <th>#</th>
                                <!--<th>ID</th>-->
                                <th>Transaction ID</th>
                                <th>Lesson</th> 
                                <th>Number of Classes</th>                                 
                                <th>Start Date</th> 
                                <th>Approve</th>
                                <th>Reject</th>
                                <th>End Lesson</th>
                                <th>Status</th>
                            <?php } ?>  
                        </tr>
                    </thead>
                    <tbody>                        
                        <?php
                        $count = 1;
                        if (!empty($_POST)) {
                            $lesson_booking_transactions_details[] = $transactions->execute();
                        } else {
                            if (is_menu_set('view_individual_booking_transaction') != "") {
                                $transaction_id = $_GET['code'];
                                $lesson_booking_transactions_details[] = $transactions->getLessonBookingTransactionsDetails($transaction_id);
                            } else if (is_menu_set('view_my_assigned_lessons') != "") {
                                $lesson_booking_transactions_details[] = $transactions->getTheLessonsIHaveBeenAssigned();
                            } else if (is_menu_set('view_my_lesson_bookings') != "") {
                                $lesson_booking_transactions_details[] = $transactions->getTheLessonsIHaveBooked();
                            } else {
                                $lesson_booking_transactions_details[] = $transactions->getAllLessonBookingTransactionsDetails();
                            }
                        }
                        if (isset($_SESSION['no_records']) AND $_SESSION['no_records'] == true) {
                            echo "<tr>";

                            if ($_SESSION['logged_in_user_type_details']['name'] == "STAFF" OR $_SESSION['logged_in_user_type_details']['name'] == "SYSTEM ADMINISTRATOR") {
                                echo "<td>  No record found...</td>";
//                            echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                            } else if ($_SESSION['logged_in_user_type_details']['name'] == "TRAINER") {
                                echo "<td>  No record found...</td>";
//                            echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                            }

                            echo "</tr>";
                            unset($_SESSION['no_records']);
                        } else if (isset($_SESSION['yes_records']) AND $_SESSION['yes_records'] == true) {
                            foreach ($lesson_booking_transactions_details as $key => $value) {
                                $inner_array[$key] = json_decode($value, true); // this will give key val pair array
                                foreach ((array) $inner_array[$key] as $key2 => $value2) {

                                    $lesson_details = $lessons->fetchLessonDetails($value2['lesson']);
//                                    $lesson_category_details = $lessons->fetchLessonCategoryDetails($lesson_details['category']);
                                    $status_details = $system_administration->fetchSystemStatusDetails($value2['status']);
                                    $status = $status_details['display_value'];

                                    if (empty($value2['assigned_trainer'])) {
                                        $assigned_trainer = "UNASSIGNED";
                                    } else {
                                        $user_details = $users_management->fetchUserDetails($value2['assigned_trainer']);
                                        $trainer_details = $trainers->fetchTrainerDetails($user_details['reference_id']);
                                        $assigned_trainer = $trainer_details['firstname'] . ' ' . $trainer_details['lastname'];
                                    }

                                    echo "<tr>";
                                    if ($_SESSION['logged_in_user_type_details']['name'] == "STAFF" OR $_SESSION['logged_in_user_type_details']['name'] == "SYSTEM ADMINISTRATOR") {
                                        echo "<td><a href='?view_lesson_bookings_detail_details&code={$value2['count']}'>" . $count++ . "</td>";
                                        echo "<td>" . $value2['transaction_id'] . "</td>";
                                        echo "<td>" . $lesson_details['name'] . "</td>";
//                                        echo "<td>" . $lesson_category_details['name'] . ' - ' . $lesson_details['name'] . "</td>";
                                        echo "<td>" . $value2['number_of_classes'] . "</td>";
                                        echo "<td>" . $value2['start_date'] . "</td>";
                                        echo "<td>" . $status . "</td>";    // echo "<td>" . $value2['payment_status'] . "</td>";
                                        echo "<td>" . $assigned_trainer . "</td>";

                                        if ($value2['status'] == 1005) {
                                            echo "<td> <a href='?update_element&item=lesson_booking&update_type=approve&code=" . $value2['count'] . "'> APPROVE </td>";
                                        } else {
                                            echo "<td>" . $status . "</td>";
                                        }

                                        if ($value2['status'] == 1005) {
                                            echo "<td> <a href='?update_element&item=lesson_booking&update_type=reject&code=" . $value2['count'] . "'> REJECT </td>";
                                        } else {
                                            echo "<td>" . $status . "</td>";
                                        }

                                        if ($value2['status'] == 1009) {
                                            echo "<td>" . $status . "</td>";
                                        } else {
                                            echo "<td> <a href='?update_element&item=lesson_booking&update_type=delete&code=" . $value2['count'] . "'> DELETE </td>";
                                        }
                                        echo "<td>" . $status . "</td>";
                                    } else if ($_SESSION['logged_in_user_type_details']['name'] == "TRAINER") {
                                        echo "<td><a href='?view_lesson_bookings_detail_details&code={$value2['count']}'>" . $count++ . "</td>";
                                        echo "<td>" . $value2['transaction_id'] . "</td>";
                                        echo "<td>" . $lesson_details['name'] . "</td>";
//                                        echo "<td>" . $lesson_category_details['name'] . ' - ' . $lesson_details['name'] . "</td>";
                                        echo "<td>" . $value2['number_of_classes'] . "</td>";
                                        echo "<td>" . $value2['start_date'] . "</td>";
                                        if (is_null($value2['trainer_approvedat'])) {
                                            echo "<td> <a href='?update_element&item=trainer_acceptance&update_type=approve&code=" . $value2['count'] . "'> APPROVE </td>";
                                        } else {
                                            echo "<td>" . $status . "</td>";
                                        }

                                        if (is_null($value2['trainer_approvedat'])) {
                                            echo "<td> <a href='?update_element&item=trainer_acceptance&update_type=reject&code=" . $value2['count'] . "'> REJECT </td>";
                                        } else {
                                            echo "<td>" . $status . "</td>";
                                        }

                                        if (!is_null($value2['trainer_approvedat']) AND $value2['status'] == 1003) {
                                            echo "<td> <a href='?update_element&item=lesson_training&update_type=close&code=" . $value2['count'] . "'> CLOSE </td>";
                                        } else {
                                            echo "<td>" . $status . "</td>";
                                        }
                                        echo "<td>" . $status . "</td>";
                                    }
                                    echo "</tr>";
                                }
                            }
                            unset($_SESSION['yes_records']);
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END DEFAULT DATATABLE -->
    </div>
</div>                                

</div>
<!-- PAGE CONTENT WRAPPER -->  