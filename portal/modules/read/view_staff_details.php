<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Staff.php";
require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/System_Administration.php";
$system_administration = new System_Administration();
$users_management = new Users_Management();
$staff = new Staff();
$code = $_GET['code'];
$_SESSION['staff'] = $code;

$details = $staff->fetchStaffDetails($code);
$staff_position_details = $staff->fetchStaffPositionDetails($details['position']);
$status_details = $system_administration->fetchSystemStatusDetails($details['status']);
$status = $status_details['display_value'];
$createdby = $users_management->fetchUserDetails($details['createdby']);
if ($details['gender'] == 'M') {
    $gender = 'MALE';
} else if ($details['gender'] == 'F') {
    $gender = 'FEMALE';
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">                                   
                <h3 class="panel-title"><?php echo $details['firstname'] . ' ' . $details['lastname'] . "(" . ucwords(strtolower($status)) . ") || "; ?> </h3>
                <?php
                if ($details['status'] != 1009) {
                    if ($details['status'] == 1002) {
                        ?>
                        <a href="?update_element&item=staff&update_type=activate&code=<?php echo $code; ?>"><h3 class="panel-title">Activate</h3></a> <h3 class="panel-title"> || </h3>
                    <?php } else if ($details['status'] == 1001) { ?>
                        <a href="?update_element&item=staff&update_type=deactivate&code=<?php echo $code; ?>"><h3 class="panel-title">Deactivate</h3></a> <h3 class="panel-title"> || </h3>
                    <?php } ?>
                    <a href="?update_element&item=staff&update_type=delete&code=<?php echo $code; ?>"><h3 class="panel-title">Delete </h3></a> <h3 class="panel-title"> || </h3>                    
                    <a href="?update_staff_details"><h3 class="panel-title">Edit </h3></a> <h3 class="panel-title"> || </h3>
                <?php } ?>
                <a href="?view_staff"><h3 class="panel-title">Staff Members </h3></a>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">  
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-3 control-label">PF Number</label>
                        <label class="col-md-3 control-label"><?php echo $details['pf_number']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">First Name</label>
                        <label class="col-md-3 control-label"><?php echo $details['firstname']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Middle Name</label>
                        <label class="col-md-3 control-label"><?php echo $details['middlename']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Last Name</label>
                        <label class="col-md-3 control-label"><?php echo $details['lastname']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Gender</label>
                        <label class="col-md-3 control-label"><?php echo $gender; ?></label>
                    </div><div class="form-group">
                        <label class="col-md-3 control-label">ID Number</label>
                        <label class="col-md-3 control-label"><?php echo $details['idnumber']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Position</label>
                        <label class="col-md-3 control-label"><?php echo $staff_position_details['name']; ?></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>              