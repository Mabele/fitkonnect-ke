<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Transactions.php";
require_once WPATH . "modules/classes/System_Administration.php";
$system_administration = new System_Administration();
$transactions = new Transactions();
$code = $_GET['code'];
$_SESSION['payment_transaction'] = $code;

$details = $transactions->fetchPaymentTransactionDetails($code);
$status_details = $system_administration->fetchSystemStatusDetails($details['status']);
$status = $status_details['display_value'];

if (is_null($details['business_short_code']) OR empty($details['business_short_code'])) {
    $business_short_code = "N/A";
} else {
    $business_short_code = $details['business_short_code'];
}

if (is_null($details['invoice_number']) OR empty($details['invoice_number'])) {
    $invoice_number = "N/A";
} else {
    $invoice_number = $details['invoice_number'];
}

if (is_null($details['middlename']) OR empty($details['middlename'])) {
    $middlename = "N/A";
} else {
    $middlename = $details['middlename'];
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">                                   
                <h3 class="panel-title"><?php echo $details['transaction_reference'] . ' - ' . $details['count'] . "(" . ucwords(strtolower($status)) . ") || "; ?> </h3>
                <a href="?view_payments"><h3 class="panel-title">Payment Transactions </h3></a>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">  
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-5 control-label">Transaction ID</label>
                        <label class="col-md-5 control-label"><?php echo $details['transaction_id']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Source</label>
                        <label class="col-md-5 control-label"><?php echo $details['source']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Account</label>
                        <label class="col-md-5 control-label"><?php echo $business_short_code; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Transaction Type</label>
                        <label class="col-md-5 control-label"><?php echo $details['transaction_type']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Transaction Reference</label>
                        <label class="col-md-5 control-label"><?php echo $details['transaction_reference']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Transaction Time</label>
                        <label class="col-md-5 control-label"><?php echo $details['transaction_time']; ?></label>
                    </div>                    
                    <div class="form-group">
                        <label class="col-md-5 control-label">Received At</label>
                        <label class="col-md-5 control-label"><?php echo $details['createdat']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Amount</label>
                        <label class="col-md-5 control-label"><?php echo $details['transaction_amount']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Invoice Number</label>
                        <label class="col-md-5 control-label"><?php echo $invoice_number; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Third Party Transaction Reference</label>
                        <label class="col-md-5 control-label"><?php echo $details['third_party_trans_id']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Sender's Firstname</label>
                        <label class="col-md-5 control-label"><?php echo $details['firstname']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Sender's Middlename</label>
                        <label class="col-md-5 control-label"><?php echo $middlename; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Sender's Lastname</label>
                        <label class="col-md-5 control-label"><?php echo $details['lastname']; ?></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">Sender's Contact</label>
                        <label class="col-md-5 control-label"><?php echo $details['phone_number']; ?></label>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>              