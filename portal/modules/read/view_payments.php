<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Transactions.php";
require_once WPATH . "modules/classes/System_Administration.php";
$system_administration = new System_Administration();
$transactions = new Transactions();

unset($_SESSION['payment_transaction']);
?>

<div class="row">
    <div class="col-md-12">
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-heading">                                
                <h3 class="panel-title">Payments</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Reference #</th>
                            <th>Source</th>
                            <th>Transaction ID</th>
                            <th>Transaction Type</th> 
                            <th>Amount</th>                                 
                            <th>Created At</th>                                                                         
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>                        
                        <?php
                        $count = 1;
                        if (!empty($_POST)) {
                            $payment_transactions[] = $transactions->execute();
                        } else {
                            $payment_transactions[] = $transactions->getAllPaymentTransactions();
                        }
                        if (isset($_SESSION['no_records']) AND $_SESSION['no_records'] == true) {
                            echo "<tr>";
                            echo "<td>  No record found...</td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "</tr>";
                            unset($_SESSION['no_records']);
                        } else if (isset($_SESSION['yes_records']) AND $_SESSION['yes_records'] == true) {
                            foreach ($payment_transactions as $key => $value) {
                                $inner_array[$key] = json_decode($value, true); // this will give key val pair array
                                foreach ((array) $inner_array[$key] as $key2 => $value2) {
                                    $status_details = $system_administration->fetchSystemStatusDetails($value2['status']);
                                    $status = $status_details['display_value'];

                                    echo "<tr>";
                                    echo "<td><a href='?view_payment_details&code={$value2['count']}'>" . $count++ . "</td>";
                                    echo "<td>" . $value2['transaction_reference'] . "</td>";
                                    echo "<td>" . $value2['source'] . "</td>";
                                    echo "<td>" . $value2['transaction_id'] . "</td>";
                                    echo "<td>" . $value2['transaction_type'] . "</td>";
                                    echo "<td>" . $value2['transaction_amount'] . "</td>";
                                    echo "<td>" . $value2['createdat'] . "</td>";
                                    echo "<td>" . $status . "</td>";
                                    echo "</tr>";
                                }
                            }
                            unset($_SESSION['yes_records']);
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END DEFAULT DATATABLE -->
    </div>
</div>                                

</div>
<!-- PAGE CONTENT WRAPPER -->  