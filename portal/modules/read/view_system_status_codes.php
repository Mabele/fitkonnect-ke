<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/System_Administration.php";
$system_administration = new System_Administration();

unset($_SESSION['system_status_code']);
unset($_SESSION['search']);
?>

<div class="row">
    <div class="col-md-12">
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-heading">            
                <h3 class="panel-title">System Status Codes || </h3>
                <a href="?add_system_status_code"><h3 class="panel-title">Add System Status Code</h3></a> 
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Code</th>
                            <th>Description</th>
                            <th>Displayed Value</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>                        
                        <?php
                        $count = 1;
                        if (!empty($_POST)) {
                            $system_status_codes[] = $system_administration->execute();
                        } else {
                            $system_status_codes[] = $system_administration->getAllSystemStatusCodes();
                        }
                        if (isset($_SESSION['no_records']) AND $_SESSION['no_records'] == true) {
                            echo "<tr>";
                            echo "<td>  No record found...</td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "<td> </td>";
                            echo "</tr>";
                            unset($_SESSION['no_records']);
                        } else if (isset($_SESSION['yes_records']) AND $_SESSION['yes_records'] == true) {
                            foreach ($system_status_codes as $key => $value) {
                                $inner_array[$key] = json_decode($value, true); // this will give key val pair array
                                foreach ((array) $inner_array[$key] as $key2 => $value2) {
                                    echo "<tr>";
                                    echo "<td>" . $count++ . "</td>";
                                    echo "<td>" . $value2['status_code'] . "</td>";
                                    echo "<td>" . $value2['description'] . "</td>";
                                    echo "<td>" . $value2['display_value'] . "</td>";
                                    echo "<td> <a href='?update_system_status_code_details&code=" . $value2['id'] . "'> EDIT </td>";
                                }
                            }
                            unset($_SESSION['yes_records']);
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>                                
