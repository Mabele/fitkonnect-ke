<?php

require_once WPATH . "modules/classes/Transactions.php";
$transactions = new Transactions();

if (isset($_GET['keyword'])) {
    if (strtoupper($keyword) == "LESSON_ASSIGNMENT") {  // When lesson is assigned an SMS/Email is sent to the trainer requesting him to accept/decline the assignment [TESTSMS:LESSON ASSIGNMENT,DECLINE]
        if (strpos(strtoupper($response), 'DECLINE') !== false) {
            $_SESSION['reassign_trainer'] = true;
            $reassign_reason = "TRAINER DECLINED LESSON ASSIGNMENT";
            $code = substr(strtoupper($response), 7);
            $transactions->declineLessonAssignment($code, $decline_reason);
//            $new_trainer = 5;
//            $transactions->autoReassignLessonTrainer($code, $new_trainer, $reassign_reason);
        }
    } else if (strtoupper($keyword) == "TRAINER ACCEPTANCE") {  // Once trainer accepts, trainee is notified and requested to accept/decline the assigned trainer [TESTSMS:TRAINER ACCEPTANCE,DECLINE]
        if (strpos(strtoupper($response), 'DECLINE') !== false) {
            $_SESSION['reassign_trainer'] = true;
            $reassign_reason = "TRAINEE DECLINED ASSIGNED TRAINER";
            $code = substr(strtoupper($response), 7);
            $new_trainer = 8;

            $transactions->autoReassignLessonTrainer($code, $new_trainer, $reassign_reason);
        }
    } else if (strtoupper($keyword) == "ATTENDANCE CONFIRMATION") {  // 5 hours before the lesson is due, an SMS is sent to the trainee to confirm attendance as scheduled. Trainee can reply either reschedule/cancel [TESTSMS:ATTENDANCE CONFIRMATION,RESCHEDULE], [TESTSMS:ATTENDANCE CONFIRMATION,CANCEL]
        if (strpos(strtoupper($response), 'RESCHEDULE') !== false) {
            $code = substr($received_message[1], 10);
            $new_start_date = $received_message[2];
            $reschedule_reason = strtoupper($received_message[3]);
            $transactions->rescheduleLesson($code, $new_start_date, $reschedule_reason);
        } else if (strpos(strtoupper($response), 'CANCEL') !== false) {
            $code = substr(strtoupper($response), 6);
            $transactions->cancelLesson($code);
        }
    } else if (strpos(strtoupper($keyword), 'CLOSE') !== false) {  // Once lesson training is done, trainer closses the lesson by sending [TESTSMS:CLOSE,{LESSONID}]
        // check if parsed lessonID ($response) relates to the trainer in question --compare phone numbers--
        // if yes () {
        //          update lesson record to 'closed'
        //          send trainee an SMS requesting him/her to rate trainer/facility 
        // } else {
        // send the trainer a message informing him of the mismatch and requesting him/her to check and resend the response
        // }
        $code = substr(strtoupper($keyword), 5);
        $record_details = $transactions->fetchBookingTransactionDetailDetails($code);
        $transactions->closeLessonTraining($code, $record_details['transaction_id']);
    } else if (strpos(strtoupper($keyword), 'RATE') !== false) {  // Once lesson is closed trainee is prompted to rate the trainer/facility by sending [TESTSMS:RATE,TRAINER,1],[TESTSMS:RATE,TRAINER,2] etc ........[TESTSMS:RATE,FACILITY,1],[TESTSMS:RATE,FACILITY,2] etc
        $code = substr(strtoupper($received_message[0]), 4);
        $trainer_rating = substr(strtoupper($received_message[1]), 7);
        $facility_rating = substr(strtoupper($received_message[2]), 8);
        $record_details = $transactions->fetchBookingTransactionDetailDetails($code);
        $transactions->rateTrainingSession($code, $record_details['transaction_id'], $trainer_rating, $facility_rating);
    }
    echo "Successful";
} else {
    echo "Failed";
}
?>

