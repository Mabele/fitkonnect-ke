<!-- START PAGE SIDEBAR -->
<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <li class="xn-logo">
            <a href="?"> <img style="vertical-align: center;"src="img/logo/fitnessIQ.png" width="140" alt=""></a>
            <a href="#" class="x-navigation-control"></a>
        </li>
        <li class="xn-profile">

            <?php
            if ($_SESSION['logged_in_user_type_details']['name'] == "STAFF") {
                $user = $_SESSION['logged_in_user_details']['firstname'] . " " . $_SESSION['logged_in_user_details']['lastname'];
                $title = 'Staff Member';
            } else if ($_SESSION['logged_in_user_type_details']['name'] == "TRAINEE") {
                $user = $_SESSION['logged_in_user_details']['firstname'] . " " . $_SESSION['logged_in_user_details']['lastname'];
                $title = 'Trainee';
            } else if ($_SESSION['logged_in_user_type_details']['name'] == "TRAINER") {
                $user = $_SESSION['logged_in_user_details']['firstname'] . " " . $_SESSION['logged_in_user_details']['lastname'];
                $title = 'Trainer';
            } else if ($_SESSION['logged_in_user_type_details']['name'] == "TRAINING FACILITY ADMINISTRATOR") {
                $user = $_SESSION['logged_in_user_details']['firstname'] . " " . $_SESSION['logged_in_user_details']['lastname'];
                $title = $_SESSION['training_facility_details']['name'] . ' ' . 'Administrator';
            } else if ($_SESSION['logged_in_user_type_details']['name'] == "SYSTEM ADMINISTRATOR") {
                $user = $_SESSION['logged_in_user_details']['firstname'] . " " . $_SESSION['logged_in_user_details']['lastname'];
                $title = 'System Administrator';
            }
            ?>

            <a href="#" class="profile-mini">
                <img src="assets/images/users/avatar.jpg" alt="John Doe"/>
            </a>
            <div class="profile">
                <div class="profile-image">
                    <img src="assets/images/users/avatar.jpg" alt="John Doe"/>
                </div>
                <div class="profile-data">
                    <div class="profile-data-name"><?php echo ucwords(strtolower($user)); ?></div>
                    <div class="profile-data-title"><?php echo ucwords(strtolower($title)); ?></div>
                </div>
                <div class="profile-controls">
                    <a href="?profile" class="profile-control-left"><span class="fa fa-info"></span></a>
                    <a href="?incoming" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                </div>
            </div>                                                                        
        </li>
        <li class="xn-title">Navigation</li>
        <li class="active">
            <a href="?home"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
        </li>

        <?php if ($_SESSION['logged_in_user_type_details']['name'] == "STAFF") { ?>
            <li class="xn-openable">
                <a href="#"><span class="fa fa-xing"></span> <span class="xn-text"> Training Management</span></a>
                <ul>
                    <li class="xn-text">
                        <a href="?view_trainers"><span class="fa fa-group"></span> Trainers</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_trainees"><span class="fa fa-group"></span> Trainees</a>
                    </li>
                </ul>
            </li>        
            <li class="xn-openable">
                <a href="#"><span class="fa fa-bookmark"></span> <span class="xn-text"> Lessons Management</span></a>
                <ul>
                    <li class="xn-text">
                        <a href="?view_lessons"><span class="fa fa-group"></span> All Lessons</a>
                    </li>
                    <li class="xn-text">
                        <a href="?book_lesson"><span class="fa fa-group"></span> Book a Lesson</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_lessons"><span class="fa fa-group"></span> My Lessons</a>
                    </li>
                </ul>
            </li> 
            <li class="xn-openable">
                <a href="#"><span class="fa fa-group"></span> <span class="xn-text"> Contacts Management</span></a>
                <ul>
                    <li class="xn-text">
                        <a href="?view_contacts"><span class="fa fa-group"></span> Contacts</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_incoming_sms"><span class="fa fa-group"></span> Incoming SMSs</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_outgoing_sms"><span class="fa fa-group"></span> Outgoing SMSs</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_sms"><span class="fa fa-group"></span> All SMSs</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_inbox_messages"><span class="fa fa-group"></span> All Inbox Messages</a>
                    </li>
                </ul>
            </li> 

            <li class="xn-openable">
                <a href="#"><span class="fa fa-location-arrow"></span> <span class="xn-text"> Facilities Management</span></a>
                <ul>     
                    <li class="xn-text">
                        <a href="?view_training_facilities"><span class="fa fa-group"></span> Training Facilities</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_training_facility_administrators"><span class="fa fa-group"></span> Facility Administrators</a>
                    </li>
                </ul>
            </li>         
            <li class="xn-openable">
                <a href="#"><span class="fa fa-money"></span> <span class="xn-text">Transactions Management</span></a>
                <ul>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-group"></span> Lesson Bookings</a>
                        <ul>
                            <li><a href="?view_lesson_bookings"><span class="fa fa-database"></span> Booking Transactions</a></li>
                            <li><a href="?view_lesson_bookings_details"><span class="fa fa-database"></span> Lesson Bookings</a></li>
                        </ul>
                    </li> 
                    <li class="xn-text">
                        <a href="?view_payments"><span class="fa fa-group"></span> Payments</a>
                    </li>
                </ul>
            </li>
        <?php // } else if ($_SESSION['logged_in_user_type_details']['name'] == "TRAINEE") { ?>
<!--            <li class="xn-openable">
                <a href="#"><span class="fa fa-xing"></span> <span class="xn-text"> Training Management</span></a>
                <ul>     
                    <li class="xn-text">
                        <a href="?view_trainers"><span class="fa fa-group"></span> My Trainers</a>
                    </li>
                </ul>
            </li>    
            <li class="xn-openable">
                <a href="#"><span class="fa fa-bookmark"></span> <span class="xn-text"> Lessons Management</span></a>
                <ul>
                    <li class="xn-text">
                        <a href="?book_lesson"><span class="fa fa-group"></span> Book a Lesson</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_lessons"><span class="fa fa-group"></span> My Lessons</a>
                    </li>
                </ul>
            </li> 
            <li class="xn-openable">
                <a href="#"><span class="fa fa-location-arrow"></span> <span class="xn-text"> Facilities Management</span></a>
                <ul>   
                    <li class="xn-text">
                        <a href="?view_training_facilities"><span class="fa fa-group"></span> All Facilities</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_training_facilities"><span class="fa fa-group"></span> My Facilities</a>
                    </li>
                </ul>
            </li>         
            <li class="xn-openable">
                <a href="#"><span class="fa fa-money"></span> <span class="xn-text">Transactions Management</span></a>
                <ul>   
                    <li class="xn-text">
                        <a href="?view_lesson_bookings"><span class="fa fa-group"></span> My Transactions</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_lesson_bookings_details"><span class="fa fa-group"></span> My Lesson Bookings</a>
                    </li>
                </ul>
            </li>-->
        <?php } else if ($_SESSION['logged_in_user_type_details']['name'] == "TRAINER") { ?>            
            <li class="xn-openable">
                <a href="#"><span class="fa fa-xing"></span> <span class="xn-text"> Training Management</span></a>
                <ul>     
                    <li class="xn-text">
                        <a href="?view_trainees"><span class="fa fa-group"></span> My Trainees</a>
                    </li>
                </ul>
            </li>                  
            <li class="xn-openable">
                <a href="#"><span class="fa fa-bookmark"></span> <span class="xn-text"> Lessons Management</span></a>
                <ul>
                    <li class="xn-text">
                        <a href="?view_lessons"><span class="fa fa-group"></span> Lessons</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_lessons"><span class="fa fa-group"></span> My Lessons</a>
                    </li>
                    <li class="xn-text">
                        <a href="?book_lesson"><span class="fa fa-group"></span> Book a Lesson</a>
                    </li>
                </ul>
            </li> 
            <li class="xn-openable">
                <a href="#"><span class="fa fa-location-arrow"></span> <span class="xn-text"> Facilities Management</span></a>
                <ul> 
                    <li class="xn-text">
                        <a href="?view_training_facilities"><span class="fa fa-group"></span> All Facilities</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_training_facilities"><span class="fa fa-group"></span> My Facilities</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_training_facility_administrators"><span class="fa fa-group"></span> My Facility Administrators</a>
                    </li>
                </ul>
            </li>         
            <li class="xn-openable">
                <a href="#"><span class="fa fa-money"></span> <span class="xn-text">Transactions Management</span></a>                
                <ul> 
                    <li class="xn-text">
                        <a href="?view_my_assigned_lessons"><span class="fa fa-group"></span> My Assigned Lessons</a>
                    </li>                    
                    <li class="xn-text">
                        <a href="?view_my_lesson_bookings"><span class="fa fa-group"></span> My Lesson Bookings</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_training_facility_administrators"><span class="fa fa-group"></span> Facility Administrators</a>
                    </li>
                </ul>
            </li> 
        <?php } else if ($_SESSION['logged_in_user_type_details']['name'] == "TRAINING FACILITY ADMINISTRATOR") { ?>
            <li class="xn-openable">
                <a href="#"><span class="fa fa-xing"></span> <span class="xn-text"> Training Management</span></a>
                <ul>     
                    <li class="xn-text">
                        <a href="?view_trainers"><span class="fa fa-group"></span> Our Trainers</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_trainees"><span class="fa fa-group"></span> Our Trainees</a>
                    </li>
                </ul>
            </li>     
            <li class="xn-openable">
                <a href="#"><span class="fa fa-bookmark"></span> <span class="xn-text"> Lesson Management</span></a>
                <ul>                            
                    <li class="xn-text">
                        <a href="?view_lessons"><span class="fa fa-group"></span> All Lessons</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_lessons"><span class="fa fa-group"></span> Our Lessons</a>
                    </li>
                </ul>
            </li>        
            <li class="xn-openable">
                <a href="#"><span class="fa fa-money"></span> <span class="xn-text">Transactions Management</span></a>                
                <ul>                     
                    <li class="xn-text">
                        <a href="?view_lesson_bookings_details"><span class="fa fa-group"></span> Our Lesson Bookings</a>
                    </li>
                </ul>
            </li> 
        <?php } else if ($_SESSION['logged_in_user_type_details']['name'] == "SYSTEM ADMINISTRATOR") { ?>
            <li class="xn-openable">
                <a href="#"><span class="fa fa-xing"></span> <span class="xn-text"> Training Management</span></a>
                <ul>   
                    <li class="xn-text">
                        <a href="?view_staff"><span class="fa fa-group"></span> Staff</a>
                    </li>                    
                    <li class="xn-text">
                        <a href="?view_trainers"><span class="fa fa-group"></span> Trainers</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_trainees"><span class="fa fa-group"></span> Trainees</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_staff_positions"><span class="fa fa-group"></span> Staff Positions</a>
                    </li>
                </ul>
            </li>        
            <li class="xn-openable">
                <a href="#"><span class="fa fa-bookmark"></span> <span class="xn-text"> Lessons Management</span></a>
                <ul>
                    <li class="xn-text">
                        <a href="?view_lessons"><span class="fa fa-group"></span> Lessons</a>
                    </li>                    
                    <li class="xn-text">
                        <a href="?view_lesson_categories"><span class="fa fa-group"></span> Lesson Categories</a>
                    </li>
                </ul>
            </li> 
            <li class="xn-openable">
                <a href="#"><span class="fa fa-group"></span> <span class="xn-text"> Users Management</span></a>
                <ul>                    
                    <li class="xn-text">
                        <a href="?view_system_roles"><span class="fa fa-group"></span> System Roles</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_system_administrators"><span class="fa fa-group"></span> System Administrators</a>
                    </li>                    
                    <li class="xn-text">
                        <a href="?view_system_privileges"><span class="fa fa-group"></span> System Privileges</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_system_status_codes"><span class="fa fa-group"></span> System Status Codes</a>
                    </li>                    
                    <li class="xn-text">
                        <a href="?view_user_types"><span class="fa fa-group"></span> User Types</a>
                    </li>
<!--                    <li class="xn-text">
                        <a href="?view_role_privileges"><span class="fa fa-group"></span> Role-Privileges</a>
                    </li>                    -->
<!--                    <li class="xn-text">
                        <a href="?view_user_privileges"><span class="fa fa-group"></span> User-Privileges</a>
                    </li>-->
<!--                    <li class="xn-text">
                        <a href="?view_user_roles"><span class="fa fa-group"></span> User-Roles</a>
                    </li>-->
                </ul>
            </li>
            <li class="xn-openable">
                <a href="#"><span class="fa fa-group"></span> <span class="xn-text"> Contacts Management</span></a>
                <ul>
                    <li class="xn-text">
                        <a href="?view_contacts"><span class="fa fa-group"></span> Contacts</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_incoming_sms"><span class="fa fa-group"></span> Incoming SMSs</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_outgoing_sms"><span class="fa fa-group"></span> Outgoing SMSs</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_sms"><span class="fa fa-group"></span> All SMSs</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_inbox_messages"><span class="fa fa-group"></span> All Inbox Messages</a>
                    </li>
                </ul>
            </li> 
            <li class="xn-openable">
                <a href="#"><span class="fa fa-location-arrow"></span> <span class="xn-text"> Facility Management</span></a>
                <ul> 
                    <li class="xn-text">
                        <a href="?view_training_facilities"><span class="fa fa-group"></span> Training Facilities</a>
                    </li>                    
                    <li class="xn-text">
                        <a href="?view_training_facility_administrators"><span class="fa fa-group"></span> Training Facility Administrators</a>
                    </li>
                    <li class="xn-text">
                        <a href="?view_training_facility_services"><span class="fa fa-group"></span> Training Facility Services</a>
                    </li>
                </ul>
            </li>         
            <li class="xn-openable">
                <a href="#"><span class="fa fa-money"></span> <span class="xn-text">Transactions Management</span></a>
                <ul>
                    <li class="xn-text">
                        <a href="?view_lesson_bookings"><span class="fa fa-group"></span> Booking Transactions</a>
                    </li>                    
                    <li class="xn-text">
                        <a href="?view_lesson_bookings_details"><span class="fa fa-group"></span> Lesson Bookings</a>
                    </li>                   
                    <li class="xn-text">
                        <a href="?view_payments"><span class="fa fa-group"></span> Payments</a>
                    </li>
                </ul>
            </li>
            <li class="xn-openable">
                <a href="#"><span class="fa fa-money"></span> <span class="xn-text">System Management</span></a>
                <ul>
                    <li class="xn-text">
                        <a href="?view_system_components"><span class="fa fa-group"></span> System Components</a>
                    </li>                  
                    <li class="xn-text">
                        <a href="?view_system_privileges"><span class="fa fa-group"></span> System Privileges</a>
                    </li>               
                    <li class="xn-text">
                        <a href="?view_counties"><span class="fa fa-group"></span> Counties</a>
                    </li>                   
                    <li class="xn-text">
                        <a href="?view_wards"><span class="fa fa-group"></span> Wards</a>
                    </li>  
                </ul>
            </li>
        <?php } ?>







        <!--        <li class="xn-openable">
                    <a href="#"><span class="fa fa-xing"></span> <span class="xn-text"> Trainers Management</span></a>
                    <ul>                            
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-group"></span> Staff</a>
                            <ul>
                                <li><a href="?add_staff"><span class="fa fa-plus"></span> Add Staff</a></li>
                                <li><a href="?view_staff"><span class="fa fa-database"></span> View Staff</a></li>
                            </ul>
                        </li>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-navicon"></span> Staff Position</a>
                            <ul>
                                <li><a href="?add_staff_position"><span class="fa fa-plus"></span> Add Staff Position</a></li>
                                <li><a href="?view_staff_positions"><span class="fa fa-database"></span> View Staff Positions</a></li>
                            </ul>
                        </li>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-group"></span> Trainers</a>
                            <ul>
                                <li><a href="?add_trainer"><span class="fa fa-plus"></span> Add Trainer</a></li>
                                <li><a href="?view_trainers"><span class="fa fa-database"></span> View Trainers</a></li>
                            </ul>
                        </li>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-group"></span> Trainees</a>
                            <ul>
                                <li><a href="?add_trainee"><span class="fa fa-plus"></span> Add Trainee</a></li>
                                <li><a href="?view_trainees"><span class="fa fa-database"></span> View Trainees</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>        
                <li class="xn-openable">
                    <a href="#"><span class="fa fa-bookmark"></span> <span class="xn-text"> Lesson Management</span></a>
                    <ul>                            
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-group"></span> Lesson Categories</a>
                            <ul>
                                <li><a href="?add_lesson_category"><span class="fa fa-plus"></span> Add Lesson Category</a></li>
                                <li><a href="?view_lesson_categories"><span class="fa fa-database"></span> View Lesson Categories</a></li>
                            </ul>
                        </li>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-group"></span> Lessons</a>
                            <ul>
                                <li><a href="?add_lesson"><span class="fa fa-plus"></span> Add Lesson</a></li>
                                <li><a href="?view_lessons"><span class="fa fa-database"></span> View Lessons</a></li>
                                <li><a href="?book_lesson"><span class="fa fa-plus"></span> Book a Lesson</a></li>
                            </ul>
                        </li>
                    </ul>
                </li> 
                <li class="xn-openable">
                    <a href="#"><span class="fa fa-group"></span> <span class="xn-text"> User Management</span></a>
                    <ul>                            
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-group"></span> Contacts</a>
                            <ul>
                                <li><a href="?add_contact"><span class="fa fa-plus"></span> Add Contact</a></li>
                                <li><a href="?view_contacts"><span class="fa fa-database"></span> View Contacts</a></li>
                            </ul>
                        </li>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-group"></span> Roles</a>
                            <ul>
                                <li><a href="?add_system_role"><span class="fa fa-plus"></span> Add System Role</a></li>
                                <li><a href="?view_system_roles"><span class="fa fa-database"></span> View System Roles</a></li>
                            </ul>
                        </li>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-group"></span> System Administration</a>
                            <ul>
                                <li><a href="?add_system_administrator"><span class="fa fa-plus"></span> Add System Administrator</a></li>
                                <li><a href="?view_system_administrators"><span class="fa fa-database"></span> View System Administration</a></li>
                            </ul>
                        </li>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-group"></span> System Privileges</a>
                            <ul>
                                <li><a href="?add_system_privilege"><span class="fa fa-plus"></span> Add System Privilege</a></li>
                                <li><a href="?view_system_privileges"><span class="fa fa-database"></span> View System Privileges</a></li>
                            </ul>
                        </li>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-group"></span> System Status Code</a>
                            <ul>
                                <li><a href="?add_system_status_code"><span class="fa fa-plus"></span> Add System Status Code</a></li>
                                <li><a href="view_system_status_codes?"><span class="fa fa-database"></span> View System Status Codes</a></li>
                            </ul>
                        </li>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-group"></span> User Types</a>
                            <ul>
                                <li><a href="?add_user_type"><span class="fa fa-plus"></span> Add User Type</a></li>
                                <li><a href="?view_user_types"><span class="fa fa-database"></span> View User Types</a></li>
                            </ul>
                        </li>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-group"></span> Privileges to Roles</a>
                            <ul>
                                <li><a href="?assign_privileges_to_role"><span class="fa fa-plus"></span> Assign Privileges to Role</a></li>
                                <li><a href="?view_role_privileges"><span class="fa fa-database"></span> View Role-Privileges</a></li>
                            </ul>
                        </li>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-group"></span> Privileges to User</a>
                            <ul>
                                <li><a href="?assign_privileges_to_user"><span class="fa fa-plus"></span> Assign Privileges to User</a></li>
                                <li><a href="?view_user_privileges"><span class="fa fa-database"></span> View User-Privileges</a></li>
                            </ul>
                        </li>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-group"></span> Roles to User</a>
                            <ul>
                                <li><a href="?assign_role_to_user"><span class="fa fa-plus"></span> Assign Role to User</a></li>
                                <li><a href="?view_user_roles"><span class="fa fa-database"></span> View User-Roles</a></li>
                            </ul>
                        </li>
                    </ul>
                </li> 
        
                <li class="xn-openable">
                    <a href="#"><span class="fa fa-location-arrow"></span> <span class="xn-text"> Facility Management</span></a>
                    <ul>                            
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-clock-o"></span> Training Facility Administrators</a>
                            <ul>
                                <li><a href="?add_training_facility_administrator"><span class="fa fa-plus"></span> Add Training Facility Administrator</a></li>
                                <li><a href="?view_training_facility_administrators"><span class="fa fa-database"></span> View  Training Facility Administrators</a></li>
                            </ul>
                        </li>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-clock-o"></span> Training Facilities</a>
                            <ul>
                                <li><a href="?add_training_facility"><span class="fa fa-plus"></span> Add Training Facility</a></li>
                                <li><a href="?view_training_facilities"><span class="fa fa-database"></span> View  Training Facilities</a></li>
                            </ul>
                        </li>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-clock-o"></span> Training Facility Services</a>
                            <ul>
                                <li><a href="?add_training_facility_service"><span class="fa fa-plus"></span> Add Training Facility Service</a></li>
                                <li><a href="?view_training_facility_services"><span class="fa fa-database"></span> View  Training Facility Services</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>         
                <li class="xn-openable">
                    <a href="#"><span class="fa fa-money"></span> <span class="xn-text">Transactions Management</span></a>
                    <ul>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-group"></span> Lesson Bookings</a>
                            <ul>
                                <li><a href="?view_lesson_bookings"><span class="fa fa-database"></span> View Booking Transactions</a></li>
                                <li><a href="?view_lesson_bookings_details"><span class="fa fa-database"></span> View Lesson Bookings</a></li>
                            </ul>
                        </li>                
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-group"></span> Payments</a>
                            <ul>
                                <li><a href="?view_payments"><span class="fa fa-database"></span> Payments</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
        
                <li class="xn-openable">
                    <a href="#"><span class="fa fa-money"></span> <span class="xn-text">System Management</span></a>
                    <ul>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-group"></span> System Components</a>
                            <ul>
                                <li><a href="?add_system_component"><span class="fa fa-plus"></span> Add System Component</a></li>
                                <li><a href="?view_system_components"><span class="fa fa-database"></span> View System Components</a></li>
                            </ul>
                        </li>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-group"></span> Counties</a>
                            <ul>
                                <li><a href="?add_county"><span class="fa fa-plus"></span> Add County</a></li>
                                <li><a href="?view_counties"><span class="fa fa-database"></span> View Counties</a></li>
                            </ul>
                        </li> 
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-group"></span> Wards</a>
                            <ul>
                                <li><a href="?add_ward"><span class="fa fa-plus"></span> Add Ward</a></li>
                                <li><a href="?view_wards"><span class="fa fa-database"></span> View Wards</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>-->


    </ul>
    <!-- END X-NAVIGATION -->
</div>
<!-- END PAGE SIDEBAR -->
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
