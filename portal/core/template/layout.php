<?php

if (is_menu_set('mpesa_validation') != "" OR is_menu_set('mpesa_confirmation') != "" OR is_menu_set('mpesa_registration') != "") {
    header("Content-Type:application/json");
} else {

// Before anything is sent, set the appropriate header
header('Content-Type: text/html; charset=UTF-8');
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
    <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <!--[if IE]>
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <![endif]-->
        <meta name="description" content="Fitness IQ Ventures | Redefining Fitness">
        <meta name="keywords" content="fitnessIQ, health and wellness, health and fitness">
        <meta name="author" content="Be Bulinda[be.co.ke]">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="images/favicon_r.ico" type="image/ico" sizes="16x16 32x32">
        <link rel="icon" href="images/favicon_r.png" type="image/png" sizes="16x16 32x32">
        <link rel="icon" href="images/favicon_r.svg" type="image/png" sizes="16x16 32x32">         
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" id="theme" href="web/css/theme-default.css"/>
        <?php
        /*         * *
         * This section specifies the page header
         */

        // The page title
        if ($templateResource = TemplateResource::getResource('title')) {
            ?>
            <title><?php echo $templateResource; ?></title>
        <?php } ?>	
        <!-- Basic CSS -->
        <!-- End of basic CSS -->
        <?php
        // The CSS included
        if ($templateResource = TemplateResource::getResource('css')) {
            ?>
            <!-- Additional CSS -->
            <?php
            foreach ($templateResource as $style) {
                $style = "web/$style";
                ?>
                <link rel="stylesheet" href="<?php echo $style; ?>" />
                <?php
            }
            ?>
            <!-- Additional CSS end -->
            <?php
        }
        ?>

        <!-- Favicon and touch icons -->


    </head>
    <!--    <body>-->

    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">


            <?php
            if (App::isLoggedIn()) {
                require "header.php";
                require "vertical_header.php";
                require "breadcrumps.php";
            }
            require_once $currentPage;

            if (App::isLoggedIn()) {
                require_once "footer.php";
            }
            ?>



            <?php
//                require "header.php";
//                require "vertical_header.php";
//                require "breadcrumps.php";
//
//                require $currentPage;
//
//                require "footer.php";
            ?>

            <!-- Basic scripts -->  
            <!-- END MESSAGE BOX-->

            <!-- START PRELOADS -->
            <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
            <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
            <!-- END PRELOADS -->                  

            <!-- START SCRIPTS -->
            <!-- START PLUGINS -->
            <script type="text/javascript" src="web/js/plugins/jquery/jquery.min.js"></script>
            <script type="text/javascript" src="web/js/plugins/jquery/jquery-ui.min.js"></script>
            <script type="text/javascript" src="web/js/plugins/bootstrap/bootstrap.min.js"></script>        
            <!-- END PLUGINS -->

            <!-- START THIS PAGE PLUGINS-->        
            <script type='text/javascript' src='web/js/plugins/icheck/icheck.min.js'></script>        
            <script type="text/javascript" src="web/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
            <script type="text/javascript" src="web/js/plugins/scrolltotop/scrolltopcontrol.js"></script>

            <script type="text/javascript" src="web/js/plugins/morris/raphael-min.js"></script>
            <script type="text/javascript" src="web/js/plugins/morris/morris.min.js"></script>       
            <script type="text/javascript" src="web/js/plugins/rickshaw/d3.v3.js"></script>
            <script type="text/javascript" src="web/js/plugins/rickshaw/rickshaw.min.js"></script>
            <script type='text/javascript' src='web/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
            <script type='text/javascript' src='web/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>                
            <script type='text/javascript' src='web/js/plugins/bootstrap/bootstrap-datepicker.js'></script>                
            <script type="text/javascript" src="web/js/plugins/owl/owl.carousel.min.js"></script>                 

            <script type="text/javascript" src="web/js/plugins/moment.min.js"></script>
            <script type="text/javascript" src="web/js/plugins/daterangepicker/daterangepicker.js"></script>
            <!-- END THIS PAGE PLUGINS-->        

            <!-- START TEMPLATE -->
            <script type="text/javascript" src="web/js/settings.js"></script>

            <script type="text/javascript" src="web/js/plugins.js"></script>        
            <script type="text/javascript" src="web/js/actions.js"></script>

            <script type="text/javascript" src="web/js/demo_dashboard.js"></script>
            <!-- END TEMPLATE -->
            <!-- END SCRIPTS -->         
            <!-- End of basic scripts -->
            <?php
            /*             * *
             * Specify the scripts that are to be added.
             */
            if ($templateResource = TemplateResource::getResource('js')) {
                ?>
                <!-- Additional Scripts -->
                <?php
                foreach ($templateResource as $js) {
                    $js = "web/$js";
                    ?>
                    <script src="<?php echo $js; ?>"></script>
                    <?php
                }
                ?>
                <?php
            }
            ?>
            <?php if (!App::isLoggedIn()) { ?>
                <script>
                    jQuery(document).ready(function () {
                        App.initLogin();
                    });
                </script>
            <?php } else { ?>
                <script>
                    jQuery(document).ready(function () {
                        // initiate layout and plugins
                        App.init();
                        //App.setMainPage(true);

                    });
                </script>
                <?php
            }
            ?>
        </div>
    </body>
</html>
<?php } ?>