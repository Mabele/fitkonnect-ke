<?php

require WPATH . "core/include.php";
$currentPage = "";

$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

if (strpos($actual_link, 'mobile') !== false AND strpos($actual_link, 'message') !== false AND strpos($actual_link, 'shortcode') !== false) {
    $currentPage = WPATH . "modules/sms/receive_shortcode_messages.php";
    set_title("FitKonnect KE | Receive Shortcode Messages");
} else if (is_menu_set('logout') != "") {
    App::logOut();
} else if (is_menu_set('home') != "") {
    $currentPage = WPATH . "modules/login.php";
    set_title("FitKonnect KE | Login");
} else if (is_menu_set('dashboard') != "") {
    $currentPage = WPATH . "modules/home.php";
    set_title("FitKonnect KE | Dashboard");
} else if (is_menu_set('login') != "") {
    $currentPage = WPATH . "modules/login.php";
    set_title("FitKonnect KE | Login");
} else if (is_menu_set('forgot_password') != "") {
    $currentPage = WPATH . "modules/forgot_password.php";
    set_title("FitKonnect KE | Reset Password");
} else if (is_menu_set('home') != "") {
    $currentPage = WPATH . "modules/home.php";
    set_title("FitKonnect KE | Fitness Redefined  | Portal");
} else if (is_menu_set('website_requests') != "") {
    $currentPage = WPATH . "modules/website_requests.php";
    set_title("FitKonnect KE | Website Requests");
} else if (is_menu_set('transaction_interactions') != "") {
    $currentPage = WPATH . "modules/transaction_interactions.php";
    set_title("FitKonnect KE | Transaction Interactions");
} else if (is_menu_set('add_system_status_code') != "") {
    $currentPage = WPATH . "modules/add/add_system_status_code_form.php";
    set_title("FitKonnect KE | Add System Status Code Form");
} else if (is_menu_set('add_county') != "") {
    $currentPage = WPATH . "modules/add/add_county_form.php";
    set_title("FitKonnect KE | Add County Form");
} else if (is_menu_set('add_ward') != "") {
    $currentPage = WPATH . "modules/add/add_ward_form.php";
    set_title("FitKonnect KE | Add Ward Form");
} else if (is_menu_set('add_training_facility_service') != "") {
    $currentPage = WPATH . "modules/add/add_training_facility_service_form.php";
    set_title("FitKonnect KE | Add Training Facility Service Form");
} else if (is_menu_set('add_lesson_category') != "") {
    $currentPage = WPATH . "modules/add/add_lesson_category_form.php";
    set_title("FitKonnect KE | Add Lesson Category Form");
} else if (is_menu_set('add_staff_position') != "") {
    $currentPage = WPATH . "modules/add/add_staff_position_form.php";
    set_title("FitKonnect KE | Add Staff Position Form");
} else if (is_menu_set('add_user_type') != "") {
    $currentPage = WPATH . "modules/add/add_user_type_form.php";
    set_title("FitKonnect KE | Add User Type Form");
} else if (is_menu_set('add_system_role') != "") {
    $currentPage = WPATH . "modules/add/add_system_role_form.php";
    set_title("FitKonnect KE | Add System Role Form");
} else if (is_menu_set('assign_privileges_to_role') != "") {
    $currentPage = WPATH . "modules/add/assign_privileges_to_role_form.php";
    set_title("FitKonnect KE | Role-Privileges Assignment Form");
} else if (is_menu_set('assign_role_to_user') != "") {
    $currentPage = WPATH . "modules/add/assign_role_to_user_form.php";
    set_title("FitKonnect KE | User-Role Assignment Form");
} else if (is_menu_set('assign_privileges_to_user') != "") {
    $currentPage = WPATH . "modules/add/assign_privileges_to_user_form.php";
    set_title("FitKonnect KE | User-Privileges Assignment Form");
} else if (is_menu_set('add_system_component') != "") {
    $currentPage = WPATH . "modules/add/add_system_component_form.php";
    set_title("FitKonnect KE | Add System Component Form");
} else if (is_menu_set('add_system_privilege') != "") {
    $currentPage = WPATH . "modules/add/add_system_privilege_form.php";
    set_title("FitKonnect KE | Add System Privilege Form");
} else if (is_menu_set('add_training_facility') != "") {
    $currentPage = WPATH . "modules/add/add_training_facility_form.php";
    set_title("FitKonnect KE | Add Training Facility Form");
} else if (is_menu_set('add_training_facility_administrator') != "") {
    $currentPage = WPATH . "modules/add/add_training_facility_administrator_form.php";
    set_title("FitKonnect KE | Add Training Facility Administrator Form");
} else if (is_menu_set('add_trainer') != "") {
    $currentPage = WPATH . "modules/add/add_trainer_form.php";
    set_title("FitKonnect KE | Add Trainer Form");
} else if (is_menu_set('add_trainee') != "") {
    $currentPage = WPATH . "modules/add/add_trainee_form.php";
    set_title("FitKonnect KE | Add Trainee Form");
} else if (is_menu_set('add_staff') != "") {
    $currentPage = WPATH . "modules/add/add_staff_form.php";
    set_title("FitKonnect KE | Add Staff Form");
} else if (is_menu_set('add_system_administrator') != "") {
    $currentPage = WPATH . "modules/add/add_system_administrator_form.php";
    set_title("FitKonnect KE | Add System Administrator Form");
} else if (is_menu_set('add_contact') != "") {
    $currentPage = WPATH . "modules/add/add_contacts_form.php";
    set_title("FitKonnect KE | Add Contact Form");
} else if (is_menu_set('add_lesson') != "") {
    $currentPage = WPATH . "modules/add/add_lesson_form.php";
    set_title("FitKonnect KE | Add Lesson Form");
} else if (is_menu_set('book_lesson') != "") {
    $currentPage = WPATH . "modules/add/lesson_booking_form.php";
    set_title("FitKonnect KE | Lesson Booking Form");
} else if (is_menu_set('send_email') != "") {
    $currentPage = WPATH . "modules/add/send_email.php";
    set_title("FitKonnect KE | Send Email");
} else if (is_menu_set('view_inbox_messages') != "") {
    $currentPage = WPATH . "modules/read/view_inbox_messages.php";
    set_title("FitKonnect KE | Inbox Messages");
} else if (is_menu_set('view_inbox_message_details') != "") {
    $currentPage = WPATH . "modules/read/view_inbox_message_details.php";
    set_title("FitKonnect KE | Inbox Message Details");
} else if (is_menu_set('send_sms') != "") {
    $currentPage = WPATH . "modules/add/send_sms.php";
    set_title("FitKonnect KE | Send SMS");
} else if (is_menu_set('view_contacts') != "") {
    $currentPage = WPATH . "modules/read/view_contacts.php";
    set_title("FitKonnect KE | View Contacts");
} else if (is_menu_set('view_contact_details') != "") {
    $currentPage = WPATH . "modules/read/view_contact_details.php";
    set_title("FitKonnect KE | View Contact Details");
} else if (is_menu_set('view_sms') != "") {
    $currentPage = WPATH . "modules/read/view_sms.php";
    set_title("FitKonnect KE | View SMS Messages");
} else if (is_menu_set('view_incoming_sms') != "") {
    $currentPage = WPATH . "modules/read/view_sms.php";
    set_title("FitKonnect KE | View Incoming SMS Messages");
} else if (is_menu_set('view_outgoing_sms') != "") {
    $currentPage = WPATH . "modules/read/view_sms.php";
    set_title("FitKonnect KE | View Outgoing SMS Messages");
} else if (is_menu_set('view_default_sms') != "") {
    $currentPage = WPATH . "modules/read/view_sms.php";
    set_title("FitKonnect KE | View Default SMS Messages");
} else if (is_menu_set('view_counties') != "") {
    $currentPage = WPATH . "modules/read/view_counties.php";
    set_title("FitKonnect KE | View Counties");
} else if (is_menu_set('view_wards') != "") {
    $currentPage = WPATH . "modules/read/view_wards.php";
    set_title("FitKonnect KE | View Wards");
} else if (is_menu_set('view_lesson_categories') != "") {
    $currentPage = WPATH . "modules/read/view_lesson_categories.php";
    set_title("FitKonnect KE | View Lesson Categories");
} else if (is_menu_set('view_lessons') != "") {
    $currentPage = WPATH . "modules/read/view_lessons.php";
    set_title("FitKonnect KE | View Lessons");
} else if (is_menu_set('view_lesson_details') != "") {
    $currentPage = WPATH . "modules/read/view_lesson_details.php";
    set_title("FitKonnect KE | View Lesson Details");
} else if (is_menu_set('view_system_roles') != "") {
    $currentPage = WPATH . "modules/read/view_system_roles.php";
    set_title("FitKonnect KE | View System Roles");
} else if (is_menu_set('view_user_roles') != "") {
    $currentPage = WPATH . "modules/read/view_user_roles.php";
    set_title("FitKonnect KE | View User Roles");
} else if (is_menu_set('view_staff') != "") {
    $currentPage = WPATH . "modules/read/view_staff.php";
    set_title("FitKonnect KE | View Staff");
} else if (is_menu_set('view_staff_details') != "") {
    $currentPage = WPATH . "modules/read/view_staff_details.php";
    set_title("FitKonnect KE | View Staff Details");
} else if (is_menu_set('view_staff_positions') != "") {
    $currentPage = WPATH . "modules/read/view_staff_positions.php";
    set_title("FitKonnect KE | View Staff Positions");
} else if (is_menu_set('view_system_administrators') != "") {
    $currentPage = WPATH . "modules/read/view_system_administrators.php";
    set_title("FitKonnect KE | View System Administrators");
} else if (is_menu_set('view_system_components') != "") {
    $currentPage = WPATH . "modules/read/view_system_components.php";
    set_title("FitKonnect KE | View System Components");
} else if (is_menu_set('view_system_privileges') != "") {
    $currentPage = WPATH . "modules/read/view_system_privileges.php";
    set_title("FitKonnect KE | View System Privileges");
} else if (is_menu_set('view_role_privileges') != "") {
    $currentPage = WPATH . "modules/read/view_role_privileges.php";
    set_title("FitKonnect KE | View Role Privileges");
} else if (is_menu_set('view_user_privileges') != "") {
    $currentPage = WPATH . "modules/read/view_user_privileges.php";
    set_title("FitKonnect KE | View User Privileges");
} else if (is_menu_set('view_system_status_codes') != "") {
    $currentPage = WPATH . "modules/read/view_system_status_codes.php";
    set_title("FitKonnect KE | View System Status Codes");
} else if (is_menu_set('view_trainees') != "") {
    $currentPage = WPATH . "modules/read/view_trainees.php";
    set_title("FitKonnect KE | View Trainees");
} else if (is_menu_set('view_trainers') != "") {
    $currentPage = WPATH . "modules/read/view_trainers.php";
    set_title("FitKonnect KE | View Trainers");
} else if (is_menu_set('view_training_facility_administrators') != "") {
    $currentPage = WPATH . "modules/read/view_training_facility_administrators.php";
    set_title("FitKonnect KE | View Training Facility Administrators");
} else if (is_menu_set('view_training_facilities') != "") {
    $currentPage = WPATH . "modules/read/view_training_facilities.php";
    set_title("FitKonnect KE | View Training Facilities");
} else if (is_menu_set('view_training_facility_services') != "") {
    $currentPage = WPATH . "modules/read/view_training_facility_services.php";
    set_title("FitKonnect KE | View Training Facility Services");
} else if (is_menu_set('view_user_types') != "") {
    $currentPage = WPATH . "modules/read/view_user_types.php";
    set_title("FitKonnect KE | View User Types");
} else if (is_menu_set('view_lesson_bookings') != "") {
    $currentPage = WPATH . "modules/read/view_lesson_bookings.php";
    set_title("FitKonnect KE | View Lesson Bookings");
} else if (is_menu_set('view_payments') != "") {
    $currentPage = WPATH . "modules/read/view_payments.php";
    set_title("FitKonnect KE | View Payments");
} else if (is_menu_set('view_payment_details') != "") {
    $currentPage = WPATH . "modules/read/view_payment_details.php";
    set_title("FitKonnect KE | View Payment Details");
} else if (is_menu_set('view_individual_booking_transaction') != "") {
    $currentPage = WPATH . "modules/read/view_lesson_bookings_details.php";
    set_title("FitKonnect KE | View Booking Transaction Details");
} else if (is_menu_set('view_lesson_bookings_details') != "") {
    $currentPage = WPATH . "modules/read/view_lesson_bookings_details.php";
    set_title("FitKonnect KE | View Lesson Bookings Details");
} else if (is_menu_set('view_my_assigned_lessons') != "") {
    $currentPage = WPATH . "modules/read/view_lesson_bookings_details.php";
    set_title("FitKonnect KE | My Assigned Lessons");
} else if (is_menu_set('view_my_lesson_bookings') != "") {
    $currentPage = WPATH . "modules/read/view_lesson_bookings_details.php";
    set_title("FitKonnect KE | My Lesson Bookings");
} else if (is_menu_set('view_lesson_bookings_detail_details') != "") {
    $currentPage = WPATH . "modules/read/view_lesson_bookings_detail_details.php";
    set_title("FitKonnect KE | View Lesson Booking Detail Details");
} else if (is_menu_set('update_element') != "") {
    $currentPage = WPATH . "modules/update/update_element.php";
    set_title("FitKonnect KE | Update Record");
} else if (is_menu_set('update_contact_details') != "") {
    $currentPage = WPATH . "modules/update/update_contact_details.php";
    set_title("FitKonnect KE | Update Contact Details");
} else if (is_menu_set('update_county_details') != "") {
    $currentPage = WPATH . "modules/update/update_county_details.php";
    set_title("FitKonnect KE | Update County Details");
} else if (is_menu_set('update_lesson_category_details') != "") {
    $currentPage = WPATH . "modules/update/update_lesson_category_details.php";
    set_title("FitKonnect KE | Update Lesson Category Details");
} else if (is_menu_set('update_lesson_details') != "") {
    $currentPage = WPATH . "modules/update/update_lesson_details.php";
    set_title("FitKonnect KE | Update Lesson Details");
} else if (is_menu_set('update_staff_details') != "") {
    $currentPage = WPATH . "modules/update/update_staff_details.php";
    set_title("FitKonnect KE | Update Staff Details");
} else if (is_menu_set('update_staff_position_details') != "") {
    $currentPage = WPATH . "modules/update/update_staff_position_details.php";
    set_title("FitKonnect KE | Update Staff Position Details");
} else if (is_menu_set('update_system_administrator_details') != "") {
    $currentPage = WPATH . "modules/update/update_system_administrator_details.php";
    set_title("FitKonnect KE | Update System Administrator Details");
} else if (is_menu_set('update_system_component_details') != "") {
    $currentPage = WPATH . "modules/update/update_system_component_details.php";
    set_title("FitKonnect KE | Update System Component Details");
} else if (is_menu_set('update_system_privilege_details') != "") {
    $currentPage = WPATH . "modules/update/update_system_privilege_details.php";
    set_title("FitKonnect KE | Update System Privilege Details");
} else if (is_menu_set('update_system_role_details') != "") {
    $currentPage = WPATH . "modules/update/update_system_role_details.php";
    set_title("FitKonnect KE | Update System Role Details");
} else if (is_menu_set('update_system_status_code_details') != "") {
    $currentPage = WPATH . "modules/update/update_system_status_code_details.php";
    set_title("FitKonnect KE | Update System Status Code Details");
} else if (is_menu_set('update_trainee_details') != "") {
    $currentPage = WPATH . "modules/update/update_trainee_details.php";
    set_title("FitKonnect KE | Update Trainee Details");
} else if (is_menu_set('update_trainer_details') != "") {
    $currentPage = WPATH . "modules/update/update_trainer_details.php";
    set_title("FitKonnect KE | Update Trainer Details");
} else if (is_menu_set('update_training_facility_administrator_details') != "") {
    $currentPage = WPATH . "modules/update/update_training_facility_administrator_details.php";
    set_title("FitKonnect KE | Update Training Facility Administrator Details");
} else if (is_menu_set('update_training_facility_details') != "") {
    $currentPage = WPATH . "modules/update/update_training_facility_details.php";
    set_title("FitKonnect KE | Update Training Facility Details");
} else if (is_menu_set('update_training_facility_service_details') != "") {
    $currentPage = WPATH . "modules/update/update_training_facility_service_details.php";
    set_title("FitKonnect KE | Update Training Facility Service Details");
} else if (is_menu_set('update_user_type_details') != "") {
    $currentPage = WPATH . "modules/update/update_user_type_details.php";
    set_title("FitKonnect KE | Update User Type Details");
} else if (is_menu_set('update_ward_details') != "") {
    $currentPage = WPATH . "modules/update/update_ward_details.php";
    set_title("FitKonnect KE | Update Ward Details");
} else if (is_menu_set('lessons_category_browse') != "") {
    $currentPage = WPATH . "modules/read/lessons_category_browse.php";
    set_title("FitKonnect KE | View Lessons By Category");
} else if (is_menu_set('lessons_cart') != "") {
    $currentPage = WPATH . "modules/read/lessons_cart.php";
    set_title("FitKonnect KE | View My Lessons Cart Items");
} else if (is_menu_set('checkout') != "") {
    $currentPage = WPATH . "modules/read/checkout.php";
    set_title("FitKonnect KE | Checkout");
} else if (is_menu_set('view_trainer_details') != "") {
    $currentPage = WPATH . "modules/read/view_trainer_details.php";
    set_title("FitKonnect KE | View Trainer Details");
} else if (is_menu_set('mpesa_validation') != "") {
    $currentPage = WPATH . "modules/mpesa/mpesa_validation.php";
} else if (is_menu_set('mpesa_confirmation') != "") {
    $currentPage = WPATH . "modules/mpesa/mpesa_confirmation.php";
} else if (is_menu_set('mpesa_registration') != "") {
    $currentPage = WPATH . "modules/mpesa/mpesa_registration.php";
} else if (is_menu_set('kopokopo_confirm') != "") {
    $currentPage = WPATH . "modules/mpesa/kopokopo_confirm.php";
} else if (is_menu_set('cron_file') != "") {
    $currentPage = WPATH . "modules/cron_job/cron_file.php";
    set_title("FitKonnect KE | Run Cron Jobs");
} else if (!empty($_GET)) {
    App::redirectTo("?");
} else {
    $currentPage = WPATH . "modules/login.php";
    if (App::isLoggedIn()) {
        set_title("Home | FitKonnect KE | Fitness Redefined ");
    }
}

if (App::isAjaxRequest())
    include $currentPage;
else {
    require WPATH . "core/template/layout.php";
}
?>