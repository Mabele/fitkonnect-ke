<?php

require WPATH . "core/include.php";
$currentPage = "";

if ( is_menu_set('logout') != "" ) 
    App::logOut();

else if ( is_menu_set('?') != ""){
    $currentPage = WPATH . "modules/home.php";
    set_title("FitKonnect KE | Home");
} 

else if ( is_menu_set('signup') != ""){
    $currentPage = WPATH . "modules/signup.php";
    set_title("FitKonnect KE | Sign Up");
}

else if ( is_menu_set('signin') != ""){
    $currentPage = WPATH . "modules/signin.php";
    set_title("FitKonnect KE | Sign In");
}

else if ( is_menu_set('password_reset') != ""){
    $currentPage = WPATH . "modules/password_reset.php";
    set_title("FitKonnect KE | Password Reset");
}

else if ( is_menu_set('sport') != ""){
    $currentPage = WPATH . "modules/sport.php";
    set_title("FitKonnect KE | Choose Sport/Activity");
}

else if ( is_menu_set('location') != ""){
    $currentPage = WPATH . "modules/location.php";
    set_title("FitKonnect KE | Choose a Location");
}

else if ( is_menu_set('trainers') != ""){
    $currentPage = WPATH . "modules/trainers.php";
    set_title("FitKonnect KE | Choose a Trainer");
}

else if ( is_menu_set('coming_soon') != "" || is_menu_set('iq_points') != "" || is_menu_set('trainer_rankings') != "" || is_menu_set('partners') != "" || is_menu_set('chat') != ""){
    $currentPage = WPATH . "modules/coming_soon.php";
    set_title("FitKonnect KE | #IHaveFitnessID - Coming Soon");
}

else if ( is_menu_set('shop') != ""){
    $currentPage = WPATH . "modules/shop.php";
    set_title("FitKonnect KE | Shop");
}

else if ( is_menu_set('test') != ""){
    $currentPage = WPATH . "modules/test.php";
    set_title("FitKonnect KE | Test");
}

else if ( is_menu_set('thankyou') != ""){
    $currentPage = WPATH . "modules/thankyou.php";
    set_title("FitKonnect KE | Thank You");
}

else if ( is_menu_set('careers') != ""){
    $currentPage = WPATH . "modules/careers.php";
    set_title("FitKonnect KE | Careers");
}

else if ( is_menu_set('trainee_transactions') != ""){
    $currentPage = WPATH . "modules/trainee_transactions.php";
    set_title("FitKonnect KE | My Transactions");
}

else if ( is_menu_set('individual_transaction_details') != ""){
    $currentPage = WPATH . "modules/individual_transaction_details.php";
    set_title("FitKonnect KE | Transaction Details");
}

else if ( is_menu_set('home') != ""){
    $currentPage = WPATH . "modules/home.php";
    set_title("FitKonnect KE | Home");
}

else if ( is_menu_set('login') != ""){
    $currentPage = WPATH . "modules/login.php";
    set_title("FitKonnect KE | Login");
}

else if ( is_menu_set('trainer_application') != ""){
    $currentPage = WPATH . "modules/trainer_application.php";
    set_title("FitKonnect KE | Trainer Application");
}

else if ( is_menu_set('coach_application') != ""){
    $currentPage = WPATH . "modules/coach_application.php";
    set_title("FitKonnect KE | Coach Application");
}

else if ( is_menu_set('checkout') != ""){
    $currentPage = WPATH . "modules/checkout.php";
    set_title("FitKonnect KE | Checkout");
}

else if ( is_menu_set('services') != ""){
    $currentPage = WPATH . "modules/coming_soon.php";
    set_title("FitKonnect KE | Product & Services");
}

else if ( is_menu_set('about') != ""){
    $currentPage = WPATH . "modules/about.php";
    set_title("FitKonnect KE | About Us");
}

else if ( is_menu_set('faq') != ""){
    $currentPage = WPATH . "modules/faq.php";
    set_title("FitKonnect KE | FAQs");
}

else if ( is_menu_set('session_booking') != ""){
    $currentPage = WPATH . "modules/session_booking.php";
    set_title("FitKonnect KE | Session Booking");
}

else if ( is_menu_set('privacy') != ""){
    $currentPage = WPATH . "modules/privacy.php";
    set_title("FitKonnect KE | Privacy Policy");
}

else if ( is_menu_set('tc') != ""){
    $currentPage = WPATH . "modules/tc.php";
    set_title("FitKonnect KE | Terms & Conditions");
}

else if ( is_menu_set('client_register') != ""){
    $currentPage = WPATH . "modules/client_register.php";
    set_title("FitKonnect KE | Client Registeration");
}

else if ( is_menu_set('facility_register') != ""){
    $currentPage = WPATH . "modules/facility_register.php";
    set_title("FitKonnect KE | Facility Registeration");
}

else if ( is_menu_set('facility_admin_register') != ""){
    $currentPage = WPATH . "modules/facility_admin_register.php";
    set_title("FitKonnect KE | Facility Administration Registeration");
}

else if ( is_menu_set('issue_reporting') != ""){
    $currentPage = WPATH . "modules/issue_reporting.php";
    set_title("FitKonnect KE | Issue Reporting");
}

else if ( is_menu_set('lesson_booking') != ""){
    $currentPage = WPATH . "modules/lesson_booking.php";
    set_title("FitKonnect KE | Lesson Booking");
}

else if ( is_menu_set('lessons_cart') != ""){
    $currentPage = WPATH . "modules/lessons_cart.php";
    set_title("FitKonnect KE | Lesson Booking");
}

else if ( is_menu_set('lesson_booking2') != ""){
    $currentPage = WPATH . "modules/lesson_booking2.php";
    set_title("FitKonnect KE | Lesson Booking 2");
}

else if ( is_menu_set('lesson_booking_new') != ""){
    $currentPage = WPATH . "modules/lesson_booking_new.php";
    set_title("FitKonnect KE | Lesson Booking New");
}

else if ( is_menu_set('verify_trainer') != ""){
    $currentPage = WPATH . "modules/verify_trainer.php";
    set_title("FitKonnect KE | Verify Trainer");
}

else if ( is_menu_set('pricing') != ""){
    $currentPage = WPATH . "modules/pricing.php";
    set_title("FitKonnect KE | Pricing");
}

else if ( is_menu_set('verification') != ""){
    $currentPage = WPATH . "modules/verification.php";
    set_title("FitKonnect KE | Verification Status");
}
//
//else if ( is_menu_set('solar_features') != ""){
//    $currentPage = WPATH . "modules/solar_features.php";
//    set_title("FitKonnect KE | Solar Features");
//}
//
//else if ( is_menu_set('knowledge_base') != ""){
//    $currentPage = WPATH . "modules/knowledge_base.php";
//    set_title("FitKonnect KE | Knowledge Base");
//}
//
else if ( is_menu_set('contact') != ""){
    $currentPage = WPATH . "modules/contact.php";
    set_title("FitKonnect KE | Contact Us");
}

else if ( is_menu_set('welcome') != ""){
    $currentPage = WPATH . "modules/welcome.php";
    set_title("FitKonnect KE | Welcome");
}

else if ( is_menu_set('welcome_coach') != ""){
    $currentPage = WPATH . "modules/welcome_coach.php";
    set_title("FitKonnect KE | Welcome Coach");
}
//
//else if ( is_menu_set('partners') != ""){
//    $currentPage = WPATH . "modules/partners.php";
//    set_title("FitKonnect KE | Partners");
//}
//
//else if ( is_menu_set('heat_pumps') != ""){
//    $currentPage = WPATH . "modules/heat_pumps.php";
//    set_title("FitKonnect KE | Heat Pumps");
//}
//
//else if ( is_menu_set('transformers') != ""){
//    $currentPage = WPATH . "modules/transformers.php";
//    set_title("FitKonnect KE | Transformers");
//}
//
//else if ( is_menu_set('wind_power') != ""){
//    $currentPage = WPATH . "modules/wind_power.php";
//    set_title("FitKonnect KE | Wind Power");
//}
//
//else if ( is_menu_set('led_lighting') != ""){
//    $currentPage = WPATH . "modules/led_lighting.php";
//    set_title("FitKonnect KE | LED Lighting");
//}
//
//else if ( is_menu_set('generator_sets') != ""){
//    $currentPage = WPATH . "modules/generator_sets.php";
//    set_title("FitKonnect KE | Generator Sets");
//}
//
//else if ( is_menu_set('solar_power') != ""){
//    $currentPage = WPATH . "modules/solar_power.php";
//    set_title("FitKonnect KE | Solar Power");
//}
//
//else if ( is_menu_set('expansion_tanks') != ""){
//    $currentPage = WPATH . "modules/expansion_tanks.php";
//    set_title("FitKonnect KE | Expansion Tanks");
//}
//
//else if ( is_menu_set('thermostatic') != ""){
//    $currentPage = WPATH . "modules/thermostatic.php";
//    set_title("FitKonnect KE | Thermostatic And Temp Pressure Relief Valve");
//}
//
//else if ( is_menu_set('solar_controller') != ""){
//    $currentPage = WPATH . "modules/solar_controller.php";
//    set_title("FitKonnect KE | Solar Controller");
//}
//
//else if ( is_menu_set('solar_heating') != ""){
//    $currentPage = WPATH . "modules/solar_heating.php";
//    set_title("FitKonnect KE | Solar Heating");
//}
//
//else if ( is_menu_set('projects') != ""){
//    $currentPage = WPATH . "modules/projects.php";
//    set_title("FitKonnect KE | Projects");
//}

else if (!empty($_GET)) {
    App::redirectTo("?");
}

else{
    $currentPage = WPATH . "modules/home.php";
    if ( App::isLoggedIn() ) {
		set_title("Home | FitKonnect KE ");                
	}        
}

if (App::isAjaxRequest())
    include $currentPage;
else {
    require WPATH . "core/template/layout.php";
}
?>