<section class="ds section_padding_90 page_testimonials">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="flexslider">
                    <div class="container">
                        <div class="row bottommargin_15">
                            <div class="col-sm-12 text-center">
                                <div class="cornered-heading center-heading">
                                     <a href="./" class="logo">
                                        <img src="images/logo/logo.png" width="140" alt="">
                                    </a><br/>
                                    <span class="text-uppercase">Almost There</span>
                                    <h2 class="text-uppercase">Sign Up</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <ul class="slides">
                                    <li>
                                        <div class="row">
                                            <div class="col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-3">
                                            </div>
                                            <div class="col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-6">
                                                <blockquote class="blockquote-vertical with_bottom_border ls topmargin_50">
                                                    <img src="./images/faces/1.jpg" width="50" alt=""> 
                                                    <hr class="divider_3_60 main_bg_color">
                                                    <div class="blockquote-meta">
                                                    <form class="client-register" role="form" method="POST">
                                                            <input type="hidden" name="action" value="signup"/>
                                                            <?php if (isset($_GET['activity'])) { ?>
                                                                <input type="hidden" name="activity" value="<?php echo $_GET['activity']; ?>"/>
                                                            <?php } ?>
                                                            <?php if (isset($previous_url)) { ?>
                                                                <input type="hidden" name="previous_url" value="<?php echo $previous_url; ?>"/>
                                                            <?php } ?>
                                                                <div class="form-group validate-required" id="username">
                                                                    <input type="text" class="form-control " name="username" id="username" placeholder="Username" required="">
                                                                </div>
                                                                <div class="form-group validate-required" id="password">
                                                                    <input type="password" class="form-control " name="password" id="password" placeholder="Password" required="">
                                                                </div>
                                                                <div class="form-group validate-required" id="confirm_password">
                                                                    <input type="password" class="form-control " name="confirm_password" id="confirm_password" placeholder="Confirm Password" required="">
                                                                </div>
                                                                <div class="form-group">
                                                                <button type="submit" class="form-control theme_button wide_button color1">Sign Up Now</button>
                                                                </div>
                                                        </form>
                                                        <div class="blockquote-meta">I have an account?<a href="?signin"> Sign In</a></div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                            <div class="col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-3">
                                        </div>
                                        </div>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- eof flexslider -->
            </div>
        </div>
    </div>
</section>