<section class="ds parallax section_padding_100 page_not_found">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <p class="not_found">
                    <span class="highlight"><a target="_blank" title="Follow us on Instagram, we will keep you posted!" href="https://instagram.com/fitnessiq_ke">#IHaveFitnessIQ</a></span>
                </p>
                <h2 class="text-uppercase">Coming Soon</h2>
                <p>
                    <a href="?lesson_booking" class="theme_button color1">Book Lesson</a>
                </p>
            </div>
        </div>
    </div>
</section>