<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">Products</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li>
                        <a href="./">Home</a>
                    </li>
                    
                    <li class="active">Shop</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="ls section_padding_top_100 section_padding_bottom_75">
    <div class="container">
        <div class="row">

            <div class="col-sm-7 col-md-8 col-lg-8">

                <div class="storefront-sorting muted_background bottommargin_60 clearfix">

                    <form class="form-inline">

                        <div class="form-group">
                            <label class="grey" for="orderby">Sort By:</label>
                            <select class="form-control orderby" name="orderby" id="orderby">
                                <option value="menu_order" selected>Default</option>
                                <option value="popularity">Popularity</option>
                                <option value="rating">Rating</option>
                                <option value="date">Newness</option>
                                <option value="price">Low To High</option>
                                <option value="price-desc">High To Low</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <a href="#" id="sort_view">
                                <i class="fa fa-long-arrow-up"></i>
                            </a>

                            <a href="#" id="toggle_shop_view" class=""></a>
                        </div>

                        <div class="form-group pull-right">

                            <label class="grey" for="showcount">Show:</label>
                            <select class="form-control showcount" name="showcount" id="showcount">
                                <option value="8" selected>8</option>
                                <option value="12">12</option>
                                <option value="16">16</option>
                                <option value="20">20</option>
                                <option value="24">24</option>
                                <option value="28">28</option>
                            </select>

                        </div>

                    </form>

                </div>


                <div class="columns-2">

                    <ul id="products" class="products list-unstyled grid-view">
                        <li class="product type-product">
                            <div class="side-item with_shadow">
                                <div class="row">

                                    <div class="col-md-6">

                                        <div class="item-media">
                                            <a href="#">
                                                <img src="images/shop/sneakers.jpeg" alt="">
                                                <span class="newproduct">New</span>
                                            </a>
                                            <div class="product-buttons">
                                                <a href="#" rel="nofollow" class="favorite_button">
                                                    <i class="rt-icon2-heart"></i>
                                                </a>
                                                <a href="#" rel="nofollow" class="add_to_cart_button">
                                                    <i class="rt-icon2-basket"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <h3>
                                            <a href="#">Sneakers</a>
                                        </h3>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="item-content">

                                            <h3>
                                                <a href="#">Sneakers</a>
                                            </h3>

                                            <h4>Product Description</h4>
                                            <p class="product-description">
                                                An assorted collection of comfortable sneakers.
                                            </p>

                                            <h4>Product Rating</h4>
                                            <div class="star-rating" title="Rated 4.00 out of 5">
                                                <span style="width:80%">
                                                    <strong class="rating">4.00</strong> out of 5
                                                </span>
                                            </div>

                                            <span class="price">
                                                <span>
                                                    <span class="amount">KES.3,000.00/<strong>15 IQ pt</strong></span>
                                                </span>
                                            </span>


                                        </div>
                                    </div>

                                </div>
                            </div>
                        </li>

                        <li class="product type-product">
                            <div class="side-item with_shadow">
                                <div class="row">

                                    <div class="col-md-6">

                                        <div class="item-media">
                                            <a href="#">
                                                <img src="images/shop/backpacks.jpeg" alt="">
                                                <span class="onsale">Sale!</span>
                                            </a>
                                            <div class="product-buttons">
                                                <a href="#" rel="nofollow" class="favorite_button">
                                                    <i class="rt-icon2-heart"></i>
                                                </a>
                                                <a href="#" rel="nofollow" class="add_to_cart_button">
                                                    <i class="rt-icon2-basket"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <h3>
                                            <a href="#">Back Packs</a>
                                        </h3>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="item-content">

                                            <h3>
                                                <a href="#">Back Packs</a>
                                            </h3>

                                            <h4>Product Description</h4>
                                            <p class="product-description">
                                                An assortment of secure modern backpacks that can double up as office and gym carry-ons.
                                            </p>

                                            <h4>Product Rating</h4>
                                            <div class="star-rating" title="Rated 3.50 out of 5">
                                                <span style="width:70%">
                                                    <strong class="rating">3.50</strong> out of 5
                                                </span>
                                            </div>

                                            <span class="price">
                                                <del>
                                                    <span class="amount">KES.5,000.00</span>
                                                </del>

                                                <ins>
                                                    <span class="amount">KES.4,500.00/<strong>22 IQ pt</strong></span>
                                                </ins>
                                            </span>


                                        </div>
                                    </div>

                                </div>
                            </div>
                        </li>

                        <li class="product type-product">
                            <div class="side-item with_shadow">
                                <div class="row">

                                    <div class="col-md-6">

                                        <div class="item-media">
                                            <a href="#">
                                                <img src="images/shop/fila sneakers.jpeg" alt="">
                                            </a>
                                            <div class="product-buttons">
                                                <a href="#" rel="nofollow" class="favorite_button">
                                                    <i class="rt-icon2-heart"></i>
                                                </a>
                                                <a href="#" rel="nofollow" class="add_to_cart_button">
                                                    <i class="rt-icon2-basket"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <h3>
                                            <a href="#">Fila Sneakers</a>
                                        </h3>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="item-content">

                                            <h3>
                                                <a href="#">Fila Sneakers</a>
                                            </h3>

                                            <h4>Product Description</h4>
                                            <p class="product-description">
                                                Awesome comfortable sneakers, with breathable technology.
                                            </p>

                                            <h4>Product Rating</h4>
                                            <div class="star-rating" title="Rated 5.00 out of 5">
                                                <span style="width:100%">
                                                    <strong class="rating">5.00</strong> out of 5
                                                </span>
                                            </div>

                                            <span class="price">
                                                <span>
                                                    <span class="amount">KES.3,000.00/<strong>15 IQ pt</strong></span>
                                                </span>
                                            </span>


                                        </div>
                                    </div>

                                </div>
                            </div>
                        </li>

                        <li class="product type-product">
                            <div class="side-item with_shadow">
                                <div class="row">

                                    <div class="col-md-6">

                                        <div class="item-media">
                                            <a href="#">
                                                <img src="images/shop/black converse.jpeg" alt="">
                                                <span class="newproduct">New</span>
                                                <span class="onsale">Sale!</span>
                                            </a>
                                            <div class="product-buttons">
                                                <a href="#" rel="nofollow" class="favorite_button">
                                                    <i class="rt-icon2-heart"></i>
                                                </a>
                                                <a href="#" rel="nofollow" class="add_to_cart_button">
                                                    <i class="rt-icon2-basket"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <h3>
                                            <a href="#">Black Converse</a>
                                        </h3>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="item-content">

                                            <h3>
                                                <a href="#">Black Converse</a>
                                            </h3>

                                            <h4>Product Description</h4>
                                            <p class="product-description">
                                                Style and comfort in one.
                                            </p>

                                            <h4>Product Rating</h4>
                                            <div class="star-rating" title="Rated 2.50 out of 5">
                                                <span style="width:50%">
                                                    <strong class="rating">2.50</strong> out of 5
                                                </span>
                                            </div>

                                            <span class="price">
                                                <del>
                                                    <span class="amount">KES.2,400.00</span>
                                                </del>

                                                <ins>
                                                    <span class="amount">KES.1,800.00/<strong>9 IQ pt</strong></span>
                                                </ins>
                                            </span>


                                        </div>
                                    </div>

                                </div>
                            </div>
                        </li>

                        <li class="product type-product">
                            <div class="side-item with_shadow">
                                <div class="row">

                                    <div class="col-md-6">

                                        <div class="item-media">
                                            <a href="#">
                                                <img src="images/shop/white converse.jpeg" alt="">
                                                <span class="onsale">Sale!</span>
                                            </a>
                                            <div class="product-buttons">
                                                <a href="#" rel="nofollow" class="favorite_button">
                                                    <i class="rt-icon2-heart"></i>
                                                </a>
                                                <a href="#" rel="nofollow" class="add_to_cart_button">
                                                    <i class="rt-icon2-basket"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <h3>
                                            <a href="#">White Converse</a>
                                        </h3>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="item-content">
                                            <h3>
                                                <a href="#">White Converse</a>
                                            </h3>

                                            <h4>Product Description</h4>
                                            <p class="product-description">
                                                Style and comfort.
                                            </p>

                                            <h4>Product Rating</h4>
                                            <div class="star-rating" title="Rated 4.50 out of 5">
                                                <span style="width:90%">
                                                    <strong class="rating">4.50</strong> out of 5
                                                </span>
                                            </div>

                                            <span class="price">
                                                <del>
                                                    <span class="amount">KES.2,400.00</span>
                                                </del>

                                                <ins>
                                                    <span class="amount">KES.1,800.00/<strong>9 IQ pt</strong></span>
                                                </ins>
                                            </span>


                                        </div>
                                    </div>

                                </div>
                            </div>
                        </li>

                    
                    </ul>

                </div>
                <!-- eof .columns-* -->


                <div class="row">
                    <div class="col-sm-12 text-center">
                        <div class="with_shadow inline-block">
                            <ul class="pagination">
                                <li>
                                    <a href="#">
                                        <i class="rt-icon2-chevron-thin-left"></i>
                                        <span class="sr-only">Prev</span>
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="#">1</a>
                                </li>
                                <li>
                                    <a href="#">2</a>
                                </li>
                                <li>
                                    <a href="#">3</a>
                                </li>
                                <li>
                                    <a href="#">4</a>
                                </li>
                                <li>
                                    <a href="#">5</a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="rt-icon2-chevron-thin-right"></i>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>


            </div>
            <!--eof .col-sm-8 (main content)-->


            <!-- sidebar -->
            <aside class="col-sm-5 col-md-4 col-lg-4">


<!--                <div class="with_padding with_color_border">
                    <div class="widget widget_search ">
                        <h3 class="widget-title">Search</h3>

                        <form method="get" class="searchform form-inline" action="/">
                            <div class="form-group">
                                <label class="screen-reader-text" for="widget-search">Search for:</label>
                                <input id="widget-search" type="text" value="" name="search" class="form-control" placeholder="Search here...">
                            </div>
                            <button type="submit" class="theme_button">Search</button>
                        </form>
                    </div>
                </div>-->

                <div class="muted_background with_padding">
                    <div class="widget widget_price_filter">

                        <h3 class="widget-title">Price Filter</h3>
                        <!-- price slider -->
                        <form method="get" action="/" class="form-inline">

                            <div class="slider-range-price"></div>

                            <div class="form-group">
                                <label class="grey" for="slider_price_min">From:</label>
                                <input type="text" class="slider_price_min form-control text-center" id="slider_price_min" readonly>
                            </div>

                            <div class="form-group">
                                <label class="grey" for="slider_price_max"> to:</label>
                                <input type="text" class="slider_price_max form-control text-center" id="slider_price_max" readonly>
                            </div>

                            <div class="text-right">
                                <button type="submit" class="theme_button small_button color1">Filter</button>
                            </div>
                        </form>
                    </div>
                </div>

<!--                <div class="with_background with_padding">
                    <div class="widget widget_products widget_popular_entries">

                        <h3 class="widget-title">Products</h3>
                        <ul class="media-list">
                            <li class="media">
                                <a class="media-left" href="#">
                                    <img class="media-object" src="images/shop/01.jpg" alt="">
                                </a>
                                <div class="media-body">
                                    <h3>
                                        <a href="#">Rebum stet clita kasd lorem ipsum dolor</a>
                                    </h3>

                                    <div class="star-rating" title="Rated 4.50 out of 5">
                                        <span style="width:90%">
                                            <strong class="rating">4.50</strong> out of 5
                                        </span>
                                    </div>

                                    <a href="#" rel="nofollow" class="add_to_cart_button">
                                        <i class="rt-icon2-basket"></i>
                                    </a>

                                    <span class="price">
                                        <ins>
                                            <span class="amount">$299.00</span>
                                        </ins>
                                    </span>
                                </div>
                            </li>

                            <li class="media">
                                <a class="media-left" href="#">
                                    <img class="media-object" src="images/shop/02.jpg" alt="">
                                </a>
                                <div class="media-body">
                                    <h3>
                                        <a href="#">Rebum stet clita kasd sit amet</a>
                                    </h3>

                                    <div class="star-rating" title="Rated 3.50 out of 5">
                                        <span style="width:70%">
                                            <strong class="rating">3.50</strong> out of 5
                                        </span>
                                    </div>

                                    <a href="#" rel="nofollow" class="add_to_cart_button">
                                        <i class="rt-icon2-basket"></i>
                                    </a>

                                    <span class="price">
                                        <del>
                                            <span class="amount">$269.00</span>
                                        </del>

                                        <ins>
                                            <span class="amount">$239.00</span>
                                        </ins>
                                    </span>
                                </div>
                            </li>

                            <li class="media">
                                <a class="media-left" href="#">
                                    <img class="media-object" src="images/shop/03.jpg" alt="">
                                </a>
                                <div class="media-body">
                                    <h3>
                                        <a href="#">Lorem stet clita kasd</a>
                                    </h3>

                                    <div class="star-rating" title="Rated 2.50 out of 5">
                                        <span style="width:50%">
                                            <strong class="rating">2.50</strong> out of 5
                                        </span>
                                    </div>

                                    <a href="#" rel="nofollow" class="add_to_cart_button">
                                        <i class="rt-icon2-basket"></i>
                                    </a>

                                    <span class="price">
                                        <ins>
                                            <span class="amount">$2.00</span>
                                        </ins>
                                    </span>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>-->

                <div class="with_background with_padding">
                    <div class="widget widget_tag_cloud widget_product_tag_cloud">

                        <h3 class="widget-title">Size</h3>
                        <div class="tagcloud">
                            <a href="#" title="">XS</a>
                            <a href="#" title="">S</a>
                            <a href="#" title="">M</a>
                            <a href="#" title="">L</a>
                            <a href="#" title="">XL</a>
                            <a href="#" title="">XXL</a>
                        </div>
                    </div>
                </div>

                <div class="with_background with_padding">
                    <div class="widget widget_layered_nav widget_categories">

                        <h3 class="widget-title">Attributes</h3>
                        <ul class="list-unstyled greylinks">
                            <li>
                                <a href="shop-right.html" class="white">White</a>
                                <span class="count highlight">(3)</span>
                            </li>
                            <li>
                                <a href="shop-right.html" class="grey">Grey</a>
                                <span class="count highlight">(2)</span>
                            </li>
                            <li>
                                <a href="shop-right.html" class="darkgrey">Darkgrey</a>
                                <span class="count highlight">(12)</span>
                            </li>
                            <li>
                                <a href="shop-right.html" class="black">Black</a>
                                <span class="count highlight">(15)</span>
                            </li>
                            <li>
                                <a href="shop-right.html" class="darkblue">Darkblue</a>
                                <span class="count highlight">(25)</span>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="with_background with_padding">
                    <div class="widget widget_layered_nav">

                        <h3 class="widget-title">Color</h3>
                        <ul class="list-unstyled color-filters">
                            <li>
                                <a href="shop-right.html" data-background-color="#cacaca">White</a>
                                <span class="count grey">(12)</span>
                            </li>
                            <li>
                                <a href="shop-right.html" data-background-color="#67676a">Grey</a>
                                <span class="count grey">(52)</span>
                            </li>
                            <li>
                                <a href="shop-right.html" data-background-color="#1b1b19">Darkgrey</a>
                                <span class="count grey">(31)</span>
                            </li>
                            <li>
                                <a href="shop-right.html" data-background-color="#255a8b">Black</a>
                                <span class="count grey">(24)</span>
                            </li>
                            <li>
                                <a href="shop-right.html" data-background-color="#0b8abf">Darkblue</a>
                                <span class="count grey">(16)</span>
                            </li>
                            <li>
                                <a href="shop-right.html" data-background-color="#bc1624">Red</a>
                                <span class="count grey">(5)</span>
                            </li>
                            <li>
                                <a href="shop-right.html" data-background-color="#4b0082">Purpule</a>
                                <span class="count grey">(8)</span>
                            </li>
                        </ul>
                    </div>
                </div>


            </aside>
            <!-- eof aside sidebar -->


        </div>
    </div>
</section>