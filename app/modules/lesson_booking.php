<?php
require_once WPATH . "modules/classes/Lessons.php";
//require_once WPATH . "modules/classes/Training_Facilities.php";
//$training_facilities = new Training_Facilities();
$lessons = new Lessons();

//$item_total = 0;
$item_total = 0;

//if (isset($_SERVER['HTTP_REFERER'])) {
//    $previous_url = $_SERVER['HTTP_REFERER'];
//}

if (isset($_SESSION["cart_item"])) {
    $_SESSION["cart_number_of_items"] = count($_SESSION["cart_item"]);
    foreach ($_SESSION["cart_item"] as $item) {
        $item_total += ($item["price"] * $item["number_of_classes"]);
        $_SESSION["cart_total_cost"] = $item_total;
    }
} else {
    $_SESSION["cart_number_of_items"] = 0;
    $_SESSION["cart_total_cost"] = 0;
}

if (!empty($_POST) AND $_POST['action'] == "add_to_lessons_cart") {
    $productByCode = $lessons->fetchLessonDetails($_POST["code"]);
    $itemArray = array($productByCode["id"] => array('id' => $productByCode["id"], 'name' => $productByCode["name"], 'price' => $productByCode["price"], 'number_of_classes' => $_POST["number_of_classes"]));
    if (!empty($_SESSION["cart_item"])) {
        if (in_array($productByCode["id"], array_keys($_SESSION["cart_item"]))) {
            foreach ($_SESSION["cart_item"] as $k => $v) {
                if ($productByCode["id"] == $v['id']) {
                    if (empty($_SESSION["cart_item"][$k]["number_of_classes"])) {
                        $_SESSION["cart_item"][$k]["number_of_classes"] = 0;
                    }
                    $_SESSION["cart_item"][$k]["number_of_classes"] += $_POST["number_of_classes"];
                }
            }
        } else {
            $_SESSION["cart_item"] = array_merge($_SESSION["cart_item"], $itemArray);
        }
    } else {
        $_SESSION["cart_item"] = $itemArray;
    }

//    App::redirectTo("{$previous_url}");
    App::redirectTo("?checkout");
}

//if (isset($_GET['filter_type'])) {
//    $lessons_data[] = $lessons->getAllFilteredLessons($_GET['filter_type'], $_GET['filter_value']);
//} else {
$lessons_data[] = $lessons->getAllLessons();
//}
//$_SESSION['filtered_lessons'] = $filtered_lessons;
?>

<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">Book Appointment</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li>
                        <a href="./">Home</a>
                    </li>
                    <li class="active">Book Appointment</li>
                    <!--                    <li>
                                            <a href="?lessons_cart"><?php // echo '(' . $_SESSION["cart_number_of_items"] . ') Lessons Booked';  ?> </a>
                                        </li>-->
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="ls section_padding_bottom_25">
    <div class="container">

        <?php if (isset($_SESSION['no_records']) AND $_SESSION['no_records'] == true) {
            ?>
            <div style="text-align:left"><strong>No lesson found....</strong></div>
            <?php
            unset($_SESSION['no_records']);
        } else if (isset($_SESSION['yes_records']) AND $_SESSION['yes_records'] == true) {
            ?> 

            <div class="row masonry-layout isotope_container">
                <?php
                foreach ($lessons_data as $key => $value) {
                    $inner_array[$key] = json_decode($value, true); // this will give key val pair array
                    foreach ((array) $inner_array[$key] as $key2 => $value2) {
//                        $lesson_category_details = $lessons->fetchLessonCategoryDetails($value2['category']);
//                        $training_facility_details = $training_facilities->fetchTrainingFacilityDetails($value2['training_facility']);
//                        $available_capacity = $value2['capacity'] - $value2['current_population'];
                        ?>

                        <div class="col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-4">
                            <div class="vertical-item content-padding topmargin_10">

                                <blockquote class="blockquote-vertical with_bottom_border ls topmargin_50">
                                                    <img src="./images/faces/1.jpg" width="250" alt=""> 
                                                    <h5> <a href="#"><?php echo $value2['name']; ?></a></h5>
                                                    <p>
                                                        <?php echo 'DESCRIPTION: ' . substr($value2['description'], 0, 30) . "..."; ?><br/>
                                                        <?php echo 'PRICE: KES ' . $value2['price']; ?> <br/>
                                                    </p>
                                                    <hr class="divider_3_60 main_bg_color">
                                    <div class="blockquote-meta">
                                       
                                       <form role="form" method="post">
                                            <input type="hidden" name="action" value="add_to_lessons_cart"/>
                                            <input type="hidden" name="code" value="<?php echo $value2['id']; ?>"/>
                                            <input type="hidden" name="number_of_classes" value="1"/>
                                            <button type="submit" class="theme_button color2">View Locations</button>
                                        </form>
                                    </div>
                                </blockquote>
                                        
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
            <?php
            unset($_SESSION['yes_records']);
        }
        ?>
    </div>
</section>