<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">Lesson Booking</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li><a href="./">Home</a></li>
                    <li class="active">Lesson Booking</li>
                    <li class="active">Checkout</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<?php // require_once 'modules/gallery_pull/gallery.php'; ?>

<section class="ls section_padding_top_100 section_padding_bottom_100">
    <div class="container">
        <div class="row">
            <form class="lesson-booking" role="form">
               <input type="hidden" name="action" value="lesson_booking"/>
                <div class="col-sm-3">
                    <div class="form-group validate-required" id="lesson">
                        <label for="lesson" class="control-label">
                            <span class="grey">Lesson </span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="lesson" id="lesson" >
                            <?php // include 'modules/snippets/lesson.php'; ?>
                        </select>
                    </div>                    
                </div>
                <div class="col-sm-3">
                    <div class="form-group validate-required" id="lesson">
                        <label for="number_of_classes" class="control-label">
                            <span class="grey">Number of Classes </span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="number_of_classes" id="number_of_classes" >
                            <?php include '../snippets/lesson.php'; ?>
                        </select>
                    </div>
                </div>
                

                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="day" class="control-label">
                            <span class="grey" title="Date of commencement">Start Day </span>
                            <span class="required">*</span>
                        </label>

                        <select class="form-control" name="day" id="day" >
                            <?php include '../snippets/day.php'; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="month" class="control-label">
                            <span class="grey" title="Date of commencement">Start Month </span>
                            <span class="required">*</span>
                        </label>

                        <select class="form-control" name="month" id="month">
                            <?php include '../snippets/month.php'; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="year" class="control-label">
                            <span class="grey" title="Date of commencement">Start Year </span>
                            <span class="required">*</span>
                        </label>

                        <select class="form-control" name="year" id="year">
                            <?php include '../snippets/lesson_year.php'; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-12">
                    <button type="submit" class="theme_button wide_button color1">Book Now</button>
                    <button type="reset" class="theme_button wide_button">Clear Form</button>
                </div>
            </form>
        </div>

    </div>
</div>
</section>