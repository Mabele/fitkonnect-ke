<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Transactions.php";
require_once WPATH . "modules/classes/System_Administration.php";
require_once WPATH . "modules/classes/Lessons.php";
$lessons = new Lessons();
$system_administration = new System_Administration();
$transactions = new Transactions();

unset($_SESSION['lesson_booking']);
?>
<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">Transaction Details</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li><a href="./">Home</a></li>
                    <li><a href="?individual_transaction_details">Transaction Details</a></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="ls ms section_padding_top_100 section_padding_bottom_75 columns_padding_25">
    <div class="container">
        <div class="row">

            <div class="cart-collaterals">
                <div class="cart_totals">
                    <table class="table table-condensed">                        

                        <thead>
                            <tr>
                                <td><strong>#</strong></td>
                                <td><strong>Transaction ID</strong></td>
                                <td><strong>Lesson</strong></td>  
                                <td><strong>Number of Classes</strong></td>                                 
                                <td><strong>Start Date</strong></td> 
                                <td><strong>Trainer</strong></td>
                                <td><strong>Lesson Status</strong></td>
                            </tr>
                        </thead> 

                        <tbody>

                            <?php
                            $count = 1;
                            if (is_menu_set('individual_transaction_details') != "") {
                                $transaction_id = $_GET['code'];
                                $lesson_booking_transactions_details[] = $transactions->getLessonBookingTransactionsDetails($transaction_id);
                            }
                            
                            if ($_SESSION['logged_in_user_type_details']['name'] == "TRAINEE" AND ( isset($_SESSION['no_records']) AND $_SESSION['no_records'] == true)) {
                                echo "<tr>";
                                echo "<td>  No record found...</td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "</tr>";
                                unset($_SESSION['no_records']);
                            } else if (isset($_SESSION['yes_records']) AND $_SESSION['yes_records'] == true) {
                                foreach ($lesson_booking_transactions_details as $key => $value) {
                                    $inner_array[$key] = json_decode($value, true); // this will give key val pair array
                                    foreach ((array) $inner_array[$key] as $key2 => $value2) {

                                        $lesson_details = $lessons->fetchLessonDetails($value2['lesson']);
                                        $status_details = $system_administration->fetchSystemStatusDetails($value2['status']);
                                        $status = $status_details['display_value'];

                                        if (empty($value2['assigned_trainer'])) {
                                            $assigned_trainer = "UNASSIGNED";
                                        } else {
                                            $user_details = $users_management->fetchUserDetails($value2['assigned_trainer']);
                                            $trainer_details = $trainers->fetchTrainerDetails($user_details['reference_id']);
                                            $assigned_trainer = $trainer_details['firstname'] . ' ' . $trainer_details['lastname'];
                                        }

                                        echo "<tr>";
                                        echo "<td><a href='#'>" . $count++ . "</td>";
                                        echo "<td>" . $value2['transaction_id'] . "</td>";
                                        echo "<td>" . $lesson_details['name'] . "</td>";
                                        echo "<td>" . $value2['number_of_classes'] . "</td>";
                                        echo "<td>" . $value2['start_date'] . "</td>";
                                        echo "<td>" . $assigned_trainer . "</td>";
                                        echo "<td>" . $status . "</td>";
                                    }
                                    echo "</tr>";
                                }
                                unset($_SESSION['yes_records']);
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>