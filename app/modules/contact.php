
<?php
require_once WPATH . "modules/classes/Transactions.php";
require_once WPATH . "modules/classes/Feedback.php";
$feedback = new Feedback();
$transactions = new Transactions();

if (!empty($_POST)) {
    $success = $transactions->execute();
    if (is_bool($success) && $success == true) {
        $title = "Fantastic";
        $message = "Your message has been sent successfully. We shall get back to you soonest possible.";
        echo $feedback->successFeedback($title, $message);
    } else {
        $title = "Sorry,";
        $message = "There was an error sending your message. Please try again.";
        echo $feedback->errorFeedback($title, $message);
    }
}
?>

<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">Contact Us</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li>
                        <a href="./">Home</a>
                    </li>
                    <li class="active">Contact Us</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="ls section_padding_top_100 section_padding_bottom_100">
    <div class="container">
        <div class="row">
            <form class="client-register" role="form" method="POST">
                <input type="hidden" name="action" value="contact_us">
                
                
                <div class="col-sm-6">
                        <div class="contact-form-name form-group">
                            <label for="name" class="control-label">Full Name
                                <span class="required">*</span>
                            </label>
                            <input type="text" name="name" class="form-control" placeholder="Enter your Name" required="">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="contact-form-subject form-group">
                            <label for="email" class="control-label">Email
                                <span class="required">*</span>
                            </label>
                            <input type="email" name="email" class="form-control" placeholder="Enter your Email" required="">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="contact-form-phone form-group">
                            <label for="subject" class="control-label">Subject
                                <span class="required">*</span>
                            </label>
                            <input type="text" name="subject" class="form-control" placeholder="Enter your Subject" required="">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="contact-form-phone form-group">
                            <label for="phone_number" class="control-label">Telephone
                                <span class="required">*</span>
                            </label>
                            <input type="tel"name="phone_number" class="form-control" placeholder="Enter your Telephone" required="">
                        </div>
                    </div>
                    <div class="col-sm-12">

                        <div class="contact-form-message form-group">
                            <label for="message" class="control-label">Message</label>
                            <textarea rows="5" cols="45" name="message" class="form-control" placeholder="Enter your Message..." required=""></textarea>
                        </div>
                    </div>
                <div class="col-sm-12">
                    <button type="submit" class="theme_button wide_button color1">Send Message</button>
                    <button type="reset" class="theme_button wide_button">Clear Form</button>
                </div>
            </form>
        </div>

    </div>
</div>
</section>


<!--<section class="ls section_padding_top_25 section_padding_bottom_25">
    <div class="container">
        <div class="row topmargin_35 contact-form-container to_animate" data-animation="fadeInUp">
            <div class="col-sm-12 text-center">
                <span class="text-uppercase">Talk to us</span>
                <h2 class="text-uppercase topmargin_5 bottommargin_15">Contact Form</h2>

                <form class="contact-form row columns_margin_0" method="POST">
                    <input type="hidden" name="action" value="contact_us"/>
                    <div class="col-sm-6">
                        <div class="contact-form-name form-group">
                            <label for="name" class="sr-only">Full Name
                                <span class="required">*</span>
                            </label>
                            <input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control" placeholder="Enter your Name">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="contact-form-subject form-group">
                            <label for="email" class="sr-only">Email
                                <span class="required">*</span>
                            </label>
                            <input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control" placeholder="Enter your Email">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="contact-form-phone form-group">
                            <label for="subject" class="sr-only">Subject
                                <span class="required">*</span>
                            </label>
                            <input type="text" aria-required="true" size="30" value="" name="subject" id="subject" class="form-control" placeholder="Enter your Subject">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="contact-form-phone form-group">
                            <label for="phone_number" class="sr-only">Telephone
                                <span class="required">*</span>
                            </label>
                            <input type="text" aria-required="true" size="30" value="" name="phone_number" id="phone_number" class="form-control" placeholder="Enter your Telephone">
                        </div>
                    </div>
                    <div class="col-sm-12">

                        <div class="contact-form-message form-group">
                            <label for="message" class="sr-only">Message</label>
                            <textarea aria-required="true" rows="5" cols="45" name="message" id="message" class="form-control" placeholder="Enter your Message..."></textarea>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="contact-form-submit topmargin_10 bottommargin_25">
                            <button type="submit" id="contact_form_submit" name="contact_submit" class="theme_button color1 margin_0">Send Message</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</section>-->