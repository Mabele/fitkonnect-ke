<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">Issue Reporting</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li>
                        <a href="./">Home</a>
                    </li>
                    <li class="active">Issue Reporting</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="ls section_padding_top_100 section_padding_bottom_100">
    <div class="container">
        <div class="row">
            <form class="issue-report" role="form">
                <div class="col-lg-6">
                    <div class="form-group validate-required" id="subject">
                        <label for="subject" class="control-label">
                            <span class="grey">Message Subject </span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="subject" id="subject" placeholder="" value="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group validate-required" id="facility_rating">
                        <label for="assign_to" class="control-label">
                            <span class="grey">Assign To </span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="assign_to" id="assign_to" >
                            <?php //include 'modules/snippets/day.php'; ?>
                        </select>

                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group validate-required"" id="description">
                        <label for="description" class="control-label">
                            <span class="grey">Description </span>
                            <span class="required">*</span>
                        </label>
                        <textarea rows="5" class="form-control " name="description" id="description" placeholder="" value=""></textarea>
                    </div>
                </div>
                <div class="col-sm-12">
                    <button type="submit" class="theme_button wide_button color1">Register Now</button>
                    <button type="reset" class="theme_button wide_button">Clear Form</button>
                </div>
            </form>
        </div>

    </div>
</div>
</section>