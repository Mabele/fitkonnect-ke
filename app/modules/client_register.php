
<?php
require_once WPATH . "modules/classes/Trainees.php";
require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/Feedback.php";
$feedback = new Feedback();
$users_management = new Users_Management();
$trainees = new Trainees();

if (!empty($_POST)) {
    if ($users_management->checkIfUserEmailExists($_POST['email']) == false AND $users_management->checkIfUsernameExists($_POST['username']) == false) {
        $success = $trainees->execute();
        if (is_bool($success) && $success == true) {
            $title = "Fantastic";
            $message = "Your registration has been effected successfully.";
            echo $feedback->successFeedback($title, $message);
            App::redirectTo("?welcome");
        } else {
            if (isset($_SESSION['mail_sending']) && $_SESSION['mail_sending'] == false) {
                $title = "Hey,";
                $message = "Error! There was an error sending details to your email address.";
                echo $feedback->errorFeedback($title, $message);
                unset($_SESSION['mail_sending']);
            }
            if (isset($_SESSION['user_data_saving']) && $_SESSION['user_data_saving'] == false) {
                $title = "Hey,";
                $message = "Error! There was an error effecting your registration. Please try again.";
                echo $feedback->errorFeedback($title, $message);
                unset($_SESSION['user_data_saving']);
            }
        }
    } else {
        if ($users_management->checkIfUserEmailExists($_POST['email']) == true AND $users_management->checkIfUsernameExists($_POST['username']) == true) {
            $title = "Oops,";
            $message = "Seems someone already registered with your preferred username and email address. Please register using a different set of username and email address.";
            echo $feedback->errorFeedback($title, $message);
        }
        if ($users_management->checkIfUserEmailExists($_POST['email']) == true AND $users_management->checkIfUsernameExists($_POST['username']) == false) {
            $title = "Oops,";
            $message = "Seems someone already registered with your preferred email address. Please register using a different email address.";
            echo $feedback->errorFeedback($title, $message);
        }
        if ($users_management->checkIfUserEmailExists($_POST['email']) == false AND $users_management->checkIfUsernameExists($_POST['username']) == true) {
            $title = "Oops,";
            $message = "Seems someone already registered with your preferred username. Please register using a different username.";
            echo $feedback->errorFeedback($title, $message);
        }
    }
}
?>

<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">Client Registration</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li>
                        <a href="./">Home</a>
                    </li>
                    <li class="active">Client Registration</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="ls section_padding_top_100 section_padding_bottom_100">
    <div class="container">
        <div class="row">
            <form class="client-register" role="form" method="POST">
                <input type="hidden" name="action" value="add_trainee"/>
                <input type="hidden" name="user_type" value="TRAINEE">
                <div class="col-sm-6">
                    <div class="form-group validate-required" id="firstname">
                        <label for="firstname" class="control-label">
                            <span class="grey">First Name </span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="firstname" id="firstname" placeholder="" required="">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group validate-required" id="lastname">
                        <label for="lastname" class="control-label">
                            <span class="grey">Last Name </span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="lastname" id="lastname" placeholder="" required="">
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group validate-required validate-email" id="email">
                        <label for="email" class="control-label">
                            <span class="grey">Email Address </span>
                            <span class="required">*</span>
                        </label>
                        <input type="email" class="form-control " name="email" id="email" placeholder="email" required="">
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group validate-required validate-phone" id="phone">
                        <label for="phone" class="control-label">
                            <span class="grey">Phone </span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="phone_number" id="phone" placeholder="" required="">
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group validate-required" id="phone">
                        <label for="gender" class="control-label">
                            <span class="grey">Gender </span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="gender" id="gender" required="">
                            <?php include '../snippets/gender.php'; ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="day" class="control-label">
                            <span class="grey" title="Used to determine age, for safety purposes">Day of Birth</span>
                            <span class="required">*</span>
                        </label>

                        <select class="form-control" name="day" id="day" required="" >
                            <?php include '../snippets/day.php'; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="month" class="control-label">
                            <span class="grey" title="Used to determine age, for safety purposes">Month of Birth </span>
                            <span class="required">*</span>
                        </label>

                        <select class="form-control" name="month" id="month" required="">
                            <?php include '../snippets/month.php'; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="year" class="control-label">
                            <span class="grey" title="Used to determine age, for safety purposes">Year of Birth </span>
                            <span class="required">*</span>
                        </label>

                        <select class="form-control" name="year" id="year" required="">
                            <?php include '../snippets/year.php'; ?>
                        </select>
                    </div>
                </div>                
                <div class="col-sm-6">
                    <div class="form-group validate-required" id="username">
                        <label for="username" class="control-label">
                            <span class="grey">Username </span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="username" id="username" placeholder="" required="">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group validate-required" id="password">
                        <label for="password" class="control-label">
                            <span class="grey">Password</span>
                            <span class="required">*</span>
                        </label>
                        <input type="password" class="form-control " name="password" id="password" placeholder="" required="">
                    </div>
                </div>

                <div class="col-sm-12">
                    <button type="submit" class="theme_button wide_button color1">Register Now</button>
                    <button type="reset" class="theme_button wide_button">Clear Form</button>
                </div>
            </form>
        </div>

    </div>
</div>
</section>