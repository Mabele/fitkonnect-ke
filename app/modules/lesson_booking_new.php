<?php
require_once WPATH . "modules/classes/Lessons.php";
require_once WPATH . "modules/classes/Training_Facilities.php";
$training_facilities = new Training_Facilities();
$lessons = new Lessons();

$item_total = 0;

if (isset($_SERVER['HTTP_REFERER'])) {
    $previous_url = $_SERVER['HTTP_REFERER'];
}

if (isset($_SESSION["cart_item"])) {
    $_SESSION["cart_number_of_items"] = count($_SESSION["cart_item"]);
    foreach ($_SESSION["cart_item"] as $item) {
        $item_total += ($item["price"] * $item["number_of_classes"]);
        $_SESSION["cart_total_cost"] = $item_total;
    }
} else {
    $_SESSION["cart_number_of_items"] = 0;
    $_SESSION["cart_total_cost"] = 0;
}

if (!empty($_POST) AND $_POST['action'] == "add_to_lessons_cart") {
    $productByCode = $lessons->fetchLessonDetails($_POST["code"]);
    $itemArray = array($productByCode["id"] => array('id' => $productByCode["id"], 'name' => $productByCode["name"], 'price' => $productByCode["price"], 'number_of_classes' => $_POST["number_of_classes"]));
    if (!empty($_SESSION["cart_item"])) {
        if (in_array($productByCode["id"], array_keys($_SESSION["cart_item"]))) {
            foreach ($_SESSION["cart_item"] as $k => $v) {
                if ($productByCode["id"] == $v['id']) {
                    if (empty($_SESSION["cart_item"][$k]["number_of_classes"])) {
                        $_SESSION["cart_item"][$k]["number_of_classes"] = 0;
                    }
                    $_SESSION["cart_item"][$k]["number_of_classes"] += $_POST["number_of_classes"];
                }
            }
        } else {
            $_SESSION["cart_item"] = array_merge($_SESSION["cart_item"], $itemArray);
        }
    } else {
        $_SESSION["cart_item"] = $itemArray;
    }
    App::redirectTo("{$previous_url}");
}

if (isset($_GET['filter_type'])) {
    $lessons_data[] = $lessons->getAllFilteredLessons($_GET['filter_type'], $_GET['filter_value']);
} else {
    $lessons_data[] = $lessons->getAllLessons();
}
//$_SESSION['filtered_lessons'] = $filtered_lessons;
?>

<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">Lesson Booking</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li>
                        <a href="./">Home</a>
                    </li>
                    <li class="active">Lesson Booking</li>
                    <li>
                        <a href="?lessons_cart"><?php echo '(' . $_SESSION["cart_number_of_items"] . ') Lessons Booked'; ?> </a>
                    </li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="ls ms section_padding_50 columns_padding_25">

    <div class="container">      
        <div class="row vertical-tabs">
            <div class="col-sm-12">
                <?php require_once 'modules/submenu/lesson_menu.php'; ?>
            </div>
              <div class="col-sm-3">
            
                            <ul class="nav" role="tablist">
                                <li class="active">
                                    <a href="#vertical-tab1" role="tab" data-toggle="tab">Foundation Level 1</a>
                                </li>
                                <li>
                                    <a href="#vertical-tab2" role="tab" data-toggle="tab">Foundation Level 2</a>
                                </li>
                                <li>
                                    <a href="#vertical-tab3" role="tab" data-toggle="tab">Advanced Level 1</a>
                                </li>
                                <li>
                                    <a href="#vertical-tab4" role="tab" data-toggle="tab">Advanced Level 2</a>
                                </li>
                                <li>
                                    <a href="#vertical-tab5" role="tab" data-toggle="tab">Club Level</a>
                                </li>
                                <li>
                                    <a href="#vertical-tab6" role="tab" data-toggle="tab">Weight Lifting</a>
                                </li>
<!--                                <li>
                                    <a href="#vertical-tab7" role="tab" data-toggle="tab">Aerobics</a>
                                </li>-->
                            </ul>
                        </div>
            <div class="col-sm-9">
                    <?php if (isset($_SESSION['no_records']) AND $_SESSION['no_records'] == true) {
                        ?>
                        <div style="text-align:left"><strong>No lesson found....</strong></div>
                        <?php
                        unset($_SESSION['no_records']);
                    } else if (isset($_SESSION['yes_records']) AND $_SESSION['yes_records'] == true) {
                        ?>                   

                        <div class="tab-pane fade in active" id="vertical-tab1">

                            <?php
                            foreach ($lessons_data as $key => $value) {
                                $inner_array[$key] = json_decode($value, true); // this will give key val pair array
                                foreach ((array) $inner_array[$key] as $key2 => $value2) {
                                    $lesson_category_details = $lessons->fetchLessonCategoryDetails($value2['category']);
                                    $training_facility_details = $training_facilities->fetchTrainingFacilityDetails($value2['training_facility']);
                                    $available_capacity = $value2['capacity'] - $value2['current_population'];
                                    ?>
                                    <div class="col-sm-4">
                                        <div class="bg_teaser after_cover color_bg_1">
                                            <img src="images/teaser01.jpg" alt="">
                                            <div class="teaser_content media">
                                                <div class="teaser text-center">
                                                    <div class="teaser_icon highlight2 size_big">
                                                        <i class="rt-icon2-stopwatch"></i>
                                                    </div>
                                                    <h3><?php echo $lesson_category_details['name']; ?></h3>
                                                    <p><?php echo 'NAME: ' . $value2['name']; ?><br/>
                                                        <?php echo 'FACILITY: ' . $training_facility_details['name']; ?><br/>
                                                        <!--<strong class="grey">KES</strong>--> 
                                                        <?php echo 'PRICE: KES ' . $value2['price']; ?> <br/>
                                                        <strong class="grey"><?php echo 'AVAILABLE CAPACITY: ' . $available_capacity; ?></strong> persons <br/>
                                                        <!-- Time should be slected after the client books -->
                <!--                                        <strong class="grey">Start time</strong> {12:00H} <br/>
                                                        <strong class="grey">End Time</strong> {16:00H} <br/>-->
                                                        <!--<a href="#" class="theme_button color2">Book Now</a>-->

                                                    <form role="form" method="post">
                                                        <input type="hidden" name="action" value="add_to_lessons_cart"/>
                                                        <input type="hidden" name="code" value="<?php echo $value2['id']; ?>"/>
                                                        <input type="hidden" name="number_of_classes" value="1"/>
                                                        <button type="submit" class="theme_button color2">Book Now</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <?php
                        unset($_SESSION['yes_records']);
                    }
                    ?>
            </div>
          
<!--            <div class="col-sm-3 promotions">
            </div>-->
        </div>
    </div>
    <a href="https://www.facebook.com/events/1840804329297312/" target="_blank">
        <div class="teaser table_section main_bg_color" style="background-image: url('images/promos/Naivasha-Challenge.png'); background-size: cover; background-repeat: no-repeat;">
            <div class="row">
                <div class="col-sm-5">
                    <h3 class="text-md-right" style="color: #ffffff;">The Naivasha Challenge</h3>
                </div>

                <div class="col-sm-1 text-center">
                    <div class="teaser_icon border_icon size_big round">
                        <i class="rt-icon2-clock3"></i>
                    </div>
                </div>

                <div class="col-sm-5">
                    <p>
                        Bookings ongoing. Click here for details 
                    </p>
                </div>
            </div>
        </div>
    </a>
</div>
</section>
