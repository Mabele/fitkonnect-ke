
<?php
require_once WPATH . "modules/classes/Feedback.php";
$feedback = new Feedback();

    if (isset($_SESSION['lesson_booking_transaction_status']) AND ( $_SESSION['lesson_booking_transaction_status'] == true)) {
        $title = "Fantastic";
        $message = "The lesson booking transaction has been saved successfully.";
        echo $feedback->successFeedback($title, $message);
        unset($_SESSION['lesson_booking_transaction_status']);
    }
?>

<?php //require_once 'modules/home/sliders.php';  ?>
<?php //require_once 'modules/home/what-we-offer.php';  ?>
<?php //require_once 'modules/home/projects.php';  ?>
<?php require_once 'modules/home/ideation.php'; ?>
<?php require_once 'modules/home/numbers.php'; ?>
<?php //require_once 'modules/home/blog.php';  ?>
<?php require_once 'modules/home/service_select.php'; ?>
<?php require_once 'modules/home/why-choose-us.php'; ?>
<?php //require_once 'modules/home/connect-with-us.php';  ?>
<?php //require_once 'modules/home/partners.php'; ?>