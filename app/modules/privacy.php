<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">Privacy Policy</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li>
                        <a href="./">Home</a>
                    </li>
                    <li class="active">Privacy Policy</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="ls ms section_padding_50">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <ul style="list-style-type: none;">
                        <h3 class="topmargin_0_privacy"><li>Introduction<li></h3>
                        <p class="bottommargin_10">When you use FitnessIQ, you trust us with your information. We are committed to keeping that trust. That starts with helping you understand our privacy practices.</p>
                        <p class="bottommargin_10">This policy describes the information we collect, how it is used and shared, and your choices regarding this information. We recommend that you read this - which highlights key points about our privacy practices.</p>
                        <p class="bottommargin_10">This policy applies to any users of the services of FitnessIQ or its affiliates anywhere in the world, and to anyone else who contacts FitnessIQ or otherwise submits information to FitnessIQ, unless noted below. This includes those who use FitnessIQ or its affiliates' services to:</p>
                        <p class="bottommargin_10">Request or receive our services or goods</p>
                        <p class="bottommargin_10">Provide services to our customers</p>
                        <p class="bottommargin_10">Any other user of FitnessIQ's services (including apps, websites and API), and anyone else who contacts FitnessIQ or otherwise submits information to FitnessIQ, unless subject to a separate privacy policy, notice or agreement such as the FitnessIQ Freight Privacy Policy</p>
                        <p class="bottommargin_10">FitnessIQ collects the following categories of information:</p>

                        <h3 class="topmargin_0_privacy"><li>Information you provide<li></h3>
                        <p class="bottommargin_10">This includes information submitted when you:</p>
                        <p class="bottommargin_10">Create or update your FitnessIQ account, which depending on your location and the FitnessIQ services you use may include your name, email, phone number, login name and password, address, payment or banking information, government identification numbers, birth date, and photo</p>
                        <p class="bottommargin_10">Consent to a background check (for agents where permitted by law)</p>
                        <p class="bottommargin_10">Request services through an FitnessIQ app or website</p>
                        <p class="bottommargin_10">Contact FitnessIQ, including for customer support</p>
                        <p class="bottommargin_10">Contact other FitnessIQ users through our services</p>
                        <p class="bottommargin_10">Complete surveys sent to you by FitnessIQ or on behalf of FitnessIQ</p>
                        <p class="bottommargin_10">Enable features that require FitnessIQ's access to your address book or calendar</p>

                        <h3 class="topmargin_0_privacy"><li>Information created when you use our services<li></h3>
                        <p class="bottommargin_10">This includes:</p>
                        <p class="bottommargin_10">Location Information</p>
                        <p class="bottommargin_10">Depending on the FitnessIQ services that you use, and your app settings or device permissions, FitnessIQ may collect your precise or approximate location information as determined through data such as GPS, IP address and WiFi.</p>
                        <p class="bottommargin_10">If you are an agent, FitnessIQ collects location information when the FitnessIQ app is running in the foreground (app open and on-screen) or background (app open but not on screen) of your device.</p>
                        <p class="bottommargin_10">If you are an agent, FitnessIQ may collect location information when the FitnessIQ app is running in the foreground. In certain regions, FitnessIQ may also collect this information when the FitnessIQ app is running in the background of your device if this collection is enabled through your app settings or device permissions.</p>
                        <p class="bottommargin_10">Transaction Information</p>
                        <p class="bottommargin_10">We collect transaction details related to your use of our services, including the type of services you requested or provided, date and time the service was provided, amount charged, duration of session, and other related transaction details. Additionally, if someone uses your promotion code, we may associate your name with that person.</p>
                        <p class="bottommargin_10">Usage and Preference Information</p>
                        <p class="bottommargin_10">We collect information about how you interact with our services, preferences expressed, and settings chosen. In some cases we do this through the use of cookies, pixel tags, and similar technologies that create and maintain unique identifiers. To learn more about these technologies, please see our Cookie Statement.
                        <p class="bottommargin_10">Device Information</p>
                        <p class="bottommargin_10">We may collect information about the devices you use to access our services, including the hardware models, operating systems and versions, software, file names and versions, preferred languages, unique device identifiers, advertising identifiers, serial numbers, device motion information, and mobile network information.</p>
                        <p class="bottommargin_10">Log Information</p>
                        <p class="bottommargin_10">When you interact with our services, we collect server logs, which may include information like device IP address, access dates and times, app features or pages viewed, app crashes and other system activity, type of browser, and the third-party site or service you were using before interacting with our services.</p>



                        <h3 class="topmargin_0_privacy"><li>Information from other sources<li></h3>
                        <p class="bottommargin_10">These may include:</p>
                        <p class="bottommargin_10">Users providing feedback, such as as ratings or compliments</p>
                        <p class="bottommargin_10">Insurance providers (if you are an agent or client)</p>
                        <p class="bottommargin_10">Financial services providers (if you are an agent or client)</p>
                        <p class="bottommargin_10">Publicly available sources</p>
                        <p class="bottommargin_10">Marketing service providers</p>
                        <p class="bottommargin_10">FitnessIQ may combine the information collected from these sources with other information in its possession.</p>
                        <p class="bottommargin_10">FitnessIQ uses the information it collects for purposes including:</p>
                        <p class="bottommargin_10">Providing services and features</p>
                        <p class="bottommargin_10">FitnessIQ uses the information we collect to provide, personalize, maintain and improve our products and services.</p> 
                        <p class="bottommargin_10">Enable effective delivery of services and sale of goods</p>
                        <p class="bottommargin_10">Process or facilitate payments for those services or goods</p>
                        <p class="bottommargin_10">Offer, obtain, provide or facilitate insurance or financing solutions in connection with our services</p>
                        <p class="bottommargin_10">Enable features that allow you to share information with other people, such as when you submit a compliment about an agent, refer a friend to FitnessIQ, or split bills.</p>
                        <p class="bottommargin_10">Perform internal operations necessary to provide our services, including to troubleshoot software bugs and operational problems, to conduct data analysis, testing, and research, and to monitor and analyze usage and activity trends</p>

                        <h3 class="topmargin_0_privacy"><li>Safety and security<li></h3>
                        <p class="bottommargin_10">We use your data to help maintain the safety, security and integrity of our services. For example, we collect information from agents' devices to identify bad behavior and raise awareness among agents regarding such behaviors. This often helps in preventing fraud and helping to protect other users.</p>

                        <h3 class="topmargin_0_privacy"><li>Customer support<li></h3>
                        <p class="bottommargin_10">FitnessIQ uses the information we collect (including recordings of customer support calls after notice to you and with your consent) to assist you when you contact our customer support services, including to:</p>
                        <p class="bottommargin_10">Direct your questions to the appropriate customer support person</p>
                        <p class="bottommargin_10">Investigate and address your concerns</p>
                        <p class="bottommargin_10">Monitor and improve our customer support responses</p>

                        <h3 class="topmargin_0_privacy"><li>Research and development<li></h3>
                        <p class="bottommargin_10">We may use the information we collect for testing, research, analysis and product development. This allows us to improve and enhance the safety and security of our services, develop new features and products, and facilitate insurance and finance solutions in connection with our services.</p>

                        <h3 class="topmargin_0_privacy"><li>Communications among users<li></h3>
                        <p class="bottommargin_10">FitnessIQ uses the information we collect to enable communications between our users. For example, agents may text or call a client to confirm a location.</p>          

                        <h3 class="topmargin_0_privacy"><li>Communications from FitnessIQ<li></h3>
                        <p class="bottommargin_10">FitnessIQ may use the information we collect to communicate with you about products, services, promotions, studies, surveys, news, updates and events. FitnessIQ may also use the information to promote and process contests and sweepstakes, fulfill any related awards, and serve you relevant ads and content about our services and those of our business partners. FitnessIQ may also use the information to inform you about elections, ballots, referenda and other political and policy processes that relate to our services.</p>

                        <h3 class="topmargin_0_privacy"><li>Legal proceedings and requirements<li></h3>
                        <p class="bottommargin_10">We may use the information we collect to investigate or address claims or disputes relating to your use of FitnessIQ's services, or as otherwise allowed by applicable law.</p>
                        <p class="bottommargin_10">Cookies are small text files that are stored on your browser or device by websites, apps, online media, and advertisements.</p>
                        <p class="bottommargin_10">FitnessIQ uses cookies and similar technologies for purposes such as:</p>
                        <p class="bottommargin_10">Authenticating users</p>
                        <p class="bottommargin_10">Remembering user preferences and settings</p>
                        <p class="bottommargin_10">Determining the popularity of content</p>
                        <p class="bottommargin_10">Delivering and measuring the effectiveness of advertising campaigns</p>
                        <p class="bottommargin_10">Analyzing site traffic and trends, and generally understanding the online behaviors and interests of people who interact with our services</p>
                        <p class="bottommargin_10">We may also allow others to provide audience measurement and analytics services for us, to serve advertisements on our behalf across the Internet, and to track and report on the performance of those advertisements. These entities may use cookies, web beacons, SDKs, and other technologies to identify your device when you visit our site and use our services, as well as when you visit other online sites and services.</p>
                        <p class="bottommargin_10">Please see our Cookie Statement for more information regarding the use of cookies and other technologies described in this section, including regarding your choices relating to such technologies.</p>

                        <h3 class="topmargin_0_privacy"><li>FitnessIQ may share the information we collect:<li></h3>
                        <p class="bottommargin_10">With other users</p>
                        <p class="bottommargin_10">For example, if you are an agent, we may share your first name, average agent rating given by clients.</p>
                        <p class="bottommargin_10">At your request</p>
                        <p class="bottommargin_10">This includes sharing your information with:</p>
                        <p class="bottommargin_10">FitnessIQ business partners. For example, if you requested a service through a partnership or promotional offering made by a third party, FitnessIQ may share your information with those third parties. This may include, for example, other apps or websites that integrate with our APIs or services, or those with an API or service with which we integrate, or business partners with whom FitnessIQ may partner with to deliver a promotion, a contest or a specialized service.</p>
                        <p class="bottommargin_10">With the general public when you submit content to a public forum.</p>
                        <p class="bottommargin_10">We love hearing from our users -- including through public forums such as FitnessIQ blogs, social media, and certain features on our network. When you communicate with us through those channels, your communications may be viewable by the public.</p>
                        <p class="bottommargin_10">With the owner of FitnessIQ accounts that you may use</p>
                        <p class="bottommargin_10">If you use a profile associated with another party we may share your  information with the owner of that profile profile. This occurs, for example, if you are:</p>
                        <p class="bottommargin_10">An agent using your employer's FitnessIQ for Business profile</p>
                        <p class="bottommargin_10">An agent who takes a services arranged by a friend or under a Group Profile</p>
                        <p class="bottommargin_10">With FitnessIQ subsidiaries and affiliates</p>
                        <p class="bottommargin_10">We share information with our subsidiaries and affiliates to help us provide our services or conduct data processing on our behalf. For example, FitnessIQ may process or store information in the one country on behalf of its international subsidiaries and affiliates.</p>
                        <p class="bottommargin_10">With FitnessIQ service providers and business partners</p>
                        <p class="bottommargin_10">FitnessIQ may provide information to its vendors, consultants, marketing partners, research firms, and other service providers or business partners. For example, FitnessIQ may provide information to such parties to help facilitate insurance coverage, to conduct surveys on our behalf, and to process payments for our services.</p>
                        <p class="bottommargin_10">For legal reasons or in the event of a dispute</p>
                        <p class="bottommargin_10">FitnessIQ may share your information if we believe it is required by applicable law, regulation, operating agreement, legal process or governmental request.</p>
                        <p class="bottommargin_10">This includes sharing your information with law enforcement officials, government authorities, or other third parties as necessary to enforce our Terms of Service, user agreements, or other policies, to protect FitnessIQ's rights or property or the rights or property of others, or in the event of a claim or dispute relating to your use of our services. If you use another person's credit card, we may be required by law to share information with that credit card holder, including service information.</p>
                        <p class="bottommargin_10">This also includes sharing your information with others in connection with, or during negotiations of, any merger, sale of company assets, consolidation or restructuring, financing, or acquisition of all or a portion of our business by or into another company.</p>
                        <p class="bottommargin_10">Please see FitnessIQ's Guidelines for Law Enforcement Authorities for more information.</p>
                        <p class="bottommargin_10">With your consent</p>
                        <p class="bottommargin_10">FitnessIQ may share your information other than as described in this policy if we notify you and you consent to the sharing.</p>
                        <p class="bottommargin_10">FitnessIQ retains your information while your account remains active, unless you ask us to delete your information or your account. Subject to the exceptions described below.</p>
                        <p class="bottommargin_10">FitnessIQ deletes or anonymizes your information upon request.</p>

                        <h3 class="topmargin_0_privacy"><li>Subject to applicable law, FitnessIQ may retain information after account deletion:<li></h3>
                        <p class="bottommargin_10">If there is an unresolved issue relating to your account, such as an outstanding credit on your account or an unresolved claim or dispute;</p>
                        <p class="bottommargin_10">If we are required to by applicable law; and/or in aggregated and/or anonymized form.</p>
                        <p class="bottommargin_10">FitnessIQ may also retain certain information if necessary for its legitimate business interests, such as fraud prevention and enhancing users' safety and security. For example, if FitnessIQ shuts down a user's account because of unsafe behavior or security incidents, FitnessIQ may retain certain information about that account to prevent that user from opening a new</p>
                        <p class="bottommargin_10">FitnessIQ account in the future.</p>

                        <h3 class="topmargin_0_privacy"><li>Device Permissions<li></h3>
                        <p class="bottommargin_10">Most mobile platforms (iOS, Android, etc.) have defined certain types of device data that apps cannot access without your consent. And these platforms have different permission systems for obtaining your consent. The iOS platform will alert you the first time the FitnessIQ app wants permission to access certain types of data and will let you consent (or not consent) to that request. Android devices will notify you of the permissions that the FitnessIQ app seeks before you first use the app, and your use of the app constitutes your consent.</p>

                        <h3 class="topmargin_0_privacy"><li>Ratings Look-Up<li></h3>
                        <p class="bottommargin_10">After every service, agent and clients are able to rate each other, as well as give feedback on how the service went. This two-way system holds everyone accountable for their behavior. Accountability helps create a respectful, safe environment for both parties.</p>

                        <h3 class="topmargin_0_privacy"><li>Accessing and Correcting Your Information<li></h3>
                        <p class="bottommargin_10">You can edit the name, phone number and email address associated with your account through the Settings menu in FitnessIQ's apps. You can also look up your services, orders and deliveries history in the FitnessIQ apps. You may also request access to, correction of, or a copy of your information by contacting FitnessIQ </p>

                        <h3 class="topmargin_0_privacy"><li>Marketing Opt-Outs<li></h3>
                        <p class="bottommargin_10">You may opt out of receiving promotional emails from FitnessIQ here. You may also opt out of receiving emails and other messages from FitnessIQ by following the instructions in those messages. Please note that if you opt out, we may still send you non-promotional messages, such as receipts for your rides or information about your account.</p>
                        <p class="bottommargin_10">We may occasionally update this policy. If we make significant changes, we will notify you of the changes through the FitnessIQ apps or through others means, such as email. To the extent permitted under applicable law, by using our services after such notice, you consent to our updates to this policy.</p>
                        <p class="bottommargin_10">We encourage you to periodically review this policy for the latest information on our privacy practices. We will also make prior versions of our privacy policies available for review.</p>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</section>