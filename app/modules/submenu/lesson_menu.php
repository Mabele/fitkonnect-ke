<?php
//if (!App::isLoggedIn()) App::redirectTo("?");
require_once WPATH . "modules/classes/Lessons.php";
$lessons = new Lessons();
?>

<div id='cssmenu'>
    <ul>
        <li class='has-sub'><a href='#'><span>Lesson Categories</span></a>
            <ul>
                <?php echo $lessons->menuLessonCategories(); ?>
            </ul>
        </li>
        <li class='has-sub'><a href='#'><span>Start Time</span></a>
            <ul>
                <li><a href='?lesson_booking&filter_type=start_time&filter_value=0600 HOURS'><span>0600 HOURS</span></a></li>
                <li><a href='?lesson_booking&filter_type=start_time&filter_value=0700 HOURS'><span>0700 HOURS</span></a></li>
                <li><a href='?lesson_booking&filter_type=start_time&filter_value=0800 HOURS'><span>0800 HOURS</span></a></li>
                <li><a href='?lesson_booking&filter_type=start_time&filter_value=0900 HOURS'><span>0900 HOURS</span></a></li>
                <li><a href='?lesson_booking&filter_type=start_time&filter_value=1000 HOURS'><span>1000 HOURS</span></a></li>
                <li><a href='?lesson_booking&filter_type=start_time&filter_value=1100 HOURS'><span>1100 HOURS</span></a></li>
                <li><a href='?lesson_booking&filter_type=start_time&filter_value=1200 HOURS'><span>1200 HOURS</span></a></li>
                <li><a href='?lesson_booking&filter_type=start_time&filter_value=1300 HOURS'><span>1300 HOURS</span></a></li>
                <li><a href='?lesson_booking&filter_type=start_time&filter_value=1400 HOURS'><span>1400 HOURS</span></a></li>
                <li><a href='?lesson_booking&filter_type=start_time&filter_value=1500 HOURS'><span>1500 HOURS</span></a></li>
                <li><a href='?lesson_booking&filter_type=start_time&filter_value=1600 HOURS'><span>1600 HOURS</span></a></li>
                <li><a href='?lesson_booking&filter_type=start_time&filter_value=1700 HOURS'><span>1700 HOURS</span></a></li>
                <li><a href='?lesson_booking&filter_type=start_time&filter_value=1800 HOURS'><span>1800 HOURS</span></a></li>
                <li><a href='?lesson_booking&filter_type=start_time&filter_value=1900 HOURS'><span>1900 HOURS</span></a></li>
                <li><a href='?lesson_booking&filter_type=start_time&filter_value=2000 HOURS'><span>2000 HOURS</span></a></li> 
            </ul>
        </li>
        <li class='has-sub'><a href='#'><span>Price</span></a>
            <ul>
                <li><a href='?lesson_booking&filter_type=price&filter_value=0to2000'><span>(KES) 0 - 2000</span></a></li>
                <li><a href='?lesson_booking&filter_type=price&filter_value=2001to4000'><span>(KES) 2001 - 4000</span></a></li>
                <li><a href='?lesson_booking&filter_type=price&filter_value=4001to6000'><span>(KES) 4001 - 6000</span></a></li>
                <li><a href='?lesson_booking&filter_type=price&filter_value=6001to8000'><span>(KES) 6001 - 8000</span></a></li>
                <li><a href='?lesson_booking&filter_type=price&filter_value=8001to10000'><span>(KES) 8001 - 10000</span></a></li>
                <li><a href='?lesson_booking&filter_type=price&filter_value=10001to10000000'><span>(KES) Over 10000</span></a></li>
            </ul>
        </li>
        <li class='has-sub last'><a href='#'><span>Availability/Capacity</span></a>
            <ul>
                <li><a href='?lesson_booking&filter_type=available_capacity&filter_value=1to10'><span>1 - 10</span></a></li>
                    <li><a href='?lesson_booking&filter_type=available_capacity&filter_value=11to20'><span>11 - 20</span></a></li>
                    <li><a href='?lesson_booking&filter_type=available_capacity&filter_value=21to30'><span>21 - 30</span></a></li>
                    <li><a href='?lesson_booking&filter_type=available_capacity&filter_value=31to40'><span>31 - 40</span></a></li>
                    <li><a href='?lesson_booking&filter_type=available_capacity&filter_value=41to50'><span>41 - 50</span></a></li>
                    <li><a href='?lesson_booking&filter_type=available_capacity&filter_value=51to1000000'><span>Over 50</span></a></li>
            </ul>
        </li>
        <li class='has-sub'><a href='#'><span>Training Facility</span></a>
            <ul>
                <?php echo $lessons->menuTrainingFacilities(); ?>
            </ul>
        </li>
    </ul>
</div>
