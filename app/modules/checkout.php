<?php
require_once WPATH . "modules/classes/Transactions.php";
require_once WPATH . "modules/classes/Lessons.php";
require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/Feedback.php";
$feedback = new Feedback();
$users_management = new Users_Management();
$lessons = new Lessons();
$transactions = new Transactions();

//if (isset($_SESSION["cart_number_of_items"]) AND $_SESSION["cart_number_of_items"] == 0) {
//    ?>
<!--    <div class="alert alert-block alert-error fade in">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>No items added to cart. <a href="?lesson_booking"> Click here </a> to add lessons to your cart before proceeding.</strong>
    </div>-->
        <?php
//}

if (!empty($_POST) AND $_POST['action'] == "checkout_transaction") {
    if (isset($_SESSION["cart_item"])) {

        if (!App::isLoggedIn()) {
            if ($users_management->checkIfUserEmailExists($_POST['email_address']) == true) {
                $_SESSION['check_if_email_exists'] = true;
                App::redirectTo("?checkout");
            }
        }

        // Billing details
        $_SESSION['ref_type'] = $users_management->getUserRefTypeId($_POST['user_type']);
        $_SESSION["billing_firstname"] = $_POST["firstname"];
        $_SESSION["billing_lastname"] = $_POST["lastname"];
        $_SESSION["billing_phone_number"] = $_POST["phone_number"];
        $_SESSION["billing_email_address"] = $_POST["email_address"];
        if (!App::isLoggedIn()) {
            $_SESSION["billing_gender"] = $_POST["gender"];
            $_SESSION["billing_birth_date"] = $_POST['dob_year'] . "-" . $_POST['dob_month'] . "-" . $_POST['dob_day'];
            if ($_SESSION["billing_birth_date"] > date("Y-m-d")) {
                $_SESSION['check_birth_date'] = true;
                App::redirectTo("?checkout");
            }
        }

        $_SESSION["billing_additional_comments"] = $_POST["additional_comments"];
        $_SESSION["payment_option"] = $_POST["payment_method"];
        $_SESSION["transaction_ref_number"] = $_POST["transaction_ref_number"];
        $_SESSION["start_date"] = $_POST['start_year'] . "-" . $_POST['start_month'] . "-" . $_POST['start_day'];

        if ($_SESSION["start_date"] < date("Y-m-d")) {
            $_SESSION['check_start_date'] = true;
            App::redirectTo("?checkout");
        }

        $transaction = $transactions->addTransaction();
        if (is_bool($transaction) && $transaction == true) {
            foreach ($_SESSION["cart_item"] as $item) {
                $_SESSION["number_of_classes"] = $item["number_of_classes"];
                $_SESSION['lesson_details'] = $lessons->fetchLessonDetails($item["id"]);
                $transaction_details = $transactions->addTransactionDetails();
            }

            $recipient_email = $_POST['email_address'];
            $recipient_name = $_POST['firstname'] . " " . $_POST['lastname'];
            $email_subject = "Transaction Acknowledgement";
            $email_message = "<html><body>"
                    . "<p><b>Hello " . $_POST['firstname'] . ",</b><br/>"
                    . "Thank you for transacting with us on Fitness IQ. <br/>"
                    . "To track your transaction, please <a href='{$_SESSION["web_url"]}?login&activity=trainee_transactions'>click here</a>. <br/>"
                    . "For any enquiries, kindly contact us on:   <br/>"
                    . "<ul>"
                    . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                    . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                    . "</ul>"
                    . "</body></html>";
            $users_management = new Users_Management();
            $send_email = $users_management->sendEmail($recipient_email, $recipient_name, $email_subject, $email_message);
                        
            if ($send_email == true) {
                return true;
                App::redirectTo("?thankyou");
            } else {
                return false;
            }

            unset($_SESSION['ref_type']);
            unset($_SESSION["billing_firstname"]);
            unset($_SESSION['billing_lastname']);
            unset($_SESSION["billing_phone_number"]);
            unset($_SESSION['billing_email_address']);
            unset($_SESSION["billing_gender"]);
            unset($_SESSION['billing_birth_date']);
            unset($_SESSION["billing_additional_comments"]);
            unset($_SESSION["start_date"]);
            unset($_SESSION['payment_option']);
            unset($_SESSION['cart_total_cost']);
            unset($_SESSION["transaction_id"]);
            unset($_SESSION["transaction_ref_number"]);
            unset($_SESSION["cart_item"]);
            $_SESSION["lesson_booking_transaction_status"] = true;

            App::redirectTo("?");
        } else {
            if (isset($_SESSION['payment_received']) && $_SESSION['payment_received'] == false) {
                $title = "Oops";
                $message = "Sorry, there was an error with the payment reference code entered. Please check and try again.";
                echo $feedback->errorFeedback($title, $message);
                unset($_SESSION['payment_received']);
                $title = "Oops";
                $message = "Sorry, there was an error processing your transaction. Please check and try again.";
                echo $feedback->errorFeedback($title, $message);
            }
        }
    }
}
?>

<?php
if (isset($_SESSION["check_if_email_exists"]) && $_SESSION["check_if_email_exists"] == true) {
    $title = "Oops,";
    $message = "Seems someone already registered with your preferred email address. Please proceed using a different email address or log in using this one.";
    echo $feedback->errorFeedback($title, $message);
    unset($_SESSION["check_if_email_exists"]);
}
if (isset($_SESSION["check_start_date"]) && $_SESSION["check_start_date"] == true) {
    $title = "Error";
    $message = "The start date must be later than today.";
    echo $feedback->errorFeedback($title, $message);
    unset($_SESSION["check_start_date"]);
}
if (isset($_SESSION["check_birth_date"]) && $_SESSION["check_birth_date"] == true) {
    $title = "Error";
    $message = "The birth date must be later than today.";
    echo $feedback->errorFeedback($title, $message);
    unset($_SESSION["check_birth_date"]);
}
?>

<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">Checkout</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li><a href="./">Home</a></li>
                    <li><a href="?lesson_booking">Lesson Booking</a></li>
                    <li class="active">Checkout</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="ls ms section_padding_top_100 section_padding_bottom_75 columns_padding_25">
    <div class="container">
        <div class="row">
            <!--<aside class="col-sm-5 col-md-4 col-lg-4">-->

            <!--</aside>-->



            <form class="form-horizontal checkout shop-checkout" role="form" method="post">                    
                <input type="hidden" name="action" value="checkout_transaction"/>
                <input type="hidden" name="user_type" value="TRAINEE">

                <div class="col-sm-7 col-md-8 col-lg-8">
                    <div id="order_review" class="shop-checkout-review-order">
                        <?php
                        if (isset($_SESSION["cart_item"])) {
                            $item_total = 0;
                            foreach ($_SESSION["cart_item"] as $item) {
                                $item_total += ($item["price"] * $item["number_of_classes"]);
                                $_SESSION["cart_total_cost"] = $item_total;
                            }
                            ?>
                            <h3 class="widget-title" id="order_review_heading">Your order <a href="?lesson_booking"><button class="theme_button color1">Add More Lessons</button></a> </h3> 
                            <table class="table shop_table shop-checkout-review-order-table">
                                <tbody>
                                    <tr>
                                        <td class="product-name"><strong>Product Name </strong></td>
                                        <td class="product-total"><strong>Amount (KES)</strong></td>                                            
                                        <td></td>
                                    </tr>
                                    <?php
                                    foreach ($_SESSION["cart_item"] as $item) {
                                        $lesson_details = $lessons->fetchLessonDetails($item["id"]);
                                        ?>
                                        <tr>
                                            <td class="product-total"><strong><?php echo $lesson_details['name']; ?></strong></td>      
                                            <td class="product-total"><strong><?php echo $lesson_details['price']; ?></strong></td>
                                            <td><a href="?lessons_cart&cart_action=remove&code=<?php echo $item["id"]; ?>"><button class="theme_button color2">Remove</button></a></td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td class="product-name"><strong>Grand Total</strong></td>
                                        <td class="product-total"><strong><?php echo "KES " . $_SESSION["cart_total_cost"]; ?></strong></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="product-name"><strong>IQ Points</strong></td>
                                        <td class="product-total"><strong><?php echo 0.10 * $_SESSION["cart_total_cost"]; ?></strong></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        <?php } else { ?>

                            <div class="alert alert-block alert-error fade in">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>You have not booked any lesson. Please <a href="?lesson_booking"> Click here </a> to book a lesson before checking out.</strong>
                            </div>
                        <?php } ?>


                    </div>


                    <?php if (!App::isLoggedIn()) { ?>
                        <p>If you are registered with us, please <a href="?login">click here</a> to login, otherwise, enter your details below. </p>
                    <?php } ?>

                    <h3>Billing Address</h3>
                    <?php
                    if (App::isLoggedIn()) {
                        ?>
                        <div class = "form-group validate-required">
                            <label for = "firstname" class = "col-sm-3 control-label">
                                <span class = "grey">First Name </span>
                                <span class = "required">*</span>
                            </label>
                            <div class = "col-sm-9">
                                <input type = "text" class = "form-control " name = "firstname" value = "<?php echo $_SESSION['logged_in_user_details']['firstname']; ?>">
                            </div>
                        </div>
                        <div class = "form-group validate-required">
                            <label for = "lastname" class = "col-sm-3 control-label">
                                <span class = "grey">Last Name </span>
                                <span class = "required">*</span>
                            </label>
                            <div class = "col-sm-9">
                                <input type = "text" class = "form-control " name = "lastname" value = "<?php echo $_SESSION['logged_in_user_details']['lastname']; ?>">
                            </div>
                        </div>
                        <div class = "form-group validate-required validate-email">
                            <label for = "email" class = "col-sm-3 control-label">
                                <span class = "grey">Email Address </span>
                                <span class = "required">*</span>
                            </label>
                            <div class = "col-sm-9">
                                <input type = "email" class = "form-control " name = "email_address" value = "<?php
                                if ($_SESSION['logged_in_user_type_details']['name'] == "STAFF" OR $_SESSION['logged_in_user_type_details']['name'] == "TRAINEE" OR $_SESSION['logged_in_user_type_details']['name'] == "TRAINER") {
                                    echo $_SESSION['contacts']['email'];
                                } else if ($_SESSION['logged_in_user_type_details']['name'] == "TRAINING FACILITY ADMINISTRATOR") {
                                    echo $_SESSION['logged_in_user_details']['email'];
                                } else if ($_SESSION['logged_in_user_type_details']['name'] == "SYSTEM ADMINISTRATOR") {
                                    echo $_SESSION['logged_in_user_details']['email'];
                                }
                                ?>">
                            </div>
                        </div>
                        <div class = "form-group address-field validate-required">
                            <label for = "phone_number" class = "col-sm-3 control-label">
                                <span class = "grey">Phone Number </span>
                                <span class = "required">*</span>
                            </label>
                            <div class = "col-sm-9">
                                <input type = "text" class = "form-control " name = "phone_number" value = "<?php
                                if ($_SESSION['logged_in_user_type_details']['name'] == "STAFF" OR $_SESSION['logged_in_user_type_details']['name'] == "TRAINEE" OR $_SESSION['logged_in_user_type_details']['name'] == "TRAINER") {
                                    echo $_SESSION['contacts']['phone_number1'];
                                } else if ($_SESSION['logged_in_user_type_details']['name'] == "TRAINING FACILITY ADMINISTRATOR") {
                                    echo $_SESSION['logged_in_user_details']['phone_number'];
                                } else if ($_SESSION['logged_in_user_type_details']['name'] == "SYSTEM ADMINISTRATOR") {
                                    echo $_SESSION['logged_in_user_details']['phone_number'];
                                }
                                ?>">
                            </div>
                        </div>
                    <?php } else { ?>   
                        <div class="form-group validate-required">
                            <label for="firstname" class="col-sm-3 control-label">
                                <span class="grey">First Name </span>
                                <span class="required">*</span>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control " name="firstname" placeholder="" <?php
                                if (isset($_SESSION["billing_firstname"])) {
                                    echo "value='{$_SESSION["billing_firstname"]}'";
                                }
                                ?> required="">
                            </div>
                        </div>
                        <div class="form-group validate-required">
                            <label for="lastname" class="col-sm-3 control-label">
                                <span class="grey">Last Name </span>
                                <span class="required">*</span>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control " name="lastname" placeholder="" <?php
                                if (isset($_SESSION["billing_lastname"])) {
                                    echo "value='{$_SESSION["billing_lastname"]}'";
                                }
                                ?> required="">
                            </div>
                        </div>
                        <div class="form-group validate-required validate-email">
                            <label for="email" class="col-sm-3 control-label">
                                <span class="grey">Email Address </span>
                                <span class="required">*</span>
                            </label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control " name="email_address" placeholder="email" <?php
                                if (isset($_SESSION["billing_email_address"])) {
                                    echo "value='{$_SESSION["billing_email_address"]}'";
                                }
                                ?> required="">
                            </div>
                        </div>
                        <div class="form-group address-field validate-required">
                            <label for="phone_number" class="col-sm-3 control-label">
                                <span class="grey">Phone Number </span>
                                <span class="required">*</span>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control " name="phone_number" placeholder="+2547XXXXXXXX" <?php
                                if (isset($_SESSION["billing_phone_number"])) {
                                    echo "value='{$_SESSION["billing_phone_number"]}'";
                                }
                                ?> required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="gender" class="col-sm-3 control-label">
                                <span class="grey">Gender </span>
                                <span class="required">*</span>
                            </label>
                            <div class="col-sm-9">
                                <select class="form-control" name="gender" id="gender" required="">
                                    <?php include '../snippets/gender.php'; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="day" class="col-sm-3 control-label">
                                <span class="grey">Date of Birth</span>
                                <span class="required">*</span>
                            </label>
                            <div class="col-sm-3">
                                <select class="form-control" name="dob_day" id="day" required="" >
                                    <?php include '../snippets/day.php'; ?>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control" name="dob_month" id="month" required="" >
                                    <?php include '../snippets/month.php'; ?>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control" name="dob_year" id="year" required="" >
                                    <?php include '../snippets/year.php'; ?>
                                </select>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="form-group">
                        <label for="day" class="col-sm-3 control-label">
                            <span class="grey">Starting Date</span>
                            <span class="required">*</span>
                        </label>
                        <div class="col-sm-3">
                            <select class="form-control" name="start_day" id="day" required="" >
                                <?php include '../snippets/day.php'; ?>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <select class="form-control" name="start_month" id="month" required="" >
                                <?php include '../snippets/month.php'; ?>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <select class="form-control" name="start_year" id="year" required="" >
                                <?php include '../snippets/year.php'; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="additional_comments" class="col-sm-3 control-label">
                            <span class="grey">Additional Notes</span>
                        </label>
                        <div class="col-sm-9">
                            <textarea name="additional_comments" class="form-control" id="additional_comments" placeholder="Please enter your preferred training facility and lesson start time....." rows="5" required="yes"><?php
                                if (isset($_SESSION["billing_additional_comments"])) {
                                    echo $_SESSION["billing_additional_comments"];
                                }
                                ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="checkbox" name="terms_and_conditions" id="terms_and_conditions" value="Yes" required=""/> 
                        <label for="terms_and_conditions"> 
                            I accept Fitkonnect KE's 
                            <a href="?tc">terms and conditions</a>
                        </label>
                    </div>
                </div>
                <!--eof .col-sm-8 (main content)-->

                <!-- sidebar -->
                <aside class="col-sm-5 col-md-4 col-lg-4" style='background-color: #0a343b; padding: 10px;'>
                    <div id="order_review" class="shop-checkout-review-order">
                        <div id="payment" class="shop-checkout-payment">
                            <h3 class="widget-title" style="color: white;"> <i class="rt-icon2-wallet"></i> Payment</h3>
                            <ul class="list1 no-bullets payment_methods methods">
                                <li class="payment_method_mpesa">
                                    <div class="radio">
                                        <label for="payment_method_mpesa">
                                            <!--<input id="payment_method_mpesa" type="radio" name="payment_method" value="mpesa" checked="checked">-->
                                            <input type="hidden" name="payment_method" value="mpesa">
                                            <span class="white">Follow the below steps</span>
                                        </label>
                                    </div>
                                    <div class="payment_box payment_method_mpesa">
                                        <ol>         
                                            <li>Go to Mpesa on your phone</li>
                                            <li>Select Lipa na Mpesa</li>
                                            <li>Select Paybill</li>
                                            <li>Enter Paybill Number: <?php echo $_SESSION['till_number']; ?></li>
                                            <li>Enter Paybill Number: <?php echo $_SESSION['account_no']; ?></li>
                                            <?php if (isset($_SESSION["cart_item"])) { ?>
                                                <li>Enter Amount: <?php echo $_SESSION["cart_total_cost"]; ?></li>
                                            <?php } else { ?>
                                                <li>Enter Amount: 0</li>
                                            <?php } ?>

                                            <li>Enter your Mpesa PIN and send</li>
                                            <li>Once you receive a successful reply from Mpesa, click the submit button below</li>
                                        </ol>
                                    </div>
                                    <br />
                                    <div class="form-group validate-required">
                                        <label for="mpesa_reference" class="col-sm-3 control-label">
                                            <span class="white">Trans. Ref </span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control " name="transaction_ref_number" placeholder="E.g. MX***********" <?php
                                            if (isset($_SESSION["transaction_ref_number"])) {
                                                echo "value='{$_SESSION["transaction_ref_number"]}'";
                                            }
                                            ?> required="">
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="place-order">
                                <input type="submit" class="theme_button color1" value="Submit">
                            </div>
                        </div>
                    </div>
                </aside>
                <!-- eof aside sidebar -->
            </form>
        </div>
    </div>
</section>