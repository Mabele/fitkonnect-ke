<?php
require_once WPATH . "modules/classes/Trainers.php";
require_once WPATH . "modules/classes/Feedback.php";
$feedback = new Feedback();
$trainers = new Trainers();

if (!isset($_SESSION['trainer_details'])) {
    App::redirectTo("?verify_trainer");
}
?>

<section class="ls ms section_padding_50">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>Trainer Verification Status</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="thumbnail">
                    <?php
                    if (empty($trainers)) {
                        ?>
                        <img src="<?php echo $_SESSION['admin_url']; ?>/img/trainers/profile_pictures/<?php echo $_SESSION['trainer_details']['prof_picture']; ?>" alt=""/>
                        <?php
                    }
                    if (!empty($trainers)) {
                        ?>
                        <img src="<?php echo $_SESSION['admin_url']; ?>/img/trainers/profile_pictures/<?php echo $_SESSION['trainer_details']['prof_picture']; ?>" alt=""/>
                        <?php
                    }
                    ?>
                    <div class="caption">
                        <h3>
                            <?php echo $_SESSION['trainer_details']['firstname'] . ' ' . $_SESSION['trainer_details']['lastname']; ?>
                        </h3>
                        <p><?php echo $_SESSION['trainer_details']['training_level']; ?></p>
                        <p class="text-center social-icons">
                            <!--                            <a class="social-icon soc-facebook" href="#" title="Facebook" data-toggle="tooltip"></a>
                                                        <a class="social-icon soc-twitter" href="#" title="Twitter" data-toggle="tooltip"></a>
                                                        <a class="social-icon soc-google" href="#" title="Google" data-toggle="tooltip"></a>-->
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <h3>Short Profile</h3>
                <p><?php echo $_SESSION['trainer_details']['fk_profile']; ?></p>
            </div>



        </div>
    </div>
</section>