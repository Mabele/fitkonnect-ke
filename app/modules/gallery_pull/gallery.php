<section class="ls page_portfolio section_padding_top_100 section_padding_bottom_0 columns_padding_0">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="isotope_container isotope row masonry-layout columns_margin_0" data-filters=".isotope_filters">

                    <div class="isotope-item col-lg-2 col-md-4 col-sm-6 environment">
                        <div class="vertical-item gallery-item content-absolute bottom-content text-center ds">
                            <div class="item-media">
                                <img src="images/gallery/31.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap darklinks">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="images/gallery/31.jpg"></a>
                                        <a class="p-link" title="" href="gallery-single.html"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-content darken_gradient">
                                <h4 class="item-meta text-uppercase bottommargin_0">
                                    <a href="gallery-single.html">Consetetur sadipscing elitr, sed diam nonumy</a>
                                </h4>
                            </div>
                        </div>
                    </div>

                    <div class="isotope-item col-lg-4 col-md-4 col-sm-6 heat">
                        <div class="vertical-item gallery-item content-absolute bottom-content text-center ds">
                            <div class="item-media">
                                <img src="images/gallery/02.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap darklinks">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="images/gallery/02.jpg"></a>
                                        <a class="p-link" title="" href="gallery-single.html"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-content darken_gradient">
                                <h4 class="item-meta text-uppercase bottommargin_0">
                                    <a href="gallery-single.html">Consetetur sadipscing elitr, sed diam nonumy</a>
                                </h4>
                            </div>
                        </div>
                    </div>

                    <div class="isotope-item col-lg-2 col-md-4 col-sm-6 wind">
                        <div class="vertical-item gallery-item content-absolute bottom-content text-center ds">
                            <div class="item-media">
                                <img src="images/gallery/03.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap darklinks">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="images/gallery/03.jpg"></a>
                                        <a class="p-link" title="" href="gallery-single.html"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-content darken_gradient">
                                <h4 class="item-meta text-uppercase bottommargin_0">
                                    <a href="gallery-single.html">Consetetur sadipscing elitr, sed diam nonumy</a>
                                </h4>
                            </div>
                        </div>
                    </div>

                    <div class="isotope-item col-lg-2 col-md-4 col-sm-6 solar">
                        <div class="vertical-item gallery-item content-absolute bottom-content text-center ds">
                            <div class="item-media">
                                <img src="images/gallery/04.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap darklinks">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="images/gallery/04.jpg"></a>
                                        <a class="p-link" title="" href="gallery-single.html"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-content darken_gradient">
                                <h4 class="item-meta text-uppercase bottommargin_0">
                                    <a href="gallery-single.html">Consetetur sadipscing elitr, sed diam nonumy</a>
                                </h4>
                            </div>
                        </div>
                    </div>

                    <div class="isotope-item col-lg-2 col-md-4 col-sm-6 process">
                        <div class="vertical-item gallery-item content-absolute bottom-content text-center ds">
                            <div class="item-media">
                                <img src="images/gallery/05.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap darklinks">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="images/gallery/05.jpg"></a>
                                        <a class="p-link" title="" href="gallery-single.html"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-content darken_gradient">
                                <h4 class="item-meta text-uppercase bottommargin_0">
                                    <a href="gallery-single.html">Consetetur sadipscing elitr, sed diam nonumy</a>
                                </h4>
                            </div>
                        </div>
                    </div>

                    <div class="isotope-item col-lg-2 col-md-4 col-sm-6 solar">
                        <div class="vertical-item gallery-item content-absolute bottom-content text-center ds">
                            <div class="item-media">
                                <img src="images/gallery/06.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap darklinks">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="images/gallery/06.jpg"></a>
                                        <a class="p-link" title="" href="gallery-single.html"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-content darken_gradient">
                                <h4 class="item-meta text-uppercase bottommargin_0">
                                    <a href="gallery-single.html">Consetetur sadipscing elitr, sed diam nonumy</a>
                                </h4>
                            </div>
                        </div>
                    </div>

                    <div class="isotope-item col-lg-2 col-md-4 col-sm-6 environment">
                        <div class="vertical-item gallery-item content-absolute bottom-content text-center ds">
                            <div class="item-media">
                                <img src="images/gallery/28.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap darklinks">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="images/gallery/28.jpg"></a>
                                        <a class="p-link" title="" href="gallery-single.html"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-content darken_gradient">
                                <h4 class="item-meta text-uppercase bottommargin_0">
                                    <a href="gallery-single.html">Consetetur sadipscing elitr, sed diam nonumy</a>
                                </h4>
                            </div>
                        </div>
                    </div>

                    <div class="isotope-item col-lg-2 col-md-4 col-sm-6 heat">
                        <div class="vertical-item gallery-item content-absolute bottom-content text-center ds">
                            <div class="item-media">
                                <img src="images/gallery/08.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap darklinks">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="images/gallery/08.jpg"></a>
                                        <a class="p-link" title="" href="gallery-single.html"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-content darken_gradient">
                                <h4 class="item-meta text-uppercase bottommargin_0">
                                    <a href="gallery-single.html">Consetetur sadipscing elitr, sed diam nonumy</a>
                                </h4>
                            </div>
                        </div>
                    </div>

                    <div class="isotope-item col-lg-2 col-md-4 col-sm-6 environment">
                        <div class="vertical-item gallery-item content-absolute bottom-content text-center ds">
                            <div class="item-media">
                                <img src="images/gallery/09.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap darklinks">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="images/gallery/09.jpg"></a>
                                        <a class="p-link" title="" href="gallery-single.html"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-content darken_gradient">
                                <h4 class="item-meta text-uppercase bottommargin_0">
                                    <a href="gallery-single.html">Consetetur sadipscing elitr, sed diam nonumy</a>
                                </h4>
                            </div>
                        </div>
                    </div>

                    <div class="isotope-item col-lg-4 col-md-4 col-sm-6 process">
                        <div class="vertical-item gallery-item content-absolute bottom-content text-center ds">
                            <div class="item-media">
                                <img src="images/gallery/30.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap darklinks">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="images/gallery/30.jpg"></a>
                                        <a class="p-link" title="" href="gallery-single.html"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-content darken_gradient">
                                <h4 class="item-meta text-uppercase bottommargin_0">
                                    <a href="gallery-single.html">Consetetur sadipscing elitr, sed diam nonumy</a>
                                </h4>
                            </div>
                        </div>
                    </div>

                    <div class="isotope-item col-lg-2 col-md-4 col-sm-6 solar">
                        <div class="vertical-item gallery-item content-absolute bottom-content text-center ds">
                            <div class="item-media">
                                <img src="images/gallery/11.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap darklinks">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="images/gallery/11.jpg"></a>
                                        <a class="p-link" title="" href="gallery-single.html"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-content darken_gradient">
                                <h4 class="item-meta text-uppercase bottommargin_0">
                                    <a href="gallery-single.html">Consetetur sadipscing elitr, sed diam nonumy</a>
                                </h4>
                            </div>
                        </div>
                    </div>

                    <div class="isotope-item col-lg-2 col-md-4 col-sm-6 process">
                        <div class="vertical-item gallery-item content-absolute bottom-content text-center ds">
                            <div class="item-media">
                                <img src="images/gallery/12.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap darklinks">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="images/gallery/12.jpg"></a>
                                        <a class="p-link" title="" href="gallery-single.html"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-content darken_gradient">
                                <h4 class="item-meta text-uppercase bottommargin_0">
                                    <a href="gallery-single.html">Consetetur sadipscing elitr, sed diam nonumy</a>
                                </h4>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- eof .isotope_container.row -->


            </div>
        </div>
    </div>
</section>