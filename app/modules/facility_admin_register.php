<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">Facility Administration Registration</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li>
                        <a href="./">Home</a>
                    </li>
                    <li class="active">Facility Administration Registration</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="ls section_padding_top_100 section_padding_bottom_100">
    <div class="container">
        <div class="row">
            <form class="facility-admin-register" role="form">
                <div class="col-sm-6">
                    <div class="form-group validate-required" id="firstname">
                        <label for="firstname" class="control-label">
                            <span class="grey">First Name </span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="firstname" id="firstname" placeholder="" value="">
                    </div>
                    
                    <div class="form-group validate-required validate-email" id="email">
                        <label for="email" class="control-label">
                            <span class="grey">Email</span>
                            <span class="required">*</span>
                        </label>
                        <input type="email" class="form-control " name="email" id="email" placeholder="" value="">
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group validate-required" id="lastname">
                        <label for="lastname" class="control-label">
                            <span class="grey">Last Name</span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="lastname" id="lastname" placeholder="" value="">
                    </div>
                </div>
                <div class="col-lg-3">
                     <div class="form-group validate-required validate-phone" id="phone_number">
                        <label for="phone_number" class="control-label">
                            <span class="grey">Phone Number </span>
                            <span class="required">*</span>
                        </label>
                        <input type="tel" class="form-control " name="phone_number" id="phone_number" placeholder="" value="">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group validate-required" id="idnumber">
                        <label for="idnumber" class="control-label">
                            <span class="grey">ID Number </span>
                            <span class="required">*</span>
                        </label>
                       <input type="number" class="form-control " name="idnumber" id="idnumber" placeholder="" value="">
                    </div>
                </div>
                <div class="col-sm-12">
                    <button type="submit" class="theme_button wide_button color1">Register Now</button>
                    <button type="reset" class="theme_button wide_button">Clear Form</button>
                </div>
            </form>
        </div>

    </div>
</div>
</section>