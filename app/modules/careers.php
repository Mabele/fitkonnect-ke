<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">Why Choose us for your Career?</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li>
                        <a href="./">Home</a>
                    </li>
                    
                    <li>
                        <a href="?coach_application">Application</a>
                    </li>
                    <li class="active">Careers</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="ls ms section_padding_50">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ol>
                    <h3 class="topmargin_0_privacy"><li>Feel welcome!</li></h3>
                    Whether you are a student sports trainer, a young graduate in your chosen sport’s field or an experienced trainer, we care about you from day one! Case in point: our induction and onboarding procedures will make your first steps with Fitness IQ exciting! We’ve put everything in place to allow you to deepen your knowledge about your chosen sport at the professional level, our client’s and our organization and start building your career as a Fitness trainer.
                    You'll have access to our portal to help you picture what your engagement with Fitness IQ and our clients will be like. At the portal, you'll find all the information you need, including the agenda of your first week. When joining Fitness IQ, we'll take you through some essential training sessions to kick off your new career. In addition, you'll meet your buddy agent who will answer all your questions and help you throughout the entire process.

                    <ol class="li-child">
                        <strong>Why you should work for Fitness IQ?</strong>
                        <li class="topmargin_0_privacy">Our people are our brand</li>
                        People come to Fitness IQ for a lot of reasons. Our people are one of them. This is an organization where you are surrounded by intelligent, forward-thinking people. This is a chance to work within a culture of camaraderie and grow your career the way you want. So come for the big opportunities, and stay for the people. At Fitness IQ, you’ll love the company you keep.
                        <li class="topmargin_0_privacy">A powerful network of talented trainers and coaches</li>
                        When you join Fitness IQ, you join a powerful network of talented professionals. We provide opportunities to build lifelong connections with colleagues and clients – because we perform better when we connect the dots between our people and their strengths.
                        <li class="topmargin_0_privacy">Leveraging diversity and inclusion in everything we do</li>
                        We are proud that long before other companies can take action to support diversity and inclusion in the workplace, Fitness IQ takes the lead. We recognize that a culture that celebrates individuality and leverages those differences represents an enormous competitive advantage. Simply put, a workplace where people feel valued and included is a place where people soar.
                        As part of our commitment to inclusion, we sponsor several active communities that any employee can join. Take advantage of networking events and mentorship opportunities or attend cultural celebrations, workshops, trainings, holiday camps and social outings – and whenever you do, make your voice heard.
                        <li class="topmargin_0_privacy">Mobility options and flex work arrangements</li>
                        Sometimes you need a change or personal obligations have to come first. That is why we are committed to giving our people the choices and flexibility they need to be at their best, and stay at their best. At Fitness IQ, there are a variety of work arrangements you can take advantage of, including compressed work weeks, sabbaticals and flex work hours. We know the days of one set path to career success are long over. So join Fitness IQ and find the flexibility to build the career and life you want.

                        Itching for a bigger change? Then take advantage of our various short or long-term international opportunities to see the world without having to put your career on pause.
                        <li class="topmargin_0_privacy">A leader in professional services</li>
                        At Fitness IQ, we are redefining what it means to be a professional services firm because we make excellence our standard. We know that being average just won’t cut it. We are all about exceptional. Exceptional people. Exceptional services. And exceptional careers. We operate with the shared values of integrity, quality and accountability in everything we do. 
                        <li class="topmargin_0_privacy">Fitness IQ is a great place to push your career to the next level</li>
                        Whether you are starting your career or looking to move to the next level, a career at Fitness IQ empowers you to customize a career that works for you. There’s a reason why we are one of the most sought-after employers around – we offer challenging work, meaningful development opportunities and flexibility that’s hard to match. Above all, we have an enviable culture built on the smart, likeable people we surround ourselves with. Broaden your skills. Broaden your reach. Broaden your career. It’s all possible at Fitness IQ.



                        <li class="topmargin_0_privacy">World-class learning and professional development opportunities</li>
                        An investment in your learning and development is an investment in our people and our clients, so we don’t cut corners. Whether it’s an e-course, managerial training programs, or other courses, your learning and development is one of our priorities.


                        <li class="topmargin_0_privacy">A culture built on collaboration and innovation</li>
                        Innovation is our key to success and we practice it in everything we do. With substantial resources at our fingertips such as innovative workspaces, virtual meeting technology - not to mention a collection of diverse and smart minds - our collaboration sessions are both stimulating and effective. The result? Creative, out-of-the-box ideas that are redefining everything. 


                        <li class="topmargin_0_privacy">Fitness IQ is an inspiring place to work</li>
                        Imagine a workplace that challenges the norm. A place where you can be inspired and be inspiring.  At Fitness IQ, just about anything – from your workplace, to the people, to the community - can help you uncover a solution or spark a fresh idea.
                        <br/><strong>People</strong><br/>
                        We seek out the most likeable professionals with the brightest minds. Stuck for ideas on a new project? Holding a collaborative brainstorming session with a few of your peers may be all it takes to trigger a breakthrough.
                        <br/><strong>Community</strong><br/>
                        At Fitness IQ we integrate corporate responsibility into our core business strategy. This is why we encourage practices that protect the environment, support local communities and have a positive impact on society.
                        <li class="topmargin_0_privacy">Empowerment and recognition</li>
                        Find yourself in a place that empowers you to own your career. We encourage our people to use all of their resources and talents to reach their full potential at Fitness IQ.  And when our people show us what they’ve got, we reward them with the flexibility and extraordinary life experiences they desire. Along with traditional compensation and our competitive benefits package, we offer flexibility and variable pay to eligible employees. At our company, recognition doesn’t just come from leaders. It’s part of a culture where we take pride in our colleagues’ accomplishments as much as we do our own.
                    </ol>

                    <h3 class="topmargin_0_privacy"><li>What matters to us</li></h3>
                    Your individual development matters to us. This is why we'll give you clear guidance and support and on-the-job coaching, on-going performance feedback and team-based learning. This way, you'll get a grasp of your evolution and know in each moment where you stand. In order to navigate we offer a leadership framework. It's called Fitness IQ Development and is articulated around 5 focus areas:

                    <ol class="li-child">
                        <br/><strong>Leadership</strong><br/>
                        We want you to lead yourself and the others, regardless of your level. Your work can make a difference through delivering results in a responsible, authentic, resilient, inclusive and passionate way.
                        <li class="topmargin_0_privacy">Do you learn from your experiences and take the time to personalize your approach to work?</li>
                        <li class="topmargin_0_privacy">Do you lead others to be the best they can, whether you're part of a team or leading one?</li>
                        <li class="topmargin_0_privacy">Do you act with integrity and uphold professional standards at all times?</li>
                    </ol>

                    <ol class="li-child">
                        <br/><strong>Business Minded</strong><br/>
                        We value the fresh and creative thinking of beginners, but also the business knowledge that comes with more experienced people. Get ready to power up your skills and generate value for our clients, for Fitness IQ and, most importantly, for yourself.
                        <li class="topmargin_0_privacy">How do you keep up-to-date with business and industry trends?</li>
                        <li class="topmargin_0_privacy">Do you consider all the facts and think broadly about data when making recommendations?</li>
                        <li class="topmargin_0_privacy">Do you see valuable opportunities in changing business environments?</li>
                    </ol>

                    <ol class="li-child">
                        <br/><strong>Technical Expertise</strong><br/>
                        To fulfil your role, you need a range of technical capabilities.
                        <li class="topmargin_0_privacy">Do you maintain professional standards to provide high quality results?</li>
                        <li class="topmargin_0_privacy">How do you build and maintain your technical expertise and knowledge?</li>
                        <li class="topmargin_0_privacy">Do you share your knowledge with others?</li>
                    </ol>

                    <ol class="li-child">
                        <br/><strong>Diversity</strong><br/>
                        Being a company that serves diverse customers, our daily activity is marked by change and transformation. You'll need to operate and collaborate effectively, with a mindset that transcends geographic and cultural boundaries.
                        <li class="topmargin_0_privacy">Do you consider a broad range of perspectives in your thinking?</li>
                        <li class="topmargin_0_privacy">Do you see and seize the opportunities in change?</li>
                        <li class="topmargin_0_privacy">Can you bring fresh ideas to our clients and Fitness IQ?</li>
                    </ol>

                    <ol class="li-child">
                        <br/><strong>Relationships</strong><br/>
                        Genuine relationships, rooted in trust, are essential in your job
                        <li class="topmargin_0_privacy">Do you communicate with confidence?</li>
                        <li class="topmargin_0_privacy">Do you build and maintain strong and authentic relationships, within and beyond your professional network?</li>
                        <li class="topmargin_0_privacy">Are you passionate about providing exceptional services?</li>
                    </ol>

                    <h3 class="topmargin_0_privacy"><li>We develop our talents through the 80-10-10 approach.</li></h3>
                    Besides technical courses we offer you development solutions focusing on soft & business skills. We offer you the most adapted frame for development if you take the ownership to identify and use the most appropriate learning experiences according to your business and development requirements:
                    A wide range of courses is available to be trained on knowledge and skills (10% - Education). Mentors, role models and other coaches can accompany you through your development (10% - Exchange & Exposure). The most effective learning comes from doing. Up to you to grad the opportunities to get out of your comfort and work on unfamiliar projects and challenges (80% - Experience).

                    <ol class="li-child">
                        <br/><strong>Technical training</strong><br/>
                        One of our priorities is to support you in getting strong technical skills and in becoming a knowledgeable expert. Accordingly, we provide you with a wide range of training courses responding to gaps in your skill set.

                        <br/><strong>Soft skills</strong><br/>
                        Working with a diverse clientele and dealing with various teams of co-workers requires sound behavioral capabilities. This is why we set up a dedicated program focused on self-awareness, client and people development skills.
                        We designed the soft skills curriculum in a scalable format matching your various career steps and maturity levels. Each program is run on a cross-competency basis to connect people, create synergies and promote the sharing of experiences.

                        <br/><strong>Private training/ education</strong><br/>
                        Besides the training paths set up and sponsored by Fitness IQ, you can also enroll in training courses responding to your personal needs and aspirations on a private basis. Fitness IQ allows you to take an Individual Training Leave to acquire new skills and qualifications.

                    </ol>

                    <h3 class="topmargin_0_privacy"><li>Global mobility: we don't promise the moon, we offer the world</li></h3>
                    Who hasn't dreamt of gaining some work experience abroad, getting to exposure to new environments and discover other cultures and business practices?
                    We've developed a vast network of international mobility opportunities.
                    As from entry level, some of our trainers can go either on international assignments or educational tours for a weeks or months. 

                    <h3 class="topmargin_0_privacy"><li>First week</li></h3>
                    Your buddy agent will stand by your side in the first week, to support you in developing on a personal and professional level. His/her coaching will provide you with all the general and specific technical skills you need for a smooth integration.

                    <h3 class="topmargin_0_privacy"><li>Rewards and Benefits: Fitness IQ Points</li></h3>
                    We offer our People a flexible and innovative remuneration system, with a wide range of benefits tailored to suit your needs and individual situations.
                    Points
                    Joining our Company, receiving training and accumulating points is a milestone in your professional life. This increases your earning ability with us and puts you at a vantage point as we allocate lessons in the system. It is simple, you get trained by attending trainings/ courses/ workshops organized by us and other institutions and you earn points by the number of hours you teach/ train. 

                    <ol class="li-child">
                        <br/><strong>Lifestyle: Adapt your benefits to your lifestyle and focus on what really matters.</strong><br/>

                        <li class="topmargin_0_privacy">Time management: holiday savings account, flexible schedule possibilities, extra holiday buying, home area - based working</li>
                        <li class="topmargin_0_privacy">Breaks: social engagement, career break;</li>
                        <li class="topmargin_0_privacy">Commuting: car financing, fuel card, carpooling, bus card;</li>
                        <li class="topmargin_0_privacy">Connectivity: Mobile phone/ iPhone financing, laptop financing;</li>
                        <li class="topmargin_0_privacy">Leisure: holiday discounts;</li>
                        <li class="topmargin_0_privacy">Self-development: professional qualifications, networks.</li>

                    </ol>

                    <ol class="li-child">
                        <br/><strong>Compensation: Easy access to tax compliant solutions with positive impact on your compensation!</strong><br/>

                        <li class="topmargin_0_privacy">Timely compensation: By Fitness IQ – before 5th of every month - of KSh. 2,500 for every customer taught/ trained.</li>
                        <li class="topmargin_0_privacy">You can redeem your accumulated points once every month for an item, a course/ workshop/ training, an educational tour, a holiday plan, a pension plan, an insurance scheme, our financial services product etc. You earn one point for every customer taught and five points for every course or workshop you attend.    </li>
                        <li class="topmargin_0_privacy">Bonus: you get your bonus in any of the forms mentioned above or as a cash withdrawal for your personal use. You are entitled to a bonus of KSh. 10,000 for every 20 non referral customers taught within 1 month.</li>
                        <li class="topmargin_0_privacy">Referrals: you get KSh. 1,000 for every client you refer to us. Such a customer for which you get paid cannot be taught be taught/ trained by you. You can however refer a customer to be trained by you but for which you will not be paid the KSh. 1,000.</li>

                    </ol>

                    <ol class="li-child">
                        <br/><strong>Health: We've designed these benefits because your well-being matters to us.</strong><br/>

                        <li class="topmargin_0_privacy">Outpatient care (Ambulatory care): Fitness IQ gives you the opportunity to personally subscribe to an insurance policy to extend your reimbursement coverage of medical expenses not related to hospitalization for the duration of your employment with us – 500 Points;</li>
                        <li class="topmargin_0_privacy">Inpatient care (Hospitalization): Some medical expenses are not fully covered by the social security (NSSF). Therefore, Fitness IQ gives you the opportunity to subscribe to such a plan for the duration of your employment with us – 900 Points. </li>
                    </ol>

                    <ol class="li-child">
                        <br/><strong>Risk: Be covered in all situations! Benefit from Fitness IQ coverage complete for the duration of your employment with us – 1,200 Points.</strong><br/>

                        <li class="topmargin_0_privacy">Life & disability insurance for you aims at preventing you and your dependents from having any financial hardship in case of disability or death of the insured person. Fitness IQ entirely finances any insurance covering both death and disability risks for all our agents above 2,000 points.</li>
                    </ol>

                    <ol class="li-child">
                        <br/><strong>Fitness IQ ensures to all employees the possibility to contribute to an extra legal pension plan so as to build a complement to the state social security retirement pension – for agents above 1,000 points.</strong><br/>
                        2 cases are possible:
                        <li class="topmargin_0_privacy">Your personal contribution to the pension plan</li>
                        To enable you to combine a personal pension scheme with tax savings, you have the opportunity to personally contribute.
                        <li class="topmargin_0_privacy">Fitness IQ’s contribution to the pension plan</li>
                        Within the scope of your long-term retirement pension planning, Fitness IQ finances a pension plan individually for each eligible agent so to build a complement to the state social security retirement pension.
                    </ol>

                    <h3 class="topmargin_0_privacy"><li>Other Benefits</li></h3>
                    Our other major benefits include:

                    <ol class="li-child">
                        <li class="topmargin_0_privacy">90 % Housing Finance – (4,600 Points – 10,100 Points)</li>
                        <li class="topmargin_0_privacy">90% Land Asset Financing – (4,600 Points – 10,100 Points)</li>
                        <li class="topmargin_0_privacy">90% Motor Vehicle Financing – (4,600 Points – 5,100 Points)</li>
                        <li class="topmargin_0_privacy">100 % Local Holiday Destinations Financing for 2 – including travel tickets (350 Points – 600 Points)</li>
                        <li class="topmargin_0_privacy">100 % Foreign Holiday Destinations Financing for 2 – including travel tickets (600 Points – 1,100 Points)</li>
                        <li class="topmargin_0_privacy">Undergraduate education financing – including all course requirements (2,100 Points)</li>
                        <li class="topmargin_0_privacy">Master/ Postgraduate education financing – including all course requirements (2,100 Points)</li>
                        <li class="topmargin_0_privacy">Diploma/ certificate education financing – including all course requirements (1,600 points)</li>
                        <li class="topmargin_0_privacy">Child education plan – 10 years with a maximum of KSh. 100,000/= in school fees per year – or alternative school requirements like uniform and personal effects (5,100 points)</li>
                        <li class="topmargin_0_privacy">Electronic gadgets; mobile phones, laptops, household appliances (150 Points – 10,100 Points)</li>
                        <i>(For more information on what you can purchase/ redeem with your points, kindly visit the <a href="?shop">Fitness IQ Shop</a>)</i>
                    </ol>

                    <h3 class="topmargin_0_privacy"><li>New Talent</li></h3>
                    At Fitness IQ, we aim at welcoming around 50 interns each year and we make our best to integrate, coach them and give them the best first working experience.

                    Our rankings as an employer of choice are based on the Happy Trainees. Our intern training reviews and scores are given exclusively by interns, who evaluate our company on various dimensions.

                    <h3 class="topmargin_0_privacy"><li>Our Recruitment Process</li></h3>
                    Depending on the position and service you apply for, you'll benefit from a recruitment process fit for your position!

                    <ol class="li-child">
                        <li class="topmargin_0_privacy">Trainee/ Intern - we consider those pursuing certificates, coaching levels, inset certification, diplomas, degrees and post graduate degrees.</li>
                        We generally ask our candidates for a face-to-face interview.
                        We might come ourselves to special events organized in your campus, where you can take an internship interview.
                        We've adapted our selection process to areas we feel were inadequately staffed in. 
                        We might conduct the following procedure: 
                        A test (30-60 minutes) to help us assess your basic technical knowledge (e.g. in the sport that you’re experienced in).
                        An interview with an expert in your area of expertise (1 hour) to give you the opportunity to discuss the test results and to prove your motivation and enthusiasm for our company and our business model. 
                        We will ask you to complete your online application.
                        We'll let you know the outcome in a few days after the interview.

                        <li class="topmargin_0_privacy">Graduate of a sports program – we consider those holding certificates, coaching levels, inset certification, diplomas, degrees and post graduate degrees.</li>
                        Individual Interview:
                        We'll generally ask you for a face-to-face interview.
                        If you're located too far, we can arrange a Skype interview.
                        We've adapted our selection process to different areas of expertise and sports you're interested in, but the general procedure is the following:
                        A test (30-60 minutes) to help us assess your basic technical knowledge in your area.
                        An interview with an expert of the business area (1 hour) to give you the opportunity to discuss the test results and to prove your motivation and enthusiasm for our company and our business model.
                        An interview with a Human Resources recruiter (1 hour) to review your education and work experience. This interview will help us assess your motivation for the position and our company, as well as your willingness to work with us.
                        Depending on the position you apply for, you might have an interview with one of our Directors from the relevant business area (30-60 minutes).
                        We will ask you to complete your online application.
                        We'll let you know the outcome in a few days after the interview.

                        <li class="topmargin_0_privacy">Experienced hire</li>
                        Individual Interview:
                        We'll generally contact you to ask for a face-to-face interview or an arrangement for a Skype interview.
                        We've adapted our selection process to different areas of expertise and sports you're interested in, but the general procedure is the following:

                        An interview with an expert of the business area (1 hour) to give you the opportunity to discuss the test results and to prove your motivation and enthusiasm for our company and our business model.
                        An interview with a Human Resources recruiter (1 hour) to review your education and work experience. This interview will help us assess your motivation for the position and our company, as well as your willingness to work with us.
                        Depending on the position you are interviewing for, you might have an interview with one of our Directors from the relevant business area (30-60 minutes).
                        We will ask you to complete your online application.
                        We'll let you know the outcome in a few days after the interview.
                    </ol>

                    <h3 class="topmargin_0_privacy"><li>How can I randomly apply for a job at Fitness IQ?</li></h3>
                    You can register <a href="?coach_application">here.</a>
                    Then, fill in your applicant profile and upload your CV and a covering letter.
                    You can either apply for a specific job opportunity matching your background and interests or send us a spontaneous job application at <a href="mailto:go@fitkonnect.com">go@fitkonnect.com</a> or <a href="mailto:operations@fitkonnect.com">operations@fitkonnect.com</a>  
                    <h3 class="topmargin_0_privacy"><li>How can I best prepare for my interview?</li></h3>
                    You can navigate through our website; practice your languages and your technical knowledge. During the interview, try to answer all the questions as honestly as possible, admit if you don't know something and don't pretend to be someone you're not.
                    In case you have any difficulties in your application with us, kindly contact our representatives through the above email addresses. Your journey with Fitness IQ might start here. 
                    NB: Fitness IQ is an equal opportunity employer. Also, we do not solicit money at any stage of our recruitment.


                </ol>
            </div>
        </div>
    </div>
</section>