<?php
require_once WPATH . "modules/classes/Trainers.php";
require_once WPATH . "modules/classes/Trainees.php";
require_once WPATH . "modules/classes/Training_Facilities.php";
require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/Feedback.php";
$feedback = new Feedback();
$users_management = new Users_Management();
$trainers = new Trainers();
$trainees = new Trainees();
$training_facilities = new Training_Facilities();

if (!empty($_POST)) {
//    if ($users_management->checkIfUserEmailExists($_POST['email']) == false AND $users_management->checkIfUsernameExists($_POST['username']) == false) {
    if ($users_management->checkIfUserEmailExists($_POST['email'])) {
        $filename1 = md5('' . $trainees->randomString(10) . time());
        $curriculum_vitae_name = $_FILES['curriculum_vitae']['name'];
        $tmp_name_curriculum_vitae = $_FILES['curriculum_vitae']['tmp_name'];
        $curriculum_vitae_type = $_FILES['curriculum_vitae']['type'];
        $extension_curriculum_vitae = substr($curriculum_vitae_name, strpos($curriculum_vitae_name, '.') + 1);
        $curriculum_vitae = strtoupper($filename1 . '.' . $extension_curriculum_vitae);
        $_SESSION['curriculum_vitae_filename'] = $curriculum_vitae;
        $location1 = 'images/trainers/CVs/';

        $filename2 = md5('' . $trainees->randomString(10) . time());
        $certificate_name = $_FILES['certificate']['name'];
        $tmp_name_certificate = $_FILES['certificate']['tmp_name'];
        $certificate_type = $_FILES['certificate']['type'];
        $extension_certificate = substr($certificate_name, strpos($certificate_name, '.') + 1);
        $certificate = strtoupper($filename2 . '.' . $extension_certificate);
        $_SESSION['certificate_filename'] = $certificate;
        $location2 = 'images/trainers/certificates/';

        $filename3 = md5($trainees->randomString(10) . time());
        $prof_picture_name = $_FILES['prof_picture']['name'];
        $tmp_prof_picture = $_FILES['prof_picture']['tmp_name'];
        $prof_picture_type = $_FILES['prof_picture']['type'];
        $extension_prof_picture = substr($prof_picture_name, strpos($prof_picture_name, '.') + 1);
        $prof_picture = strtoupper($filename3 . '.' . $extension_prof_picture);
        $_SESSION['prof_picture_filename'] = $prof_picture;
        $location3 = 'images/trainers/profile_pictures/';

        $url = $_SESSION['admin_url'] . '?website_requests&';

        $curriculum_vitae_file = new CURLFile($tmp_name_curriculum_vitae, $curriculum_vitae_type, $curriculum_vitae_name);
        $certificate_file = new CURLFile($tmp_name_certificate, $certificate_type, $certificate_name);
        $prof_picture_file = new CURLFile($tmp_prof_picture, $prof_picture_type, $prof_picture_name);
        $data = array("curriculum_vitae_attachment" => $curriculum_vitae_file, "certificate_attachment" => $certificate_file, "prof_picture_attachment" => $prof_picture_file, "curriculum_vitae_name" => $curriculum_vitae, "certificate_name" => $certificate, "prof_picture_name" => $prof_picture);

        $curl_session = curl_init();
        curl_setopt($curl_session, CURLOPT_URL, $url);
        curl_setopt($curl_session, CURLOPT_POST, true);
        curl_setopt($curl_session, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl_session);
        curl_close($curl_session);

        if ($response == true) {
            if (move_uploaded_file($tmp_name_curriculum_vitae, $location1 . $curriculum_vitae) AND move_uploaded_file($tmp_name_certificate, $location2 . $certificate) AND move_uploaded_file($tmp_prof_picture, $location3 . $prof_picture)) {
                $success = $trainers->execute();
                if (is_bool($success) && $success == true) {
                    $title = "Fantastic";
                    $message = "Your registration has been effected successfully.";
                    echo $feedback->successFeedback($title, $message);
                    App::redirectTo("?welcome_coach");
                } else {
                    $title = "Hey,";
                    $message = "Error! There was an error effecting your registration. Please try again.";
                    echo $feedback->errorFeedback($title, $message);
                }
            } else {
                $title = "Oops,";
                $message = "There was an error uploading your attachments. Please try registering again.";
                echo $feedback->errorFeedback($title, $message);
            }
        }
    } else {
//        if ($users_management->checkIfUserEmailExists($_POST['email']) == true AND $users_management->checkIfUsernameExists($_POST['username']) == true) {
//            $title = "Oops,";
//            $message = "Seems someone already registered with your preferred username and email address. Please register using a different set of username and email address.";
//            echo $feedback->errorFeedback($title, $message);
//        }
//        if ($users_management->checkIfUserEmailExists($_POST['email']) == true AND $users_management->checkIfUsernameExists($_POST['username']) == false) {
//            $title = "Oops,";
//            $message = "Seems someone already registered with your preferred email address. Please register using a different email address.";
//            echo $feedback->errorFeedback($title, $message);
//        }
//        if ($users_management->checkIfUserEmailExists($_POST['email']) == false AND $users_management->checkIfUsernameExists($_POST['username']) == true) {
//            $title = "Oops,";
//            $message = "Seems someone already registered with your preferred username. Please register using a different username.";
//            echo $feedback->errorFeedback($title, $message);
//        }
    }
}
?>
<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">Become a Trainer/Coach</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li>
                        <a href="./">Home</a>
                    </li>
                    <li class="active">Become a Trainer/Coach</li>
                </ol>
            </div>
        </div>
    </div>
</section>


<section class="ls section_padding_top_100 section_padding_bottom_100">
    <div class="container">
        <div class="row">
            <form class="client-register" role="form" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="action" value="add_trainer"/>
                <input type="hidden" name="user_type" value="TRAINER">
                <input type="hidden" name="physical_state" value="NO">
                <div class="col-sm-6">
                    <div class="form-group validate-required" id="firstname">
                        <label for="firstname" class="control-label">
                            <span class="grey">First Name </span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="firstname" id="firstname" placeholder="" value="">
                    </div>

                    <div class="form-group validate-required" id="gender">
                        <label for="gender" class="control-label">
                            <span class="grey">Gender </span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="gender" id="gender">
                            <?php include 'snippets/gender.php'; ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group validate-required" id="lastname">
                        <label for="lastname" class="control-label">
                            <span class="grey">Last Name </span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="lastname" id="lastname" placeholder="" value="">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group validate-required validate-email" id="email">
                        <label for="email" class="control-label">
                            <span class="grey">Email Address </span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="email" id="email" placeholder="email" value="">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group validate-required validate-phone" id="phone">
                        <label for="phone" class="control-label">
                            <span class="grey">Phone </span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="phone" id="phone" placeholder="" value="">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group validate-required" id="password">
                        <label for="training_level" class="control-label">
                            <span class="grey">What is your highest level of education?</span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="training_level" id="training_level" placeholder="" value="">
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="day" class="control-label">
                            <span class="grey" title="Used to determine age, for safety purposes">Day of Birth</span>
                            <span class="required">*</span>
                        </label>

                        <select class="form-control" name="day" id="day" >
                            <?php include 'snippets/day.php'; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="month" class="control-label">
                            <span class="grey" title="Used to determine age, for safety purposes">Month of Birth </span>
                            <span class="required">*</span>
                        </label>

                        <select class="form-control" name="month" id="month">
                            <?php include 'snippets/month.php'; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="year" class="control-label">
                            <span class="grey" title="Used to determine age, for safety purposes">Year of Birth </span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="year" id="year">
                            <?php include 'snippets/year.php'; ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="certificate" class="control-label">
                            <span class="grey" title="Are you a certificate lifeguard/gym instructor/both? Specify">Certification? </span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="certificate" id="year">
                            <?php include 'snippets/certification.php'; ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="certificate_details" class="control-label">
                            <span class="grey" title="Certification Specification">Certification Details </span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="certificate_details" id="certificate_details" placeholder="" value="">
                    </div>
                </div>


                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="fitness_certification" class="control-label">
                            <span class="grey" title="Are you a certificate lifeguard/gym instructor/both? Specify">Certified Swimming/Fitness Coach? </span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="fitness_certification" id="year">
                            <?php include 'snippets/yes_no.php'; ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="curriculum_vitae" class="control-label">
                            <span class="grey" title="Attach your certificates. Only pdf/jpeg allowed">Attach Credentials</span>
                            <span class="required">*</span>
                        </label>
                        <input type="file" class="form-control " name="curriculum_vitae" id="curriculum_vitae" placeholder="" value="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="first_aid" class="control-label">
                            <span class="grey" title="If you have done First Aid, indicate where and when">If you have done First Aid, indicate where and when. </span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="first_aid" id="first_aid" placeholder="" value="N/A">
                    </div>
                </div>
<!--                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="physical_state" class="control-label">
                            <span class="grey" title="Do you have any physical challenges?">Physical Challenges? </span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="physical_state" id="year">
                            <?php //include 'snippets/yes_no.php'; ?>
                        </select>
                    </div>
                </div>-->
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="expertise" class="control-label">
                            <span class="grey" title="How many years of experience do you have?">Years of Experience? </span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="expertise" id="year">
                            <?php include 'snippets/count.php'; ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="past_employers" class="control-label">
                            <span class="grey" title="Give a list of places you have trained and/or taught">Past Experience</span>
                            <span class="required">*</span>
                        </label>
                        <textarea class="form-control " rows="4" name="past_employers" id="past_employers" placeholder="Give a list of places you have trained and/or taught" value=""></textarea>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="present_employer" class="control-label">
                            <span class="grey" title="Are you currently on a related or non-related form of employment? Please specify. If no, indicate 'N/A'.">Are you currently on a related or non-related form of employment? Please specify. If no, indicate 'N/A'. </span>
                            <span class="required">*</span>
                        </label>
                        <textarea class="form-control " rows="4" name="present_employer" id="past_employer" placeholder="Are you currently on a related or non-related form of employment? Please specify. If no, indicate 'N/A'" value=""></textarea>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="form-group">
                        <label for="commission_labour" class="control-label">
                            <span class="grey" title="Are you willing to work part time and be paid on a 37% to 51% commission basis?">Are you willing to work part time and be paid on a commission basis? </span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="commission_labour" id="year">
                            <?php include 'snippets/yes_no.php'; ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="form-group">
                        <label for="renumeration_date_accesptance" class="control-label">
                            <span class="grey" title="Are you willing to be paid all arrears on a date not later than the 5th after the month your services were offered?">Are you willing to be paid all arrears on a date not later than the 5th after the month your services were offered? </span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="renumeration_date_accesptance" id="year">
                            <?php include 'snippets/yes_no.php'; ?>
                        </select>
                    </div>
                </div>

                <div class="col-lg-5">
                    <div class="form-group">
                        <label for="taxation_acceptance" class="control-label">
                            <span class="grey" title="Are you willing for your remuneration to be subjected to tax laws relating to your area/ jurisdiction?">Are you willing for your remuneration to be subjected to tax laws relating to your area/ jurisdiction? </span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="taxation_acceptance" id="year">
                            <?php include 'snippets/yes_no.php'; ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="form-group">
                        <label for="guarantors" class="control-label">
                            <span class="grey" title="Give names of at least two guarantors">Give names of at least two guarantors </span>
                            <span class="required">*</span>
                        </label>
                        <textarea class="form-control " rows="2" name="references" id="references" placeholder="Give names of at least two guarantors." value=""></textarea>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="references" class="control-label">
                            <span class="grey" title="Give names and contacts of at least three referees - people we can contact to inquire about your educational/professional history. ">Give names and contacts of at least three referees - people we can contact to inquire about your educational/professional history. </span>
                            <span class="required">*</span>
                        </label>
                        <textarea class="form-control " rows="4" name="references" id="references" placeholder="Give names and contacts of at least three referees - people we can contact to inquire about your educational/professional history." value=""></textarea>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="certificate" class="control-label">
                            <span class="grey" title="Attach your certificates. Only .zip allowed">Professional Certificates[in ZIP folder]</span>
                            <span class="required">*</span>
                        </label>
                        <input type="file" class="form-control " name="certificate" id="certificate" placeholder="" value="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="prof_picture" class="control-label">
                            <span class="grey" title="Attach your certificates. Only .jpeg or .png allowed">Passport Size Photo[as .jpeg or .png]</span>
                            <span class="required">*</span>
                        </label>
                        <input type="file" class="form-control " name="prof_picture" id="prof_picture" placeholder="" value="">
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        </label>
                        <input type="checkbox" name="tc" id="tc" required="yes">
                        <label for="tc">By completing this form, I confirm that I've read and understood all incidental terms and conditions of this site and service and will be bound by them. </label>
                    </div>
                </div>
                <div class="col-sm-12">
                    <button type="submit" class="theme_button wide_button color1">Complete Stage 1</button>
                    <button type="reset" class="theme_button wide_button">Clear Form</button>
                </div>
            </form>
        </div>

    </div>
</div>
</section>