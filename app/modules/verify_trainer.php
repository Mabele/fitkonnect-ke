<?php
require_once WPATH . "modules/classes/Trainers.php";
require_once WPATH . "modules/classes/Feedback.php";
$feedback = new Feedback();
$trainers = new Trainers();

if (isset($_SESSION['trainer_details'])) {
    session_destroy();
}

if (!empty($_POST)) {
    $_SESSION['trainer_details'] = $trainers->verifyTrainer($_POST['id']);
    App::redirectTo("?verification");
}
?>

<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">Trainer Verification</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li>
                        <a href="./">Home</a>
                    </li>
                    <li>
                        <a href="?careers">Careers</a>
                    </li>
                    <li class="active">Trainer Verification</li>
                </ol>
            </div>
        </div>
    </div>
</section>


<section class="ls section_padding_top_100 section_padding_bottom_100">
    <div class="container">
        <div class="row">
            <form class="client-register" role="form" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="action" value="verify_trainer"/>
                <div class="col-sm-4">
                    <div class="thumbnail">
                        <?php
                        if (!empty($trainers)) {
                            ?>
                            <img src="images/trainers/unverified.png" alt="">
                            <?php
                        }
                        if (empty($trainers)) {
                            ?>
                            <img src="images/trainers/verified.png" alt="">
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group validate-required" id="firstname">
                        <label for="firstname" class="control-label">
                            <span class="grey">Trainer ID </span>
                            <span class="required">*</span>
                        </label>
                        <p>To be able to verify the trustworthiness of our trainers, you can check their Trainer ID's validity.</p>

                        <input type="text" class="form-control " name="id" id="id" placeholder="FK-000000" value="" required="yes">
                    </div>
                </div>
                <div class="col-sm-6">
                    <b>
                        <?php
                        if (!empty($trainers)) {
                            echo "<p>Not a valid ID.</p>";
                        }
                        ?>  
                    </b>
                </div>
                <div class="col-sm-6">
                    <button type="submit" class="theme_button wide_button color1">Verify Now</button>
                </div>
            </form>
        </div>
    </div>
</div>
</section>