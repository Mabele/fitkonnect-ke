<?php
require_once WPATH . "modules/classes/Trainers.php";
$trainers = new Trainers();
?>

<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">Tests</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li>
                        <a href="./">Home</a>
                    </li>
                    <li>
                        <a href="?test">Tests</a>
                    </li>
                </ol>
            </div>
        </div>
    </div>
</section>
<form role="form" method="POST">
     <input type="hidden" name="action" value="add_test"/>
     <input type="text" class="form-control " name="name" id="name" required="yes">
     <button type="submit" class="theme_button wide_button color1">Test Now</button>
</form>
</section>
<?php //var_dump($_POST['name']);