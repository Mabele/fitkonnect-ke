<section class="ds section_padding_90 page_testimonials">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="flexslider">
                    <div class="container">
                        <div class="row bottommargin_15">
                            <div class="col-sm-12 text-center">
                                <div class="cornered-heading center-heading">
                                    <span class="text-uppercase">Your Activity</span>
                                    <h2 class="text-uppercase">Choose Activity/Sport</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <ul class="slides">
                                    <li>
                                        <div class="row">
                                            <div class="col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-3">
                                                <blockquote class="blockquote-vertical with_bottom_border ls topmargin_50">
                                                    <img src="./images/faces/1.jpg" width="50" alt=""> 
                                                    <hr class="divider_3_60 main_bg_color">
                                                    <div class="blockquote-meta">
                                                        <div class="blockquote-meta">
                                                        <h5>Activity</h5>
                                                    </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                            <div class="col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-3">
                                                <blockquote class="blockquote-vertical with_bottom_border ls topmargin_50">
                                                    <img src="./images/faces/1.jpg" width="50" alt=""> 
                                                    <hr class="divider_3_60 main_bg_color">
                                                    <div class="blockquote-meta">
                                                        <div class="blockquote-meta">
                                                        <h5>Activity</h5>
                                                    </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                            <div class="col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-3">
                                                <blockquote class="blockquote-vertical with_bottom_border ls topmargin_50">
                                                    <img src="./images/faces/1.jpg" width="50" alt=""> 
                                                    <hr class="divider_3_60 main_bg_color">
                                                    <div class="blockquote-meta">
                                                        <div class="blockquote-meta">
                                                        <h5>Activity</h5>
                                                    </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                            <div class="col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-3">
                                                <blockquote class="blockquote-vertical with_bottom_border ls topmargin_50">
                                                    <img src="./images/faces/1.jpg" width="50" alt=""> 
                                                    <hr class="divider_3_60 main_bg_color">
                                                    <div class="blockquote-meta">
                                                        <div class="blockquote-meta">
                                                        <h5>Activity</h5>
                                                    </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                            <div class="col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-3">
                                                <blockquote class="blockquote-vertical with_bottom_border ls topmargin_50">
                                                    <img src="./images/faces/1.jpg" width="50" alt=""> 
                                                    <hr class="divider_3_60 main_bg_color">
                                                    <div class="blockquote-meta">
                                                        <div class="blockquote-meta">
                                                        <h5>Activity</h5>
                                                    </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                            <div class="col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-3">
                                                <blockquote class="blockquote-vertical with_bottom_border ls topmargin_50">
                                                    <img src="./images/faces/1.jpg" width="50" alt=""> 
                                                    <hr class="divider_3_60 main_bg_color">
                                                    <div class="blockquote-meta">
                                                        <div class="blockquote-meta">
                                                        <h5>Activity</h5>
                                                    </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                            <div class="col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-3">
                                                <blockquote class="blockquote-vertical with_bottom_border ls topmargin_50">
                                                    <img src="./images/faces/1.jpg" width="50" alt=""> 
                                                    <hr class="divider_3_60 main_bg_color">
                                                    <div class="blockquote-meta">
                                                        <div class="blockquote-meta">
                                                        <h5>Activity</h5>
                                                    </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                            <div class="col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-3">
                                                <blockquote class="blockquote-vertical with_bottom_border ls topmargin_50">
                                                    <img src="./images/faces/1.jpg" width="50" alt=""> 
                                                    <hr class="divider_3_60 main_bg_color">
                                                    <div class="blockquote-meta">
                                                        <div class="blockquote-meta">
                                                        <h5>Activity</h5>
                                                    </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                        </div>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- eof flexslider -->
            </div>
        </div>
    </div>
</section>