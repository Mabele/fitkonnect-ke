<section class="ls section_padding_bottom_15">
    <div class="container">
        <div class="row masonry-layout isotope_container">
            <div class="col-md-4 col-sm-6 isotope-item">
                <div class="vertical-item content-padding topmargin_10">
                    <div class="item-media">
                        <img src="./images/gallery/sma.jpg" alt="">
                        <div class="media-links p-link">
                            <div class="links-wrap">
                                <i class="flaticon-arrows-2"></i>
                            </div>
                            <a href="#" class="abs-link"></a>
                        </div>
                    </div>
                    <div class="item-content with_shadow with_bottom_border">
                        <h4 class="text-uppercase">
                            <a href="#">SMA Germany</a>
                        </h4>
                        <p>
                        SMA is a world-renowned inverter manufacturer and Dreampower
                        Ricciardi Ltd is proud to be one of the largest and oldest authorized 
                        SMA Resellers and Service Providers in East Africa. We been a 
                        Prime SMA partner and are currently the largest and fastest 
                        Growing SMA partner in the region, handling most of the region’s
                        Warranty services. We are currently the SMA Authorized partner 
</p>
                        <hr class="light-divider full-content-divider bottommargin_10">
                        <div class="media small-icon-media topmargin_5">
                            <div class="media-left">
                                <i class="fa fa-map-marker grey fontsize_18"></i>
                            </div>
                            <div class="media-body">
                                Sonnenallee 1, 34266 Niestetal, Germany
                            </div>
                        </div>
                        <div class="media small-icon-media topmargin_5">
                            <div class="media-left">
                                <i class="fa fa-phone grey fontsize_18"></i>
                            </div>
                            <div class="media-body">
                                +49 561 9522 0
                            </div>
                        </div>
                        <div class="media small-icon-media topmargin_5">
                            <div class="media-left">
                                <i class="fa fa-envelope grey fontsize_18"></i>
                            </div>
                            <div class="media-body greylinks">
                                <a href="mailto:info@SMA.de">info@SMA.de</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 isotope-item">
                <div class="vertical-item content-padding topmargin_10">
                    <div class="item-media">
                        <img src="./images/gallery/isofoton.jpg" alt="">
                        <div class="media-links p-link">
                            <div class="links-wrap">
                                <i class="flaticon-arrows-2"></i>
                            </div>
                            <a href="#" class="abs-link"></a>
                        </div>
                    </div>
                    <div class="item-content with_shadow with_bottom_border">
                        <h4 class="text-uppercase">
                            <a href="#">ISOFOTON Spain</a>
                        </h4>
                        <p>
                            ISOFOTON is a world renowned Solar Panel/ PV and Europe 
                            largest PV manufacturer and Dreampower Ricciardi Ltd 
                            is proud to be one of the largest and authorized Isofoton 
                            Resellers and Service Providers in East Africa. We have been 
                            a prime Isofoton partner and are currently the largest and 
                            fastest growing Isofoton partner in the region, handling 
                            most of the region’s warranty services. We are currently 
                            the Isofoton Authorized partner for the following service 
                            accreditations
                        </p>
                        <hr class="light-divider full-content-divider bottommargin_10">
                        <div class="media small-icon-media topmargin_5">
                            <div class="media-left">
                                <i class="fa fa-map-marker grey fontsize_18"></i>
                            </div>
                            <div class="media-body">
                                Madrid, Spain
                            </div>
                        </div>
                        <div class="media small-icon-media topmargin_5">
                            <div class="media-left">
                                <i class="fa fa-phone grey fontsize_18"></i>
                            </div>
                            <div class="media-body">
                                0800 123 2365
                            </div>
                        </div>
                        <div class="media small-icon-media topmargin_5">
                            <div class="media-left">
                                <i class="fa fa-envelope grey fontsize_18"></i>
                            </div>
                            <div class="media-body greylinks">
                                <a href="#">info@isofoton.com</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4 col-sm-6 isotope-item">
                <div class="vertical-item content-padding topmargin_10">
                    <div class="item-media">
                        <img src="./images/gallery/cappello.jpg" alt="">
                        <div class="media-links p-link">
                            <div class="links-wrap">
                                <i class="flaticon-arrows-2"></i>
                            </div>
                            <a href="#" class="abs-link"></a>
                        </div>
                    </div>
                    <div class="item-content with_shadow with_bottom_border">
                        <h4 class="text-uppercase">
                            <a href="#">Cappello Group</a>
                        </h4>
                        <p>
                            It is the year that marks the transformation of the 
                            brand in a Corporation, with the creation of Cappello 
                            Group S.p.A. In here will merge the companies Cappello 
                            Alluminio, Cappello 2 and Zinco Iblea. A path which is 
                            completed with the final transition towards internationalization, 
                            already started in 2013, with the intent to maintain the 
                            same production standards expressed in the national territory.
                        </p>
                        <hr class="light-divider full-content-divider bottommargin_10">
                        <div class="media small-icon-media topmargin_5">
                            <div class="media-left">
                                <i class="fa fa-map-marker grey fontsize_18"></i>
                            </div>
                            <div class="media-body">
                                97100 Ragusa - Italy
                            </div>
                        </div>
                        <div class="media small-icon-media topmargin_5">
                            <div class="media-left">
                                <i class="fa fa-phone grey fontsize_18"></i>
                            </div>
                            <div class="media-body">
                                +39 0932 660211
                            </div>
                        </div>
                        <div class="media small-icon-media topmargin_5">
                            <div class="media-left">
                                <i class="fa fa-envelope grey fontsize_18"></i>
                            </div>
                            <div class="media-body greylinks">
                                <a href="mailto:info@cappellogroup.it">info@cappellogroup.it</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>