<?php

class Training_Facilities extends Database {

    public function fetchTrainingFacilityDetails($code) {
        $sql = "SELECT * FROM training_facilities WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function getTrainingFacilities() {
        $stmt = $this->prepareQuery("SELECT id, name, status FROM training_facilities WHERE status=1001 ORDER BY id ASC");
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $currentGroup = null;
        $html = "";
        foreach ($info as $row) {
            if (is_null($currentGroup)) {
                $currentGroup = $row['name'];
                if (!empty($_POST['training_facility']) && $_POST['training_facility'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['name']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['name']}</option>";
                }
            } else {
                if (!empty($_POST['training_facility']) && $_POST['training_facility'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['name']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['name']}</option>";
                }
            }
        }
        if ($html == "") {
            $html = "<option value=\"\">No training facility entered into the database!</option>";
        }
        echo $html;
        return $currentGroup;
    }

}
