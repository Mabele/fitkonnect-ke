<?php

use PHPMailer\PHPMailer\PHPMailer;
require_once 'modules/mailing/vendor/autoload.php'; //Load Composer's autoloader

require_once WPATH . "modules/classes/Staff.php";
require_once WPATH . "modules/classes/System_Administration.php";
require_once WPATH . "modules/classes/Trainees.php";
require_once WPATH . "modules/classes/Trainers.php";
require_once WPATH . "modules/classes/Training_Facilities.php";

class Users_Management extends Database {

    public function execute() {
        if ($_POST['action'] == "secure_login") {
            return $this->secureLogin();
        } else if ($_POST['action'] == "forgot_password") {
            return $this->forgotPassword();
        }
    }

    private function secureLogin() {
        $sql = "SELECT * FROM system_users WHERE (username=:username OR email=:username) AND password=:password";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("username", strtoupper($_POST['username']));
        $stmt->bindValue("password", sha1($_POST['password']));
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($data) == 0) {
            $_SESSION['login_error'] = true;
            return false;
        } else {
            $data = $data[0];
            $cookie_name = "username";
            $cookie_value = $_POST['username'];
            setcookie('username', $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
            $_SESSION['username'] = $data['username'];
            $_SESSION['user_id'] = $data['id'];
            $_SESSION['password_new'] = $data['password_new'];
            $_SESSION['logged_in_user_status'] = $data['status'];
            $_SESSION['logged_in_user_id'] = $data['reference_id'];
            $_SESSION['logged_in_user_type'] = $data['reference_type'];
            $_SESSION['iq_points'] = $data['iq_points'];
            $_SESSION['logged_in_user_type_details'] = $this->fetchUserTypeDetails($_SESSION['logged_in_user_type']);

            if ($_SESSION['logged_in_user_type_details']['name'] == "STAFF") {
                $staff = new Staff();
                $_SESSION['staff'] = $_SESSION['logged_in_user_id'];
                $_SESSION['logged_in_user_details'] = $staff->fetchStaffDetails($_SESSION['logged_in_user_id']);
                $_SESSION['profile_link'] = "?view_staff_individual&code=" . $_SESSION['logged_in_user_id'];
                $_SESSION['contacts'] = $this->fetchIndividualContactDetails($_SESSION['user_id']);
            } else if ($_SESSION['logged_in_user_type_details']['name'] == "TRAINEE") {
                $trainees = new Trainees();
                $_SESSION['trainee'] = $_SESSION['logged_in_user_id'];
                $_SESSION['logged_in_user_details'] = $trainees->fetchTraineeDetails($_SESSION['logged_in_user_id']);
                $_SESSION['profile_link'] = "?view_trainees_individual&code=" . $_SESSION['logged_in_user_id'];
                $_SESSION['contacts'] = $this->fetchIndividualContactDetails($_SESSION['user_id']);
            } else if ($_SESSION['logged_in_user_type_details']['name'] == "TRAINER") {
                $trainers = new Trainers();
                $_SESSION['trainer'] = $_SESSION['logged_in_user_id'];
                $_SESSION['logged_in_user_details'] = $trainers->fetchTrainerDetails($_SESSION['logged_in_user_id']);
                $_SESSION['profile_link'] = "?view_trainers_individual&code=" . $_SESSION['logged_in_user_id'];
                $_SESSION['contacts'] = $this->fetchIndividualContactDetails($_SESSION['user_id']);
            } else if ($_SESSION['logged_in_user_type_details']['name'] == "TRAINING FACILITY ADMINISTRATOR") {
                $training_facilities = new Training_Facilities();
                $_SESSION['training_facility_administrator'] = $_SESSION['logged_in_user_id'];
                $_SESSION['logged_in_user_details'] = $training_facilities->fetchTrainingFacilityAdministratorDetails($_SESSION['logged_in_user_id']);
                $_SESSION['training_facility_details'] = $training_facilities->fetchTrainingFacilityDetails($_SESSION['logged_in_user_details']['facility']);
                $_SESSION['profile_link'] = "?view_training_facility_administrators_individual&code=" . $_SESSION['logged_in_user_id'];
            } else if ($_SESSION['logged_in_user_type_details']['name'] == "SYSTEM ADMINISTRATOR") {
                $system_administration = new System_Administration();
                $_SESSION['system_administrator'] = $_SESSION['logged_in_user_id'];
                $_SESSION['logged_in_user_details'] = $system_administration->fetchSystemAdministratorDetails($_SESSION['logged_in_user_id']);
                $_SESSION['profile_link'] = "?view_system_administrators_individual&code=" . $_SESSION['logged_in_user_id'];
            }
            return true;
        }
    }

    public function checkIfUsernameExists($username) {
        $sql = "SELECT * FROM system_users WHERE username=:username";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("username", strtoupper($username));
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($data) == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function checkIfUserEmailExists($email) {
        $sql = "SELECT * FROM system_users WHERE email=:email";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("email", strtoupper($email));
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($data) == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function sendEmail($recipient_email, $recipient_name, $email_subject, $email_message) {
        $mail = new PHPMailer(true);
        $mail->SMTPDebug = 3;                                                   // Enable verbose debug output
        $mail->isSMTP();                                                        // Set mailer to use SMTP
        $mail->Host = $_SESSION["mail_host"];                                   // Specify main and backup SMTP servers
        $mail->SMTPAuth = $_SESSION["SMTPAuth"];                                // Enable SMTP authentication
        $mail->Username = $_SESSION["MUsername"];                               // SMTP username
        $mail->Password = $_SESSION["MPassword"];                               // SMTP password
        $mail->SMTPSecure = $_SESSION["SMTPSecure"];                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $_SESSION["Port"];                                        // TCP port to connect to
        $mail->setFrom($_SESSION["MUsername"], $_SESSION["MUsernameFrom"]);
        $mail->addAddress($recipient_email, $recipient_name);                  // Add a recipient
        $mail->isHTML(true);                                                    // Set email format to HTML
        $mail->Subject = $email_subject;
        $mail->Body = $email_message;
           $mail->AltBody = 'Thank you for registering.';
//        $mail->send();
        if ($mail->send()) {
            return true;
        } else {
            return false;
        }
    }

//    public function confirmUsername() {
//        $sql = "SELECT * FROM user_logs WHERE username=:username";
//        $stmt = $this->prepareQuery($sql);
//        $stmt->bindValue("username", strtoupper($_POST['username']));
//        $stmt->execute();
//        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
//        if ($data) {
//            echo '<p class="warning">The email exists. Use a different email.</p>';
//        } else {
//            $_SESSION['username'] = $_POST['username'];
//            return true;
//        }
//    }

    private function forgotPassword() {
//        $url = "http://bookhivekenya.com?login";
//        $phone_number = "+254 726 771144";
//        $email_address = "hello@bookhivekenya.com";
//        $sql = "SELECT * FROM user_logs WHERE username=:email";
//        $stmt = $this->prepareQuery($sql);
//        $stmt->bindValue("email", strtoupper($_POST['email']));
//        $stmt->execute();
//        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
//        if (count($data) == 1) {
//            $code = $this->randomString(20);
//            $password = $this->randomString(10);
//            $reference_id = $data[0]['reference_id'];
////            $username = $this->fetchLoggedInUserDetails($reference_id);
//
//            $sender = "hello@bookhivekenya.com";
//            $headers = "From: Bookhive Kenya <$sender>\r\n";
//            $headers .= "MIME-Version: 1.0\r\n";
//            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
//            $subject = "Bookhive Password Update";
//            $message = "<html><body>"
//                    . "<p><b>Hello " . $_POST['firstname'] . ",</b><br/>"
//                    . "Your request for the reset of your account login credentials has been effected successfully. <br/>"
//                    . "<ul>"
//                    . "<li><b>Username: </b>" . $_POST['email'] . "</li>"
//                    . "<li><b>Password: </b>" . $password . "</li>"
//                    . "</ul>"
//                    . "Click on this link: <a href=' " . $url . "'>Bookhive Login</a> to proceed with the login. <br/>"
//                    . "For any enquiries, kindly contact us on:   <br/>"
//                    . "<ul>"
//                    . "<li><b>Telephone Number(s): </b>" . $phone_number . "</li>"
//                    . "<li><b>Email Address: </b>" . $email_address . "</li>"
//                    . "</ul>"
//                    . "Visit <a href='http://bookhivekenya.com'>bookhivekenya.com</a> for more information.<br/>"
//                    . "</body></html>";
//
//            $sql2 = "UPDATE user_logs SET password=:password, password_new=:new_password_state, password_code=:password_code WHERE reference_id=:reference_id";
//            $stmt2 = $this->prepareQuery($sql2);
//            $stmt2->bindValue("password", sha1($password));
//            $stmt2->bindValue("password_code", $code);
//            $stmt2->bindValue("new_password_state", 0);
//            $stmt2->bindValue("reference_id", $reference_id);
//            $stmt2->execute();
//            mail($_POST['email'], $subject, $message, $headers);
//            return true;
//        } else {
//            return false;
//        }
    }

//
//    public function getAllUserTypes() {
//        $sql = "SELECT * FROM user_types ORDER BY id ASC";
//        $stmt = $this->prepareQuery($sql);
//        $stmt->execute();
//        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
//
//        if (count($info) == 0) {
//            $_SESSION['no_records'] = true;
//        } else {
//            $_SESSION['yes_records'] = true;
//            $values2 = array();
//            foreach ($info as $data) {
//                $values = array("id" => $data['id'], "name" => $data['name'], "status" => $data['status'], "createdat" => $data['createdat'], "createdby" => $data['createdby'], "lastmodifiedat" => $data['lastmodifiedat'], "lastmodifiedby" => $data['lastmodifiedby']);
//                array_push($values2, $values);
//            }
//            return json_encode($values2);
//        }
//    }
//    public function getAllContacts() {
//        $sql = "SELECT * FROM contacts ORDER BY id ASC";
//        $stmt = $this->prepareQuery($sql);
//        $stmt->execute();
//        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
//
//        if (count($info) == 0) {
//            $_SESSION['no_records'] = true;
//        } else {
//            $_SESSION['yes_records'] = true;
//            $values2 = array();
//            foreach ($info as $data) {
//                $values = array("id" => $data['id'], "user_id" => $data['user_id'], "phone_number1" => $data['phone_number1'], "phone_number2" => $data['phone_number2'], "email" => $data['email'], "postal_number" => $data['postal_number'], "postal_code" => $data['postal_code'], "postal_town" => $data['postal_town'], "county" => $data['county'], "ward" => $data['ward'], "prof_picture" => $data['prof_picture'], "website" => $data['website'], "status" => $data['status'], "createdat" => $data['createdat'], "createdby" => $data['createdby'], "lastmodifiedat" => $data['lastmodifiedat'], "lastmodifiedby" => $data['lastmodifiedby']);
//                array_push($values2, $values);
//            }
//            return json_encode($values2);
//        }
//    }

    public function fetchIndividualContactDetails($user_id) {
        $sql = "SELECT * FROM contacts WHERE user_id=:user_id";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("user_id", $user_id);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function fetchUserTypeDetails($code) {
        $sql = "SELECT * FROM user_types WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function fetchUserDetails($code) {
        $sql = "SELECT * FROM system_users WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function getUserRefTypeId($user_type) {
        $sql = "SELECT id, status FROM user_types WHERE name=:user_type";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("user_type", strtoupper($user_type));
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data[0]['id'];
    }

    public function getNextSystemUserId() {
        $system_user_id = $this->executeQuery("SELECT max(id) as system_user_id_max FROM system_users");
        $system_user_id = $system_user_id[0]['system_user_id_max'] + 1;
        return $system_user_id;
    }

}
