<?php
require_once WPATH . "modules/classes/Trainees.php";
require_once WPATH . "modules/classes/Users_Management.php";

class Trainers extends Database {

    public function execute() {
        if ($_POST['action'] == "add_trainer") {
            return $this->addTrainer();
        } else if ($_POST['action'] == "edit_trainer") {
            return $this->editTrainer();
        } else if ($_POST['action'] == "verify_trainer") {
            return $this->verifyTrainer();
        } else if ($_POST['action'] == "add_test") {
            return $this->addTest();
        }
    }

    public function verifyTrainer($id) {
        $sql = "SELECT trainers.id, trainers.firstname, trainers.lastname, trainers.fk_profile, trainers.training_level, contacts.prof_picture FROM trainers LEFT JOIN contacts ON trainers.id = contacts.user_id WHERE trainers.id=:id AND contacts.prof_picture <> 'NULL'";
        //$sql = "SELECT * FROM trainers WHERE id=:id UNION SELECT * FROM contacts WHERE user_id=:id";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function addTest() {
        $name = $_POST['name'];
        $sql = "INSERT INTO test_table (id, name) VALUES (:id ,:name)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("id", 1);
        $stmt->bindParam("name", $name);
        $stmt->execute();           
        }

    
    public function addTrainer() {
        $trainer_id = $this->getNextTrainerId();
        $user_id = $this->getNextSystemUserId();

        $sql = "INSERT INTO trainers (firstname, lastname, gender, idnumber, birth_date, training_level, curriculum_vitae, certificate, guarantor1, guarantor2, facility1, facility2, createdby, lastmodifiedby)"
                . " VALUES (:firstname, :lastname, :gender, :idnumber, :birth_date, :training_level, :curriculum_vitae, :certificate, :guarantor1, :guarantor2, :facility1, :facility2, :createdby, :lastmodifiedby)";

        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("firstname", strtoupper($_SESSION['firstname']));
        $stmt->bindValue("lastname", strtoupper($_SESSION['lastname']));
        $stmt->bindValue("gender", strtoupper($_SESSION['gender']));
        $stmt->bindValue("idnumber", strtoupper($_SESSION['idnumber']));
        $stmt->bindValue("birth_date", $_POST['year'] . "-" . $_POST['month'] . "-" . $_POST['day']);
        $stmt->bindValue("training_level", strtoupper($_SESSION['training_level']));
        $stmt->bindValue("curriculum_vitae", $_SESSION['curriculum_vitae_filename']);
        $stmt->bindValue("certificate", $_SESSION['certificate_filename']);
        $stmt->bindValue("guarantor1", $_SESSION['guarantor1']); 
        $stmt->bindValue("guarantor2", $_SESSION['guarantor2']); 
        $stmt->bindValue("facility1", $_SESSION['facility1']); 
        $stmt->bindValue("facility2", $_SESSION['facility2']); 
        $stmt->bindValue("createdby", $_SESSION['user_id']); 
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']); 
        $stmt->execute();

        $sql_contacts = "INSERT INTO contacts (user_id, phone_number1, phone_number2, email, postal_number, postal_code, postal_town, county, ward, prof_picture, website, createdby, lastmodifiedby)"
                . " VALUES (:user_id, :phone_number1, :phone_number2, :email, :postal_number, :postal_code, :postal_town, :county, :ward, :prof_picture, :website, :createdby, :lastmodifiedby)";

        $stmt_contacts = $this->prepareQuery($sql_contacts);
        $stmt_contacts->bindValue("user_id", $user_id);
        $stmt_contacts->bindValue("phone_number1", $_SESSION['phone_number1']);
        $stmt_contacts->bindValue("phone_number2", $_SESSION['phone_number2']);
        $stmt_contacts->bindValue("email", strtoupper($_SESSION['email']));
        $stmt_contacts->bindValue("postal_number", $_SESSION['postal_number']);
        $stmt_contacts->bindValue("postal_code", $_SESSION['postal_code']);
        $stmt_contacts->bindValue("postal_town", strtoupper($_SESSION['postal_town']));
        $stmt_contacts->bindValue("county", $_SESSION['county']);
        $stmt_contacts->bindValue("ward", $_SESSION['ward']);
        $stmt_contacts->bindValue("prof_picture", $_SESSION['prof_picture_filename']);
        $stmt_contacts->bindValue("website", strtoupper($_SESSION['website']));
        $stmt_contacts->bindValue("createdby", $_SESSION['user_id']); 
        $stmt_contacts->bindValue("lastmodifiedby", $_SESSION['user_id']); 
        $stmt_contacts->execute();

        $password = $this->randomString(10);
        
        $sql_system_users = "INSERT INTO system_users (reference_type, reference_id, email, username, password)"
                . " VALUES (:reference_type, :reference_id, :email, :username, :password)";

        $stmt_system_users = $this->prepareQuery($sql_system_users);
        $stmt_system_users->bindValue("reference_type", $_SESSION['ref_type']);
        $stmt_system_users->bindValue("reference_id", $trainer_id);
        $stmt_system_users->bindValue("email", strtoupper($_SESSION['email']));
        $stmt_system_users->bindValue("username", strtoupper($_SESSION['firstname']) . " " . strtoupper($_SESSION['lastname']));
        $stmt_system_users->bindValue("password", sha1($password));
        if ($stmt_system_users->execute()) {
            return true;
        } else {
            return false;
        }

//        $this->addUserToRole($role, $id, $createdby);
//        $this->addPrivilegesToUser($ref_type, $id, $role, $createdby);
//
//        $code = $this->randomString(20);
//
//        $mail = new PHPMailer;
//        // Set mailer to use SMTP
//        $mail->Host = $_SESSION["mail_host"];                                   // Specify main and backup SMTP servers
//        $mail->SMTPAuth = $_SESSION["SMTPAuth"];                                // Enable SMTP authentication
//        $mail->Username = $_SESSION["MUsername"];                               // SMTP username
//        $mail->Password = $_SESSION["MPassword"];                               // SMTP password
//        $mail->SMTPSecure = $_SESSION["SMTPSecure"];                            // Enable TLS encryption, `ssl` also accepted
//        $mail->Port = $_SESSION["Port"];                                        // TCP port to connect to
//        $mail->setFrom($_SESSION["MUsername"], $_SESSION["MUsernameFrom"]);
//
//        $mail->addAddress($email, $firstname);                  // Add a recipient
//        $mail->isHTML(true);                                    // Set email format to HTML
//        $mail->Subject = "New User Registration";
//        $mail->Body = "<html><body>"
//                . "<p><b>Hello " . $firstname . ",</b><br/>"
//                . "Your account has been successfully created. Your login credentials are as below. <br/>"
//                . "<ul>"
//                . "<li><b>Username: </b>" . $email . "</li>"
//                . "<li><b>Password: </b>" . $password . "</li>"
//                . "</ul>"
//                . "Click on this link: <a href=' " . $_SESSION['admin_url'] . "'>User Login</a> to proceed with the login. <br/>"
//                . "For any enquiries, kindly contact us on:   <br/>"
//                . "<ul>"
//                . "<li><b>Telephone Number(s): </b>" . $_SESSION['institution_phone'] . "</li>"
//                . "<li><b>Email Address: </b>" . $_SESSION["MUsername"] . "</li>"
//                . "</ul>"
//                . "Visit <a href='{$_SESSION['website_url']}'>" . $_SESSION['displayed_website_link'] . "</a> for more information.<br/>"
//                . "</body></html>";
//        $mail->AltBody = $_SESSION["AltBody"];
//        if ($mail->send()) {
//            return true;
//        } else {
//            return false;
//        }
    }

    private function editTrainer() {
        $sql = "UPDATE trainers SET firstname=:firstname, lastname=:lastname, gender=:gender, idnumber=:idnumber, birth_date=:birth_date, training_level=:training_level, curriculum_vitae=:curriculum_vitae, certificate=:certificate, guarantor1=:guarantor1, guarantor2=:guarantor2, facility1=:facility1, facility2=:facility2, "
                . "lastmodifiedby=:lastmodifiedby, lastmodifiedat=:lastmodifiedat WHERE id=:id";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("id", $_SESSION['trainer']);
        $stmt->bindValue("firstname", strtoupper($_POST['firstname']));
        $stmt->bindValue("lastname", strtoupper($_POST['lastname']));
        $stmt->bindValue("gender", strtoupper($_POST['gender']));
        $stmt->bindValue("idnumber", strtoupper($_POST['idnumber']));
        $stmt->bindValue("birth_date", $_POST['year'] . "-" . $_POST['month'] . "-" . $_POST['day']);
        $stmt->bindValue("training_level", strtoupper($_POST['training_level']));
        $stmt->bindValue("curriculum_vitae", $_SESSION['curriculum_vitae_filename']);
        $stmt->bindValue("certificate", $_SESSION['certificate_filename']);
        $stmt->bindValue("guarantor1", $_POST['guarantor1']); 
        $stmt->bindValue("guarantor2", $_POST['guarantor2']); 
        $stmt->bindValue("facility1", $_POST['facility1']); 
        $stmt->bindValue("facility2", $_POST['facility2']);
        $stmt->bindValue("lastmodifiedby", $_SESSION['user_id']); 
        $stmt->bindValue("lastmodifiedat", date("Y-m-d H:i:s"));
        if ($stmt->execute()) {
            return true;
        } else
            return false;
    }
    
    public function getTrainers() {
        $stmt = $this->prepareQuery("SELECT id, firstname, lastname status FROM trainers ORDER BY id ASC");
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $currentGroup = null;
        $html = "";
        foreach ($info as $row) {
            if (is_null($currentGroup)) {
                $currentGroup = $row['firstname'];
                if (!empty($_POST['trainer']) && $_POST['trainer'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['firstname']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['firstname']}</option>";
                }
            } else {
                if (!empty($_POST['trainer']) && $_POST['trainer'] == $row['id']) {
                    $html .= "<option value=\"{$row['id']}\" selected='selected'>{$row['firstname']}</option>";
                } else {
                    $html .= "<option value=\"{$row['id']}\">{$row['firstname']}</option>";
                }
            }
        }
        if ($html == "") {
            $html = "<option value=\"\">No trainer entered into the database!</option>";
        }
        echo $html;
        return $currentGroup;
    }

    public function getAllTrainers() {
        $sql = "SELECT * FROM trainers ORDER BY id ASC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("id" => $data['id'], "firstname" => $data['firstname'], "lastname" => $data['lastname'], "gender" => $data['gender'], "birth_date" => $data['birth_date'], "idnumber" => $data['idnumber'], "training_level" => $data['training_level'], "curriculum_vitae" => $data['curriculum_vitae'], "certificate" => $data['certificate'], "guarantor1" => $data['guarantor1'], "guarantor2" => $data['guarantor2'], "facility1" => $data['facility1'], "facility2" => $data['facility2'], "trainer_rating" => $data['trainer_rating'], "status" => $data['status'], "createdat" => $data['createdat'], "createdby" => $data['createdby'], "lastmodifiedat" => $data['lastmodifiedat'], "lastmodifiedby" => $data['lastmodifiedby']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function fetchTrainerDetails($code) {
        $sql = "SELECT * FROM trainers WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }
        
    public function getNextTrainerId() {
        $trainer_id = $this->executeQuery("SELECT max(id) as trainer_id_max FROM trainers");
        $trainer_id = $trainer_id[0]['trainer_id_max'] + 1;
        return $trainer_id;
    }

    private function getNextSystemUserId() {
        $users_management = new Users_Management();
        return $users_management->getNextSystemUserId();
    }

    private function randomString($number) {
        $trainees = new Trainees();
        return $trainees->randomString($number);
    }

}
