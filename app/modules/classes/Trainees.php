<?php
require_once WPATH . "modules/classes/Users_Management.php";

class Trainees extends Database {

    public function execute() {
        if ($_POST['action'] == "add_trainee") {
            return $this->addTrainee();
        }
    }

    private function addTrainee() {
        $trainee_id = $this->getNextTraineeId();
        $user_id = $this->getNextSystemUserId();

        $sql = "INSERT INTO trainees (firstname, lastname, gender, birth_date, createdby, lastmodifiedby)"
                . " VALUES (:firstname, :lastname, :gender, :birth_date, :createdby, :lastmodifiedby)";

        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("firstname", strtoupper($_POST['firstname']));
        $stmt->bindValue("lastname", strtoupper($_POST['lastname']));
        $stmt->bindValue("gender", strtoupper($_POST['gender']));
        $stmt->bindValue("birth_date", $_POST['year'] . "-" . $_POST['month'] . "-" . $_POST['day']);
        $stmt->bindValue("createdby", $user_id);
        $stmt->bindValue("lastmodifiedby", $user_id);
        $stmt->execute();


        $sql_contacts = "INSERT INTO contacts (user_id, phone_number1, phone_number2, email, postal_number, postal_code, postal_town, county, ward, prof_picture, website, createdby, lastmodifiedby)"
                . " VALUES (:user_id, :phone_number1, :phone_number2, :email, :postal_number, :postal_code, :postal_town, :county, :ward, :prof_picture, :website, :createdby, :lastmodifiedby)";

        $stmt_contacts = $this->prepareQuery($sql_contacts);
        $stmt_contacts->bindValue("user_id", $user_id);
        $stmt_contacts->bindValue("phone_number1", $_POST['phone_number']);
        $stmt_contacts->bindValue("phone_number2", NULL);
        $stmt_contacts->bindValue("email", strtoupper($_POST['email']));
        $stmt_contacts->bindValue("postal_number", NULL);
        $stmt_contacts->bindValue("postal_code", NULL);
        $stmt_contacts->bindValue("postal_town", NULL);
        $stmt_contacts->bindValue("county", NULL);
        $stmt_contacts->bindValue("ward", NULL);
        $stmt_contacts->bindValue("prof_picture", NULL);
        $stmt_contacts->bindValue("website", NULL);
        $stmt_contacts->bindValue("createdby", $user_id);
        $stmt_contacts->bindValue("lastmodifiedby", $user_id);
        $stmt_contacts->execute();

        $sql_system_users = "INSERT INTO system_users (reference_type, reference_id, email, username, password)"
                . " VALUES (:reference_type, :reference_id, :email, :username, :password)";

        $stmt_system_users = $this->prepareQuery($sql_system_users);
        $stmt_system_users->bindValue("reference_type", $this->getUserRefTypeId($_POST['user_type']));
        $stmt_system_users->bindValue("reference_id", $trainee_id);
        $stmt_system_users->bindValue("email", strtoupper($_POST['email']));
        $stmt_system_users->bindValue("username", strtoupper($_POST['username']));
        $stmt_system_users->bindValue("password", sha1($_POST['password']));
        if ($stmt_system_users->execute()) {
            $recipient_email = $_POST['email'];
            $recipient_name = $_POST['firstname'] . " " . $_POST['lastname'];
            $email_subject = "New User Registration";
            $email_message = "<html><body>"
                    . "<p><b>Hello " . $_POST['firstname'] . ",</b><br/>"
                    . "Your account has been successfully created. Your login credentials are as below. <br/>"
                    . "<ul>"
                    . "<li><b>Username: </b>" . $_POST['username'] . " or " . $_POST['email'] . "</li>"
                    . "<li><b>Password: </b>" . $_POST['password'] . "</li>"
                    . "</ul>"
                    . "For any enquiries, kindly contact us on:   <br/>"
                    . "<ul>"
                    . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                    . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                    . "</ul>"
                    . "</body></html>";
            $users_management = new Users_Management();
            $send_email = $users_management->sendEmail($recipient_email, $recipient_name, $email_subject, $email_message);
            if ($send_email == true) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function fetchTraineeDetails($code) {
        $sql = "SELECT * FROM trainees WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function getNextTraineeId() {
        $trainee_id = $this->executeQuery("SELECT max(id) as trainee_id_max FROM trainees");
        $trainee_id = $trainee_id[0]['trainee_id_max'] + 1;
        return $trainee_id;
    }

    private function getNextSystemUserId() {
        $users_management = new Users_Management();
        return $users_management->getNextSystemUserId();
    }

    public function getUserRefTypeId($user_type) {
        $sql = "SELECT id, status FROM user_types WHERE name=:user_type";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("user_type", strtoupper($user_type));
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data[0]['id'];
    }

    public function randomString($length) {
        $original_string = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
        $original_string = implode("", $original_string);
        return substr(str_shuffle($original_string), 0, $length);
    }

}
