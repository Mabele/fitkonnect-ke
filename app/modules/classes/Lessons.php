<?php

class Lessons extends Database {

    public function menuLessonCategories() {
        $sql = "SELECT id, name, status FROM lesson_categories WHERE status=1011 "
                . " ORDER BY name ASC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $currentGroup = null;
        $html = "";
        while ($row = $stmt->fetch()) {
            if (is_null($currentGroup)) {
                $html .= "<li><a href='?lesson_booking&filter_type=category&filter_value={$row['id']}'><span>{$row['name']}</span></a></li>";
            } else {
                $html .= "<li><a href='?lesson_booking&filter_type=category&filter_value={$row['id']}'><span>{$row['name']}</span></a></li>";
            }
        }
        if ($html == "")
            $html = "<option value=\"\">No Lesson Category</option>";
        echo $html;
        return $currentGroup;
    }

    public function menuTrainingFacilities() {
        $sql = "SELECT id, name, status FROM training_facilities WHERE status=1001 "
                . " ORDER BY name ASC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $currentGroup = null;
        $html = "";
        while ($row = $stmt->fetch()) {
            if (is_null($currentGroup)) {
                $html .= "<li><a href='?lesson_booking&filter_type=training_facility&filter_value={$row['id']}'><span>{$row['name']}</span></a></li>";
            } else {
                $html .= "<li><a href='?lesson_booking&filter_type=training_facility&filter_value={$row['id']}'><span>{$row['name']}</span></a></li>";
            }
        }
        if ($html == "")
            $html = "<option value=\"\">No Training Facility</option>";
        echo $html;
        return $currentGroup;
    }

    public function getAllLessons() {
        $sql = "SELECT * FROM lessons WHERE status = '1001' ORDER BY id ASC";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("id" => $data['id'], "name" => $data['name'], "description" => $data['description'], "category" => $data['category'], "start_time" => $data['start_time'], "end_time" => $data['end_time'], "training_facility" => $data['training_facility'], "price" => $data['price'], "capacity" => $data['capacity'], "current_population" => $data['current_population'], "status" => $data['status'], "createdat" => $data['createdat'], "createdby" => $data['createdby'], "authorizedat" => $data['authorizedat'], "authorizedby" => $data['authorizedby'], "lastmodifiedat" => $data['lastmodifiedat'], "lastmodifiedby" => $data['lastmodifiedby']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function getAllFilteredLessons($filter_type, $filter_value) {
        if ($filter_type == 'category') {
            $sql = "SELECT * FROM lessons WHERE category=:category AND status=1001 ORDER BY id ASC";
            $stmt = $this->prepareQuery($sql);
            $stmt->bindValue("category", $filter_value);
        } else if ($filter_type == 'training_facility') {
            $sql = "SELECT * FROM lessons WHERE training_facility=:training_facility AND status=1001 ORDER BY id ASC";
            $stmt = $this->prepareQuery($sql);
            $stmt->bindValue("training_facility", $filter_value);
        } else if ($filter_type == 'start_time') {
            $sql = "SELECT * FROM lessons WHERE start_time=:start_time AND status=1001 ORDER BY id ASC";
            $stmt = $this->prepareQuery($sql);
            $stmt->bindValue("start_time", $filter_value);
        } else if ($filter_type == 'price') {
            $decomposed_filter_value = explode("to", $filter_value);
            $sql = "SELECT * FROM lessons WHERE price>=:start AND price<=:end AND status=1001 ORDER BY id ASC";
            $stmt = $this->prepareQuery($sql);
            $stmt->bindValue("start", $decomposed_filter_value[0]);
            $stmt->bindValue("end", $decomposed_filter_value[1]);
        } else if ($filter_type == 'available_capacity') {
            $decomposed_filter_value = explode("to", $filter_value);
            $sql = "SELECT * FROM lessons WHERE capacity-current_population>=:start AND capacity-current_population<=:end AND status=1001 ORDER BY id ASC";
            $stmt = $this->prepareQuery($sql);
            $stmt->bindValue("start", $decomposed_filter_value[0]);
            $stmt->bindValue("end", $decomposed_filter_value[1]);
        }

        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("id" => $data['id'], "name" => $data['name'], "description" => $data['description'], "category" => $data['category'], "start_time" => $data['start_time'], "end_time" => $data['end_time'], "training_facility" => $data['training_facility'], "price" => $data['price'], "capacity" => $data['capacity'], "current_population" => $data['current_population'], "status" => $data['status'], "createdat" => $data['createdat'], "createdby" => $data['createdby'], "authorizedat" => $data['authorizedat'], "authorizedby" => $data['authorizedby'], "lastmodifiedat" => $data['lastmodifiedat'], "lastmodifiedby" => $data['lastmodifiedby']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function fetchLessonDetails($code) {
        $sql = "SELECT * FROM lessons WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function fetchLessonCategoryDetails($code) {
        $sql = "SELECT * FROM lesson_categories WHERE id=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

}
