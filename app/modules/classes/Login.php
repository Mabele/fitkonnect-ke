<?php

require_once WPATH . "modules/classes/Feedback.php";

class Login extends Database {

    public function execute() {
        if ($_POST['action'] == "secure_login") {
            return $this->secureLogin();
        }
    }

    public function secureLogin() {
        $sql = "SELECT * FROM user_logs WHERE username=:username AND password=:password";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("password", md5($_POST['password']));
        $stmt->bindValue("username", $_POST['username']);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($data) == 0) {
            $title = "Unable to login,";
            $message = "Wrong username/password combination. Please check and try again.";
            $feedback = new Feedback();
            echo $feedback->errorFeedback($title, $message);
        } else {
            $data = $data[0];
            $cookie_name = "username";
            $cookie_value = $_POST['username'];
            setcookie('username', $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
            $_SESSION['username'] = $data['username'];
            $_SESSION['userid'] = $data['id'];
            $_SESSION['user'] = $data['ref_id'];
            $_SESSION['login_user_type'] = $data['ref_type'];
            return true;
        }
    }

}
