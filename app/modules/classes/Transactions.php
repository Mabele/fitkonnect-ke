<?php

require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/Trainers.php";
require_once WPATH . "modules/classes/Trainees.php";
require_once WPATH . "modules/classes/Lessons.php";

class Transactions extends Database {

    public function execute() {
        if ($_POST['action'] == "contact_us") {
            return $this->sendMessage();
        }
    }

    public function sendReminderSMSCronJob() {
        $sql = "SELECT * FROM booking_transactions_details WHERE approvedat IS NOT NULL AND reminder_sms_sent=:is_reminder_sms_sent AND trainer_approvedat IS NULL ORDER BY count DESC";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("is_reminder_sms_sent", "NO");
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($info) == 0) {
            $_SESSION['no_reminder_sms_records'] = true;
            return false;
        } else {
            foreach ($info as $data) {
                $check_time = date("Y-m-d H:i:s", strtotime("+ 8Hours", strtotime($data['approvedat'])));
                if ($check_time < date("Y-m-d H:i:s")) {
                    $users_management = new Users_Management();
                    $trainers = new Trainers();
                    $trainer_contact_details = $users_management->fetchIndividualContactDetails($data['assigned_trainer']);
                    $trainer_system_user_details = $users_management->fetchUserDetails($data['assigned_trainer']);
                    $trainer_details = $trainers->fetchTrainerDetails($trainer_system_user_details['reference_id']);
                    $trainer_message = "Hello " . $trainer_details['firstname'] . ", this is a reminder SMS. Kindly log onto the FitnessIQ portal or check your email to approve an earlier lesson assigned to you.";
                    $send_trainer_sms = $this->sendSingleSMS($trainer_contact_details['phone_number1'], $trainer_message);
                    $sms_details[] = $send_trainer_sms;
                    foreach ($sms_details as $key => $value) {
                        $inner_array[$key] = json_decode($value, true); // this will give key val pair array
                        foreach ((array) $inner_array[$key] as $key2 => $value2) {
                            if (isset($value2['messageid'])) {
                                $sql_sms_sent_update = "UPDATE booking_transactions_details SET reminder_sms_sent=:reminder_sms_sent WHERE count=:code";
                                $stmt_sms_sent_update = $this->prepareQuery($sql_sms_sent_update);
                                $stmt_sms_sent_update->bindValue("code", $data['count']);
                                $stmt_sms_sent_update->bindValue("reminder_sms_sent", "YES");
                                $stmt_sms_sent_update->execute();
                            } else {
                                $_SESSION['sms_sent'] = false;
                            }
                        }
                    }
                    return true;
                }
            }
        }
    }

    public function sendCheckSMSCreditBalanceRequest($data_string) {
        $curl_session = curl_init();
        curl_setopt($curl_session, CURLOPT_URL, 'http://messaging.advantasms.com/bulksms/smscredit.jsp?');
        curl_setopt($curl_session, CURLOPT_POST, true);
        curl_setopt($curl_session, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl_session);
        curl_close($curl_session);
        return $response;
    }

    public function sendBulkSMSRequest($data_string) {

        argDump($data_string);
        exit();

        $curl_session = curl_init();
        curl_setopt($curl_session, CURLOPT_URL, 'http://bulksms.reflexconcepts.co.ke/sendsms.jsp?');
        curl_setopt($curl_session, CURLOPT_POST, true);
        curl_setopt($curl_session, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($curl_session, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl_session);
        curl_close($curl_session);
        return $response;
    }

    public function sendSingleSMSRequest($data_string) {
        $curl_session = curl_init();
        curl_setopt($curl_session, CURLOPT_URL, 'http://messaging.advantasms.com/bulksms/sendsms.jsp?');
        curl_setopt($curl_session, CURLOPT_POST, true);
        curl_setopt($curl_session, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl_session);
        curl_close($curl_session);
        return $response;
    }

    public function getSMSDeliveryReportRequest($data_string) {
        $curl_session = curl_init();
        curl_setopt($curl_session, CURLOPT_URL, 'http://messaging.advantasms.com/bulksms/getDLR.jsp?');
        curl_setopt($curl_session, CURLOPT_POST, true);
        curl_setopt($curl_session, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl_session);
        curl_close($curl_session);
        return $response;
    }

    public function getSMSDeliveryReport() {
        $data['userid'] = "Reflexconcepts"; // $configs["sms_username"];
        $data['password'] = "r12345"; // $configs["sms_password"];

        $data['messageid'] = ""; // optional: whenever you send a message you will get this message id as a response. eg. 671295686, 671295687
        $data['externalid'] = ""; // optional: set your message unique id, if you have
        $data['drquantity'] = ""; // optional: how many dr records you want
        $data['fromdate'] = ""; // optional
        $data['todate'] = ""; // optional
        $data['redownload'] = ""; // optional: If you want to a DR in a second time you must set redownload paramater “yes” other wise you will not get DR, if you first time download you do not set this paramater.
        $data['responcetype'] = ""; //In which format you want to get response (ex. Xml, csv, tsv)

        $data_string = http_build_query($data);
        $process_request = $this->getSMSDeliveryReportRequest($data_string);
        $decoded_response = simplexml_load_string($process_request) or die("Error: Cannot create object");
        return $decoded_response;
    }

    public function checkSMSCreditBalance() {
        $data['user'] = "Reflexconcepts"; // $configs["sms_username"];
        $data['password'] = "r12345"; // $configs["sms_password"];
        $data_string = http_build_query($data);
        $process_request = $this->sendCheckSMSCreditBalanceRequest($data_string);
        $decoded_response = simplexml_load_string($process_request) or die("Error: Cannot create object");
        return $decoded_response;
    }

    public function sendSingleSMS($phone_number, $message) {
        $data['user'] = "Reflexconcepts";
        $data['password'] = "r12345";
        $data['mobiles'] = $phone_number;
        $data['sms'] = $message;
        $data_string = http_build_query($data);
        $process_request = $this->sendSingleSMSRequest($data_string);
        $decoded_response = simplexml_load_string($process_request) or die("Error: Cannot create object");
        $this->logSentSMSDetails("OUTBOX", json_encode($decoded_response));
        return json_encode($decoded_response);
    }

    public function sendShortCodeSMS($phone_number, $message) {
        $client = new IXR_Client('http://prs.advantasms.com/sdp_apps/services/thirdparty/sdp_server.php');
        $response = $client->query('ADVANTA.sendSms', array(
            array(
                'USERNAME' => 'testadmin',
                'PASSWORD' => 'testadmin'
            ),
            array(
                'CHANNELID' => '60',
                'MSISDN' => $phone_number,
                'SOURCEADDR' => '21211',
                'MESSAGE' => $message
            )
        ));
        if (!$response) {
            die('An error occurred - ' . $client->getErrorCode() . ":" . $client->getErrorMessage());
        }
        print_r($client->getResponse());
    }

    public function sendBulkSMS() {
        $user = "Reflexconcepts";
        $password = "r12345";

        if ($_POST['recipient_group'] == "ALL") {
            $users_management = new Users_Management();
            $contact_details[] = $users_management->getAllContacts();

            $data_string = "<?xml version='1.0'?>";
            $data_string .= "<smslist>";
            foreach ($contact_details as $key => $value) {
                $inner_array[$key] = json_decode($value, true);
                foreach ((array) $inner_array[$key] as $key2 => $value2) {
                    $data_string .= "<sms>";
                    $data_string .= "<user>" . $user . "</user>";
                    $data_string .= "<password>" . $password . "</password>";
                    $data_string .= "<message>" . $_POST['message'] . "</message>";
                    $data_string .= "<mobiles>" . $value2['phone_number1'] . "</mobiles>";
                    $data_string .= "<clientsmsid>1</clientsmsid>";
                    $data_string .= "<unicode>0</unicode>";
                    $data_string .= "</sms>";
                }
            }
            $data_string .= "</smslist>";
        } else if ($_POST['recipient_group'] == "INDIVIDUAL") {
            $contact_details[] = explode(",", $_POST['recipient_contacts']);

            $data_string = "<?xml version='1.0'?>";
            $data_string .= "<smslist>";
            foreach ($contact_details as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    $data_string .= "<sms>";
                    $data_string .= "<user>" . $user . "</user>";
                    $data_string .= "<password>" . $password . "</password>";
                    $data_string .= "<message>" . $_POST['message'] . "</message>";
                    $data_string .= "<mobiles>" . $value2 . "</mobiles>";
                    $data_string .= "<clientsmsid>1</clientsmsid>";
                    $data_string .= "<unicode>0</unicode>";
                    $data_string .= "</sms>";
                }
            }
            $data_string .= "</smslist>";
        } else {
// get all contacts where user_id (in contacts table) is an id (in system_users table) that has a refence type equivalent to the passed $_POST['recipient_group'] value
            $users_management = new Users_Management();
            $contact_details[] = $users_management->getGroupContacts($_POST['recipient_group']);

            $data_string = "<?xml version='1.0'?>";
            $data_string .= "<smslist>";
            foreach ($contact_details as $key => $value) {
                $inner_array[$key] = json_decode($value, true);
                foreach ((array) $inner_array[$key] as $key2 => $value2) {
                    $data_string .= "<sms>";
                    $data_string .= "<user>" . $user . "</user>";
                    $data_string .= "<password>" . $password . "</password>";
                    $data_string .= "<message>" . $_POST['message'] . "</message>";
                    $data_string .= "<mobiles>" . $value2['phone_number1'] . "</mobiles>";
                    $data_string .= "<clientsmsid>1</clientsmsid>";
                    $data_string .= "<unicode>0</unicode>";
                    $data_string .= "</sms>";
                }
            }
            $data_string .= "</smslist>";
        }
        $process_request = $this->sendBulkSMSRequest($data_string);
        $decoded_response = simplexml_load_string($process_request) or die("Error: Cannot create object");
        return $process_request;
    }

    public function getTransactionId($payment_option, $transactedby, $total_amount) {
        $transaction_id = md5($payment_option . $transactedby . $total_amount . time());
        return strtoupper($transaction_id);
    }
    
    public function getAllTraineeTransactions($trainee_id) {
        $sql = "SELECT * FROM booking_transactions WHERE booker_id=:booker_id ORDER BY count DESC";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("booker_id", $trainee_id);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("count" => $data['count'], "transaction_id" => $data['transaction_id'], "transaction_type" => $data['transaction_type'], "amount" => $data['amount'], "booker_id" => $data['booker_id'], "payment_option" => $data['payment_option'], "createdat" => $data['createdat'], "payment_status" => $data['payment_status'], "status" => $data['status']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }
    
    public function getLessonBookingTransactionsDetails($transaction_id) {
        $sql = "SELECT * FROM booking_transactions_details WHERE transaction_id=:transaction_id ORDER BY count DESC";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("transaction_id", $transaction_id);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($info) == 0) {
            $_SESSION['no_records'] = true;
        } else {
            $_SESSION['yes_records'] = true;
            $values2 = array();
            foreach ($info as $data) {
                $values = array("count" => $data['count'], "transaction_id" => $data['transaction_id'], "lesson" => $data['lesson'], "number_of_classes" => $data['number_of_classes'], "unit_price" => $data['unit_price'], "start_date" => $data['start_date'], "training_facility" => $data['training_facility'], "payment_status" => $data['payment_status'], "approvedat" => $data['approvedat'], "approvedby" => $data['approvedby'], "approval_comment" => $data['approval_comment'], "assigned_trainer" => $data['assigned_trainer'], "reminder_sms_sent" => $data['reminder_sms_sent'], "trainer_approvedat" => $data['trainer_approvedat'], "trainer_approval_comment" => $data['trainer_approval_comment'], "attendance_status" => $data['attendance_status'], "trainer_rating" => $data['trainer_rating'], "facility_rating" => $data['facility_rating'], "status" => $data['status']);
                array_push($values2, $values);
            }
            return json_encode($values2);
        }
    }

    public function logSentSMSDetails($message_type, $sms_delivery_response) {
        $sms_details[] = $sms_delivery_response;
        foreach ($sms_details as $key => $value) {
            $inner_array[$key] = json_decode($value, true); // this will give key val pair array
            foreach ((array) $inner_array[$key] as $key2 => $value2) {
                if ($message_type == "OUTBOX") {
                    if (isset($value2['messageid'])) {
                        $sql = "INSERT INTO sms_log (phone_number, smsclientid, messageid, type)"
                                . " VALUES (:phone_number, :smsclientid, :messageid, :message_type)";
                        $stmt = $this->prepareQuery($sql);
                        $stmt->bindValue("phone_number", $value2['mobile-no']);  // 
                        $stmt->bindValue("smsclientid", $value2['smsclientid']);
                        $stmt->bindValue("messageid", $value2['messageid']);
                        $stmt->bindValue("message_type", $message_type);
                        $stmt->execute();
                    } else {
                        $sql = "INSERT INTO sms_log (phone_number, smsclientid, error_code, error_description, error_action, type)"
                                . " VALUES (:phone_number, :smsclientid, :error_code, :error_description, :error_action, :message_type)";
                        $stmt = $this->prepareQuery($sql);
                        $stmt->bindValue("phone_number", $value2['mobile-no']);
                        $stmt->bindValue("smsclientid", $value2['smsclientid']);
                        $stmt->bindValue("error_code", $value2['error-code']);
                        $stmt->bindValue("error_description", $value2['error-description']);
                        $stmt->bindValue("error_action", $value2['error-action']);
                        $stmt->bindValue("message_type", $message_type);
                        $stmt->execute();
                    }
                }
            }
        }
        return true;
    }

//With manual approving of lesson bookings
    public function addTransaction() {
        $sql = "SELECT * FROM payment_transactions WHERE transaction_reference=:transaction_ref_number AND business_short_code=:business_short_code AND status=:status";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("transaction_ref_number", $_SESSION['transaction_ref_number']);
        $stmt->bindValue("business_short_code", $_SESSION['till_number']);
        $stmt->bindValue("status", 2004);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($data) == 0) {
            $_SESSION['payment_received'] = false;
            return false;
            exit();
        } else {
            $data = $data[0];
            if ($data['transaction_amount'] < $_SESSION["cart_total_cost"]) {
                $payment_status = 2002;
            } else if ($data['transaction_amount'] = $_SESSION["cart_total_cost"]) {
                $payment_status = 2003;
            } else if ($data['transaction_amount'] > $_SESSION["cart_total_cost"]) {
                $payment_status = 2006;
            }            
            $payment_acknowledgement_message = "Hello " . $_SESSION["billing_firstname"] . ", your payment referenced " . $_SESSION['transaction_ref_number'] . " has been received and your request is undergoing processing. Thank you for choosing FitnessIQ";

            $payment_acknowledgement_email = $_SESSION["billing_email_address"];
            $payment_acknowledgement_name = $_SESSION['billing_firstname'] . " " . $_SESSION['billing_lastname'];
            $payment_acknowledgement_email_subject = "Payment Acknowledgement";
            $payment_acknowledgement_email_message = "<html><body>"
                    . "<p><b>Hello " . $_SESSION["billing_firstname"] . ",</b><br/>"
                    . "Your payment referenced " . $_SESSION['transaction_ref_number'] . " has been received and your request is undergoing processing. Thank you for choosing FitnessIQ. "
                    . "For any enquiries, kindly contact us on:   <br/>"
                    . "<ul>"
                    . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                    . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                    . "</ul>"
                    . "</body></html>";

            $users_management = new Users_Management();
        //    $send_acknowledgement_email = $users_management->sendEmail($payment_acknowledgement_email, $payment_acknowledgement_name, $payment_acknowledgement_email_subject, $payment_acknowledgement_email_message);

        //    $send_payment_acknowledgement_sms = $this->sendSingleSMS($_SESSION["billing_phone_number"], $payment_acknowledgement_message);

            if (App::isLoggedIn()) {
                $_SESSION["transaction_id"] = $this->getTransactionId($_SESSION["payment_option"], $_SESSION["user_id"], $_SESSION["cart_total_cost"]);

                $sql = "INSERT INTO booking_transactions (transaction_id, amount, booker_id, payment_option, additional_comments, payment_status)"
                        . " VALUES (:transaction_id, :amount, :booker_id, :payment_option, :additional_comments, :payment_status)";
                $stmt = $this->prepareQuery($sql);
                $stmt->bindValue("transaction_id", $_SESSION["transaction_id"]);
                $stmt->bindValue("amount", $_SESSION["cart_total_cost"]);
                $stmt->bindValue("booker_id", $_SESSION["user_id"]);
                $stmt->bindValue("payment_option", strtoupper($_SESSION['payment_option']));
                $stmt->bindValue("additional_comments", strtoupper($_SESSION['billing_additional_comments']));
                $stmt->bindValue("payment_status", $payment_status);
                if ($stmt->execute()) {
                    $update_payment_status = $this->updatePaymentStatus();
                    if ($update_payment_status == true) {
                        $sql_iq_points = "INSERT INTO iq_points (user_id, transaction_id, transaction_type, points)"
                                . " VALUES (:user_id, :transaction_id, :transaction_type, :points)";
                        $stmt_iq_points = $this->prepareQuery($sql_iq_points);
                        $stmt_iq_points->bindValue("user_id", $_SESSION["user_id"]);
                        $stmt_iq_points->bindValue("transaction_id", $_SESSION["transaction_id"]);
                        $stmt_iq_points->bindValue("transaction_type", 2);
                        $stmt_iq_points->bindValue("points", 0.16 * $_SESSION["cart_total_cost"]);
                        if ($stmt_iq_points->execute()) {
                            $user_iq_points_gained = $this->getUserIqPointsGained($_SESSION["user_id"]);
                            $user_iq_points_redeemed = $this->getUserIqPointsRedeemed($_SESSION["user_id"]);
                            $iq_points = $user_iq_points_gained - $user_iq_points_redeemed;

                            $sql_update_iq_points_balance = "UPDATE system_users SET iq_points=:iq_points WHERE id=:user_id";
                            $stmt_update_iq_points_balance = $this->prepareQuery($sql_update_iq_points_balance);
                            $stmt_update_iq_points_balance->bindValue("user_id", $_SESSION["user_id"]);
                            $stmt_update_iq_points_balance->bindValue("iq_points", $iq_points);
                            $stmt_update_iq_points_balance->execute();
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else
                    return false;
            } else {
                $trainees = new Trainees();
                $users_management = new Users_Management();
                $trainee_id = $trainees->getNextTraineeId();
                $user_id = $users_management->getNextSystemUserId();
                $_SESSION["transaction_id"] = $this->getTransactionId($_SESSION["payment_option"], $user_id, $_SESSION["cart_total_cost"]);
                $sql_trainees = "INSERT INTO trainees (firstname, lastname, gender, birth_date, createdby, lastmodifiedby)"
                        . " VALUES (:firstname, :lastname, :gender, :birth_date, :createdby, :lastmodifiedby)";
                $stmt_trainees = $this->prepareQuery($sql_trainees);
                $stmt_trainees->bindValue("firstname", strtoupper($_SESSION["billing_firstname"]));
                $stmt_trainees->bindValue("lastname", strtoupper($_SESSION["billing_lastname"]));
                $stmt_trainees->bindValue("gender", strtoupper($_SESSION["billing_gender"]));
                $stmt_trainees->bindValue("birth_date", $_SESSION["billing_birth_date"]);
                $stmt_trainees->bindValue("createdby", $user_id);
                $stmt_trainees->bindValue("lastmodifiedby", $user_id);
                $stmt_trainees->execute();

                $sql_contacts = "INSERT INTO contacts (user_id, phone_number1, phone_number2, email, postal_number, postal_code, postal_town, county, ward, prof_picture, website, createdby, lastmodifiedby)"
                        . " VALUES (:user_id, :phone_number1, :phone_number2, :email, :postal_number, :postal_code, :postal_town, :county, :ward, :prof_picture, :website, :createdby, :lastmodifiedby)";

                $stmt_contacts = $this->prepareQuery($sql_contacts);
                $stmt_contacts->bindValue("user_id", $user_id);
                $stmt_contacts->bindValue("phone_number1", $_SESSION["billing_phone_number"]);
                $stmt_contacts->bindValue("phone_number2", NULL);
                $stmt_contacts->bindValue("email", strtoupper($_SESSION["billing_email_address"]));
                $stmt_contacts->bindValue("postal_number", NULL);
                $stmt_contacts->bindValue("postal_code", NULL);
                $stmt_contacts->bindValue("postal_town", NULL);
                $stmt_contacts->bindValue("county", NULL);
                $stmt_contacts->bindValue("ward", NULL);
                $stmt_contacts->bindValue("prof_picture", NULL);
                $stmt_contacts->bindValue("website", NULL);
                $stmt_contacts->bindValue("createdby", $user_id);
                $stmt_contacts->bindValue("lastmodifiedby", $user_id);
                $stmt_contacts->execute();

                $sql_system_users = "INSERT INTO system_users (reference_type, reference_id, email, password)"
                        . " VALUES (:reference_type, :reference_id, :email, :password)";

                $stmt_system_users = $this->prepareQuery($sql_system_users);
                $stmt_system_users->bindValue("reference_type", $_SESSION['ref_type']);
                $stmt_system_users->bindValue("reference_id", $trainee_id);
                $stmt_system_users->bindValue("email", strtoupper($_SESSION["billing_email_address"]));
                $stmt_system_users->bindValue("password", sha1($_SESSION["billing_lastname"]));
                $stmt_system_users->execute();

                $sql_transactions = "INSERT INTO booking_transactions (transaction_id, amount, booker_id, payment_option, additional_comments, payment_status)"
                        . " VALUES (:transaction_id, :amount, :booker_id, :payment_option, :additional_comments, :payment_status)";
                $stmt_transactions = $this->prepareQuery($sql_transactions);
                $stmt_transactions->bindValue("transaction_id", $_SESSION["transaction_id"]);
                $stmt_transactions->bindValue("amount", $_SESSION["cart_total_cost"]);
                $stmt_transactions->bindValue("booker_id", $user_id);
                $stmt_transactions->bindValue("payment_option", strtoupper($_SESSION['payment_option']));
                $stmt_transactions->bindValue("additional_comments", strtoupper($_SESSION['billing_additional_comments']));
                $stmt_transactions->bindValue("payment_status", $payment_status);
                if ($stmt_transactions->execute()) {
                    $update_payment_status = $this->updatePaymentStatus();
                    if ($update_payment_status == true) {
                        $sql_iq_points = "INSERT INTO iq_points (user_id, transaction_id, transaction_type, points)"
                                . " VALUES (:user_id, :transaction_id, :transaction_type, :points)";
                        $stmt_iq_points = $this->prepareQuery($sql_iq_points);
                        $stmt_iq_points->bindValue("user_id", $user_id);
                        $stmt_iq_points->bindValue("transaction_id", $_SESSION["transaction_id"]);
                        $stmt_iq_points->bindValue("transaction_type", 2);
                        $stmt_iq_points->bindValue("points", 0.10 * $_SESSION["cart_total_cost"]);
                        if ($stmt_iq_points->execute()) {
                            $user_iq_points_gained = $this->getUserIqPointsGained($user_id);
                            $user_iq_points_redeemed = $this->getUserIqPointsRedeemed($user_id);
                            $iq_points = $user_iq_points_gained - $user_iq_points_redeemed;

                            $sql_update_iq_points_balance = "UPDATE system_users SET iq_points=:iq_points WHERE id=:user_id";
                            $stmt_update_iq_points_balance = $this->prepareQuery($sql_update_iq_points_balance);
                            $stmt_update_iq_points_balance->bindValue("user_id", $user_id);
                            $stmt_update_iq_points_balance->bindValue("iq_points", $iq_points);
                            $stmt_update_iq_points_balance->execute();
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else
                    return false;
            }
        }
    }

// With auto-approving of lesson bookings
//    public function addTransaction() {
//        $sql = "SELECT * FROM payment_transactions WHERE transaction_reference=:transaction_ref_number AND business_short_code=:business_short_code AND status=:status";
//        $stmt = $this->prepareQuery($sql);
//        $stmt->bindValue("transaction_ref_number", $_SESSION['transaction_ref_number']);
//        $stmt->bindValue("business_short_code", $_SESSION['till_number']);
//        $stmt->bindValue("status", 2004);
//        $stmt->execute();
//        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
//        if (count($data) == 0) {
//            $_SESSION['payment_received'] = false;
//            return false;
//            exit();
//        } else {
//            $data = $data[0];
//            $payment_acknowledgement_message = "Hello " . $_SESSION["billing_firstname"] . ", your payment referenced " . $_SESSION['transaction_ref_number'] . " has been received and your request is undergoing processing. Thank you for choosing FitnessIQ";
//            $payment_acknowledgement_email = $_SESSION["billing_email_address"];
//            $payment_acknowledgement_name = $_SESSION['billing_firstname'] . " " . $_SESSION['billing_lastname'];
//            $payment_acknowledgement_email_subject = "Payment Acknowledgement";
//            $payment_acknowledgement_email_message = "<html><body>"
//                    . "<p><b>Hello " . $_SESSION["billing_firstname"] . ",</b><br/>"
//                    . "Your payment referenced " . $_SESSION['transaction_ref_number'] . " has been received and your request is undergoing processing. Thank you for choosing FitnessIQ."
//                    . "For any enquiries, kindly contact us on:   <br/>"
//                    . "<ul>"
//                    . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
//                    . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
//                    . "</ul>"
//                    . "</body></html>";
//
//            $users_management = new Users_Management();
//            $send_acknowledgement_email = $users_management->sendEmail($payment_acknowledgement_email, $payment_acknowledgement_name, $payment_acknowledgement_email_subject, $payment_acknowledgement_email_message);
//            $send_payment_acknowledgement_sms = $this->sendSingleSMS($_SESSION["billing_phone_number"], $payment_acknowledgement_message);
//
//            if (App::isLoggedIn()) {
//                $_SESSION["transaction_id"] = $this->getTransactionId($_SESSION["payment_option"], $_SESSION["user_id"], $_SESSION["cart_total_cost"]);
//
//                $sql = "INSERT INTO booking_transactions (transaction_id, amount, booker_id, payment_option, additional_comments)"
//                        . " VALUES (:transaction_id, :amount, :booker_id, :payment_option, :additional_comments)";
//                $stmt = $this->prepareQuery($sql);
//                $stmt->bindValue("transaction_id", $_SESSION["transaction_id"]);
//                $stmt->bindValue("amount", $_SESSION["cart_total_cost"]);
//                $stmt->bindValue("booker_id", $_SESSION["user_id"]);
//                $stmt->bindValue("payment_option", strtoupper($_SESSION['payment_option']));
//                $stmt->bindValue("additional_comments", strtoupper($_SESSION['billing_additional_comments']));
//                if ($stmt->execute()) {
//                    $update_payment_status = $this->updatePaymentStatus();
//                    if ($update_payment_status == true) {
//                        $users_management = new Users_Management();
//                        $trainers = new Trainers();
//                        $trainees = new Trainees();
//                        $lessons = new Lessons();
//
//                        $assigned_trainer = 4;
//
//                        foreach ($_SESSION["cart_item"] as $item) {
//                            $_SESSION["number_of_classes"] = $item["number_of_classes"];
//                            $_SESSION['lesson_details'] = $lessons->fetchLessonDetails($item["id"]);
//                            if ($this->addTransactionDetails() == true) {
//                                if ($this->autoApproveLessonBookingTransaction($_SESSION['next_transaction_detail_id'], $_SESSION["transaction_id"], "Auto Assigned", $assigned_trainer, $_SESSION["user_id"]) == true) {
//                                    $trainer_contact_details = $users_management->fetchIndividualContactDetails($assigned_trainer);
//                                    $trainer_system_user_details = $users_management->fetchUserDetails($assigned_trainer);
//                                    $trainer_details = $trainers->fetchTrainerDetails($trainer_system_user_details['reference_id']);
//                                    $transaction_details = $this->fetchBookingTransactionDetails($_SESSION["transaction_id"]);
//                                    $trainee_contact_details = $users_management->fetchIndividualContactDetails($_SESSION["user_id"]);
//                                    $trainee_system_user_details = $users_management->fetchUserDetails($_SESSION["user_id"]);
//                                    $trainee_details = $trainees->fetchTraineeDetails($trainee_system_user_details['reference_id']);
//
//$trainer_message = "Hello " . $trainer_details['firstname'] . ", you have been assigned a trainee - " . $trainee_details['firstname'] . " " . $trainee_details['lastname'] . ". You can reach your trainee on " . $trainee_contact_details['phone_number1'] . ". If you have any reservations on the assignment, please reply: TESTSMS:LESSON ASSIGNMENT,DECLINE" . $_SESSION['next_transaction_detail_id'] . " to 21211 to decline the assignment, else do not reply to this text. You can approve the same from your email or by logging into the FitnessIQ portal.";
//
//$trainer_email = $trainer_contact_details['email'];
//$trainer_name = $trainer_details['firstname'] . " " . $trainer_details['lastname'];
//$trainer_email_subject = "Lesson Assignment Notification";
//$trainer_email_message = "<html><body>"
//. "<p><b>Hello " . $trainer_details['firstname'] . ",</b><br/>"
//. "You have been assigned a trainee - " . $trainee_details['firstname'] . " " . $trainee_details['lastname'] . ". "
//. "You can reach your trainee on " . $trainee_contact_details['phone_number1'] . ". "
//. "If you have any reservations on the lesson assignment, please <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=lesson_assignment&response=decline{$_SESSION['next_transaction_detail_id']}'> Click here </a> to decline the assignment. <br/>"
//. "For any enquiries, kindly contact us on:   <br/>"
//. "<ul>"
//. "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
//. "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
//. "</ul>"
//. "</body></html>";
//                                    if (isset($_SESSION['reassign_trainer'])) {
//                                        $trainee_message = "Hello " . $trainee_details['firstname'] . ", thank you for engaging us for your fitness sessions. Your assigned trainer is " . $trainer_details['firstname'] . " " . $trainer_details['lastname'] . ". You can reach your trainer on - Telephone (" . $trainer_contact_details['phone_number1'] . "), Email(" . $trainee_contact_details['email'] . "). If you have any reservations on the allocated trainer please reply: TESTSMS:TRAINER ACCEPTANCE,DECLINE" . $_SESSION['next_transaction_detail_id'] . " to 21211, else do not reply to this text.";
//                        $trainee_email = $trainee_contact_details['email'];
//                        $trainee_name = $trainee_details['firstname'] . " " . $trainee_details['lastname'];
//                        $trainee_email_subject = "Lesson Assignment Notification";
//                        $trainee_email_message = "<html><body>"
//                                . "<p><b>Hello " . $trainee_details['firstname'] . ",</b><br/>"
//                                . "Thank you for engaging us for your fitness sessions. "
//                                . "Your assigned trainer is " . $trainer_details['firstname'] . " " . $trainer_details['lastname'] . ". "
//                                . "You can reach your trainer on - Telephone (" . $trainer_contact_details['phone_number1'] . "), Email(" . $trainee_contact_details['email'] . "). "
//                                . "If you have any reservations on the allocated trainer please <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=trainer_acceptance&response=decline{$_SESSION['next_transaction_detail_id']}'> Click here </a> to decline the assigned trainer. <br/>"
//                                . "For any enquiries, kindly contact us on:   <br/>"
//                                . "<ul>"
//                                . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
//                                . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
//                                . "</ul>"
//                                . "</body></html>";
//
//} else {
//                                        $trainee_message = "Hello " . $trainee_details['firstname'] . ", thank you for engaging us for your fitness sessions. Your assigned trainer is " . $trainer_details['firstname'] . " " . $trainer_details['lastname'] . ". You can reach your trainer on - Telephone (" . $trainer_contact_details['phone_number1'] . "), Email(" . $trainee_contact_details['email'] . "). If you have any reservations on the allocated trainer please reply: TESTSMS:TRAINER ACCEPTANCE,DECLINE" . $_SESSION['next_transaction_detail_id'] . " to 21211, else do not reply to this text.";
//                        $trainee_email = $trainee_contact_details['email'];
//                        $trainee_name = $trainee_details['firstname'] . " " . $trainee_details['lastname'];
//                        $trainee_email_subject = "Lesson Assignment Notification";
//                        $trainee_email_message = "<html><body>"
//                                . "<p><b>Hello " . $trainee_details['firstname'] . ",</b><br/>"
//                                . "Thank you for engaging us for your fitness sessions. "
//                                . "Your assigned trainer is " . $trainer_details['firstname'] . " " . $trainer_details['lastname'] . ". "
//                                . "You can reach your trainer on - Telephone (" . $trainer_contact_details['phone_number1'] . "), Email(" . $trainee_contact_details['email'] . "). "
//                                . "If you have any reservations on the allocated trainer please <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=trainer_acceptance&response=decline{$_SESSION['next_transaction_detail_id']}'> Click here </a> to decline the assigned trainer. <br/>"
//                                . "For any enquiries, kindly contact us on:   <br/>"
//                                . "<ul>"
//                                . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
//                                . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
//                                . "</ul>"
//                                . "</body></html>";
//    
//}
//
//$users_management = new Users_Management();
//$send_trainer_email = $users_management->sendEmail($trainer_email, $trainer_name, $trainer_email_subject, $trainer_email_message);
//$send_trainee_email = $users_management->sendEmail($trainee_email, $trainee_name, $trainee_email_subject, $trainee_email_message);
//                                    $send_trainer_sms = $this->sendSingleSMS($trainer_contact_details['phone_number1'], $trainer_message);
//                                    $send_trainee_sms = $this->sendSingleSMS($trainee_contact_details['phone_number1'], $trainee_message);
//                                    return true;
//                                } else
//                                    return false;
//                            } else
//                                return false;
//                        }
//                    } else {
//                        return false;
//                    }
//                } else
//                    return false;
//            } else {
//                $update_payment_status = $this->updatePaymentStatus();
//                if ($update_payment_status == true) {
//                    $trainees = new Trainees();
//                    $trainee_id = $trainees->getNextTraineeId();
//                    $user_id = $trainees->getNextUserId();
//                    $_SESSION["transaction_id"] = $this->getTransactionId($_SESSION["payment_option"], $user_id, $_SESSION["cart_total_cost"]);
//                    $sql_trainees = "INSERT INTO trainees (firstname, lastname, username, gender, birth_date, createdby, lastmodifiedby)"
//                            . " VALUES (:firstname, :lastname, :username, :gender, :birth_date, :createdby, :lastmodifiedby)";
//                    $stmt_trainees = $this->prepareQuery($sql_trainees);
//                    $stmt_trainees->bindValue("firstname", strtoupper($_SESSION["billing_firstname"]));
//                    $stmt_trainees->bindValue("lastname", strtoupper($_SESSION["billing_lastname"]));
//                    $stmt_trainees->bindValue("username", strtoupper($_SESSION["billing_firstname"]));
//                    $stmt_trainees->bindValue("gender", strtoupper($_SESSION["billing_gender"]));
//                    $stmt_trainees->bindValue("birth_date", $_SESSION["billing_birth_date"]);
//                    $stmt_trainees->bindValue("createdby", $user_id);
//                    $stmt_trainees->bindValue("lastmodifiedby", $user_id);
//                    $stmt_trainees->execute();
//
//                    $sql_contacts = "INSERT INTO contacts (user_id, phone_number1, phone_number2, email, postal_number, postal_code, postal_town, county, ward, prof_picture, website, createdby, lastmodifiedby)"
//                            . " VALUES (:user_id, :phone_number1, :phone_number2, :email, :postal_number, :postal_code, :postal_town, :county, :ward, :prof_picture, :website, :createdby, :lastmodifiedby)";
//
//                    $stmt_contacts = $this->prepareQuery($sql_contacts);
//                    $stmt_contacts->bindValue("user_id", $user_id);
//                    $stmt_contacts->bindValue("phone_number1", $_SESSION["billing_phone_number"]);
//                    $stmt_contacts->bindValue("phone_number2", NULL);
//                    $stmt_contacts->bindValue("email", strtoupper($_SESSION["billing_email_address"]));
//                    $stmt_contacts->bindValue("postal_number", NULL);
//                    $stmt_contacts->bindValue("postal_code", NULL);
//                    $stmt_contacts->bindValue("postal_town", NULL);
//                    $stmt_contacts->bindValue("county", NULL);
//                    $stmt_contacts->bindValue("ward", NULL);
//                    $stmt_contacts->bindValue("prof_picture", NULL);
//                    $stmt_contacts->bindValue("website", NULL);
//                    $stmt_contacts->bindValue("createdby", $user_id);
//                    $stmt_contacts->bindValue("lastmodifiedby", $user_id);
//                    $stmt_contacts->execute();
//
//                    $sql_system_users = "INSERT INTO system_users (reference_type, reference_id, username, password)"
//                            . " VALUES (:reference_type, :reference_id, :username, :password)";
//
//                    $stmt_system_users = $this->prepareQuery($sql_system_users);
//                    $stmt_system_users->bindValue("reference_type", $_SESSION['ref_type']);
//                    $stmt_system_users->bindValue("reference_id", $trainee_id);
//                    $stmt_system_users->bindValue("username", strtoupper($_SESSION["billing_email_address"]));
//                    $stmt_system_users->bindValue("password", sha1($_SESSION["billing_lastname"]));
//                    $stmt_system_users->execute();
//
//                    $sql_transactions = "INSERT INTO booking_transactions (transaction_id, amount, booker_id, payment_option, additional_comments)"
//                            . " VALUES (:transaction_id, :amount, :booker_id, :payment_option, :additional_comments)";
//                    $stmt_transactions = $this->prepareQuery($sql_transactions);
//                    $stmt_transactions->bindValue("transaction_id", $_SESSION["transaction_id"]);
//                    $stmt_transactions->bindValue("amount", $_SESSION["cart_total_cost"]);
//                    $stmt_transactions->bindValue("booker_id", $user_id);
//                    $stmt_transactions->bindValue("payment_option", strtoupper($_SESSION['payment_option']));
//                    $stmt_transactions->bindValue("additional_comments", strtoupper($_SESSION['billing_additional_comments']));
//                    if ($stmt_transactions->execute()) {
//                        $users_management = new Users_Management();
//                        $trainers = new Trainers();
//                        $trainees = new Trainees();
//                        $lessons = new Lessons();
//
//                        $assigned_trainer = 4;
//
//                        foreach ($_SESSION["cart_item"] as $item) {
//                            $_SESSION["number_of_classes"] = $item["number_of_classes"];
//                            $_SESSION['lesson_details'] = $lessons->fetchLessonDetails($item["id"]);
//                            if ($this->addTransactionDetails() == true) {
//                                if ($this->autoApproveLessonBookingTransaction($_SESSION['next_transaction_detail_id'], $_SESSION["transaction_id"], "Auto Assigned", $assigned_trainer, $user_id) == true) {
//                                    $trainer_contact_details = $users_management->fetchIndividualContactDetails($assigned_trainer);
//                                    $trainer_system_user_details = $users_management->fetchUserDetails($assigned_trainer);
//                                    $trainer_details = $trainers->fetchTrainerDetails($trainer_system_user_details['reference_id']);
//                                    $transaction_details = $this->fetchBookingTransactionDetails($_SESSION["transaction_id"]);
//                                    $trainee_contact_details = $users_management->fetchIndividualContactDetails($user_id);
//                                    $trainee_system_user_details = $users_management->fetchUserDetails($user_id);
//                                    $trainee_details = $trainees->fetchTraineeDetails($trainee_system_user_details['reference_id']);
//
//                                    $trainer_message = "Hello " . $trainer_details['firstname'] . ", you have been assigned a trainee - " . $trainee_details['firstname'] . " " . $trainee_details['lastname'] . ". You can reach your trainee on " . $trainee_contact_details['phone_number1'] . ". If you have any reservations on the assignment, please reply: TESTSMS:LESSON ASSIGNMENT,DECLINE" . $_SESSION['next_transaction_detail_id'] . " to 21211 to decline the assignment, else do not reply to this text.. You can approve the same from your email or by logging into the FitnessIQ portal.";
//
//$trainer_email = $trainer_contact_details['email'];
//$trainer_name = $trainer_details['firstname'] . " " . $trainer_details['lastname'];
//$trainer_email_subject = "Lesson Assignment Notification";
//$trainer_email_message = "<html><body>"
//. "<p><b>Hello " . $trainer_details['firstname'] . ",</b><br/>"
//. "You have been assigned a trainee - " . $trainee_details['firstname'] . " " . $trainee_details['lastname'] . ". "
//. "You can reach your trainee on " . $trainee_contact_details['phone_number1'] . ". "
//. "If you have any reservations on the lesson assignment, please <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=lesson_assignment&response=decline{$_SESSION['next_transaction_detail_id']}'> Click here </a> to decline the assignment. <br/>"
//. "For any enquiries, kindly contact us on:   <br/>"
//. "<ul>"
//. "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
//. "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
//. "</ul>"
//. "</body></html>";
//                                    if (isset($_SESSION['reassign_trainer'])) {
//                                        $trainee_message = "Hello " . $trainee_details['firstname'] . ", your lesson training has been re-assigned to " . $trainer_details['firstname'] . " " . $trainer_details['lastname'] . ". You can reach your new trainer on - Telephone (" . $trainer_contact_details['phone_number1'] . "), Email(" . $trainee_contact_details['email'] . "). If you have any reservations on the allocated trainer please reply: TESTSMS:TRAINER ACCEPTANCE,DECLINE" . $_SESSION['next_transaction_detail_id'] . " to 21211, else do not reply to this text.";
//                        $trainee_email = $trainee_contact_details['email'];
//                        $trainee_name = $trainee_details['firstname'] . " " . $trainee_details['lastname'];
//                        $trainee_email_subject = "Lesson Assignment Notification";
//                        $trainee_email_message = "<html><body>"
//                                . "<p><b>Hello " . $trainee_details['firstname'] . ",</b><br/>"
//                                . "Your lesson training has been re-assigned to " . $trainer_details['firstname'] . " " . $trainer_details['lastname'] . ". "
//                                . "You can reach your new trainer on - Telephone (" . $trainer_contact_details['phone_number1'] . "), Email(" . $trainee_contact_details['email'] . "). "
//                                . "If you have any reservations on the allocated trainer please <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=trainer_acceptance&response=decline{$_SESSION['next_transaction_detail_id']}'> Click here </a> to decline the assigned trainer. <br/>"
//                                . "For any enquiries, kindly contact us on:   <br/>"
//                                . "<ul>"
//                                . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
//                                . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
//                                . "</ul>"
//                                . "</body></html>";
//                                    } else {
//                                        $trainee_message = "Hello " . $trainee_details['firstname'] . ", thank you for engaging us for your fitness sessions. Your assigned trainer is " . $trainer_details['firstname'] . " " . $trainer_details['lastname'] . ". You can reach your trainer on - Telephone (" . $trainer_contact_details['phone_number1'] . "), Email(" . $trainee_contact_details['email'] . "). If you have any reservations on the allocated trainer please reply: TESTSMS:TRAINER ACCEPTANCE,DECLINE" . $_SESSION['next_transaction_detail_id'] . " to 21211, else do not reply to this text.";
//$trainee_message = "Hello " . $trainee_details['firstname'] . ", your lesson training has been re-assigned to " . $trainer_details['firstname'] . " " . $trainer_details['lastname'] . ". You can reach your new trainer on - Telephone (" . $trainer_contact_details['phone_number1'] . "), Email(" . $trainee_contact_details['email'] . "). If you have any reservations on the allocated trainer please reply: TESTSMS:TRAINER ACCEPTANCE,DECLINE" . $_SESSION['next_transaction_detail_id'] . " to 21211, else do not reply to this text.";
//                        $trainee_email = $trainee_contact_details['email'];
//                        $trainee_name = $trainee_details['firstname'] . " " . $trainee_details['lastname'];
//                        $trainee_email_subject = "Lesson Assignment Notification";
//                        $trainee_email_message = "<html><body>"
//                                . "<p><b>Hello " . $trainee_details['firstname'] . ",</b><br/>"
//                                . "Thank you for engaging us for your fitness sessions. "
//                                . "Your assigned trainer is " . $trainer_details['firstname'] . " " . $trainer_details['lastname'] . ". "
//                                . "You can reach your trainer on - Telephone (" . $trainer_contact_details['phone_number1'] . "), Email(" . $trainee_contact_details['email'] . "). "
//                                . "If you have any reservations on the allocated trainer please <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=trainer_acceptance&response=decline{$_SESSION['next_transaction_detail_id']}'> Click here </a> to decline the assigned trainer. <br/>"
//                                . "For any enquiries, kindly contact us on:   <br/>"
//                                . "<ul>"
//                                . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
//                                . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
//                                . "</ul>"
//                                . "</body></html>";
//                     }
//                     
//                    $users_management = new Users_Management();
//                    $send_trainer_email = $users_management->sendEmail($trainer_email, $trainer_name, $trainer_email_subject, $trainer_email_message);
//                    $send_trainee_email = $users_management->sendEmail($trainee_email, $trainee_name, $trainee_email_subject, $trainee_email_message);
//                                    $send_trainer_sms = $this->sendSingleSMS($trainer_contact_details['phone_number1'], $trainer_message);
//                                    $send_trainee_sms = $this->sendSingleSMS($trainee_contact_details['phone_number1'], $trainee_message);
//                                    return true;
//                                } else
//                                    return false;
//                            } else
//                                return false;
//                        }
//                    } else
//                        return false;
//                } else {
//                    return false;
//                }
//            }
//        }
//    }

    public function updatePaymentStatus() {
        $sql_update_payment_status = "UPDATE payment_transactions SET transaction_id=:transaction_id, status=:status WHERE transaction_reference=:transaction_reference";
        $stmt_update_payment_status = $this->prepareQuery($sql_update_payment_status);
        $stmt_update_payment_status->bindValue("transaction_reference", $_SESSION['transaction_ref_number']);
        $stmt_update_payment_status->bindValue("transaction_id", $_SESSION["transaction_id"]);
        $stmt_update_payment_status->bindValue("status", 2005);
        if ($stmt_update_payment_status->execute()) {
            return true;
        } else
            return false;
    }

//With manual approving of lesson bookings
    public function addTransactionDetails() {
        $_SESSION['next_transaction_detail_id'] = $this->getNextTransactionDetailId();
        $sql = "INSERT INTO booking_transactions_details (transaction_id, lesson, number_of_classes, unit_price, start_date, training_facility)"
                . " VALUES (:transaction_id, :lesson, :number_of_classes, :unit_price, :start_date, :training_facility)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("transaction_id", $_SESSION["transaction_id"]);
        $stmt->bindValue("lesson", $_SESSION['lesson_details']["id"]);
        $stmt->bindValue("number_of_classes", $_SESSION["number_of_classes"]);
        $stmt->bindValue("unit_price", $_SESSION['lesson_details']["price"]);
        $stmt->bindValue("start_date", $_SESSION["start_date"]);
        $stmt->bindValue("training_facility", $_SESSION['lesson_details']["training_facility"]);
        $stmt->execute();

        return true;
    }

// With auto-approving of lesson bookings
//    public function addTransactionDetails() {
//        $_SESSION['next_transaction_detail_id'] = $this->getNextTransactionDetailId();
//        $sql = "INSERT INTO booking_transactions_details (transaction_id, lesson, number_of_classes, unit_price, start_date, training_facility)"
//                . " VALUES (:transaction_id, :lesson, :number_of_classes, :unit_price, :start_date, :training_facility)";
//        $stmt = $this->prepareQuery($sql);
//        $stmt->bindValue("transaction_id", $_SESSION["transaction_id"]);
//        $stmt->bindValue("lesson", $_SESSION['lesson_details']["id"]);
//        $stmt->bindValue("number_of_classes", $_SESSION["number_of_classes"]);
//        $stmt->bindValue("unit_price", $_SESSION['lesson_details']["price"]);
//        $stmt->bindValue("start_date", $_SESSION["start_date"]);
//        $stmt->bindValue("training_facility", $_SESSION['lesson_details']["training_facility"]);
//        $stmt->execute();
//
//        $this->approveTrainingRequest($_SESSION['next_transaction_detail_id'], $_SESSION["transaction_id"], "Auto-approved");
//
//        return true;
//    }

    public function autoApproveLessonBookingTransaction($code, $transaction_id, $approval_comment, $assigned_trainer, $user_id) {
        $sql_booking_transactions_details = "UPDATE booking_transactions_details SET approvedat=:approvedat, approvedby=:approvedby, approval_comment=:approval_comment, assigned_trainer=:assigned_trainer WHERE count=:code";
        $stmt_booking_transactions_details = $this->prepareQuery($sql_booking_transactions_details);
        $stmt_booking_transactions_details->bindValue("code", $code);
        $stmt_booking_transactions_details->bindValue("approvedat", date("Y-m-d H:i:s"));
        $stmt_booking_transactions_details->bindValue("approvedby", $user_id);
        $stmt_booking_transactions_details->bindValue("approval_comment", strtoupper($approval_comment));
        $stmt_booking_transactions_details->bindValue("assigned_trainer", $assigned_trainer);
        if ($stmt_booking_transactions_details->execute()) {
            return true;
// Check the status of all items in the booking_transactions_details with the associated transaction_id then update the status in the booking_transactions table accordingly
//            $sql_booking_transactions = "UPDATE booking_transactions SET status=1011 WHERE transaction_id=:transaction_id";
//            $stmt_booking_transactions = $this->prepareQuery($sql_booking_transactions);
//            $stmt_booking_transactions->bindValue("transaction_id", $transaction_id);
//            if ($stmt_booking_transactions->execute()) {
//                $users_management = new Users_Management();
//                $trainers = new Trainers();
//                $trainees = new Trainees();
//                $trainer_contact_details = $users_management->fetchIndividualContactDetails($assigned_trainer);
//                $trainer_system_user_details = $users_management->fetchUserDetails($assigned_trainer);
//                $trainer_details = $trainers->fetchTrainerDetails($trainer_system_user_details['reference_id']);
//                $transaction_details = $this->fetchBookingTransactionDetails($transaction_id);
//                $trainee_contact_details = $users_management->fetchIndividualContactDetails($transaction_details['booker_id']);
//                $trainee_system_user_details = $users_management->fetchUserDetails($transaction_details['booker_id']);
//                $trainee_details = $trainees->fetchTraineeDetails($trainee_system_user_details['reference_id']);
//                return true;
//            } else
//                return false;
        } else
            return false;
    }

    public function approveTrainingRequest($code, $transaction_id, $approval_comment) {
        $sql_trainer_approval = "UPDATE booking_transactions_details SET status=:status, trainer_approvedat=:trainer_approvedat, trainer_approval_comment=:approval_comment WHERE count=:code";
        $stmt_trainer_approval = $this->prepareQuery($sql_trainer_approval);
        $stmt_trainer_approval->bindValue("code", $code);
        $stmt_trainer_approval->bindValue("status", 5002);
        $stmt_trainer_approval->bindValue("trainer_approvedat", date("Y-m-d H:i:s"));
        $stmt_trainer_approval->bindValue("approval_comment", strtoupper($approval_comment));
        if ($stmt_trainer_approval->execute()) {
            $users_management = new Users_Management();
            $trainees = new Trainees();
            $transaction_details = $this->fetchBookingTransactionDetails($transaction_id);
            $trainee_contact_details = $users_management->fetchIndividualContactDetails($transaction_details['booker_id']);
            $trainee_system_user_details = $users_management->fetchUserDetails($transaction_details['booker_id']);
            $trainee_details = $trainees->fetchTraineeDetails($trainee_system_user_details['reference_id']);
            $trainee_message = "Hello " . $trainee_details['firstname'] . ", your assigned trainer has approved the training request. Thank you for engaging us for your fitness sessions.";
            $send_trainee_sms = $this->sendSingleSMS($trainee_contact_details['phone_number1'], $trainee_message);
            return true;
        } else
            return false;
    }

    public function closeLessonTraining($code, $transaction_id) {
        $sql = "UPDATE booking_transactions_details SET status=:status WHERE count=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("code", $code);
        $stmt->bindValue("status", 1004);
        if ($stmt->execute()) {
            $users_management = new Users_Management();
            $trainers = new Trainers();
            $trainees = new Trainees();
            $transaction_details = $this->fetchBookingTransactionDetails($transaction_id);
            $transaction_detail_details = $this->fetchBookingTransactionDetailDetails($code);
            $trainer_system_user_details = $users_management->fetchUserDetails($transaction_detail_details['assigned_trainer']);
            $trainer_details = $trainers->fetchTrainerDetails($trainer_system_user_details['reference_id']);
            $trainee_contact_details = $users_management->fetchIndividualContactDetails($transaction_details['booker_id']);
            $trainee_system_user_details = $users_management->fetchUserDetails($transaction_details['booker_id']);
            $trainee_details = $trainees->fetchTraineeDetails($trainee_system_user_details['reference_id']);
            $trainee_message = "Hello " . $trainee_details['firstname'] . ", your training session has been marked as closed. Thank you for choosing to train with us. Please rate trainer " . $trainer_details['firstname'] . " " . $trainer_details['lastname'] . " by replying testsms to 21211.";

            $trainee_email = $trainee_contact_details['email'];
            $trainee_name = $trainee_details['firstname'] . " " . $trainee_details['lastname'];
            $trainee_email_subject = "Lesson Closure Notification";
            $trainee_email_message = "<html><body>"
                    . "<p><b>Hello " . $trainee_details['firstname'] . ",</b><br/>"
                    . "Your training session has been marked as closed. Thank you for choosing to train with us. "
                    . "Please <a href='{$_SESSION['admin_url']}?transaction_interactions&keyword=rate{$code}'> Click here </a> to rate your trainer and the training facility."
                    . "Your feedback is of great importance to us. <br/>"
                    . "For any enquiries, kindly contact us on:   <br/>"
                    . "<ul>"
                    . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                    . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                    . "</ul>"
                    . "</body></html>";

            $users_management = new Users_Management();
            $send_trainee_email = $users_management->sendEmail($trainee_email, $trainee_name, $trainee_email_subject, $trainee_email_message);

//            $trainee_message = "Hello " . $trainee_details['firstname'] . ", your training session has been marked as closed. Thank you for choosing to train with us. Please rate trainer " . $trainer_details['firstname'] . " " . $trainer_details['lastname'] . " by replying : 1* or 2* or 3* or 4* or 5* to 21211.";
            $send_trainee_sms = $this->sendSingleSMS($trainee_contact_details['phone_number1'], $trainee_message);
//            $send_trainee_shortcode_sms = $this->sendShortCodeSMS(substr($trainee_contact_details['phone_number1'], 1), $trainee_message);
            return true;
        } else
            return false;
    }

    private function sendMessage() {
        $sql = "INSERT INTO inbox_messages (name, phone_number, email, subject, message) VALUES(:name, :phone_number, :email, :subject, :message)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("name", strtoupper($_POST['name']));
        $stmt->bindValue("phone_number", strtoupper($_POST['phone_number']));
        $stmt->bindValue("email", strtoupper($_POST['email']));
        $stmt->bindValue("subject", strtoupper($_POST['subject']));
        $stmt->bindValue("message", strtoupper($_POST['message']));
        if ($stmt->execute()) {
            $recipient_email = $_POST['email'];
            $recipient_name = $_POST['name'];
            $email_subject = "Website Enquiry";
            $email_message = "<html><body>"
                    . "<p><b>Hello " . $_POST['name'] . ",</b><br/>"
                    . "Your enquiry on <b>" . $_POST['subject'] . "</b> has been received. We will get back to you in due time. <br/>"
                    . "For any other enquiries, kindly contact us on:   <br/>"
                    . "<ul>"
                    . "<li><b>Telephone Number(s): </b>" . $_SESSION['application_phone'] . "</li>"
                    . "<li><b>Email Address: </b>" . $_SESSION["application_email"] . "</li>"
                    . "</ul>"
                    . "</body></html>";
            $users_management = new Users_Management();
            $send_email = $users_management->sendEmail($recipient_email, $recipient_name, $email_subject, $email_message);
            if ($send_email == true) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function getUserIqPointsGained($user_id) {
        $sql = "SELECT SUM(points) FROM iq_points WHERE user_id=:user_id AND transaction_type=2";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("user_id", $user_id);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data[0]['SUM(points)'];
    }

    public function getUserIqPointsRedeemed($user_id) {
        $sql = "SELECT SUM(points) FROM iq_points WHERE user_id=:user_id AND transaction_type=3";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("user_id", $user_id);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (is_null($data[0]['SUM(points)'])) {
            $data = 0;
        }
        return $data;
    }

    public function getNextTransactionDetailId() {
        $transaction_detail_id = $this->executeQuery("SELECT max(count) as transaction_detail_id_max FROM booking_transactions_details");
        $transaction_detail_id = $transaction_detail_id[0]['transaction_detail_id_max'] + 1;
        return $transaction_detail_id;
    }

    public function fetchBookingTransactionDetails($transaction_id) {
        $sql = "SELECT * FROM booking_transactions WHERE transaction_id=:transaction_id";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("transaction_id", $transaction_id);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

    public function fetchBookingTransactionDetailDetails($code) {
        $sql = "SELECT * FROM booking_transactions_details WHERE count=:code";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindParam("code", $code);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $info[0];
    }

}
