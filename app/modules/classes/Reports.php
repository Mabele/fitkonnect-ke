<?php

class Reports extends Database {

    public function countAllLessonTransactions() {
        $sql = "SELECT * FROM booking_transactions_details";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }

    public function countAllTrainees() {
        $sql = "SELECT * FROM trainees";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }
    
    public function countAllTrainers() {
        $sql = "SELECT * FROM trainers";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }
    
    public function countAllTrainingFacilities() {
        $sql = "SELECT * FROM training_facilities";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }

    public function countAllRedeemableItems() {
        $sql = "SELECT * FROM redeemable_items";
        $stmt = $this->prepareQuery($sql);
        $stmt->execute();
        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return count($info);
    }

    
    
//    public function countAllUnassignedLessonTransactions() {
//        $sql = "SELECT * FROM booking_transactions_details";
//        $stmt = $this->prepareQuery($sql);
//        $stmt->bindValue("status", 2005);
//        $stmt->execute();
//        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
//        return count($info);
//    }
//
//    public function countAllBidsByProject($project_id) {
//        $sql = "SELECT * FROM project_bids WHERE project_id=:project_id AND bid_amount != 0";
//        $stmt = $this->prepareQuery($sql);
//        $stmt->bindValue("project_id", $project_id);
//        $stmt->execute();
//        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
//        return count($info);
//    }
//
//    public function sumAllBidsByProject($project_id) {
//        $sql = "SELECT SUM(bid_amount) as sum FROM project_bids WHERE project_id=:project_id";
//        $stmt = $this->prepareQuery($sql);
//        $stmt->bindValue("project_id", $project_id);
//        $stmt->execute();
//        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
//        return $info[0]['sum'];
//    }
//
//    public function countAllTransactionRecordsByTransactionType($transaction_type) {
//        if ($transaction_type == "ALL") {
//            $sql = "SELECT * FROM transactions";
//            $stmt = $this->prepareQuery($sql);
//        } else {
//            $settings = new Settings();
//            $transaction_type_ref_id = $settings->getTransactionRefTypeId($transaction_type);
//            $sql = "SELECT * FROM transactions WHERE transaction_type=:transaction_type";
//            $stmt = $this->prepareQuery($sql);
//            $stmt->bindValue("transaction_type", $transaction_type_ref_id);
//        }
//        $stmt->execute();
//        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
//        return count($info);
//    }
//
//    public function sumAllTransactionAmountsByTransactionType($transaction_type) {
//        if ($transaction_type == "ALL") {
//            $sql = "SELECT SUM(amount) as sum FROM transactions";
//            $stmt = $this->prepareQuery($sql);
//        } else {
//            $settings = new Settings();
//            $transaction_type_ref_id = $settings->getTransactionRefTypeId($transaction_type);
//            $sql = "SELECT SUM(amount) as sum FROM transactions WHERE transaction_type=:transaction_type";
//            $stmt = $this->prepareQuery($sql);
//            $stmt->bindValue("transaction_type", $transaction_type_ref_id);
//        }
//        $stmt->execute();
//        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
//        return $info[0]['sum'];
//    }
//
//    public function countAllAccountTransactionRecordsByTransactionType($transaction_type, $account) {
//        if ($transaction_type == "ALL") {
//            $sql = "SELECT * FROM transactions WHERE account_number=:account_number";
//            $stmt = $this->prepareQuery($sql);
//            $stmt->bindValue("account_number", $account);
//        } else {
//            $settings = new Settings();
//            $transaction_type_ref_id = $settings->getTransactionRefTypeId($transaction_type);
//            $sql = "SELECT * FROM transactions WHERE  account_number=:account_number AND transaction_type=:transaction_type";
//            $stmt = $this->prepareQuery($sql);
//            $stmt->bindValue("account_number", $account);
//            $stmt->bindValue("transaction_type", $transaction_type_ref_id);
//        }
//        $stmt->execute();
//        $info = $stmt->fetchAll(PDO::FETCH_ASSOC);
//        return count($info);
//    }

}
