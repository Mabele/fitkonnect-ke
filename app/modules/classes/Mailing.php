<?php
$configs = parse_ini_file(WPATH . "core/configs.ini");
use PHPMailer\PHPMailer\PHPMailer;
require 'modules/mailing/vendor/autoload.php';

class Mailing extends Database {

    public function execute() {
        if ($_POST['action'] == "lesson_booking") {
            return $this->sendMessageReceipt();
        } if ($_POST['action'] == "account_setup") {
            return $this->sendMessageAccountActivate();
        } 
    }
    
    //Template function
    public function sendMessageReceipt() {
        $name = strtoupper($_POST['name']);
        //$phone = "+254" . substr($_POST['phone'], -9);
        $email = $_POST['email'];
        $message = implode(', ', $_POST['message']);
        $d_time = date("Y-m-d H:i:s");
        $sql = "INSERT INTO inquiries(name, email, message, d_time) "
                . "VALUES(:name, :email, :message, :d_time)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("name", $name);
        //$stmt->bindValue("phone", $phone);
        $stmt->bindValue("email", $email);
        $stmt->bindValue("message", $message);
        $stmt->bindValue("d_time", $d_time);
        if ($stmt->execute()) {
            $mail = new PHPMailer;                             // Passing `true` enables exceptions
            $mail->isSendmail();
            //Recipients
            $mail->setFrom($_SESSION["MUsername"], $_SESSION["MUsernameFrom"]);
            $mail->addAddress($email, $name);                   // Add a recipient
            //$mail->addBCC($_SESSION["MUsername"], $_SESSION["MUsernameFrom"]);
            //Content
            $mail->Subject = 'Receipt Confirmation';
            $mail->isHTML(true);                                 // Set email format to HTML
            //$myname = 'We are doing Maintenance';
            //$is = ' we will be back';
            $information = array($name, $email, $message, $d_time);
            $replace_information = array('%name%', '%email%', '%message%', '%d_time%');

            $content = str_replace($replace_information, $information, file_get_contents('modules/mailing/order_receipt.html'));

            $mail->msgHTML($content, dirname(__FILE__));
            
            $mail->AltBody = 'Your email is loading.';

            if (!$mail->send()) {
                echo '<div class="error-box">
                      <div class="alert alert-warning">' . $_SESSION["Null_Feedback"] . '</div>
                      </div>';
            } else {
                echo '<div class="success-box">
                      <div class="alert alert-success">' . $_SESSION["Feedback"] . '</div>
                      </div>';
            }
//            return $this->addMessageAdmin();
        } else {
            return false;
        }
    }
    
    //Template function
    public function sendMessageAccountActivate() {
        $name = strtoupper($_POST['name']);
        //$phone = "+254" . substr($_POST['phone'], -9);
        $email = $_POST['email'];
        $message = implode(', ', $_POST['message']);
        $d_time = date("Y-m-d H:i:s");
        $sql = "INSERT INTO inquiries(name, email, message, d_time) "
                . "VALUES(:name, :email, :message, :d_time)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("name", $name);
        //$stmt->bindValue("phone", $phone);
        $stmt->bindValue("email", $email);
        $stmt->bindValue("message", $message);
        $stmt->bindValue("d_time", $d_time);
        if ($stmt->execute()) {
            $mail = new PHPMailer;                             // Passing `true` enables exceptions
            $mail->isSendmail();
            //Recipients
            $mail->setFrom($_SESSION["MUsername"], $_SESSION["MUsernameFrom"]);
            $mail->addAddress($email, $name);     // Add a recipient
            //$mail->addBCC($_SESSION["MUsername"], $_SESSION["MUsernameFrom"]);
            //Content
            $mail->Subject = 'Receipt Confirmation';
            $mail->isHTML(true);                                 // Set email format to HTML
            //$myname = 'We are doing Maintenance';
            //$is = ' we will be back';
            $information = array($name, $email, $message, $d_time);
            $replace_information = array('%name%', '%email%', '%message%', '%d_time%');
            $content = str_replace($replace_information, $information, file_get_contents('modules/mailing/activate_account.html'));
            $mail->msgHTML($content, dirname(__FILE__));
            $mail->AltBody = 'Your email is loading.';

            if (!$mail->send()) {
                echo '<div class="error-box">
                      <div class="alert alert-warning">' . $_SESSION["Null_Feedback"] . '</div>
                      </div>';
            } else {
                echo '<div class="success-box">
                      <div class="alert alert-success">' . $_SESSION["Feedback"] . '</div>
                      </div>';
            }
//            return $this->addMessageAdmin();
        } else {
            return false;
        }
    }
    
     //Template function
    public function sendMessageAdminNotiification() {
        $name = strtoupper($_POST['name']);
        //$phone = "+254" . substr($_POST['phone'], -9);
        $email = $_POST['email'];
        $message = implode(', ', $_POST['message']);
        $d_time = date("Y-m-d H:i:s");
        $sql = "INSERT INTO inquiries(name, email, message, d_time) "
                . "VALUES(:name, :email, :message, :d_time)";
        $stmt = $this->prepareQuery($sql);
        $stmt->bindValue("name", $name);
        //$stmt->bindValue("phone", $phone);
        $stmt->bindValue("email", $email);
        $stmt->bindValue("message", $message);
        $stmt->bindValue("d_time", $d_time);
        if ($stmt->execute()) {
            $mail = new PHPMailer;                             // Passing `true` enables exceptions
            $mail->isSendmail();
            //Recipients
            $mail->setFrom($_SESSION["MUsername"], $_SESSION["MUsernameFrom"]);
            $mail->addAddress($email, $name);     // Add a recipient
            //$mail->addBCC($_SESSION["MUsername"], $_SESSION["MUsernameFrom"]);
            //Content
            $mail->Subject = 'Receipt Confirmation';
            $mail->isHTML(true);                                 // Set email format to HTML
            //$myname = 'We are doing Maintenance';
            //$is = ' we will be back';
            $information = array($name, $email, $message, $d_time);
            $replace_information = array('%name%', '%email%', '%message%', '%d_time%');
            $content = str_replace($replace_information, $information, file_get_contents('modules/mailing/admin_notification.html'));
            $mail->msgHTML($content, dirname(__FILE__));
            $mail->AltBody = 'Your email is loading.';

            if (!$mail->send()) {
                echo '<div class="error-box">
                      <div class="alert alert-warning">' . $_SESSION["Null_Feedback"] . '</div>
                      </div>';
            } else {
                echo '<div class="success-box">
                      <div class="alert alert-success">' . $_SESSION["Feedback"] . '</div>
                      </div>';
            }
//            return $this->addMessageAdmin();
        } else {
            return false;
        }
    }
    
}