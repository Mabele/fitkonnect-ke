<?php
//if (!App::isLoggedIn())
//    App::redirectTo("?");
require_once WPATH . "modules/classes/Lessons.php";
$lessons = new Lessons();
$item_total = 0;

if (isset($_SERVER['HTTP_REFERER'])) {
    $previous_url = $_SERVER['HTTP_REFERER'];
}

if (isset($_SESSION["cart_item"])) {
    $_SESSION["cart_number_of_items"] = count($_SESSION["cart_item"]);
    foreach ($_SESSION["cart_item"] as $item) {
        $item_total += ($item["price"] * $item["number_of_classes"]);
        $_SESSION["cart_total_cost"] = $item_total;
    }
} else {
    $_SESSION["cart_number_of_items"] = 0;
    $_SESSION["cart_total_cost"] = 0;
}

if (!empty($_GET["cart_action"])) {
    switch ($_GET["cart_action"]) {
        case "remove":
            if (!empty($_SESSION["cart_item"])) {
                foreach ($_SESSION["cart_item"] as $k => $v) {
                    if ($_GET["code"] == $v['id'])
                        unset($_SESSION["cart_item"][$k]);
                    if (empty($_SESSION["cart_item"]))
                        unset($_SESSION["cart_item"]);
                }
            }
            break;
        case "empty":
            unset($_SESSION["cart_item"]);
            unset($_SESSION["item_total"]);
            break;
    }
    App::redirectTo("{$previous_url}");
}

if (!empty($_POST) AND $_POST['action'] == "update_cart") {
    if (!empty($_SESSION["cart_item"])) {
        foreach ($_SESSION["cart_item"] as $k => $v) {
            if ($_POST["code"] == $v['id'])
                unset($_SESSION["cart_item"][$k]);
            if (empty($_SESSION["cart_item"]))
                unset($_SESSION["cart_item"]);
        }
    }
    $productByCode = $lessons->fetchLessonDetails($_POST["code"]);
    $itemArray = array($productByCode["id"] => array('id' => $productByCode["id"], 'name' => $productByCode["name"], 'price' => $productByCode["price"], 'number_of_classes' => ($_POST["number_of_classes"] + 1)));

    if (!empty($_SESSION["cart_item"])) {
        if (in_array($productByCode["id"], array_keys($_SESSION["cart_item"]))) {
            foreach ($_SESSION["cart_item"] as $k => $v) {
                if ($productByCode["id"] == $k) {
                    if (empty($_SESSION["cart_item"][$v]["number_of_classes"])) {
                        $_SESSION["cart_item"][$k][$v]["number_of_classes"] = 0;
                    }
                    $_SESSION["cart_item"][$k][$v]["number_of_classes"] += ($_POST["number_of_classes"] + 1);
                }
            }
        } else {
            $_SESSION["cart_item"] = array_merge($_SESSION["cart_item"], $itemArray);
        }
    } else {
        $_SESSION["cart_item"] = $itemArray;
    }
//    App::redirectTo($_POST["previous_url"]);
}

//require_once "core/template/header.php";
?>

<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">My Cart</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li>
                        <a href="./">Home</a>
                    </li>
                    <li class="active">Booking Cart</li>
                    <!--                    <li>
                                            <a href="?lessons_cart"><?php // echo '(' . $_SESSION["cart_number_of_items"] . ') Lessons Booked';   ?> </a>
                                        </li>-->
                </ol>
            </div>
        </div>
    </div>
</section>

<?php
if (isset($_SESSION["cart_number_of_items"]) AND $_SESSION["cart_number_of_items"] == 0) {
    ?>
    <div class="alert alert-block alert-error fade in">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>No lessons added to lessons-cart. <a href="?lesson_booking"> Click here </a> to add lessons to your lessons-cart before proceeding.</strong>
    </div>
    <?php
}
?>

<section class="ls ms section_padding_50 columns_padding_25">
    <div class="container">  

        <?php
        if (isset($_SESSION["cart_item"])) {
            $item_total = 0;
            ?>

            <table>
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Product Name</th>
                        <th>Unit Price</th>
                        <th>Number of Classes</th>
                        <th>Sub-Total (KES)</th>
                    </tr>
                </thead>
                <tbody>    

                    <?php
                    foreach ($_SESSION["cart_item"] as $item) {
                        $sub_item_total = ($item["price"] * $item["number_of_classes"]);
                        $lesson_details = $lessons->fetchLessonDetails($item["id"]);
                        ?>
                        <tr>
                            <td>
                                <a href="?lessons_cart&cart_action=remove&code=<?php echo $item["id"]; ?>" >Remove Item</a>
                            </td>
                            <td>
                                <a href="#"><?php echo $lesson_details['name']; ?></a>					
                            </td>
                            <td>
                                <?php echo "KES " . $lesson_details['price']; ?>				
                            </td>
                    <form method="post">
                        <input type="hidden" name="action" value="update_cart"/>
                        <input type="hidden" name="code" value="<?php echo $item['id']; ?>"/>
                        <input type="hidden" name="previous_url" value="<?php echo $previous_url; ?>"/>
                        <td>
                            <input type="number" name="number_of_classes" placeholder="Number of Classes" max-width="16" value="<?php echo $item["number_of_classes"]; ?>">
        <!--                                    <input type="submit" value="Update Number" name="update_cart" class="button">-->
                        </td>
                    </form>

                    <td>
                        <?php echo "KES " . $sub_item_total; ?>				
                    </td>
                    </tr>
                    <?php
                    $item_total += ($item["price"] * $item["number_of_classes"]);
                }
                ?>
                </tbody>
            </table>
        <?php } ?>

        <h3>Booking Summary</h3>
            <table cellspacing="0" class="table">
                <tbody>
                    <tr>
                        <th>Total Amount</th>
                        <td><strong><?php echo "KES " . $item_total; ?></strong></td>
                    </tr>
                    <tr>
                        <th>IQ Points</th>
                        <td><strong><?php echo "KES " . 0.20 * $item_total; ?></strong></td>
                    </tr>
                </tbody>
            </table>
        <a href="?lessons_cart&cart_action=empty">Empty Lessons Cart</a> | <a href="?lesson_booking">Book more lessons</a> | <a href="?checkout">Proceed to Checkout</a>
    </div>
</section>
