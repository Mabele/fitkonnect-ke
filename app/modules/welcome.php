<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">Welcome to fitness IQ</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li>
                        <a href="./">Home</a>
                    </li>
                    <li class="active">Welcome</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="ls section_padding_top_30 section_padding_bottom_25">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-push-6 col-lg-5 col-lg-push-7">
                <div class="with_corners image_corners topmargin_5 bottommargin_30">
                    <img src="./images/welcome.jpg" alt="">
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 col-lg-7 col-lg-pull-5">
                <div class="text-uppercase">The Fitness IQ</div>
                <h2 class="text-uppercase topmargin_0 bottommargin_150">Welcome</h2>

                <p class="bottommargin_10 entry-excerpt">
                     Together  We Will Create A Plan To Help You Reach Your Goals!
                </p>
                <p class="bottommargin_30">
                    Fitness IQ is about motivating, empowering and energizing you 
                    to reach your fitness goals. Regardless of age, fitness level or 
                    pre existing conditions; we make fitness work for you and ensure you get 
                    fit and tone up in a fun and exciting way. Then, you also get to earn 
                    Fitness IQ for Fitness sessions you complete or get others to complete 
                    through your account with us. You can redeem your Fitness IQ points for 
                    sporting gear, holiday packs and other personal development plans.
                </p>
                <a href="?lesson_booking" class="theme_button color1">Book Lesson</a>
            </div>
        </div>
    </div>
</section>