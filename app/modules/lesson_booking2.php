<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">Lesson Booking</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li>
                        <a href="./">Home</a>
                    </li>
                    <li class="active">Lesson Booking</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="ls ms section_padding_50 columns_padding_25">
    <div class="container"> 
        <div class="row vertical-tabs">
            <div class="col-sm-3">

                <!-- Nav tabs -->
                <ul class="nav" role="tablist">
                    <li class="active">
                        <a href="#vertical-tab1" role="tab" data-toggle="tab">Lesson Category</a>
                    </li>
                    <li>
                        <a href="#vertical-tab2" role="tab" data-toggle="tab">Training Facility</a>
                    </li>
                    <li>
                        <a href="#vertical-tab3" role="tab" data-toggle="tab">Price</a>
                    </li>
                    <li>
                        <a href="#vertical-tab4" role="tab" data-toggle="tab">Start Time</a>
                    </li>
                    <li>
                        <a href="#vertical-tab5" role="tab" data-toggle="tab">Available Capacity</a>
                    </li>
                </ul>

            </div>



            <div class="col-sm-9">
                <div class="tab-content no-border">
                    <div class="tab-pane fade in active" id="vertical-tab1">
                        <div class="col-sm-4">
                            <div class="bg_teaser after_cover color_bg_1">
                                <img src="images/teaser01.jpg" alt="">
                                <div class="teaser_content media">
                                    <div class="teaser text-center">
                                        <div class="teaser_icon highlight2 size_big">
                                            <i class="rt-icon2-stopwatch"></i>
                                        </div>
                                        <h3>{Swimming}</h3>
                                        <p>{Front Crawl}<br/>
                                            {Umoja Recreation}<br/>
                                            <strong class="grey">KES</strong> {2,000} <br/>
                                            <strong class="grey">{10}</strong> persons <br/>
                                            <!-- Time should be slected after the client books -->
    <!--                                        <strong class="grey">Start time</strong> {12:00H} <br/>
                                            <strong class="grey">End Time</strong> {16:00H} <br/>-->
                                            <a href="#" class="theme_button color2">Book Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="bg_teaser after_cover color_bg_2">
                                <img src="images/teaser01.jpg" alt="">
                                <div class="teaser_content media">
                                    <div class="teaser text-center">
                                        <div class="teaser_icon highlight2 size_big">
                                            <i class="rt-icon2-stopwatch"></i>
                                        </div>
                                        <h3>{Swimming}</h3>
                                        <p>{Front Crawl}<br/>
                                            {Umoja Recreation}<br/>
                                            <strong class="grey">KES</strong> {2,000} <br/>
                                            <strong class="grey">{10}</strong> persons <br/>
                                            <!-- Time should be slected after the client books -->
    <!--                                        <strong class="grey">Start time</strong> {12:00H} <br/>
                                            <strong class="grey">End Time</strong> {16:00H} <br/>-->
                                            <a href="#" class="theme_button color2">Book Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="bg_teaser after_cover color_bg_3">
                                <img src="images/teaser01.jpg" alt="">
                                <div class="teaser_content media">
                                    <div class="teaser text-center">
                                        <div class="teaser_icon highlight2 size_big">
                                            <i class="rt-icon2-stopwatch"></i>
                                        </div>
                                        <h3>{Swimming}</h3>
                                        <p>{Front Crawl}<br/>
                                            {Umoja Recreation}<br/>
                                            <strong class="grey">KES</strong> {2,000} <br/>
                                            <strong class="grey">{10}</strong> persons <br/>
                                            <!-- Time should be slected after the client books -->
    <!--                                        <strong class="grey">Start time</strong> {12:00H} <br/>
                                            <strong class="grey">End Time</strong> {16:00H} <br/>-->
                                            <a href="#" class="theme_button color2">Book Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade in" id="vertical-tab2">
                        <div class="col-sm-4">
                            <div class="bg_teaser after_cover color_bg_2">
                                <img src="images/teaser01.jpg" alt="">
                                <div class="teaser_content media">
                                    <div class="teaser text-center">
                                        <div class="teaser_icon highlight2 size_big">
                                            <i class="rt-icon2-stopwatch"></i>
                                        </div>
                                        <h3>{Swimming}</h3>
                                        <p>{Front Crawl}<br/>
                                            {Umoja Recreation}<br/>
                                            <strong class="grey">KES</strong> {2,000} <br/>
                                            <strong class="grey">{10}</strong> persons <br/>
                                            <!-- Time should be slected after the client books -->
    <!--                                        <strong class="grey">Start time</strong> {12:00H} <br/>
                                            <strong class="grey">End Time</strong> {16:00H} <br/>-->
                                            <a href="#" class="theme_button color2">Book Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="bg_teaser after_cover color_bg_2">
                                <img src="images/teaser01.jpg" alt="">
                                <div class="teaser_content media">
                                    <div class="teaser text-center">
                                        <div class="teaser_icon highlight2 size_big">
                                            <i class="rt-icon2-stopwatch"></i>
                                        </div>
                                        <h3>{Swimming}</h3>
                                        <p>{Front Crawl}<br/>
                                            {Umoja Recreation}<br/>
                                            <strong class="grey">KES</strong> {2,000} <br/>
                                            <strong class="grey">{10}</strong> persons <br/>
                                            <!-- Time should be slected after the client books -->
    <!--                                        <strong class="grey">Start time</strong> {12:00H} <br/>
                                            <strong class="grey">End Time</strong> {16:00H} <br/>-->
                                            <a href="#" class="theme_button color2">Book Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="bg_teaser after_cover color_bg_2">
                                <img src="images/teaser01.jpg" alt="">
                                <div class="teaser_content media">
                                    <div class="teaser text-center">
                                        <div class="teaser_icon highlight2 size_big">
                                            <i class="rt-icon2-stopwatch"></i>
                                        </div>
                                        <h3>{Swimming}</h3>
                                        <p>{Front Crawl}<br/>
                                            {Umoja Recreation}<br/>
                                            <strong class="grey">KES</strong> {2,000} <br/>
                                            <strong class="grey">{10}</strong> persons <br/>
                                            <!-- Time should be slected after the client books -->
    <!--                                        <strong class="grey">Start time</strong> {12:00H} <br/>
                                            <strong class="grey">End Time</strong> {16:00H} <br/>-->
                                            <a href="#" class="theme_button color2">Book Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="https://www.facebook.com/events/1840804329297312/" target="_blank">
            <div class="teaser table_section main_bg_color" style="background-image: url('images/promos/Naivasha-Challenge.png'); background-size: cover; background-repeat: no-repeat;">
                <div class="row">
                    <div class="col-sm-5">
                        <h3 class="text-md-right" style="color: #ffffff;">The Naivasha Challenge</h3>
                    </div>

                    <div class="col-sm-1 text-center">
                        <div class="teaser_icon border_icon size_big round">
                            <i class="rt-icon2-clock3"></i>
                        </div>
                    </div>

                    <div class="col-sm-5">
                        <p>
                            Bookings ongoing. Click here for details 
                        </p>
                    </div>
                </div>
            </div>
        </a>
    </div>
</section>
