<?php
require_once WPATH . "modules/classes/Trainers.php";
require_once WPATH . "modules/classes/Trainees.php";
require_once WPATH . "modules/classes/Training_Facilities.php";
require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/Feedback.php";
$feedback = new Feedback();
$users_management = new Users_Management();
$trainers = new Trainers();
$trainees = new Trainees();
$training_facilities = new Training_Facilities();

if (!empty($_POST)) {
    if ($users_management->checkIfUserEmailExists($_POST['email']) == false AND $users_management->checkIfUsernameExists($_POST['username']) == false) {
        $filename1 = md5('' . $trainees->randomString(10) . time());
        $curriculum_vitae_name = $_FILES['curriculum_vitae']['name'];
        $tmp_name_curriculum_vitae = $_FILES['curriculum_vitae']['tmp_name'];
        $curriculum_vitae_type = $_FILES['curriculum_vitae']['type'];
        $extension_curriculum_vitae = substr($curriculum_vitae_name, strpos($curriculum_vitae_name, '.') + 1);
        $curriculum_vitae = strtoupper($filename1 . '.' . $extension_curriculum_vitae);
        $_SESSION['curriculum_vitae_filename'] = $curriculum_vitae;
        $location1 = 'images/trainers/CVs/';

        $filename2 = md5('' . $trainees->randomString(10) . time());
        $certificate_name = $_FILES['certificate']['name'];
        $tmp_name_certificate = $_FILES['certificate']['tmp_name'];
        $certificate_type = $_FILES['certificate']['type'];
        $extension_certificate = substr($certificate_name, strpos($certificate_name, '.') + 1);
        $certificate = strtoupper($filename2 . '.' . $extension_certificate);
        $_SESSION['certificate_filename'] = $certificate;
        $location2 = 'images/trainers/certificates/';

        $filename3 = md5($trainees->randomString(10) . time());
        $prof_picture_name = $_FILES['prof_picture']['name'];
        $tmp_prof_picture = $_FILES['prof_picture']['tmp_name'];
        $prof_picture_type = $_FILES['prof_picture']['type'];
        $extension_prof_picture = substr($prof_picture_name, strpos($prof_picture_name, '.') + 1);
        $prof_picture = strtoupper($filename3 . '.' . $extension_prof_picture);
        $_SESSION['prof_picture_filename'] = $prof_picture;
        $location3 = 'images/trainers/profile_pictures/';

        $url = $_SESSION['admin_url'] . '?website_requests&';

        $curriculum_vitae_file = new CURLFile($tmp_name_curriculum_vitae, $curriculum_vitae_type, $curriculum_vitae_name);
        $certificate_file = new CURLFile($tmp_name_certificate, $certificate_type, $certificate_name);
        $prof_picture_file = new CURLFile($tmp_prof_picture, $prof_picture_type, $prof_picture_name);
        $data = array("curriculum_vitae_attachment" => $curriculum_vitae_file, "certificate_attachment" => $certificate_file, "prof_picture_attachment" => $prof_picture_file, "curriculum_vitae_name" => $curriculum_vitae, "certificate_name" => $certificate, "prof_picture_name" => $prof_picture);

        $curl_session = curl_init();
        curl_setopt($curl_session, CURLOPT_URL, $url);
        curl_setopt($curl_session, CURLOPT_POST, true);
        curl_setopt($curl_session, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl_session);
        curl_close($curl_session);

        if ($response == true) {
            if (move_uploaded_file($tmp_name_curriculum_vitae, $location1 . $curriculum_vitae) AND move_uploaded_file($tmp_name_certificate, $location2 . $certificate) AND move_uploaded_file($tmp_prof_picture, $location3 . $prof_picture)) {
                $success = $trainers->execute();
                if (is_bool($success) && $success == true) {
                    $title = "Fantastic";
                    $message = "Your registration has been effected successfully.";
                    echo $feedback->successFeedback($title, $message);
                    App::redirectTo("?welcome_coach");
                } else {
                    $title = "Hey,";
                    $message = "Error! There was an error effecting your registration. Please try again.";
                    echo $feedback->errorFeedback($title, $message);
                }
            } else {
                $title = "Oops,";
                $message = "There was an error uploading your attachments. Please try registering again.";
                echo $feedback->errorFeedback($title, $message);
            }
        }
    } else {
        if ($users_management->checkIfUserEmailExists($_POST['email']) == true AND $users_management->checkIfUsernameExists($_POST['username']) == true) {
            $title = "Oops,";
            $message = "Seems someone already registered with your preferred username and email address. Please register using a different set of username and email address.";
            echo $feedback->errorFeedback($title, $message);
        }
        if ($users_management->checkIfUserEmailExists($_POST['email']) == true AND $users_management->checkIfUsernameExists($_POST['username']) == false) {
            $title = "Oops,";
            $message = "Seems someone already registered with your preferred email address. Please register using a different email address.";
            echo $feedback->errorFeedback($title, $message);
        }
        if ($users_management->checkIfUserEmailExists($_POST['email']) == false AND $users_management->checkIfUsernameExists($_POST['username']) == true) {
            $title = "Oops,";
            $message = "Seems someone already registered with your preferred username. Please register using a different username.";
            echo $feedback->errorFeedback($title, $message);
        }
    }
}
?>

<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">Trainer Application</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li>
                        <a href="./">Home</a>
                    </li>
                    <li>
                        <a href="?careers">Careers</a>
                    </li>
                    <li class="active">Trainer Application</li>
                </ol>
            </div>
        </div>
    </div>
</section>


<section class="ls section_padding_top_100 section_padding_bottom_100">
    <div class="container">
        <div class="row">
            <form class="client-register" role="form" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="action" value="add_trainer"/>
                <input type="hidden" name="user_type" value="TRAINER">
                <div class="col-sm-3">
                    <div class="form-group validate-required" id="firstname">
                        <label for="firstname" class="control-label">
                            <span class="grey">First Name </span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="firstname" id="firstname" placeholder="" value="" required="yes">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group validate-required" id="lastname">
                        <label for="lastname" class="control-label">
                            <span class="grey">Last Name </span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="lastname" id="lastname" placeholder="" value="" required="yes">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group validate-required" id="username">
                        <label for="gender" class="control-label">
                            <span class="grey">Gender </span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="gender" id="gender" required="yes">
                            <?php include '../snippets/gender.php'; ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group validate-required" id="idnumber">
                        <label for="idnumber" class="control-label">
                            <span class="grey">ID/Passport Number </span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="idnumber" id="idnumber" placeholder="ID/Passport Number" required="yes">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="day" class="control-label">
                            <span class="grey" title="Used to determine age, for safety purposes">Day of Birth</span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="day" id="day" required="yes">
                            <?php include '../snippets/day.php'; ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="month" class="control-label">
                            <span class="grey" title="Used to determine age, for safety purposes">Month of Birth </span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="month" id="month" required="yes">
                            <?php include '../snippets/month.php'; ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="year" class="control-label">
                            <span class="grey" title="Used to determine age, for safety purposes">Year of Birth </span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="year" id="year" required="yes">
                            <?php include '../snippets/year.php'; ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group validate-required validate-email" id="email">
                        <label for="email" class="control-label">
                            <span class="grey">Email Address </span>
                            <span class="required">*</span>
                        </label>
                        <input type="email" class="form-control " name="email" id="email" placeholder="email" value="" required="yes">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group validate-required validate-phone" id="phone_number">
                        <label for="phone" class="control-label">
                            <span class="grey">Phone Number </span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="phone_number" id="phone_number" required="yes">
                    </div>
                </div> 
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="training_level" class="control-label">
                            <span class="grey" title="What is your highest level of education?">Your highest level of education?</span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="training_level">
                            <option value="CERTIFICATE">CERTIFICATE</option>
                            <option value="DIPLOMA">DIPLOMA</option>
                            <option value="GRADUATE">GRADUATE</option>
                            <option value="POST-GRADUATE">POST-GRADUATE</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="certificate" class="control-label">
                            <span class="grey" title="Attach Training Certificate. Only pdf allowed">Attach Certificate</span>
                            <span class="required">*</span>
                        </label>
                        <input type="file" class="form-control " name="certificate" placeholder="Training Certificate" required="yes">
                    </div>
                </div>
                <div class="col-lg-3">                    
                    <div class="form-group">
                        <label for="curriculum_vitae" class="control-label">
                            <span class="grey" title="Attach Curriculum Vitae. Only pdf allowed">Attach Curriculum Vitae</span>
                            <span class="required">*</span>
                        </label>
                        <input type="file" class="form-control " name="curriculum_vitae" placeholder="Curriculum Vitae" required="yes">
                    </div>
                </div> 
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="prof_picture" class="control-label">
                            <span class="grey" title="Attach your certificates. Only .jpeg or .png allowed">Passport Size Photo</span>
                            <span class="required">*</span>
                        </label>
                        <input type="file" class="form-control " name="prof_picture" placeholder="Profile Picture" required="yes">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="referee" class="control-label">
                            <span class="grey" title="Referee">Referee</span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="referee" id="referee" />
                        <!--<select class="form-control" name="referee" required="yes">
                        <?php //$trainers->getTrainers();  ?>
                        </select>-->
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="preferred_facility" class="control-label">
                            <span class="grey" title="Select your preferred facility">Preferred Facility</span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="preferred_facility" id="preferred_facility"/>
                        <!--<select class="form-control" name="preferred_facility" required="yes">
                        <?php //$training_facilities->getTrainingFacilities();  ?>
                        </select>-->
                    </div>
                </div>             
                <div class="col-lg-3">
                    <div class="form-group validate-required" id="username">
                        <label for="username" class="control-label">
                            <span class="grey">Username </span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="username" id="username" placeholder="" required="">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group validate-required" id="password">
                        <label for="password" class="control-label">
                            <span class="grey">Password </span>
                            <span class="required">*</span>
                        </label>
                        <input type="password" class="form-control " name="password" id="password" required="" required="yes">
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <input type="checkbox" name="terms_and_conditions" id="terms_and_conditions" value="Yes" required=""/> 
                        <label for="terms_and_conditions"> 
                            I accept FitKonnect's 
                            <a href="?tc">terms and conditions</a>
                        </label>
                    </div>
                </div>
                <div class="col-sm-12">
                    <button type="submit" class="theme_button wide_button color1">Complete Application</button>
                    <button type="reset" class="theme_button wide_button">Clear Form</button>
                </div>
            </form>
        </div>

    </div>
</div>
</section>