<section class="ds section_padding_90 page_testimonials parallax">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="flexslider">

                    <div class="container">
                        <div class="row bottommargin_15">
                            <div class="col-sm-12 text-center">
                                <div class="cornered-heading center-heading">
                                    <span class="text-uppercase">Our Services</span>
                                    <h2 class="text-uppercase">Looking for a <br/>Fitness Coach/Trainer near You?</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">

                                <ul class="slides">
                                    <li>
                                        <div class="row">
                                            <div class="col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-2">
                                            </div>
                                            <a href="?signup">
                                            <div class="col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-4">
                                                <blockquote class="blockquote-vertical with_bottom_border ls topmargin_50">
                                                    <img src="./images/faces/1.jpg" width="250" alt=""> Loves giving solutions to real world problems. Experienced in provision.
                                                    <hr class="divider_3_60 main_bg_color">
                                                    <div class="blockquote-meta">
                                                        <h5>I Need a Trainer/Coach </h5>
                                                    </div>
                                                </blockquote>
                                            </div>
                                            </a>
                                            <a href="?coach_application">
                                            <div class="col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-4">
                                                <blockquote class="blockquote-vertical with_bottom_border ls topmargin_50">
                                                    <img src="./images/faces/1.jpg" width="250" alt=""> Been involved in several projects around the globe in different positions.
                                                    <hr class="divider_3_60 main_bg_color">
                                                    <div class="blockquote-meta">
                                                        <h5>I am a Trainer/Coach</h5>
                                                    </div>
                                                </blockquote>
                                            </div>
                                            </a>
                                            <div class="col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-2">
                                        </div>
                                        </div>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>

                </div>
                <!-- eof flexslider -->

            </div>
        </div>
    </div>
</section>