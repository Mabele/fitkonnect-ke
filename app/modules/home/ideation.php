<?php
require_once WPATH . "modules/classes/Transactions.php";
$transactions = new Transactions();
?>
<section class="cs section_padding_110 services">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-6">
                <div class="cornered-heading bottommargin_60">
                    <span class="text-uppercase">INSPIRING HEALTHY LIVING</span>
                    <h2 class="text-uppercase">THROUGH FITNESS</h2>
                </div>            
                    <div class="row columns_margin_0 service-teasers-row">
                        <a href="?signin">
                            <div class="col-sm-4 col-xs-6 to_animate" data-animation="pullUp">
                                <div class="service-teaser with_corners hover_corners small_corners topmargin_30">
                                    <div class="teaser_content">
                                        <div class="teaser">
                                            <div class="teaser_icon grey size_normal">
                                                <i class="flaticon-magnifying-glass"></i>
                                            </div>
                                            <p class="text-uppercase raleway bold darklinks bottommargin_0">
                                                <span class="x-label">Book Appointment</span>
                                            </p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="?trainers">
                            <div class="col-sm-4 col-xs-6 to_animate" data-animation="pullUp">
                                <div class="service-teaser with_corners hover_corners small_corners topmargin_30">
                                    <div class="teaser_content">
                                        <div class="teaser">
                                            <div class="teaser_icon grey size_normal">
                                                <i class="flaticon-wrench"></i>
                                            </div>
                                            <p class="text-uppercase raleway bold darklinks bottommargin_0">
                                                <span class="x-label">Available Trainers</span>
                                            </p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="?location">
                            <div class="col-sm-4 col-xs-6 to_animate" data-animation="pullUp">
                                <div class="service-teaser with_corners hover_corners small_corners topmargin_30">
                                    <div class="teaser_content">
                                        <div class="teaser">
                                            <div class="teaser_icon grey size_normal">
                                                <i class="flaticon-global"></i>
                                            </div>
                                            <p class="text-uppercase raleway bold darklinks bottommargin_0">
                                                <span class="x-label">Available Locations</span>
                                            </p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="?my_transactions">
                            <div class="col-sm-4 col-xs-6 to_animate" data-animation="pullUp">
                                <div class="service-teaser with_corners hover_corners small_corners topmargin_30">
                                    <div class="teaser_content">
                                        <div class="teaser">
                                            <div class="teaser_icon grey size_normal">
                                                <i class="flaticon-windmill"></i>
                                            </div>
                                            <p class="text-uppercase raleway bold darklinks bottommargin_0">
                                                <span class="x-label">My Progress</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="?legacy">
                            <div class="col-sm-4 col-xs-6 to_animate" data-animation="pullUp">
                                <div class="service-teaser with_corners hover_corners small_corners topmargin_30">
                                    <div class="teaser_content">
                                        <div class="teaser">
                                            <div class="teaser_icon grey size_normal">
                                                <i><?php //echo $transactions->getUserIqPointsGained($_SESSION['user_id']) - $transactions->getUserIqPointsRedeemed($_SESSION['user_id']); ?></i>
                                            </div>
                                            <p class="text-uppercase raleway bold darklinks bottommargin_0">
                                                <span class="x-label">Legacy Points</span>                                        
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="?shop">
                                <div class="col-sm-4 col-xs-6 to_animate" data-animation="pullUp">
                                    <div class="service-teaser with_corners hover_corners small_corners topmargin_30">
                                        <div class="teaser_content">
                                            <div class="teaser">
                                                <div class="teaser_icon grey size_normal">
                                                    <i class="flaticon-global"></i>
                                                </div>
                                                <p class="text-uppercase raleway bold darklinks bottommargin_0">
                                                    <span class="x-label">FitKonnectKE Shop</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
            </div>
        </div>
</section>