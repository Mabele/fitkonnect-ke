<?php
require_once WPATH . "modules/classes/Reports.php";
$reports = new Reports();
?>
<section class="ds section_padding_50 counters_section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="teaser info-teaser text-center">
                    <span class="info-icon info-project teaser_icon"></span>
                    <br>
                    <span class="counter highlight" data-from="0" data-to="<?php echo $reports->countAllLessonTransactions(); ?>" data-speed="1500">0</span>
                    <p class="bold text-uppercase">Completed Lessons</p>
                </div>
                <div class="teaser info-teaser text-center">
                    <span class="info-icon info-project teaser_icon"></span>
                    <br>
                    <span class="counter highlight" data-from="0" data-to="<?php echo $reports->countAllTrainees(); ?>" data-speed="1500">0</span>
                    <p class="bold text-uppercase">Happy Clients</p>
                </div>
                <div class="teaser info-teaser text-center">
                    <span class="info-icon info-project teaser_icon"></span>
                    <br>
                    <span class="counter highlight" data-from="0" data-to="<?php echo $reports->countAllTrainers(); ?>" data-speed="80">0</span>
                    <p class="bold text-uppercase">Trainers</p>
                </div>
                <div class="teaser info-teaser text-center">
                    <span class="info-icon info-project teaser_icon"></span>
                    <br>
                    <span class="counter highlight" data-from="0" data-to="<?php echo $reports->countAllTrainingFacilities(); ?>" data-speed="35">0</span>
                    <p class="bold text-uppercase">Training Facilities</p>
                </div>
                <div class="teaser info-teaser text-center">
                    <span class="info-icon info-project teaser_icon"></span>
                    <br>
                    <span class="counter highlight" data-from="0" data-to="<?php // echo $reports->countAllRedeemableItems(); ?>" data-speed="20">0</span>
                    <p class="bold text-uppercase">Redeemable Items</p>
                </div>
            </div>
        </div>
    </div>
</section>