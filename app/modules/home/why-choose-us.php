<section class="ls section_padding_110 columns_margin_0">
    <div class="container">
        <div class="row">
            <div class="col-md-4 text-center text-md-left to_animate" data-animation="fadeInLeft">
                <div class="cornered-heading bottommargin_60">
                    <h2 class="text-uppercase">Why Choose FitKonnect KE?</h2>
                </div>
                <p>We offer quality fitness training and sports coaching services in different disciplines. Our trainers and coaches are all
certified and undergo vigorous training to enhance their capacity over time</p>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="teaser media inline-block topmargin_50 features-teaser to_animate" data-animation="pullDown">
                    <div class="media-left media-middle">
                        <div class="teaser_icon fontsize_36 grey with_background">
                            <i class="flaticon-glasses"></i>
                        </div>
                    </div>
                    <div class="media-body media-middle text-left">
                        <h5 class="text-uppercase">
                            <a href="#">Learn to Swim</a>
                        </h5>
                        <p>Together We Will Create A Plan To Help You Reach Your Goals!</p>
                    </div>
                </div>
                <div class="teaser media inline-block topmargin_50 features-teaser to_animate" data-animation="pullDown">
                    <div class="media-left media-middle">
                        <div class="teaser_icon fontsize_36 grey with_background">
                            <i class="flaticon-light-bulb"></i>
                        </div>
                    </div>
                    <div class="media-body media-middle text-left">
                        <h5 class="text-uppercase">
                            <a href="#">Competitive Swimming</a>
                        </h5>
                        <p>Our curriculum is carefully curated to meet customised needs. We offer lessons to individuals and groups from the age of 2 yrs.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="teaser media inline-block topmargin_50 features-teaser to_animate" data-animation="pullDown">
                    <div class="media-left media-middle">
                        <div class="teaser_icon fontsize_36 grey with_background">
                            <i class="flaticon-plug"></i>
                        </div>
                    </div>
                    <div class="media-body media-middle text-left">
                        <h5 class="text-uppercase">
                            <a href="#">Aerobics</a>
                        </h5>
                        <p>Our trainers have a clear career growth streak. Your experience is world-class.</p>
                    </div>
                </div>
                <div class="teaser media inline-block topmargin_50 features-teaser to_animate" data-animation="pullDown">
                    <div class="media-left media-middle">
                        <div class="teaser_icon fontsize_36 grey with_background">
                            <i class="flaticon-social"></i>
                        </div>
                    </div>
                    <div class="media-body media-middle text-left">
                        <h5 class="text-uppercase">
                            <a href="#">Water Aerobics</a>
                        </h5>
                        <p>Get rewarded for being a Fitness Enthusiast. Earn points for every individual and group referral.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>