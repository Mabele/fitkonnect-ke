<?php
if (!App::isLoggedIn())
    App::redirectTo("?");
require_once WPATH . "modules/classes/Transactions.php";
require_once WPATH . "modules/classes/System_Administration.php";
$system_administration = new System_Administration();
$transactions = new Transactions();

unset($_SESSION['lesson_booking']);
?>
<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">My Transactions History</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li><a href="./">Home</a></li>
                    <li><a href="?trainee_transactions">My Transactions History</a></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="ls ms section_padding_top_100 section_padding_bottom_75 columns_padding_25">
    <div class="container">
        <div class="row">

            <div class="cart-collaterals">
                <div class="cart_totals">
                    <table class="table table-condensed">                        

                        
                            <thead>
                                <tr>
                                    <td><strong>#</strong></td>
                                    <td><strong>Transaction ID</strong></td>
                                    <td><strong>Transaction Type</strong></td>  
                                    <td><strong>Amount</strong></td>                                 
                                    <td><strong>Payment Option</strong></td>                                 
                                    <td><strong>Payment Status</strong></td> 
                                    <td><strong>Lesson Status</strong></td> 
                                    <td><strong>Creation Date</strong></td> 
                                </tr>
                            </thead> 

                        <tbody>
                            <?php
                            $count = 1;
//                        if (!empty($_POST)) {
//                            $lesson_booking_transactions[] = $transactions->execute();
//                        } else {
                            $lesson_booking_transactions[] = $transactions->getAllTraineeTransactions($_SESSION["user_id"]);
//                        }
                            if ($_SESSION['logged_in_user_type_details']['name'] == "TRAINEE" AND ( isset($_SESSION['no_records']) AND $_SESSION['no_records'] == true)) {
                                echo "<tr>";
                                echo "<td>  No record found...</td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "<td> </td>";
                                echo "</tr>";
                                unset($_SESSION['no_records']);
                            } else if (isset($_SESSION['yes_records']) AND $_SESSION['yes_records'] == true) {
                                foreach ($lesson_booking_transactions as $key => $value) {
                                    $inner_array[$key] = json_decode($value, true); // this will give key val pair array
                                    foreach ((array) $inner_array[$key] as $key2 => $value2) {
                                        $payment_status_details = $system_administration->fetchSystemStatusDetails($value2['payment_status']);
                                        $lesson_status_details = $system_administration->fetchSystemStatusDetails($value2['status']);
                                        $payment_status = $payment_status_details['display_value'];
                                        $lesson_status = $lesson_status_details['display_value'];

                                        echo "<tr>";
                                        echo "<td>" . $count++ . "</td>";
                                        echo "<td> <a href='?individual_transaction_details&code=" . $value2['transaction_id'] . "'>" . $value2['transaction_id'] . "</td>";
                                        echo "<td>" . $value2['transaction_type'] . "</td>";
                                        echo "<td>" . $value2['amount'] . "</td>";
                                        echo "<td>" . $value2['payment_option'] . "</td>";
                                        echo "<td>" . $payment_status . "</td>";
                                        echo "<td>" . $lesson_status . "</td>";
                                        echo "<td>" . $value2['createdat'] . "</td>";
                                        echo "</tr>";
                                    }
                                }
                                unset($_SESSION['yes_records']);
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>