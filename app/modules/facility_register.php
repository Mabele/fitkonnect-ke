<section id="breadcrumbs" class="page_breadcrumbs ds parallax section_padding_65 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <h1 class="cornered-heading">Facility Registration</h1>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <ol class="breadcrumb">
                    <li>
                        <a href="./">Home</a>
                    </li>
                    <li class="active">Facility Registration</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="ls section_padding_top_100 section_padding_bottom_100">
    <div class="container">
        <div class="row">
            <form class="facility-register" role="form">
                <div class="col-sm-6">
                    <div class="form-group validate-required" id="name">
                        <label for="name" class="control-label">
                            <span class="grey">Name </span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="name" id="name" placeholder="" value="">
                    </div>
                    
                    <div class="form-group validate-required" id="services_offered">
                        <label for="services_offered" class="control-label">
                            <span class="grey">Services Offered</span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="services_offered" id="services_offered" >
                            <?php //include 'modules/snippets/day.php'; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group validate-required" id="coordinates">
                        <label for="coordinates" class="control-label">
                            <span class="grey">Coordinates </span>
                            <span class="required">*</span>
                        </label>
                        <input type="text" class="form-control " name="coordinates" id="coordinates" placeholder="" value="">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group validate-required validate-email" id="ownership_type">
                        <label for="ownership_type" class="control-label">
                            <span class="grey">Ownership Type</span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="ownership_type" id="ownership_type" >
                            <?php //include 'modules/snippets/day.php'; ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group validate-required" id="facility_rating">
                        <label for="facility_rating" class="control-label">
                            <span class="grey">Facility Rating </span>
                            <span class="required">*</span>
                        </label>
                        <select class="form-control" name="facility_rating" id="facility_rating" >
                            <?php include 'modules/snippets/rating.php'; ?>
                        </select>

                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group validate-required"" id="description">
                        <label for="description" class="control-label">
                            <span class="grey">Description </span>
                            <span class="required">*</span>
                        </label>
                        <textarea rows="5" class="form-control " name="description" id="description" placeholder="" value=""></textarea>
                    </div>
                </div>
                <div class="col-sm-12">
                    <button type="submit" class="theme_button wide_button color1">Register Now</button>
                    <button type="reset" class="theme_button wide_button">Clear Form</button>
                </div>
            </form>
        </div>

    </div>
</div>
</section>