
<div class="widget widget_recent_entries">
    <h3 class="widget-title">Recent posts</h3>
    <ul class="media-list">
        <li class="media">
            <a class="media-left" href="blog-single-right.html">
                <img class="media-object" src="images/recent_post1.jpg" alt="">
            </a>
            <div class="media-body media-middle">
                <h5 class="text-uppercase">
                    <a href="blog-single-right.html">Flank andouille fatback short</a>
                </h5>
                <span class="date">
                    <time datetime="2016-08-01T15:05:23+00:00" class="entry-date">
                        18/08/2016
                    </time>
                </span>
            </div>
        </li>

        <li class="media">
            <a class="media-left" href="blog-single-right.html">
                <img class="media-object" src="images/recent_post2.jpg" alt="">
            </a>
            <div class="media-body media-middle">
                <h5 class="text-uppercase">
                    <a href="blog-single-right.html">Flank andouille fatback short</a>
                </h5>
                <span class="date">
                    <time datetime="2016-08-01T15:05:23+00:00" class="entry-date">
                        18/08/2016
                    </time>
                </span>

            </div>
        </li>

        <li class="media">
            <a class="media-left" href="blog-single-right.html">
                <img class="media-object" src="images/recent_post3.jpg" alt="">
            </a>
            <div class="media-body media-middle">
                <h5 class="text-uppercase">
                    <a href="blog-single-right.html">Flank andouille fatback short</a>
                </h5>
                <span class="date">
                    <time datetime="2016-08-01T15:05:23+00:00" class="entry-date">
                        18/08/2016
                    </time>
                </span>

            </div>
        </li>

    </ul>
</div>

<div class="widget widget_tag_cloud">

    <h3 class="widget-title">Tags widget</h3>
    <div class="tagcloud greylinks">
        <a href="#" title="">Working Process</a>
        <a href="#" title="">Solar system</a>
        <br>
        <a href="#" title="">Enviroment</a>
        <a href="#" title="">Heat pumps</a>
        <br>
        <a href="#" title="">Wind</a>
        <a href="#" title="">Turbines</a>
    </div>
</div>

<div class="widget widget_twitter">
    <h3 class="widget-title">Twitter widget</h3>
    <div class="twitter"></div>
</div>

<div class="widget widget_calendar">


    <div id="calendar_wrap">
        <table id="wp-calendar">
            <caption>Aug 2016</caption>
            <thead>
                <tr>
                    <th scope="col" title="Monday">Mon</th>
                    <th scope="col" title="Tuesday">Tue</th>
                    <th scope="col" title="Wednesday">Wed</th>
                    <th scope="col" title="Thursday">Thu</th>
                    <th scope="col" title="Friday">Fri</th>
                    <th scope="col" title="Saturday">Sat</th>
                    <th scope="col" title="Sunday">Sun</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="3" id="prev">
                        <a href="blog-right.html" data-toggle="tooltip" title="Prev">Jan</a>
                    </td>
                    <td class="pad">&nbsp;</td>
                    <td colspan="3" id="next">
                        <a href="blog-right.html" data-toggle="tooltip" title="Next">Mar</a>
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <tr>
                    <td class="not-cur-mounth">31</td>
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                    <td>5</td>
                    <td>6</td>
                </tr>
                <tr>
                    <td>7</td>
                    <td>8</td>
                    <td>9</td>
                    <td>10</td>
                    <td>11</td>
                    <td>12</td>
                    <td>13</td>
                </tr>
                <tr>
                    <td>14</td>
                    <td id="today">15</td>
                    <td>16</td>
                    <td>17</td>
                    <td>18</td>
                    <td>19</td>
                    <td>20</td>
                </tr>
                <tr>
                    <td>21</td>
                    <td>22</td>
                    <td>23</td>
                    <td>24</td>
                    <td>25</td>
                    <td>26</td>
                    <td>27</td>
                </tr>
                <tr>
                    <td>28</td>
                    <td>29</td>
                    <td>30</td>
                    <td class="not-cur-mounth">1</td>
                    <td class="not-cur-mounth">2</td>
                    <td class="not-cur-mounth">3</td>
                    <td class="not-cur-mounth">4</td>
                </tr>
                <tr>
                    <td class="not-cur-mounth">5</td>
                    <td class="not-cur-mounth">6</td>
                    <td class="not-cur-mounth">7</td>
                    <td class="not-cur-mounth">8</td>
                    <td class="not-cur-mounth">9</td>
                    <td class="not-cur-mounth">10</td>
                    <td class="not-cur-mounth">11</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>