<footer class="page_footer theme_footer ds ms parallax section_padding_top_15 section_padding_bottom_15">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 to_animate" data-animation="scaleAppear">
                <div class="widget widget_text topmargin_20">
                    <p class="bottommargin_30">
                        <img src="images/logo/logo.png" width="180" alt="">
                        <br/>
                        <a href="mailto:go@fitkonnect.com">go@fitkonnect.com</a>
                    </p>

                    <div class="page_social_icons inline-block darklinks">
                        <a class="social-icon soc-facebook" href="https://www.facebook.com/FitKonnect_KE/" target="_balnk" title="Facebook"></a>
                        <a class="social-icon soc-twitter" href="https://twitter.com/FitKonnect_KE" target="_blank" title="Twitter"></a>
                        <a class="social-icon soc-instagram" href="https://www.instagram.com/FitKonnect_KE/" target="_blank" title="Instagram"></a>
                    </div>
                </div>
            </div>


            <div class="col-md-3 col-sm-6 to_animate" data-animation="scaleAppear">
                <div class="widget widget_text topmargin_25">
                    <h4 class="text-uppercase bottommargin_20">Swimming</h4>
                    <div class="media">
                        <div class="media-left">
                            <i class="fa fa-user highlight fontsize_18"></i>
                        </div>
                        <div class="media-body">
                            <a href="#">Foundation Level 1</a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-3 col-sm-6 to_animate" data-animation="scaleAppear">
                <div class="widget widget_text topmargin_25">
                    <h4 class="text-uppercase bottommargin_20">IMPORTANT</h4>
                    <div class="media">
                        <div class="media-left">
                            <i class="fa fa-user highlight fontsize_18"></i>
                        </div>
                        <div class="media-body">
                            <a href="#">About Us</a>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left">
                            <i class="fa fa-user highlight fontsize_18"></i>
                        </div>
                        <div class="media-body">
                            <a href="#">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 to_animate" data-animation="scaleAppear">
                <div class="widget widget_text topmargin_25">
                    <h4 class="text-uppercase bottommargin_20">Trainer Portal</h4>
                    <div class="media">
<!--                        <div class="media-left">
                            <i class="fa fa-user highlight fontsize_18"></i>-->
                        </div>
                        Coming Soon
<!--                        <div class="media-body">
                            <a href="https://www.fitknonnect.com/portal/">Login</a>
                        </div>-->
                    </div>
                    <div class="media">
<!--                        <div class="media-left">
                            <i class="fa fa-sign-in highlight fontsize_18"></i>
                        </div>-->
<!--                        <div class="media-body">
                            <a href="?coach_application">Registration</a>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</footer>