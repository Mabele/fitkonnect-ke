<section class="page_toplogo ds table_section section_padding_25 columns_margin_0">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 text-center text-sm-left">
                <a href="./" class="logo">
                    <img src="images/logo/logo.png" width="140" alt="">
                </a>
            </div>

            <div class="col-sm-8 text-right hidden-xs darklinks">
                <div class="teaser media inline-block">
                    <div class="media-left media-middle">
                        <div class="teaser_icon border_icon fontsize_16 highlight">
                            <i class="flaticon-paper-plane"></i>
                        </div>
                    </div>
                    <div class="media-body media-middle text-left">
                        <p class="bold fontsize_12 grey bottommargin_5"><a href="tel:(+254)791929454">(+254) 791 929 454</a></p>
                        <p><a href="mailto:go@fitknonnect.com">go@fitknonnect.com</a></p>
                    </div>
                </div>
                
                </a>
            </div>
        </div>
    </div>
</section>