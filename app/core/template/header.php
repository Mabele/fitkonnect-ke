<?php
$configs = parse_ini_file(WPATH . "core/configs.ini");

require_once WPATH . "modules/classes/Users_Management.php";
require_once WPATH . "modules/classes/Feedback.php";
$feedback = new Feedback();
$users_management = new Users_Management();

$_SESSION['web_url'] = $configs["web_url"];
$_SESSION['admin_url'] = $configs["admin_url"];
$_SESSION['till_number'] = $configs["till_number"];
$_SESSION['application_email'] = $configs["application_email"];
$_SESSION['application_phone'] = $configs["application_phone"];
$_SESSION['mail_host'] = $configs["mail_host"];
$_SESSION['SMTPAuth'] = $configs["SMTPAuth"];
$_SESSION['MUsername'] = $configs["MUsername"];
$_SESSION['MPassword'] = $configs["MPassword"];
$_SESSION['SMTPSecure'] = $configs["SMTPSecure"];
$_SESSION['Port'] = $configs["Port"];
$_SESSION['MUsernameFrom'] = $configs["MUsernameFrom"];
$_SESSION['Feedback'] = $configs["Feedback"];
$_SESSION['Null_Feedback'] = $configs["Null_Feedback"];
$_SESSION['sms_username'] = $configs["sms_username"];
$_SESSION['sms_username'] = $configs["sms_username"];


if (isset($_SERVER['HTTP_REFERER'])) {
    @$former_page_chunks = explode('?', $_SERVER['HTTP_REFERER'], 2);
    if (@$former_page_chunks[1] == "login") {
        @$previous_url = "?";
    } else {
        @$previous_url = "?" . $former_page_chunks[1];
    }
}

if (!empty($_POST) AND $_POST['action'] == "secure_login") {
    $success = $users_management->execute();
    if (is_bool($success) && $success == true) {
        if ($_SESSION['password_new'] == 'YES') {
            $_SESSION['password_new'] = true;   // App::redirectTo("?update_password");
        }
        if ($_SESSION['logged_in_user_status'] == 1100) {
            $_SESSION['account_blocked'] = true;
        }
        if (is_menu_set('login') != "") {
            if (isset($_POST['activity']) AND $_POST['activity'] == 'redeem_points') {
                App::redirectTo('?shop');
            } else if (isset($_POST['activity']) AND $_POST['activity'] == 'my_transactions') {
                App::redirectTo('?trainee_transactions');
            } else {
                App::redirectTo($_POST["previous_url"]);
            }
        }
    }
}
?>
<header class="page_header theme_header cs">
    <div class="container">
        <div class="row mainrow">
            <div class="col-md-9">
                <!-- main nav start -->
                <nav class="mainmenu_wrapper">
                    <ul class="mainmenu nav sf-menu">
                        <li class="active">
                            <a href="?">Home</a>
                        </li>
                         <li>
                            <a href="?signup">Get a Trainer/Coach</a>
                        </li>  
                        <li>
                            <a href="?coach_application">Become a Trainer/Coach</a>
                        </li>          
                        <li>
                            <a href="?contact">Contact Us</a>
                        </li>
                    </ul>
                </nav>
                <!-- eof main nav -->
                <span class="toggle_menu">
                    <span></span>
                </span>
            </div>
            <div class="col-md-3 text-right">
                <ul class="inline-dropdown inline-block">
                    <li>
                         <a href="?signin" class="header-button">
                               <i class="flaticon-user"></i>Login
                         </a>
                    </li>
                     <li>
                         <a href="?shop" class="header-button">
                               <i class="flaticon-cart"></i>Shop
                         </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row searchrow">
            <div class="col-sm-12 text-right">
                <div class="widget_search">
                    <form method="get" class="searchform form-inline" action="/">
                        <div class="form-group">
                            <label class="sr-only" for="headeer-widget-search">Search for:</label>
                            <input id="headeer-widget-search" type="text" value="" name="search" class="form-control" placeholder="Type and Hit Enter">
                        </div>
                        <button type="submit" class="theme_button">Search</button>
                    </form>
                </div>
                <div class="search_form_close">
                    <i class="flaticon-cancel"></i>
                </div>
            </div>
        </div>
        <?php
        if (isset($_SESSION['update_pass_forgot']) AND ( $_SESSION['update_pass_forgot'] == true)) {
            $title = "Fantastic";
            $message = "Password successfully updated. Check your email.";
            echo $feedback->successFeedback($title, $message);
            unset($_SESSION['update_pass_forgot']);
        }

        if (isset($_SESSION['login_error'])) {
            $title = "Hey,";
            $message = "Login Error! We couldn\'t find you. Please try again.";
            echo $feedback->errorFeedback($title, $message);
            unset($_SESSION['login_error']);
        }
        if (isset($_SESSION['account_blocked'])) {
            $title = "Oops";
            $message = "Sorry! Your Account is deactivated. please contact {$_SESSION['application_email']}";
            echo $feedback->errorFeedback($title, $message);
            unset($_SESSION['account_blocked']);
        }
        ?> 
    </div>
</header>