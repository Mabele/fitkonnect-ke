<section class="page_copyright ds ms parallax table_section section_padding_25">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <p class="darklinks fontsize_12">
                    <span class=""><a href="?privacy" title="Privacy Policy">Privacy Policy </a> | <a href="?tc" title="Terms & Conditions">T&C</a> &copy; fitkonnect.com | <?php echo date("Y"); ?> |  with </span>
                    <i class="fa fa-heart-o highlight"></i> by
                    <img src="images/logo/logo.png" width="50" alt="" />
                </p>
            </div>
        </div>
    </div>
</section>